﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Transact.Data;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Services.CustomExceptions;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services
{
    public class UserServices : IUserServices
    {
        private readonly TransactContext context;
        private readonly IUserDtoMapper userDtoMapper;

        public UserServices(TransactContext context, IUserDtoMapper userDtoMapper)
        {
            this.context = context;
            this.userDtoMapper = userDtoMapper;
        }

        public async Task<UserDto> AddUserAsync(UserDto userDtoInput)
        {
            if (context.Users.Any(x => x.UserName == userDtoInput.UserName))
            {
                throw new BusinessLogicException(ExceptionMessages.UserNameExists);
            }

            var user = userDtoMapper.MapFrom(userDtoInput);
            user.Password = GetHashedString(userDtoInput.Password);
            user.RoleId = 1;

            await context.Users.AddAsync(user);

            await context.SaveChangesAsync();

            userDtoInput.RoleId = user.RoleId;

            return userDtoInput;
        }

        public async Task<UserDto> GetUserAsync(string userName, string password)
        {
            var user = await context.Users.Include(x=>x.Role).SingleOrDefaultAsync(x=>x.UserName == userName);

            if (user == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            var hashedPassword = GetHashedString(password);

            if (user.Password != hashedPassword)
            {
                throw new BusinessLogicException(ExceptionMessages.IncorrectUserPasswordCombo);
            }

            var userDto = userDtoMapper.MapFrom(user);

            return userDto;
        }       

        public async Task<IList<UserDto>> GetFiveUsersByIdAsync(int currPage)
        {          
            if (currPage == 1)
            {
                var fiveUsersById = await context
                                    .Users
                                    .Where(x => x.RoleId == 1)
                                    .OrderByDescending(x => x.Id)
                                    .Take(5).ToListAsync();

                var mappedUsers = userDtoMapper.MapFrom(fiveUsersById);
                return mappedUsers;
            }
            else
            {
                var fiveUsersById = await context
                                    .Users
                                    .Where(x => x.RoleId == 1)                                    
                                    .OrderByDescending(x => x.Id)
                                    .Skip((currPage - 1) * 5)
                                    .Take(5).ToListAsync();

                var mappedUsers = userDtoMapper.MapFrom(fiveUsersById);
                return mappedUsers;
            }            
        }

        public async Task<int> GetPageCountForUsersAsync(int usersPerPage)
        {
            var allUsersCount = await context.Users.Where(x => x.RoleId == 1).CountAsync();

            int pageCount = (allUsersCount - 1) / usersPerPage + 1;

            return pageCount;
        }

        private byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public string GetHashedString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
            {
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }

        public async Task<IList<UserDto>> GetFiveUsersForClientByIdAsync(int currPage, long clientId)
        {
            List<long> fiveUsersClientsByClientId;

            if (currPage == 1)
            {
                fiveUsersClientsByClientId = await context
                                    .UsersClients                                    
                                    .Where(x => x.ClientId == clientId)
                                    .OrderByDescending(x => x.UserId)
                                    .Take(5)
                                    .Select(x=>x.UserId)
                                    .ToListAsync();

                var fiveUsersByClientId = await context.Users
                                                .Where(x => fiveUsersClientsByClientId.Contains(x.Id))
                                                .ToListAsync();

                var mappedUsers = userDtoMapper.MapFrom(fiveUsersByClientId);
                return mappedUsers;
            }
            else
            {
                fiveUsersClientsByClientId = await context
                                    .UsersClients
                                    .Where(x => x.ClientId == clientId)
                                    .OrderByDescending(x => x.UserId)
                                    .Skip((currPage - 1) * 5)
                                    .Take(5)
                                    .Select(x=>x.UserId)
                                    .ToListAsync();

                var fiveUsersByClientId = await context.Users
                                                .Where(x => fiveUsersClientsByClientId.Contains(x.Id))
                                                .ToListAsync();

                var mappedUsers = userDtoMapper.MapFrom(fiveUsersByClientId);
                return mappedUsers;
            }
        }

        public async Task<int> GetPageCountForUsersWithClientIdAsync(int usersPerPage, long clientId)
        {
            var allUsersCount = await context.UsersClients.Where(x => x.ClientId == clientId).CountAsync();

            int pageCount = (allUsersCount - 1) / usersPerPage + 1;

            return pageCount;
        }

        public async Task<IList<UserDto>> FindUsersContainingAsync(string searchString, long clientId)
        {
            IQueryable<long> usersIdsThatAreForClient = context
                .UsersClients
                .Where(x => x.ClientId == clientId)
                .Select(x => x.UserId);

            var users = await context.Users
                .Where(u => !usersIdsThatAreForClient
                .Contains(u.Id) && u.UserName.Contains(searchString))
                .ToListAsync();

            var mappedUsers = userDtoMapper.MapFrom(users);

            return mappedUsers;
        }

        public async Task<UserDto> RemoveUserForClient(long clientId, long userId)
        {
            var client = await context.Clients
                .Include(uc => uc.UsersClients)
                .Include(x=>x.Accounts)
                .SingleOrDefaultAsync(uc => uc.Id == clientId);

            if (client == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ClientNull);
            }

            var user = await context.Users.SingleOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            var usersClientsForUser = client.UsersClients.SingleOrDefault(x => x.UserId == userId);

            if (usersClientsForUser == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UsersClientsNull);
            }

            context.UsersClients.Remove(usersClientsForUser);

            var accountsIdsForClient = client.Accounts.Select(x => x.Id);

            IQueryable<UsersAccounts> allRecordsToRemove =
                context
                .UsersAccounts
                .Where(ua => ua.UserId == userId
                && accountsIdsForClient.Contains(ua.AccountId));

            context.UsersAccounts.RemoveRange(allRecordsToRemove);

            await context.SaveChangesAsync();

            return userDtoMapper.MapFrom(user);
        }

        public async Task<UserDto> GetUserByIdAsync(long Id)
        {
            var user = await context.Users.FindAsync(Id);

            if (user == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            var userDto = userDtoMapper.MapFrom(user);

            return userDto;
        }

        public async Task<int> GetTotalUsersCountAsync()
        {
            return await context.Users.CountAsync();            
        }
    }
}