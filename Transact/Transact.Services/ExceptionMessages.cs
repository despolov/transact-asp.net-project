﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transact.Services
{
    public static class ExceptionMessages
    {
        public const string UserNull = "No such user in app!";
        public const string AdminNull = "No such admin in app!";
        public const string ClientNull = "No such client in app!";
        public const string BannerNull = "No such banner in app!";
        public const string BannerDateInvalid = "Invalid Banner Period!";
        public const string AccountNull = "No such account in app!";
        public const string AccountExists = "Account exists in app!";
        public const string SameAccountsForTransaction = "You cannot make transaction between same account!";
        public const string SenderAccountNull = "No such sender account in app!";
        public const string ReceiverAccountNull = "No such receiver account in app!";
        public const string IncorrectUserPasswordCombo = "Incorect Username/Password Combination!";
        public const string IncorrectAdminPasswordCombo = "Incorect Username/Password Combination!";
        public const string NotLogged = "You must be logged in to view that!";
        public const string NotAuthorized = "You do not have permissions for that!";
        public const string UserNameExists = "Username is taken!";
        public const string ClientNameExists = "Name is taken!";
        public const string InvalidUserId = "UserId is not valid!";
        public const string NotEnoughFromSenderBalance = "The sender account has insufficient funds!";
        public const string TransactionNull = "No Such Transaction in app!";
        public const string AccountNoLongerAvailable = "You do not have access to that account anymore!";
        public const string CannotEditSentTransaction = "You cannot edit sent transaction!";
        public const string GeneralOopsMessage = "Oops something went wrong!";
        public const string UsersAccountsNull = "No such account for the user!";
        public const string UsersClientsNull = "No such user for the client!";
    }
}
