﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Transact.Services.Contracts;

namespace Transact.Services
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetDateTimeNow()
        {
            return DateTime.Now;
        }
    }
}
