﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Transact.Models;
using Transact.Models.Dtos;

namespace Transact.Services.Contracts
{
    public interface ITransactionServices
    {
        Task<TransactionDto> SaveTransactionAsync(
            TransactionDto transactionDtoInput);        

        Task<TransactionDto> SendTransactionAsync(
           long transactionId);

        Task<TransactionDto> AddTransactionAsync(
            TransactionDto transactionDtoInput);

        Task<TransactionDto> UpdateTransactionAsync(TransactionDto transactionDtoInput);

        Task<TransactionDto> SendUpdatedTransactionAsync(TransactionDto transactionDtoInput);

        Task<TransactionDto> GetSingleTransactionAsync(long transactionId);

        Task<TransactionDto> GetSingleTransactionForEditingAsync(long transactionId);

        Task<IList<TransactionDto>> GetFiveTransactionsForAccountDescAsync(
             int currPage,
             long accountId,
             long userId);       

        Task<int> GetPageCountForTransactionsAsync(
            int transactionsPerPage, 
            long? accountId,
            IEnumerable<long> accountsIds);
    }
}
