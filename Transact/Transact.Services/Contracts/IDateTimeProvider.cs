﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transact.Services.Contracts
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTimeNow();
    }
}
