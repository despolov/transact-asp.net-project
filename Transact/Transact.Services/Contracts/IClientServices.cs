﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Transact.Models;
using Transact.Models.Dtos;

namespace Transact.Services.Contracts
{
    public interface IClientServices
    {
        Task<ClientDto> AddClientAsync(ClientDto dtoInput);

        Task<ClientDto> GetClientAsync(string name);

        Task<ClientDto> GetClientByIdAsync(long Id);

        Task<int> GetPageCountForClientsAsync(int clientsPerPage);

        Task<IList<ClientDto>> GetFiveClientsByIdAsync(int currPage);

        Task<UserDto> AssignUserToClientAsync(string userName, long clientId);

        Task<ICollection<long>> GetAllUserAvailableClientsIdsAsync(long userId);

        Task<ICollection<ClientDto>> GetSevenLatestClientsAsync();
    }
}
