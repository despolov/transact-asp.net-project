﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Transact.Models.Dtos;

namespace Transact.Services.Contracts
{
    public interface IBannerServices
    {
        Task<IList<BannerDto>> GetFiveBannersByExpirationDateAsync(int currPage = 1);

        Task<BannerDto> GetSingleBannerForEditingAsync(long bannerId);

        Task<int> GetPageCountForBanersAsync(int bannersPerPage);

        Task<BannerDto> SaveBannerAsync(BannerDto bannerInput);

        Task<BannerDto> UpdateBannerAsync(BannerDto bannerInput);

        Task<BannerDto> DeleteBannerAsync(long bannerToDeleteId);

        Task<ICollection<BannerDto>> GetActiveBannersImagePathsAsync(DateTime currDate);
    }
}
