﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transact.Services.Contracts
{
    public interface IAuthorizationManager
    {
        void CheckIfLogged(string tokenValue);

        long GetLoggedUserId();

        void CheckForRole(string tokenValue, string roleToCheckFor);
    }
}
