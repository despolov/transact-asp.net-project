﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Transact.Models.Dtos;

namespace Transact.Services.Contracts
{
    public interface IAccountServices
    {
        Task<IList<AccountDto>> GetAllUserAvailableAccountsAsync(long userId);

        Task<AccountDto> GetAccountAsync(long accountId);

        Task<AccountDto> AddAccountToClientAsync(AccountDto dtoInput);

        Task<IList<AccountDto>> GetFiveAccountsForClientByIdAsync(int currPage, long clientId);

        Task<int> GetPageCountForAccountsAsync(int accountsPerPage, long clientId);

        Task<IList<AccountDto>> FindAccountsContainingAsync(string searchString, long ignoredAccountId);

        Task<AccountDto> RenameAccountNicknameAsync(long accountId, string newNickname, long userId);

        Task<IList<AccountDto>> GetFiveAccountsForUserByIdAsync(int currPage, long userId);

        Task<int> GetPageCountForAccountsWithUsersIdAsync(int accountsPerPage, long userId);

        Task<AccountDto> AssignAccountToUserAsync(string accNumber, long userId);

        Task<IList<AccountDto>> FindAccountsForUserContainingAsync(string searchString, long userId);

        Task<AccountDto> RemoveAccountForUser(long userId, long accountId);
    }
}
