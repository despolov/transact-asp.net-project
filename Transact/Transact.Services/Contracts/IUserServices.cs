﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Transact.Models;
using Transact.Models.Dtos;

namespace Transact.Services.Contracts
{
    public interface IUserServices
    {
        Task<UserDto> AddUserAsync(UserDto userDtoInput);

        Task<UserDto> GetUserAsync(string username, string password);

        string GetHashedString(string inputString);

        Task<IList<UserDto>> GetFiveUsersByIdAsync(int currPage);

        Task<int> GetPageCountForUsersAsync(int usersPerPage);

        Task<IList<UserDto>> GetFiveUsersForClientByIdAsync(int currPage, long clientId);

        Task<int> GetPageCountForUsersWithClientIdAsync(int usersPerPage, long clientId);

        Task<IList<UserDto>> FindUsersContainingAsync(string searchString, long clientId);

        Task<UserDto> RemoveUserForClient(long clientId, long userId);

        Task<UserDto> GetUserByIdAsync(long Id);

        Task<int> GetTotalUsersCountAsync();
    }
}
