﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transact.Services.Contracts
{
    public interface IRandomizer
    {
        Task<List<T>> GetInRandomOrder<T>(IQueryable<T> inputCollection);
    }
}
