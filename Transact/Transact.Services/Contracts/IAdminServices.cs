﻿using System.Threading.Tasks;
using Transact.Models.Dtos;

namespace Transact.Services.Contracts
{
    public interface IAdminServices
    {
        Task<AdminDto> GetAdminAsync(string username, string password);

        string GetHashedString(string inputString);
    }
}
