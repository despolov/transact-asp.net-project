﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transact.Services.Contracts;

namespace Transact.Services
{
    public class Randomizer : IRandomizer
    {
        public async Task<List<T>> GetInRandomOrder<T>(IQueryable<T> inputCollection)
        {
            return await inputCollection.OrderBy(x => Guid.NewGuid()).ToListAsync();
        }
    }
}
