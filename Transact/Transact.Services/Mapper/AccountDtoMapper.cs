﻿using System.Collections.Generic;
using System.Linq;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services.Mapper
{
    public class AccountDtoMapper : IAccountDtoMapper
    {
        public AccountDto MapFrom(Account entity)
        {
            return new AccountDto()
            {
                Id = entity.Id,
                AccountNumber = entity.AccountNumber,
                NickName = entity.NickName,
                Balance = entity.Balance,
                ClientName = entity.Client?.Name,
                ClientId = entity.ClientId
            };
        }

        public AccountDto MapFrom(UsersAccounts entity)
        {
            return new AccountDto()
            {
                Id = entity.Account.Id,
                AccountNumber = entity.Account.AccountNumber,
                NickName = entity.UserAccountNickname ?? entity.Account.NickName,
                Balance = entity.Account.Balance,
                ClientName = entity.Account.Client?.Name,
                ClientId = entity.Account.ClientId
            };
        }

        public Account MapFrom(AccountDto entity)
        {
            return new Account()
            {
                Id = entity.Id,
                AccountNumber = entity.AccountNumber,
                ClientId = entity.ClientId,
                NickName = entity.NickName,
                Balance = entity.Balance
            };
        }

        public IList<AccountDto> MapFrom(ICollection<Account> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }

        public IList<AccountDto> MapFrom(ICollection<UsersAccounts> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }

        public IList<Account> MapFrom(ICollection<AccountDto> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }
    }
}
