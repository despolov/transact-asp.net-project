﻿using System.Collections.Generic;
using Transact.Models;
using Transact.Models.Dtos;

namespace Transact.Services.Mapper.Contracts
{
    public interface IAccountDtoMapper
    {
        AccountDto MapFrom(Account entity);

        AccountDto MapFrom(UsersAccounts entity);

        Account MapFrom(AccountDto entity);

        IList<AccountDto> MapFrom(ICollection<Account> entities);

        IList<Account> MapFrom(ICollection<AccountDto> entities);

        IList<AccountDto> MapFrom(ICollection<UsersAccounts> entities);
    }
}
