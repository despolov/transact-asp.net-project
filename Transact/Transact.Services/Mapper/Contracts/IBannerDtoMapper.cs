﻿using System.Collections.Generic;
using Transact.Models;
using Transact.Models.Dtos;

namespace Transact.Services.Mapper.Contracts
{
    public interface IBannerDtoMapper
    {
        BannerDto MapFrom(Banner entity);

        Banner MapFrom(BannerDto entity);

        IList<BannerDto> MapFrom(ICollection<Banner> entities);

        IList<Banner> MapFrom(ICollection<BannerDto> entities);
    }
}
