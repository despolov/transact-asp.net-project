﻿using System.Collections.Generic;
using Transact.Models;
using Transact.Models.Dtos;

namespace Transact.Services.Mapper.Contracts
{
    public interface ITransactionDtoMapper
    {
        TransactionDto MapFrom(Transaction entity);

        Transaction MapFrom(TransactionDto entity);

        TransactionDto MapFromWithPersonalNick(Transaction entity);

        IList<TransactionDto> MapFrom(ICollection<Transaction> entities);

        IList<TransactionDto> MapFromWithPersonalNick(ICollection<Transaction> entities);

        IList<Transaction> MapFrom(ICollection<TransactionDto> entities);
    }
}
