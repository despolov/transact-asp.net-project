﻿using System.Collections.Generic;
using Transact.Models;
using Transact.Models.Dtos;

namespace Transact.Services.Mapper.Contracts
{
    public interface IAdminDtoMapper
    {
        AdminDto MapFrom(Admin entity);

        Admin MapFrom(AdminDto entity);

        IList<AdminDto> MapFrom(ICollection<Admin> entities);

        IList<Admin> MapFrom(ICollection<AdminDto> entities);
    }
}