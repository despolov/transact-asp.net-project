﻿using System;
using System.Collections.Generic;
using System.Text;
using Transact.Models;
using Transact.Models.Dtos;

namespace Transact.Services.Mapper.Contracts
{
    public interface IUserDtoMapper
    {
        UserDto MapFrom(User entity);

        User MapFrom(UserDto entity);

        IList<UserDto> MapFrom(ICollection<User> entities);

        IList<User> MapFrom(ICollection<UserDto> entities);
    }
}
