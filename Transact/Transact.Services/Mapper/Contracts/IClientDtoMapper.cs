﻿using System.Collections.Generic;
using Transact.Models;
using Transact.Models.Dtos;

namespace Transact.Services.Mapper.Contracts
{
    public interface IClientDtoMapper
    {
        ClientDto MapFrom(Client entity);

        Client MapFrom(ClientDto entity);

        IList<ClientDto> MapFrom(ICollection<Client> entities);

        IList<Client> MapFrom(ICollection<ClientDto> entities);
    }
}