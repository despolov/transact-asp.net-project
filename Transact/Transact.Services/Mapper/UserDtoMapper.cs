﻿using System.Collections.Generic;
using System.Linq;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services.Mapper
{
    public class UserDtoMapper : IUserDtoMapper
    {
        public UserDto MapFrom(User entity)
        {
            return new UserDto()
            {
                Id = entity.Id,
                Name = entity.Name,
                RoleId = entity.RoleId,
                UserName = entity.UserName,
                RoleName = entity.Role?.RoleName
            };
        }

        public User MapFrom(UserDto entity)
        {
            return new User()
            {
                Id = entity.Id,
                Name = entity.Name,
                RoleId = entity.RoleId,
                UserName = entity.UserName
            };
        }

        public IList<UserDto> MapFrom(ICollection<User> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }

        public IList<User> MapFrom(ICollection<UserDto> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }
    }
}