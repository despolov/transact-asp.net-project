﻿using System.Collections.Generic;
using System.Linq;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services.Mapper
{
    public class BannerDtoMapper : IBannerDtoMapper
    {
        public BannerDto MapFrom(Banner entity)
        {
            return new BannerDto()
            {
                Id = entity.Id,
                ImageName = entity.ImageName,
                StartDate = entity.StartDate,
                EndDate = entity.EndDate,
                Url = entity.Url
            };
        }

        public Banner MapFrom(BannerDto entity)
        {
            return new Banner()
            {
                Id = entity.Id,
                ImageName = entity.ImageName,
                StartDate = entity.StartDate,
                EndDate = entity.EndDate,
                Url = entity.Url
            };
        }

        public IList<BannerDto> MapFrom(ICollection<Banner> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }

        public IList<Banner> MapFrom(ICollection<BannerDto> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }
    }
}
