﻿using System.Collections.Generic;
using System.Linq;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services.Mapper
{
    public class AdminDtoMapper : IAdminDtoMapper
    {
        public AdminDto MapFrom(Admin entity)
        {
            return new AdminDto()
            {
                Id = entity.Id,
                RoleId = entity.RoleId,
                RoleName = entity.Role.RoleName,
                UserName = entity.UserName
            };
        }

        public Admin MapFrom(AdminDto entity)
        {
            return new Admin()
            {
                Id = entity.Id,
                RoleId = entity.RoleId,
                UserName = entity.UserName
            };
        }

        public IList<AdminDto> MapFrom(ICollection<Admin> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }

        public IList<Admin> MapFrom(ICollection<AdminDto> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }
    }
}