﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services.Mapper
{
    public class TransactionDtoMapper : ITransactionDtoMapper
    {
        public TransactionDto MapFrom(Transaction entity)
        {
            return new TransactionDto()
            {
                Id = entity.Id,
                Amount = entity.Amount,
                SenderAccountNumber = entity.SenderAccount?.AccountNumber,
                SenderAccountNickName = entity.SenderAccount?.NickName,
                SenderClientName = entity.SenderAccount?.Client?.Name,
                ReceiverAccountNumber = entity.ReceiverAccount?.AccountNumber,
                ReceiverClientName = entity.ReceiverAccount?.Client?.Name,
                SenderAccountId = entity.SenderAccountId,
                ReceiverAccountId = entity.ReceiverAccountId,
                Description = entity.Description,
                TimeStamp = entity.TimeStamp,
                StatusName = entity.Status?.StatusName
            };                        
        }

        public TransactionDto MapFromWithPersonalNick(Transaction entity)
        {
            return new TransactionDto()
            {
                Id = entity.Id,
                Amount = entity.Amount,
                SenderAccountNumber = entity.SenderAccount?.AccountNumber,
                SenderAccountNickName = entity.SenderAccount?.UsersAccounts?.FirstOrDefault()?.UserAccountNickname ?? entity.SenderAccount.NickName,
                SenderClientName = entity.SenderAccount?.Client?.Name,
                ReceiverAccountNumber = entity.ReceiverAccount?.AccountNumber,
                ReceiverClientName = entity.ReceiverAccount?.Client?.Name,
                SenderAccountId = entity.SenderAccountId,
                ReceiverAccountId = entity.ReceiverAccountId,
                ReceiverAccountNickName = entity.ReceiverAccount?.UsersAccounts?.FirstOrDefault()?.UserAccountNickname ?? entity.ReceiverAccount.NickName,
                Description = entity.Description,
                TimeStamp = entity.TimeStamp,
                StatusName = entity.Status?.StatusName,
                StatusId = entity.StatusId

            };
        }

        public Transaction MapFrom(TransactionDto entity)
        {
            return new Transaction()
            {
                Id = entity.Id,
                Amount = entity.Amount,
                SenderAccountId = entity.SenderAccountId,
                ReceiverAccountId = entity.ReceiverAccountId,
                Description = entity.Description,
                TimeStamp = entity.TimeStamp
            };
        }      

        public IList<TransactionDto> MapFrom(ICollection<Transaction> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }

        public IList<TransactionDto> MapFromWithPersonalNick(ICollection<Transaction> entities)
        {
            return entities.Select(this.MapFromWithPersonalNick).ToList();
        }

        public IList<Transaction> MapFrom(ICollection<TransactionDto> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }
    }
}
