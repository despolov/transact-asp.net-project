﻿using System.Collections.Generic;
using System.Linq;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services.Mapper
{
    public class ClientDtoMapper : IClientDtoMapper
    {
        public ClientDto MapFrom(Client entity)
        {
            return new ClientDto()
            {
                Id = entity.Id,
                Name = entity.Name,
                NumberOfAccounts = entity.Accounts?.Count ?? 0
            };
        }

        public Client MapFrom(ClientDto entity)
        {
            return new Client()
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }

        public IList<ClientDto> MapFrom(ICollection<Client> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }

        public IList<Client> MapFrom(ICollection<ClientDto> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }
    }
}