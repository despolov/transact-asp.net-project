﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transact.Services.Contracts;

namespace Transact.Services
{
    public class AuthorizationManager : IAuthorizationManager
    {
        private readonly ITokenManager tokenManager;
        private readonly IHttpContextAccessor contextAccessor;

        public AuthorizationManager(ITokenManager tokenManager, IHttpContextAccessor contextAccessor)
        {
            this.tokenManager = tokenManager;
            this.contextAccessor = contextAccessor;
        }

        public void CheckIfLogged(string tokenValue)
        {
            var tokenCheckResult = tokenManager.GetPrincipal(tokenValue);

            if (tokenCheckResult == null)
            {
                throw new UnauthorizedAccessException(ExceptionMessages.NotLogged);
            }
        }

        public long GetLoggedUserId()
        {
            var token = contextAccessor.HttpContext.Request.Cookies["SecurityToken"];

            var userClaimPrinciple = tokenManager.GetPrincipal(token);

            if (userClaimPrinciple == null)
            {
                throw new UnauthorizedAccessException(ExceptionMessages.NotLogged);
            }

            var isIdValid = Int64.TryParse(userClaimPrinciple
                                            .Claims
                                            .Where(c => c.Type == "userId")
                                            .First()
                                            .Value,
                                            out var userId);
            if (isIdValid)
            {
                return userId;
            }
            else
            {
                throw new ArgumentException(ExceptionMessages.InvalidUserId);

            }            
        }

        public void CheckForRole(string tokenValue, string roleToCheckFor)
        {
            var userClaimPrinciple = tokenManager.GetPrincipal(tokenValue);

            var userRole = userClaimPrinciple.Claims.Where(c => c.Type == "userRole").First().Value;

            if (userRole != roleToCheckFor)
            {
                throw new UnauthorizedAccessException(ExceptionMessages.NotAuthorized);
            }
        }
    }
}
