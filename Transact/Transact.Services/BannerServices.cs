﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transact.Data;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Services.CustomExceptions;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services
{
    public class BannerServices : IBannerServices
    {
        private readonly TransactContext context;
        private readonly IBannerDtoMapper bannerDtoMapper;
        private readonly IRandomizer randomizer;

        public BannerServices(
            TransactContext context,
            IBannerDtoMapper bannerDtoMapper,
            IRandomizer randomizer)
        {
            this.context = context;
            this.bannerDtoMapper = bannerDtoMapper;
            this.randomizer = randomizer;
        }

        public async Task<IList<BannerDto>> GetFiveBannersByExpirationDateAsync(
           int currPage = 1)
        {            
               var fiveBannersByExpiration = await context.Banners                                                        
                                                   .OrderByDescending(x => x.EndDate)
                                                   .Skip((currPage - 1) * 5)
                                                   .Take(5)
                                                   .ToListAsync();

                var mappedBanners = bannerDtoMapper.MapFrom(fiveBannersByExpiration);

                return mappedBanners;
        }

        public async Task<BannerDto> GetSingleBannerForEditingAsync(long bannerId)
        {
            var bannerToEdit = await context.Banners.FindAsync(bannerId);
                                    

            if (bannerToEdit == null)
            {
                throw new BusinessLogicException(ExceptionMessages.BannerNull);
            }           

            var bannerDto = bannerDtoMapper.MapFrom(bannerToEdit);

            return bannerDto;
        }


        public async Task<int> GetPageCountForBanersAsync(int bannersPerPage)
        {
            var allBannersCount = await context
                                       .Banners
                                       .CountAsync();
            
            int pageCount = (allBannersCount - 1) / bannersPerPage + 1;
            
            return pageCount;            
        }

        public async Task<BannerDto> SaveBannerAsync(BannerDto bannerInput)
        {
            if (bannerInput.StartDate > bannerInput.EndDate)
            {
                throw new BusinessLogicException(ExceptionMessages.BannerDateInvalid);
            }

            var bannerToSave = bannerDtoMapper.MapFrom(bannerInput);

            context.Add(bannerToSave);

            await context.SaveChangesAsync();

            return bannerInput;
        }

        public async Task<BannerDto> DeleteBannerAsync(long bannerToDeleteId)
        {
            var bannerToDelete = await context.Banners.FindAsync(bannerToDeleteId);

            if (bannerToDelete == null)
            {
                throw new BusinessLogicException(ExceptionMessages.BannerNull);
            }

            context.Banners.Remove(bannerToDelete);          

            await context.SaveChangesAsync();

            var bannerDeleted = bannerDtoMapper.MapFrom(bannerToDelete);

            return bannerDeleted;
        }

        public async Task<BannerDto> UpdateBannerAsync(BannerDto bannerInput)
        {
            if (bannerInput.StartDate > bannerInput.EndDate)
            {
                throw new BusinessLogicException(ExceptionMessages.BannerDateInvalid);
            }

            var bannerToEdit = await context.Banners.FindAsync(bannerInput.Id);

            if (bannerToEdit == null)
            {
                throw new BusinessLogicException(ExceptionMessages.BannerNull);
            }            

            bannerToEdit.StartDate = bannerInput.StartDate;
            bannerToEdit.EndDate = bannerInput.EndDate;
            bannerToEdit.ImageName = bannerInput.ImageName;
            bannerToEdit.Url = bannerInput.Url;

            await context.SaveChangesAsync();

            return bannerInput;
        }

        public async Task<ICollection<BannerDto>> GetActiveBannersImagePathsAsync(DateTime currDate)
        {
            IQueryable<Banner> allActiveBannersQuerable = context.Banners
                .Where(x => x.StartDate <= currDate && x.EndDate > currDate);

            var allActiveBanners = await randomizer.GetInRandomOrder(allActiveBannersQuerable);

            var allActiveBannerDtos = bannerDtoMapper.MapFrom(allActiveBanners);

            return allActiveBannerDtos;
        }
    }
}

