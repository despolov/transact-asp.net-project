﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transact.Services.CustomExceptions
{
    public class AccountNotAvailableException : Exception
    {
        public AccountNotAvailableException()
        {
        }

        public AccountNotAvailableException(string message)
            : base(message)
        {
        }

        public AccountNotAvailableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
