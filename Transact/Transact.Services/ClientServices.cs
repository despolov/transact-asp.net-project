﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transact.Data;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Services.CustomExceptions;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services
{
    public class ClientServices : IClientServices
    {
        private readonly TransactContext context;
        private readonly IClientDtoMapper clientDtoMapper;
        private readonly IUserDtoMapper userDtoMapper;
        private readonly IAccountDtoMapper accountDtoMapper;

        public ClientServices(TransactContext context, IClientDtoMapper clientDtoMapper, IUserDtoMapper userDtoMapper, IAccountDtoMapper accountDtoMapper)
        {
            this.context = context;
            this.clientDtoMapper = clientDtoMapper;
            this.userDtoMapper = userDtoMapper;
            this.accountDtoMapper = accountDtoMapper;
        }

        public async Task<ClientDto> AddClientAsync(ClientDto dtoInput)
        {
            if (context.Clients.Any(x => x.Name == dtoInput.Name))
            {
                throw new BusinessLogicException(ExceptionMessages.ClientNameExists);
            }

            var client = clientDtoMapper.MapFrom(dtoInput);

            await context.Clients.AddAsync(client);

            await context.SaveChangesAsync();

            return dtoInput;
        }

        public async Task<ClientDto> GetClientAsync(string name)
        {
            var client = await context.Clients.Include(x => x.Accounts).SingleOrDefaultAsync(x => x.Name == name);

            if (client == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ClientNull);
            }

            var clientDto = clientDtoMapper.MapFrom(client);

            return clientDto;
        }

        public async Task<ClientDto> GetClientByIdAsync(long Id)
        {
            var client = await context.Clients.FindAsync(Id);

            if (client == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ClientNull);
            }

            var clientDto = clientDtoMapper.MapFrom(client);

            return clientDto;
        }

        public async Task<int> GetPageCountForClientsAsync(int clientsPerPage)
        {
            var allClientsCount = await context.Clients.CountAsync();

            var totalPages = (allClientsCount - 1) / clientsPerPage + 1;

            return totalPages;
        }

        public async Task<IList<ClientDto>> GetFiveClientsByIdAsync(int currPage)
        {
            List<Client> fiveClientsById;

            if (currPage == 1)
            {
                fiveClientsById = await context
                                    .Clients
                                    .OrderByDescending(x => x.Id)
                                    .Take(5).ToListAsync();

                var mappedClients = clientDtoMapper.MapFrom(fiveClientsById);
                return mappedClients;
            }
            else
            {
                fiveClientsById = await context
                                    .Clients
                                    .OrderByDescending(x => x.Id)
                                    .Skip((currPage - 1) * 5)
                                    .Take(5).ToListAsync();

                var mappedClients = clientDtoMapper.MapFrom(fiveClientsById);
                return mappedClients;
            }
        }

        public async Task<UserDto> AssignUserToClientAsync(string userName, long clientId)
        {
            var user = await context.Users.SingleOrDefaultAsync(x => x.UserName == userName);

            if (user == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            var client = await context.Clients.SingleOrDefaultAsync(x => x.Id == clientId);

            if (client == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ClientNull);
            }

            var userClient = new UsersClients() { UserId = user.Id, ClientId = clientId };

            await context.UsersClients.AddAsync(userClient);

            await context.SaveChangesAsync();

            return userDtoMapper.MapFrom(user);
        }

        public async Task<ICollection<long>> GetAllUserAvailableClientsIdsAsync(long userId)
        {
            var allClientsIdsForUser = await context
                .Clients
                .Include(x=>x.Accounts)
                .Where(c => c.UsersClients
                    .Any(uc => uc.UserId == userId))
                .Select(x=>x.Id)
                .ToListAsync();

            return allClientsIdsForUser;
        }

        public async Task<ICollection<ClientDto>> GetSevenLatestClientsAsync()
        {
            var sevenLatestClients = await context
                .Clients
                .Include(x => x.Accounts)
                .OrderByDescending(x=>x.Id)
                .Take(7)
                .ToListAsync();

            var sevenMappedClients = clientDtoMapper.MapFrom(sevenLatestClients);

            return sevenMappedClients;
        }
    }
}