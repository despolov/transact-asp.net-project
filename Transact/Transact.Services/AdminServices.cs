﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Transact.Data;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Services.CustomExceptions;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services
{
    public class AdminServices : IAdminServices
    {
        private readonly TransactContext context;
        private readonly IAdminDtoMapper adminDtoMapper;

        public AdminServices(TransactContext context, IAdminDtoMapper adminDtoMapper)
        {
            this.context = context;
            this.adminDtoMapper = adminDtoMapper;
        }

        public async Task<AdminDto> GetAdminAsync(string username, string password)
        {
            var admin = await context.Admins.Include(x => x.Role).SingleOrDefaultAsync(x => x.UserName == username);

            if (admin == null)
            {
                throw new BusinessLogicException(ExceptionMessages.AdminNull);
            }

            var hashedPassword = GetHashedString(password);

            if (admin.Password != hashedPassword)
            {
                throw new BusinessLogicException(ExceptionMessages.IncorrectAdminPasswordCombo);
            }

            var adminDto = adminDtoMapper.MapFrom(admin);

            return adminDto;
        }

        private byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public string GetHashedString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
            {
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }
    }
}