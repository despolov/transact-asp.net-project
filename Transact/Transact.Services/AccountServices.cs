﻿using Microsoft.EntityFrameworkCore;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transact.Data;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Services.CustomExceptions;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services
{
    public class AccountServices : IAccountServices
    {
        private readonly TransactContext context;
        private readonly IAccountDtoMapper accountDtoMapper;

        public AccountServices(TransactContext context, IAccountDtoMapper accountDtoMapper)
        {
            this.context = context;
            this.accountDtoMapper = accountDtoMapper;
        }

        public async Task<AccountDto> GetAccountAsync(long accountId)
        {
            var account = await context.Accounts.FindAsync(accountId);

            if (account == null)
            {
                throw new BusinessLogicException(ExceptionMessages.AccountNull);
            }

            var accountDto = accountDtoMapper.MapFrom(account);

            return accountDto;
        }

        public async Task<IList<AccountDto>> GetAllUserAvailableAccountsAsync(long userId)
        {
            var allUsersAccountsForUser = await context
                .UsersAccounts.Include(a => a.Account)
                .ThenInclude(x => x.Client)
                .Where(x => x.UserId == userId)
                .ToListAsync();

            var mappedAllAcc = accountDtoMapper.MapFrom(allUsersAccountsForUser);

            return mappedAllAcc;
        }

        public async Task<IList<AccountDto>> FindAccountsContainingAsync(string searchString, long ignoredAccountId)
        {
            var accounts = await context.Accounts.Include(x=>x.Client).Where(i => i.Id != ignoredAccountId).Where(x => x.AccountNumber.Contains(searchString)).Take(10).ToListAsync();

            var mappedAcc = accountDtoMapper.MapFrom(accounts);

            return mappedAcc;
        }

        public async Task<AccountDto> AddAccountToClientAsync(AccountDto dtoInput)
        {
            var result = "";

            bool accountNumberInDb = true;

            while (accountNumberInDb)
            {
                result = PasswordGenerator.Generate(length: 10, allowed: Sets.Digits);

                accountNumberInDb = await context.Accounts.AnyAsync(x => x.AccountNumber == result);
            }

            var client = await context.Clients.FindAsync(dtoInput.ClientId);

            if (client == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ClientNull);
            }

            var accountToAdd = accountDtoMapper.MapFrom(dtoInput);
            accountToAdd.AccountNumber = result;
            accountToAdd.NickName = result;

            await context.Accounts.AddAsync(accountToAdd);

            await context.SaveChangesAsync();

            dtoInput.AccountNumber = result;

            return dtoInput;
        }

        public async Task<int> GetPageCountForAccountsAsync(int accountsPerPage, long clientId)
        {
            var allAccountsCount = await context.Accounts.Where(x=>x.ClientId == clientId).CountAsync();

            var totalPages = (allAccountsCount - 1) / accountsPerPage + 1;

            return totalPages;
        }

        public async Task<IList<AccountDto>> GetFiveAccountsForClientByIdAsync(int currPage, long clientId)
        {
            List<Account> fiveAccountsById;

            if (currPage == 1)
            {
                fiveAccountsById = await context
                                    .Accounts
                                    .Where(x=>x.ClientId == clientId)
                                    .OrderByDescending(x => x.Id)
                                    .Take(5).ToListAsync();

                var mappedAccounts = accountDtoMapper.MapFrom(fiveAccountsById);
                return mappedAccounts;
            }
            else
            {
                fiveAccountsById = await context
                                    .Accounts
                                    .Where(x => x.ClientId == clientId)
                                    .OrderByDescending(x => x.Id)
                                    .Skip((currPage - 1) * 5)
                                    .Take(5).ToListAsync();

                var mappedAccounts = accountDtoMapper.MapFrom(fiveAccountsById);
                return mappedAccounts;
            }
        }

        public async Task<IList<AccountDto>> GetFiveAccountsForUserByIdAsync(int currPage, long userId)
        {
            List<UsersAccounts> fiveAccountsById;

            if (currPage == 1)
            {
                fiveAccountsById = await context
                                    .UsersAccounts.Include(a => a.Account)
                                    .ThenInclude(c => c.Client)
                                    .Where(x => x.UserId == userId)
                                    .OrderByDescending(x => x.AccountId)
                                    .Take(5).ToListAsync();

                var mappedAccounts = accountDtoMapper.MapFrom(fiveAccountsById);
                return mappedAccounts;
            }
            else
            {
                fiveAccountsById = await context
                                    .UsersAccounts.Include(a => a.Account)
                                    .ThenInclude(c => c.Client)
                                    .Where(x => x.UserId == userId)
                                    .OrderByDescending(x => x.AccountId)
                                    .Skip((currPage - 1) * 5)
                                    .Take(5).ToListAsync();

                var mappedAccounts = accountDtoMapper.MapFrom(fiveAccountsById);
                return mappedAccounts;
            }
        }

        public async Task<int> GetPageCountForAccountsWithUsersIdAsync(int accountsPerPage, long userId)
        {
            var allAccountssCount = await context.UsersAccounts.Where(x => x.UserId == userId).CountAsync();

            int pageCount = (allAccountssCount - 1) / accountsPerPage + 1;

            return pageCount;
        }

        public async Task<AccountDto> RenameAccountNicknameAsync(long accountId, string newNickname, long userId)
        {
            var accountToReNickName = await context.UsersAccounts.SingleOrDefaultAsync(x => x.UserId == userId && x.AccountId == accountId);

            accountToReNickName.UserAccountNickname = newNickname;

            await context.SaveChangesAsync();

            var dtoAccountsForUser = new AccountDto()
            {
                Id = accountToReNickName.AccountId,
                NickName = newNickname
            };

            return dtoAccountsForUser;
        }

        public async Task<AccountDto> AssignAccountToUserAsync(string accNumber, long userId)
        {
            var account = await context.Accounts.SingleOrDefaultAsync(x => x.AccountNumber == accNumber);

            if (account == null)
            {
                throw new BusinessLogicException(ExceptionMessages.AccountNull);
            }

            var user = await context.Users.SingleOrDefaultAsync(x => x.Id == userId);

            if (user == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            var usersAccounts = new UsersAccounts() { UserId = userId, AccountId = account.Id };

            await context.UsersAccounts.AddAsync(usersAccounts);

            await context.SaveChangesAsync();

            return accountDtoMapper.MapFrom(account);
        }

        public async Task<AccountDto> RemoveAccountForUser(long userId, long accountId)
        {
            var user = await context.Users.SingleOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            var account = await context.Accounts.Include(ua => ua.UsersAccounts).SingleOrDefaultAsync(ua => ua.Id == accountId);

            if (account == null)
            {
                throw new BusinessLogicException(ExceptionMessages.AccountNull);
            }

            var userAccountRecordToDelete = await context.UsersAccounts.SingleOrDefaultAsync(x=>x.UserId == userId && x.AccountId == accountId);

            if (userAccountRecordToDelete == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UsersAccountsNull);
            }

            context.UsersAccounts.Remove(userAccountRecordToDelete);

            await context.SaveChangesAsync();

            return accountDtoMapper.MapFrom(account);
        }

        public async Task<IList<AccountDto>> FindAccountsForUserContainingAsync(string searchString, long userId)
        {
            var accountsForUser = await context
                .Accounts
                .Where(a => (context.UsersClients
                       .Where(c => context.UsersClients.Any(uc => uc.UserId == userId)).Select(c => c.ClientId))
                .Contains(a.ClientId) && a.AccountNumber.Contains(searchString)).ToListAsync();

            var mappedAccounts = accountDtoMapper.MapFrom(accountsForUser);

            return mappedAccounts;
        }
    }
}