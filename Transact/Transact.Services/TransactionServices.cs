﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transact.Data;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Services.CustomExceptions;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services
{
    public class TransactionServices : ITransactionServices
    {
        private readonly TransactContext context;
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly ITransactionDtoMapper transactionDtoMapper;

        public TransactionServices(
            TransactContext context,
            IDateTimeProvider dateTimeProvider,
            ITransactionDtoMapper transactionDtoMapper)
        {
            this.context = context;
            this.dateTimeProvider = dateTimeProvider;
            this.transactionDtoMapper = transactionDtoMapper;
        }

        public async Task<TransactionDto> SaveTransactionAsync(TransactionDto transactionDtoInput)
        {
            var senderAccount = await context.Accounts.FindAsync(transactionDtoInput.SenderAccountId);

            if (senderAccount == null)
            {
                throw new BusinessLogicException(ExceptionMessages.SenderAccountNull);
            }

            var receiverAccount = await context.Accounts
                .SingleOrDefaultAsync(x => x.AccountNumber == transactionDtoInput.ReceiverAccountNumber);

            if (receiverAccount == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ReceiverAccountNull);
            }

            if (senderAccount == receiverAccount)
            {
                throw new BusinessLogicException(ExceptionMessages.SameAccountsForTransaction);
            }

            var transactionToSave = transactionDtoMapper.MapFrom(transactionDtoInput);
            transactionToSave.ReceiverAccountId = receiverAccount.Id;
            transactionToSave.TimeStamp = dateTimeProvider.GetDateTimeNow();
            transactionToSave.StatusId = 1;

            context.Transactions.Add(transactionToSave);

            await context.SaveChangesAsync();
            transactionDtoInput.Id = transactionToSave.Id;

            return transactionDtoInput;
        }

        public async Task<TransactionDto> SendTransactionAsync(
           long transactionId)
        {
            using (var transactionScope = await this.context.Database.BeginTransactionAsync(System.Data.IsolationLevel.Serializable))
            {
                var transactionToSend = await context.Transactions.FindAsync(transactionId);

                if (transactionToSend == null)
                {
                    throw new BusinessLogicException(ExceptionMessages.TransactionNull);
                }

                var senderAccount = await context.Accounts.FindAsync(transactionToSend.SenderAccountId);

                if (senderAccount == null)
                {
                    throw new BusinessLogicException(ExceptionMessages.SenderAccountNull);
                }

                var receiverAccount = await context.Accounts.FindAsync(transactionToSend.ReceiverAccountId);

                if (receiverAccount == null)
                {
                    throw new BusinessLogicException(ExceptionMessages.ReceiverAccountNull);
                }

                if (senderAccount == receiverAccount)
                {
                    throw new BusinessLogicException(ExceptionMessages.SameAccountsForTransaction);
                }

                if (senderAccount.Balance < transactionToSend.Amount)
                {
                    throw new BusinessLogicException(ExceptionMessages.NotEnoughFromSenderBalance);
                }

                transactionToSend.TimeStamp = dateTimeProvider.GetDateTimeNow();
                transactionToSend.StatusId = 2;

                senderAccount.Balance -= transactionToSend.Amount;
                receiverAccount.Balance += transactionToSend.Amount;

                await context.SaveChangesAsync();

                var transactionDto = transactionDtoMapper.MapFrom(transactionToSend);

                transactionScope.Commit();

                return transactionDto;
            }
        }

        public async Task<TransactionDto> AddTransactionAsync(TransactionDto transactionDtoInput)
        {
            using (var transactionScope = await this.context.Database.BeginTransactionAsync(System.Data.IsolationLevel.Serializable))
            {
                var senderAccount = await context.Accounts.FindAsync(transactionDtoInput.SenderAccountId);

                if (senderAccount == null)
                {
                    throw new BusinessLogicException(ExceptionMessages.SenderAccountNull);
                }

                var receiverAccount = await context.Accounts
                   .SingleOrDefaultAsync(
                    x => x.AccountNumber == transactionDtoInput.ReceiverAccountNumber);

                if (receiverAccount == null)
                {
                    throw new BusinessLogicException(ExceptionMessages.ReceiverAccountNull);
                }

                if (senderAccount.Balance < transactionDtoInput.Amount)
                {
                    throw new BusinessLogicException(ExceptionMessages.NotEnoughFromSenderBalance);
                }

                if (senderAccount == receiverAccount)
                {
                    throw new BusinessLogicException(ExceptionMessages.SameAccountsForTransaction);
                }

                var transactionToAdd = transactionDtoMapper.MapFrom(transactionDtoInput);
                transactionToAdd.ReceiverAccountId = receiverAccount.Id;
                transactionToAdd.StatusId = 2;
                transactionToAdd.TimeStamp = dateTimeProvider.GetDateTimeNow();

                context.Transactions.Add(transactionToAdd);

                senderAccount.Balance -= transactionDtoInput.Amount;
                receiverAccount.Balance += transactionDtoInput.Amount;

                await context.SaveChangesAsync();

                transactionScope.Commit();
                transactionDtoInput.Id = transactionToAdd.Id;

                return transactionDtoInput;
            }
        }

        public async Task<TransactionDto> UpdateTransactionAsync(TransactionDto transactionDtoInput)
        {
            var senderAccount = await context.Accounts.FindAsync(transactionDtoInput.SenderAccountId);

            if (senderAccount == null)
            {
                throw new BusinessLogicException(ExceptionMessages.SenderAccountNull);
            }

            var receiverAccount = await context.Accounts
               .SingleOrDefaultAsync(
                x => x.AccountNumber == transactionDtoInput.ReceiverAccountNumber);

            if (receiverAccount == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ReceiverAccountNull);
            }

            if (senderAccount == receiverAccount)
            {
                throw new BusinessLogicException(ExceptionMessages.SameAccountsForTransaction);
            }

            var transactionToUpdate = await context.Transactions.FindAsync(transactionDtoInput.Id);

            if (transactionToUpdate == null)
            {
                throw new BusinessLogicException(ExceptionMessages.TransactionNull);
            }

            if (transactionToUpdate.StatusId == 2)
            {
                throw new BusinessLogicException(ExceptionMessages.CannotEditSentTransaction);
            }   

            transactionToUpdate.Description = transactionDtoInput.Description;
            transactionToUpdate.Amount = transactionDtoInput.Amount;
            transactionToUpdate.SenderAccountId = transactionDtoInput.SenderAccountId;
            transactionToUpdate.ReceiverAccountId = receiverAccount.Id;
            transactionToUpdate.StatusId = 1;
            transactionToUpdate.TimeStamp = dateTimeProvider.GetDateTimeNow();           

            await context.SaveChangesAsync();

            transactionDtoInput.Id = transactionToUpdate.Id;

            return transactionDtoInput;
        }

        public async Task<TransactionDto> SendUpdatedTransactionAsync(TransactionDto transactionDtoInput)
        {
            using (var transactionScope = await this.context.Database.BeginTransactionAsync(System.Data.IsolationLevel.Serializable))
            {
                var senderAccount = await context.Accounts.FindAsync(transactionDtoInput.SenderAccountId);

                if (senderAccount == null)
                {
                    throw new BusinessLogicException(ExceptionMessages.SenderAccountNull);
                }

                var receiverAccount = await context.Accounts
                   .SingleOrDefaultAsync(
                    x => x.AccountNumber == transactionDtoInput.ReceiverAccountNumber);

                if (receiverAccount == null)
                {
                    throw new BusinessLogicException(ExceptionMessages.ReceiverAccountNull);
                }

                if (senderAccount == receiverAccount)
                {
                    throw new BusinessLogicException(ExceptionMessages.SameAccountsForTransaction);
                }

                if (senderAccount.Balance < transactionDtoInput.Amount)
                {
                    throw new BusinessLogicException(ExceptionMessages.NotEnoughFromSenderBalance);
                }

                var transactionToUpdate = await context.Transactions.FindAsync(transactionDtoInput.Id);

                if (transactionToUpdate == null)
                {
                    throw new BusinessLogicException(ExceptionMessages.TransactionNull);
                }

                if (transactionToUpdate.StatusId == 2)
                {
                    throw new BusinessLogicException(ExceptionMessages.CannotEditSentTransaction);
                }

                transactionToUpdate.Description = transactionDtoInput.Description;
                transactionToUpdate.Amount = transactionDtoInput.Amount;
                transactionToUpdate.SenderAccountId = transactionDtoInput.SenderAccountId;
                transactionToUpdate.ReceiverAccountId = receiverAccount.Id;
                transactionToUpdate.StatusId = 2;
                transactionToUpdate.TimeStamp = dateTimeProvider.GetDateTimeNow();

                senderAccount.Balance -= transactionDtoInput.Amount;
                receiverAccount.Balance += transactionDtoInput.Amount;

                await context.SaveChangesAsync();

                transactionScope.Commit();
                transactionDtoInput.Id = transactionToUpdate.Id;

                return transactionDtoInput;
            }
        }


        public async Task<TransactionDto> GetSingleTransactionAsync(long transactionId)
        {
            var transaction = await context.Transactions
                                    .Include(x=>x.SenderAccount)
                                    .Include(x=>x.ReceiverAccount)
                                    .Include(x=>x.Status)
                                    .SingleOrDefaultAsync(x=>x.Id == transactionId);

            if (transaction == null)
            {
                throw new BusinessLogicException(ExceptionMessages.TransactionNull);
            }

            var transactionDto = transactionDtoMapper.MapFrom(transaction);

            return transactionDto;            
        }

        public async Task<TransactionDto> GetSingleTransactionForEditingAsync(long transactionId)
        {
            var transaction = await context.Transactions
                                    .Include(x => x.SenderAccount)
                                    .Include(x => x.ReceiverAccount)
                                    .Include(x=>x.Status)
                                    .SingleOrDefaultAsync(x => x.Id == transactionId);

            if (transaction == null)
            {
                throw new BusinessLogicException(ExceptionMessages.TransactionNull);
            }

            if (transaction.StatusId == 2)
            {
                throw new BusinessLogicException(ExceptionMessages.CannotEditSentTransaction);
            }

            var transactionDto = transactionDtoMapper.MapFrom(transaction);

            return transactionDto;
        }

        public async Task<IList<TransactionDto>> GetFiveTransactionsForAccountDescAsync(
             int currPage,
             long accountId,
             long userId)
        {

            if (accountId > 0)
            {
                var usersAccounts = await context.
                           UsersAccounts
                           .SingleOrDefaultAsync(x => x.AccountId == accountId
                           && x.UserId == userId);

                if (usersAccounts == null)
                {
                    throw new AccountNotAvailableException(ExceptionMessages.AccountNoLongerAvailable);
                }
            }        

         
            var fiveTransactions = await context
                                   .Transactions
                                   .Include(x => x.Status)
                                   .Include(x => x.SenderAccount)
                                   .Include(x => x.ReceiverAccount)
                                   .Where(x => x.SenderAccountId == accountId
                                   || (x.ReceiverAccountId == accountId && x.StatusId == 2))
                                   .OrderByDescending(x => x.TimeStamp)
                                   .Skip((currPage - 1) * 5)
                                   .Take(5).ToListAsync();

            //Alternative Manual SQL Query to get fiveTransactions for Account 
            //Including Nickname given by User

            //var fiveTransactions = await context
            //   .Transactions
            //   .FromSql(@"SELECT DISTINCT tr.Amount, tr.StatusId, tr.Id, tr.ReceiverAccountId, tr.SenderAccountId, tr.Description, tr.TimeStamp FROM dbo.Transactions tr 
            //              JOIN dbo.Accounts a on (a.Id = {0} AND tr.ReceiverAccountId = a.Id AND tr.StatusId = 2) OR (a.Id = {0} AND tr.SenderAccountId = a.Id)
            //              JOIN dbo.UsersAccounts ua on (ua.AccountId = tr.SenderAccountId or ua.AccountId = tr.ReceiverAccountId) and ua.UserId = {1} 
            //              ORDER BY tr.TimeStamp DESC 
            //              OFFSET {2} ROWS FETCH NEXT 5 ROWS ONLY", accountId, userId, currPage - 1)
            //              .Include(x => x.SenderAccount)
            //              .ThenInclude(x => x.Client)
            //   .ToListAsync();
            

            var mappedTransactions = transactionDtoMapper.MapFromWithPersonalNick(fiveTransactions);
            return mappedTransactions;
        }

        public async Task<int> GetPageCountForTransactionsAsync(
            int transactionsPerPage, 
            long? accountId, 
            IEnumerable<long> accountsIds)
        {
            if (accountId != null)
            {
                var allTransactionsCount = await context
                                           .Transactions
                                           .Where(x => x.ReceiverAccountId == accountId 
                                           || x.SenderAccountId == accountId)
                                           .CountAsync();

                int pageCount = (allTransactionsCount - 1) / transactionsPerPage + 1;

                return pageCount;
            }
            else
            {              
                var allUserTransactionsCount = await context.Transactions
                                              .Where(t => accountsIds
                                              .Contains(t.ReceiverAccountId)
                                              || accountsIds
                                              .Contains(t.SenderAccountId))
                                              .CountAsync();                

                int pageCount = (allUserTransactionsCount - 1) / transactionsPerPage + 1;

                return pageCount;
            }            
        }     
        
    }
}
