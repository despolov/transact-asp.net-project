﻿using System.Collections.Generic;
using System.Linq;
using Transact.Models.Dtos;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.Mapper.Contracts;
using Transact.Web.Utils;

namespace Transact.Web.Mapper
{
    public class TransactionViewModelMapper : ITransactionVeiwModelMapper
    {
        public TransactionViewModel MapFrom(TransactionDto entity)
        {
            return new TransactionViewModel()
            {
                Id = entity.Id,
                Amount = entity.Amount,
                SenderAccountNumber = entity.SenderAccountNumber,
                SenderAccountNickName = entity.SenderAccountNickName,
                SenderClientName = entity.SenderClientName,
                ReceiverAccountNumber = entity.ReceiverAccountNumber,
                ReceiverAccountNickName = entity.ReceiverAccountNickName,
                ReceiverClientName = entity.ReceiverClientName,
                SenderAccountId = entity.SenderAccountId,
                ReceiverAccountId = entity.ReceiverAccountId,
                Description = entity.Description,
                TimeStamp = entity.TimeStamp,
                StatusName = entity.StatusName,
                StatusId = entity.StatusId
            };
        }

        public TransactionViewModel MapFromWithAccount(TransactionDto entity, long? accountId)
        {
            var result = new TransactionViewModel
            {
                 Id = entity.Id,
                Amount = entity.Amount,
                SenderAccountNumber = entity.SenderAccountNumber,
                SenderAccountNickName = entity.SenderAccountNickName,
                SenderClientName = entity.SenderClientName,
                ReceiverAccountNumber = entity.ReceiverAccountNumber,
                ReceiverAccountNickName = entity.ReceiverAccountNickName,
                ReceiverClientName = entity.ReceiverClientName,
                SenderAccountId = entity.SenderAccountId,
                ReceiverAccountId = entity.ReceiverAccountId,
                Description = entity.Description,
                TimeStamp = entity.TimeStamp,
                StatusName = entity.StatusName,
                StatusId = entity.StatusId
            };

            if (accountId == null)
            {
                result.IOstatus = TransactionIOStatus.IncomingAndOutgoing;
            }
            else if (accountId == entity.SenderAccountId)
            {                
                result.IOstatus = TransactionIOStatus.Outgoing;
            }
            else if (accountId == entity.ReceiverAccountId)
            {
                result.IOstatus = TransactionIOStatus.Incoming;
            }

            return result;
        }

        public TransactionDto MapFrom(TransactionViewModel entity)
        {
            return new TransactionDto()
            {
                Id = entity.Id,
                Amount = entity.Amount,
                SenderAccountId = entity.SenderAccountId,
                ReceiverAccountId = entity.ReceiverAccountId,
                ReceiverAccountNumber = entity.ReceiverAccountNumber,
                Description = entity.Description,
                TimeStamp = entity.TimeStamp
            };
        }

        public IList<TransactionViewModel> MapFrom(ICollection<TransactionDto> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }

        public IList<TransactionViewModel> MapFromWithAccount(ICollection<TransactionDto> entities, long? accountId)
        {
            return entities.Select(item => this.MapFromWithAccount(item, accountId)).ToList();
        }

        public IList<TransactionDto> MapFrom(ICollection<TransactionViewModel> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }
    }
}
