﻿using System.Collections.Generic;
using System.Linq;
using Transact.Models.Dtos;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.Mapper.Contracts;
using Transact.Web.ViewModels;

namespace Transact.Web.Mapper
{
    public class AccountViewModelMapper : IAccountViewModelMapper
    {
        public AccountViewModel MapFrom(AccountDto entity)
        {
            return new AccountViewModel()
            {
                Id = entity.Id,
                AccountNumber = entity.AccountNumber,
                NickName = entity.NickName,
                Balance = entity.Balance,
                ClientId = entity.ClientId,
                ClientName = entity.ClientName
            };          
        }

        public AccountDto MapFrom(AccountViewModel entity)
        {
            return new AccountDto()
            {
                Id = entity.Id,
                ClientId = entity.ClientId,
                Balance = entity.Balance,
                ClientName = entity.ClientName
            };
        }

        public IList<AccountViewModel> MapFrom(ICollection<AccountDto> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }

        public IList<AccountDto> MapFrom(ICollection<AccountViewModel> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }
    }
}
