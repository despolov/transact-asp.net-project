﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Transact.Models.Dtos;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Mapper.Contracts;

namespace Transact.Web.Mapper
{
    public class BannerViewModelMapper : IBannerViewModelMapper
    {
        private readonly IConfiguration configuration;

        public BannerViewModelMapper(IConfiguration configuration)
        {            
            this.configuration = configuration;
        }

        public BannerViewModel MapFrom(BannerDto entity)
        {            
            return new BannerViewModel()
            {
                Id = entity.Id,
                StartDate = entity.StartDate,
                EndDate = entity.EndDate,
                Url = entity.Url,
                ImageName = entity.ImageName,
                ImagePath = configuration.GetSection("DisplayImageFolder").Value + entity.ImageName
            };
        }

        public EditBannerViewModel MapFromForEditing(BannerDto entity)
        {
            return new EditBannerViewModel()
            {
                Id = entity.Id,
                StartDate = entity.StartDate,
                EndDate = entity.EndDate,
                Url = entity.Url,
                ImageName = entity.ImageName,
                ImagePath = configuration.GetSection("DisplayImageFolder").Value + entity.ImageName
            };
        }

        public BannerDto MapFrom(BannerViewModel entity)
        {
            return new BannerDto()
            {
                Id = entity.Id,
                StartDate = entity.StartDate,
                EndDate = entity.EndDate,
                Url = entity.Url,
                ImageName = entity.ImageName
            };
        }

        public BannerDto MapFromForEditing(EditBannerViewModel entity)
        {
            return new BannerDto()
            {
                Id = entity.Id,
                StartDate = entity.StartDate,
                EndDate = entity.EndDate,
                Url = entity.Url,
                ImageName = entity.ImageName
            };
        }

        public IList<BannerDto> MapFrom(ICollection<BannerViewModel> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }

        public IList<BannerViewModel> MapFrom(ICollection<BannerDto> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }
    }    
}
