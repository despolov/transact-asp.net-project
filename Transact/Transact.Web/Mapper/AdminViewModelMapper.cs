﻿using System.Collections.Generic;
using System.Linq;
using Transact.Models.Dtos;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Mapper.Contracts;

namespace Transact.Web.Mapper
{
    public class AdminViewModelMapper : IAdminViewModelMapper
    {
        public AdminLoginViewModel MapFrom(AdminDto entity)
        {
            return new AdminLoginViewModel()
            {
                UserName = entity.UserName
            };
        }

        public AdminDto MapFrom(AdminLoginViewModel entity)
        {
            return new AdminDto()
            {
                UserName = entity.UserName,
                Password = entity.Password
            };
        }

        public IList<AdminLoginViewModel> MapFrom(ICollection<AdminDto> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }

        public IList<AdminDto> MapFrom(ICollection<AdminLoginViewModel> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }
    }
}