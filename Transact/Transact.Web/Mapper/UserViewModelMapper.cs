﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Mapper.Contracts;

namespace Transact.Web
{
    public class UserViewModelMapper : IUserViewModelMapper
    {
        public UserViewModel MapFrom(UserDto entity)
        {
            return new UserViewModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                RoleId = entity.RoleId,
                UserName = entity.UserName
            };
        }
       
        public UserDto MapFrom(UserViewModel entity)
        {
            return new UserDto()
            {
                Id = entity.Id,
                Name = entity.Name,
                Password = entity.Password,
                UserName = entity.UserName
            };
        }

        public IList<UserViewModel> MapFrom(ICollection<UserDto> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }

        public IList<UserDto> MapFrom(ICollection<UserViewModel> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }
    }
}
