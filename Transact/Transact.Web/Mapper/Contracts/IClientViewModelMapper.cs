﻿using System.Collections.Generic;
using Transact.Models.Dtos;
using Transact.Web.Areas.Admin.ViewModels;

namespace Transact.Web.Mapper.Contracts
{
    public interface IClientViewModelMapper
    {
        ClientViewModel MapFrom(ClientDto entity);

        ClientDto MapFrom(ClientViewModel entity);

        IList<ClientViewModel> MapFrom(ICollection<ClientDto> entities);

        IList<ClientDto> MapFrom(ICollection<ClientViewModel> entities);
    }
}