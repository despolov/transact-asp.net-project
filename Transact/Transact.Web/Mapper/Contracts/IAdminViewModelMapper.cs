﻿using System.Collections.Generic;
using Transact.Models.Dtos;
using Transact.Web.Areas.Admin.ViewModels;

namespace Transact.Web.Mapper.Contracts
{
    public interface IAdminViewModelMapper
    {
        AdminLoginViewModel MapFrom(AdminDto entity);

        AdminDto MapFrom(AdminLoginViewModel entity);

        IList<AdminLoginViewModel> MapFrom(ICollection<AdminDto> entities);

        IList<AdminDto> MapFrom(ICollection<AdminLoginViewModel> entities);
    }
}