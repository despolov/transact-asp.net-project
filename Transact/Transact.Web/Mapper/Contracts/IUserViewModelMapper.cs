﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transact.Models.Dtos;
using Transact.Web.Areas.Admin.ViewModels;

namespace Transact.Web.Mapper.Contracts
{
    public interface IUserViewModelMapper
    {
        UserViewModel MapFrom(UserDto entity);

        UserDto MapFrom(UserViewModel entity);

        IList<UserViewModel> MapFrom(ICollection<UserDto> entities);

        IList<UserDto> MapFrom(ICollection<UserViewModel> entities);
    }
}
