﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transact.Models.Dtos;
using Transact.Web.Areas.Admin.ViewModels;

namespace Transact.Web.Mapper.Contracts
{
    public interface IBannerViewModelMapper
    {
        BannerViewModel MapFrom(BannerDto entity);

        EditBannerViewModel MapFromForEditing(BannerDto entity);

        BannerDto MapFrom(BannerViewModel entity);

        BannerDto MapFromForEditing(EditBannerViewModel entity);

        IList<BannerDto> MapFrom(ICollection<BannerViewModel> entities);

        IList<BannerViewModel> MapFrom(ICollection<BannerDto> entities);
    }
}
