﻿using System.Collections.Generic;
using Transact.Models.Dtos;
using Transact.Web.Areas.AppUser.ViewModels;

namespace Transact.Web.Mapper.Contracts
{
    public interface ITransactionVeiwModelMapper
    {
        TransactionViewModel MapFrom(TransactionDto entity);

        TransactionViewModel MapFromWithAccount(TransactionDto entity, long? accountId);

        TransactionDto MapFrom(TransactionViewModel entity);

        IList<TransactionDto> MapFrom(ICollection<TransactionViewModel> entities);

        IList<TransactionViewModel> MapFromWithAccount(ICollection<TransactionDto> entities, long? accountId);

        IList<TransactionViewModel> MapFrom(ICollection<TransactionDto> entities);
    }
}
