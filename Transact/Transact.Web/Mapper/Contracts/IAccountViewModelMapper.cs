﻿using System.Collections.Generic;
using Transact.Models.Dtos;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.ViewModels;

namespace Transact.Web.Mapper.Contracts
{
    public interface IAccountViewModelMapper
    {
        AccountViewModel MapFrom(AccountDto entity);

        AccountDto MapFrom(AccountViewModel entity);

        IList<AccountViewModel> MapFrom(ICollection<AccountDto> entities);

        IList<AccountDto> MapFrom(ICollection<AccountViewModel> entities);
    }
}
