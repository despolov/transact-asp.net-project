﻿using System.Collections.Generic;
using System.Linq;
using Transact.Models.Dtos;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Mapper.Contracts;

namespace Transact.Web.Areas.Admin.Mapper
{
    public class ClientViewModelMapper : IClientViewModelMapper
    {
        public ClientViewModel MapFrom(ClientDto entity)
        {
            return new ClientViewModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                NumberOfAccounts = entity.NumberOfAccounts
            };
        }

        public ClientDto MapFrom(ClientViewModel entity)
        {
            return new ClientDto()
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }

        public IList<ClientViewModel> MapFrom(ICollection<ClientDto> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }

        public IList<ClientDto> MapFrom(ICollection<ClientViewModel> entities)
        {
            return entities.Select(this.MapFrom).ToList();
        }
    }
}