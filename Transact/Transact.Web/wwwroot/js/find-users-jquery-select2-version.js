
$('#input-for-userClient').autocomplete({    
    source: function (request, response)
    {
        var clientIdToSend = ($('#client-id-for-adding-user-hidden')[0]).value;
        console.log(clientIdToSend);
        debugger;
        $.ajax({        
                url: "/Admin/User/FindSingleByUserName",
                type: "POST",
                dataType: "json",                
                data: { userNameString: request.term, clientId: clientIdToSend},
                success: function (data)
                {      
                    response($.map(data, function (item)
                    {                 
                        var itemToSave = Number(item.id);
                        $("#save-target-id").val(itemToSave);
                        var completeUserListing = item.userName;                                     
                        return { label: completeUserListing };                        
                    }))
                   
                }               
            })
        },
        minLength: 3,
        delay: 500,
            minLength: 3,
            response: function(event, ui) 
            {
                if (!ui.content.length) {
                    var noResult = { value:"",label:"No results found" };
                    ui.content.push(noResult);
                    $("#message").text("No results found");
                } else {
                    $("#message").empty();
                }
            }               
    });
