var clickedButtonEdit;
var urlToEditTransaction;

$('#update-transaction-button').on('click', function(e){
    clickedButtonEdit = $(this).attr('id');
    urlToEditTransaction = "/AppUser/Transaction/UpdateTransaction";
})

$('#send-updated-transaction-button').on('click', function(e){
    clickedButtonEdit = $(this).attr('id');
    urlToEditTransaction = "/AppUser/Transaction/SendUpdatedTransaction";
})

$('#edit-transaction-form').submit(function (event) 
{
    event.preventDefault();
    var $thisTranscationEdit = $(this);
    if($($thisTranscationEdit).valid())
    {       
        var inputsForEditing =  $thisTranscationEdit.find('.input-bordered');      
        var senderAccountNumberToCheckEdit = $('#sender-account-selector option:selected').text();
        var receiverAccountNumberToCheckEdit = $('#input-for-receiver-account').val();
        var amountOnSubmitEdit = Number($('#transaction-amount-selector').val());
        var balanceOnSubmitEdit = Number($('#sender-account-selector option:selected').attr('bal'));   
       
       if(senderAccountNumberToCheckEdit != receiverAccountNumberToCheckEdit)
        {
                var serializedInputEdit = inputsForEditing.serialize();	
                var totalInputsEdit = $('#edit-transaction-form').find('input');
                var tokenForAddingEdit = totalInputsEdit[6];  
                var senderAccountIdForEditedTransaction = $('#sender-account-selector option:selected')[0].value;  
            
                var serInputWithVerificationTokenEdit = serializedInputEdit+"&"+tokenForAddingEdit.name+"="+tokenForAddingEdit.value;

                $.post(urlToEditTransaction, serInputWithVerificationTokenEdit)
                .done(function (responseEdit) 
                {
                    var transactionIdEdit = ((responseEdit.split('#'))[1].split(' '))[0]; 
                    //gets the id If format of response is: "Word #[id] more words"

                    toastr.options = 
                    {
                        "debug": false,
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "onHidden" : function(){
                            // this will be executed after fadeout, i.e. 2secs after notification has been show
                            window.location.href = "/AppUser/Transaction/listtransactions?preSelectedAccount="+senderAccountIdForEditedTransaction;
                        }
                    }
                    
                    // $thisTranscation[0].reset();

                    toastr.success(responseEdit);

                }).fail(function (responseFailEdit) 
                {
                            toastr.options = 
                            {
                                "debug": false,
                                "positionClass": "toast-top-center",
                                "onclick": null,
                                "fadeIn": 300,
                                "fadeOut": 1000,
                                "timeOut": 3000,
                                "extendedTimeOut": 3000
                            }
                            toastr.error(responseFailEdit.responseText);
                });			
        } 
        else
        {
           toastr.options = 
             {
                 "debug": false,
                 "positionClass": "toast-top-center",
                 "onclick": null,
                 "fadeIn": 300,
                 "fadeOut": 1000,
                 "timeOut": 3000,
                 "extendedTimeOut": 3000
            }
            toastr.error("Sender and Receiver account cannot be the same!");
        }       
        
    };      
});