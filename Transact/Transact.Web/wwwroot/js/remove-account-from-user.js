$('#userAccount-manage-holder').on('click', '.unassign-account-from-user-button', function(clickedRemoveBtnEvent)
{
    $this = $(this);
    clickedRemoveBtnEvent.preventDefault();
    if (window.confirm("Are you sure?"))
    {       
        var userId = $this.attr('userId');

        var accountId = $this.attr('accountId');

        var antiforgeryspan = $('#anti-forgery-for-account-for-user-unassign').find('input');
        var antifogerytoken = antiforgeryspan[0];    
        var url = "/Admin/User/RemoveUserForClient"
        var params = "userId=" + userId + "&accountId=" + accountId + "&" + antifogerytoken.name + "=" + antifogerytoken.value;

        $.post(url, params).done(function(data)
        {
            var getUserAccountsTableUrlAfterAdding = "/Admin/User/GetFiveAccountsForUserById?currPage=1&userId=" + userId;
            $.get(getUserAccountsTableUrlAfterAdding)
            .done(function (data) {
                $('#userAccounts-list-table-holder').empty();

                $('#userAccounts-list-table-holder').append(data);
            })
            
        }).fail(function(data)
        {
        });
    }
});