 $('#submit-banner-form').submit(function (eventBanner) 
 {
   eventBanner.preventDefault();
   var $thisBannerEvent = $(this);


    if($($thisBannerEvent).valid())
    {	 
       var urlToSend = "/Admin/Banner/SaveBanner";
       var inputs = $thisBannerEvent.find('input');

       var startDate = inputs[1].value;
       var endDate = inputs[2].value;
       
       if(startDate < endDate)
       {           
            var urlOfbanner = inputs[0].value;    
            var file = inputs[3];
            var image = file.files[0];
            var antiForgeryToken = inputs[4];

            var fdata = new FormData();
            fdata.append("Url", urlOfbanner);
            fdata.append("StartDate", startDate);
            fdata.append("EndDate", endDate);
            fdata.append("Image", image);
            fdata.append(antiForgeryToken.name, antiForgeryToken.value);         
                
            $.ajax({
                type: 'post',
                url: urlToSend ,
                data: fdata,
                processData: false,
                contentType: false
                }).done(function (responseBanner)
                {          
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true
                    }

                    toastr.success(responseBanner); 

                    $.get("/Admin/Banner/GetFiveBanners")
                        .done(function (bannerData)
                         {
                            $('#target-banners-list-table').empty();
                            $('#target-banners-list-table').append(bannerData);
                        })
                        .fail(function (failResponseBanner) 
                        {
                            toastr.options = {
                                "debug": false,
                                "positionClass": "toast-top-center",
                                "onclick": null,
                                "fadeIn": 300,
                                "fadeOut": 1000,
                                "timeOut": 3000,
                                "extendedTimeOut": 3000,
                                "closeButton": true
                            }
                            toastr.error(failResponseBanner.responseText);
                        });                   

                }).fail(function (errorResponse) 
                {
                        toastr.options = {
                            "debug": false,
                            "positionClass": "toast-top-center",
                            "onclick": null,
                            "fadeIn": 300,
                            "fadeOut": 1000,
                            "timeOut": 3000,
                            "extendedTimeOut": 3000,
                            "closeButton": true
                        }
                        toastr.error(errorResponse.responseText);                                             
                });			
                
    
            } 
       }
       else
       {
            toastr.options = 
            {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }

            toastr.error("Please, enter valid Banner Activity Period!");   

       }   
      
});