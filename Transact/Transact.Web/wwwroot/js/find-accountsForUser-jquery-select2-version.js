$('#input-for-userAccounts').autocomplete({    
    source: function (request, response)
    {
        var userIdToSend = ($('#user-id-for-assigning-account-hidden')[0]).value;

        $.ajax({        
            url: "/Admin/User/FindSingleByAccountNumberForUser",
                type: "POST",
                dataType: "json",                
                data: { accountNumberString: request.term, userId: userIdToSend },
                success: function (data)
                {      
                    response($.map(data, function (item)
                    {                 
                        var itemToSave = Number(item.id);
                        $("#save-target-id").val(itemToSave);
                        var completeAccountListing = item.accountNumber;                                     
                        return { label: completeAccountListing };                        
                    }))
                }               
            })
        },
        minLength: 3,
        delay: 500,
            minLength: 3,
            response: function(event, ui) 
            {
                if (!ui.content.length) {
                    var noResult = { value:"",label:"No results found" };
                    ui.content.push(noResult);
                    $("#message").text("No results found");
                } else {
                    $("#message").empty();
                }
            }               
    });
