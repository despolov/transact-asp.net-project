$(document).ready(function() 
{
    $('#transactions-account-selector').select2(); 
});


$('#target-transaction-list-table').on('click', '.transaction-pagination-button-table', function ShowTransactions (event) 
{
        event.preventDefault();
        var $thisTransactPage = $(this);		
        var targetPage = parseInt( $thisTransactPage.attr('at'));
        var preSelectedOption = $('#transactions-account-selector option:selected').attr('accid');;
        var urlPagination;

        if(preSelectedOption)
        {
            var preSelectedAccountIdPaging = parseInt(preSelectedOption);
            urlPagination = "/AppUser/Transaction/GetFiveTransactions?currPage="+targetPage+"&preSelectedAccount="+preSelectedAccountIdPaging;
        }
        else
        {
            urlPagination = "/AppUser/Transaction/GetFiveTransactions?currPage="+targetPage;
        } 
  
		$.get(urlPagination)
            .done(function (data) 
            {
                
                $('#target-transaction-list-table').empty();

                $('#target-transaction-list-table').append(data);
            }).fail(function (failResponse) 
            {
                if(failResponse.getResponseHeader('customError') == "AccountNotAvailable")
                {            
                        toastr.options = 
                        {
                            "debug": false,
                            "positionClass": "toast-top-center",
                            "onclick": null,
                            "fadeIn": 300,
                            "fadeOut": 1000,
                            "timeOut": 2000,
                            "extendedTimeOut": 2000,
                            "closeButton": true,
                            "onHidden" : function()
                            {
                                // this will be executed after fadeout, i.e. 2secs after notification has been show
                                location.reload();
                            }
                        }           
                }
                else
                {
                        toastr.options = 
                        {
                            "debug": false,
                            "positionClass": "toast-top-center",
                            "onclick": null,
                            "fadeIn": 300,
                            "fadeOut": 1000,
                            "timeOut": 3000,
                            "extendedTimeOut": 3000,
                            "closeButton": true                
                        }             
                }
    
               toastr.error(failResponse.responseText);
    
             });  	 		
});


$('#transactions-account-selector').on('select2:select', function (eventSelectAccount) 
{
    var selectedAccountId = $('#transactions-account-selector option:selected').attr('accid'); 
    var accountId = parseInt(selectedAccountId);
           
            var currPageFromAccountSelect = parseInt($('#current-page-active-button').attr('at'));
            $thisTransactionsTable = $('#target-transaction-list-table');

            var urlSelectAccPagination = "/AppUser/Transaction/GetFiveTransactions?currPage="+currPageFromAccountSelect+"&preSelectedAccount="+accountId;

            $.get(urlSelectAccPagination)
                .done(function (data) 
                {
                    $('#target-transaction-list-table').empty();
                    $('#target-transaction-list-table').append(data);
                })         
                .fail(function (failResponse) 
                {     
                    if(failResponse.getResponseHeader('customError') == "AccountNotAvailable")
                    {    
                            toastr.options = 
                            {
                                "debug": false,
                                "positionClass": "toast-top-center",
                                "onclick": null,
                                "fadeIn": 300,
                                "fadeOut": 1000,
                                "timeOut": 2000,
                                "extendedTimeOut": 2000,
                                "closeButton": true,
                                "onHidden" : function()
                                {
                                    // this will be executed after fadeout, i.e. 2secs after notification has been show
                                    location.reload();
                                }
                            }  
                    }
                    else
                    {
                            toastr.options = 
                            {
                                "debug": false,
                                "positionClass": "toast-top-center",
                                "onclick": null,
                                "fadeIn": 300,
                                "fadeOut": 1000,
                                "timeOut": 3000,
                                "extendedTimeOut": 3000,
                                "closeButton": true                
                            }             
                    }

                toastr.error(failResponse.responseText);

                });
         	
});