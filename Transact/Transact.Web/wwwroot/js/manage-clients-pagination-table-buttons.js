$('#target-client-list-table').on('click', '.next-pagination-button-table',function (event) {
        var $this2 = $(this);	
		event.preventDefault();		
			
        var targetPage = parseInt($this2.attr('at'));
		 var urlPagination = "/Admin/Client/GetFiveClientsById?="+targetPage;
		$.get(urlPagination)
            .done(function (data) {
                $('#target-client-list-table').empty();

                $('#target-client-list-table').append(data);
            }).fail(function (failResponse) {
               toastr.options = {
                   "debug": false,
                   "positionClass": "toast-top-center",
                   "onclick": null,
                   "fadeIn": 300,
                   "fadeOut": 1000,
                   "timeOut": 3000,
                   "extendedTimeOut": 3000,
                   "closeButton": true
               }
               toastr.error(failResponse.responseText);
			});  		
		});
		
		$('#target-client-list-table').on('click', '.previous-pagination-button-table',function (event) {
		event.preventDefault();
        var $this3 = $(this);			   
        var targetPage = parseInt($this3.attr('at'));
		
	
		
		 var prevUrlPagination = "/Admin/Client/GetFiveClientsById?="+targetPage;
		$.get(prevUrlPagination)
            .done(function (data) {
                $('#target-client-list-table').empty();
				debugger;
                $('#target-client-list-table').append(data);
            }).fail(function (failResponse) {
               toastr.options = {
                   "debug": false,
                   "positionClass": "toast-top-center",
                   "onclick": null,
                   "fadeIn": 300,
                   "fadeOut": 1000,
                   "timeOut": 3000,
                   "extendedTimeOut": 3000,
                   "closeButton": true
               }
               toastr.error(failResponse.responseText);
			});  		
		});