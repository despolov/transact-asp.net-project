var allAccounts = $('#all-accounts-for-user');
var allUserAccounts = allAccounts[0].value;
var chartAccountNames = [];
var chartAccuntBalances = [];

for(var i = 0; i < jsonAccounts.length; i++)
{
    chartAccountNames[i] = jsonAccounts[i].nickName;
    chartAccuntBalances[i] = jsonAccounts[i].balance;
}

var options = {
    chart: {
        width: 380,
        type: 'pie',
    },
    labels: chartAccountNames,
    series: chartAccuntBalances,
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }]
}

var chart = new ApexCharts(
    document.querySelector("#chart"),
    options
);

chart.render();