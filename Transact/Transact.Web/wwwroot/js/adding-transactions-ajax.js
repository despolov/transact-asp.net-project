var clickedButton;
var urlToSubmitTransaction;

$('#save-transaction-button').on('click', function(e){
    clickedButton = $(this).attr('id');
    urlToSubmitTransaction = "/AppUser/Transaction/SaveTransaction";
})

$('#send-transaction-button').on('click', function(e){
    clickedButton = $(this).attr('id');
    urlToSubmitTransaction = "/AppUser/Transaction/SendJustCreatedTransaction";
})

$('#submit-transaction-form').submit(function (event) 
{
    event.preventDefault();
    var $thisTranscation = $(this);
    if($($thisTranscation).valid())
    {       
        var inputs =  $thisTranscation.find('.input-bordered');      
        var senderAccountNumberToCheck = $('#sender-account-selector option:selected').text();
        var senderAccountNumberId = $('#sender-account-selector option:selected').val();
        var receiverAccountNumberToCheck = $('#input-for-receiver-account').val();
        var amountOnSubmit = Number($('#transaction-amount-selector').val());
        var balanceOnSubmit = Number($('#sender-account-selector option:selected').attr('bal'));   
       
        if (clickedButton == "send-transaction-button" && balanceOnSubmit < amountOnSubmit)
        {            
                toastr.options = 
                {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000
                }
                toastr.error("Insufficient funds to send!");
        }         
        else if(senderAccountNumberToCheck != receiverAccountNumberToCheck)
        {
                var serializedInput = inputs.serialize();	
                var totalInputs = $('#submit-transaction-form').find('input');
                var tokenForAdding = totalInputs[5];
                var serInputWithVerificationToken = serializedInput+"&"+tokenForAdding.name+"="+tokenForAdding.value;
                $.post(urlToSubmitTransaction, serInputWithVerificationToken)
                .done(function (response) 
                {
                    // var transactionId = ((response.split('#'))[1].split(' '))[0]; 
                    // //gets the id If format of response is: "Word #[id] more words"
                    toastr.options = 
                    {
                        "debug": false,
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "onHidden" : function(){
                            // this will be executed after fadeout, i.e. 2secs after notification has been show
                            // window.location.href = "/AppUsers/Transactions/GetTransaction?="+transactionId;
                            window.location.href = "/AppUser/Transaction/listtransactions?preSelectedAccount="+senderAccountNumberId;
                        }
                    }

                    $thisTranscation[0].reset();

                    toastr.success(response);

                }).fail(function (response) 
                {
                            toastr.options = 
                            {
                                "debug": false,
                                "positionClass": "toast-top-center",
                                "onclick": null,
                                "fadeIn": 300,
                                "fadeOut": 1000,
                                "timeOut": 3000,
                                "extendedTimeOut": 3000
                            }
                            toastr.error(response.responseText);
                });			
        } 
        else
        {
           toastr.options = 
             {
                 "debug": false,
                 "positionClass": "toast-top-center",
                 "onclick": null,
                 "fadeIn": 300,
                 "fadeOut": 1000,
                 "timeOut": 3000,
                 "extendedTimeOut": 3000
            }
            toastr.error("Sender and Receiver account cannot be the same!");
        }       
        
    };      
});