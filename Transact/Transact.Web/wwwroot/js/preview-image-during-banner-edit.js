$(".special-clas-for-image-data-uploaded").change(function(e) {

    for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
        
        var file = e.originalEvent.srcElement.files[i];
        
        var img = document.createElement("img");
        var reader = new FileReader();
        reader.onloadend = function() {
             img.src = reader.result;
        }
        reader.readAsDataURL(file);

        $("#image-preview-box").empty();
        $("#image-preview-box").append(img);
        console.log($("#is-new-image-uploaded")[0].value);

        
        $("#is-new-image-uploaded").prop('value', true);

        console.log($("#is-new-image-uploaded")[0].value);
    }
});