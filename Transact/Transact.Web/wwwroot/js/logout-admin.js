$('#logout-form-admin-link').on('click', function(eventLogoutGeneral)
{
    eventLogoutGeneral.preventDefault();
    var urlToLogout = "/Admin/Identity/Logout";
    //var logoutRedirectHref = $('#logout-form-user-general').attr('logoutHref');
    var inputs = $('#logout-form-admin-general').find('input');
    var serializedInputs = inputs.serialize();

    $.post(urlToLogout, inputs)
        .done(function(logoutResponse)
        {
            toastr.options =
            {
               "debug": false,
               "positionClass": "toast-top-center",
               "onclick": null,
               "fadeIn": 200,
               "fadeOut": 1000,
               "timeOut": 1000,
               "extendedTimeOut": 1000,
               "onHidden": function()
               {
                   window.location.href = "/Home/Index";
               }
           }

           toastr.success(logoutResponse);

        }).fail(function(logoutResponse)
        {
            toastr.options = 
            {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000
            }
            toastr.error(logoutResponse.responseText);
        });
});