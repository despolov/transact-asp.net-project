$('#target-transaction-list-table').on('click', '.more-details-button-for-transaction', function (detailsEvent) 
{ 
    detailsEvent.preventDefault();
    $thisDetailsButton = $(this);
    var transactionIdForDetailsButtonAction = parseInt($thisDetailsButton.attr('trid'));
    var transactionDetailsUrl = "/AppUser/Transaction/GetTransaction?transactionId="+transactionIdForDetailsButtonAction;
    $.get(transactionDetailsUrl)
        .done(function (detailResponse) 
                {
                    console.log(detailResponse);
                    //OPEN POPUP
                }).fail(function (failDetailResponse) 
                {
                toastr.options = 
                {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                };
                toastr.error(failDetailResponse.responseText);
                });  
});

$('#target-transaction-list-table').on('click', '.pay-now-button-for-transaction', function (payEvent) 
{ 
    var confirmPay=confirm('Are you sure you want to pay?');

    if(confirmPay)
    {
        $thisPayButton = $(this);      
        var transactionPayUrl = "/AppUser/Transaction/SendTransaction"; 
        var transactionIdForPayButtonAction = parseInt($thisPayButton.attr('trid'));     
        var antiforgeryOuterSpan = $thisPayButton.find('.anti-forgery-custom');
        var antiForgerySpan = antiforgeryOuterSpan[0];     
        var antiForgeryArray = $(antiForgerySpan).find('input');
        var antiForgeryToken = antiForgeryArray[0];
        var isInModal = $thisPayButton.attr('ismodal'); 
        var serializedPayDataWithToken = "transactionId="+transactionIdForPayButtonAction+"&"+antiForgeryToken.name+"="+antiForgeryToken.value;
        $.post(transactionPayUrl, serializedPayDataWithToken)
            .done(function (payResponse) 
                    {
                        toastr.options = 
                        {
                            "debug": false,
                            "positionClass": "toast-top-center",
                            "onclick": null,
                            "fadeIn": 300,
                            "fadeOut": 1000,
                            "timeOut": 3000,
                            "extendedTimeOut": 3000,
                            "closeButton": true
                        }; 
                        toastr.success(payResponse);                          
                        var concreteModalCloseButtonId = "#modal-close-button-"+transactionIdForPayButtonAction;
                        if(isInModal === "yes")
                        {
                            $(concreteModalCloseButtonId)[0].click();                            
                        }

                        var targetPageAfterPay = parseInt( $('#current-page-active-button').attr('at'));
                        var preSelectedOptionAfterPay = $('#transactions-account-selector option:selected').attr('accid');;

                        if(preSelectedOptionAfterPay)
                        {
                            var preSelectedAccountIdAfterPay = parseInt(preSelectedOptionAfterPay);
                            var urlPaginationAfterPay = "/AppUser/Transaction/GetFiveTransactions?currPage="+targetPageAfterPay+"&preSelectedAccount="+preSelectedAccountIdAfterPay;
                        }
                        else
                        {
                            var urlPaginationAfterPay = "/AppUser/Transaction/GetFiveTransactions?currPage="+targetPageAfterPay;
                        } 
                
                        $.get(urlPaginationAfterPay)
                            .done(function (tableDataAfterPay) 
                            {                            
                                $('#target-transaction-list-table').empty();
                                $('#target-transaction-list-table').append(tableDataAfterPay);

                            }).fail(function (failAfterPayResponse) 
                            {
                            toastr.options = {
                                "debug": false,
                                "positionClass": "toast-top-center",
                                "onclick": null,
                                "fadeIn": 300,
                                "fadeOut": 1000,
                                "timeOut": 3000,
                                "extendedTimeOut": 3000,
                                "closeButton": true
                            }
                            toastr.error(failAfterPayResponse.responseText);
                            });  		


                    }).fail(function (failPayResponse) 
                    {
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true
                    }
                    toastr.error(failPayResponse.responseText);
                    });  	
    }
});


