var clientIdFromUserManagePage = ($('#client-id-for-adding-user-hidden'))[0].value;

$('#client-manage-holder').on('click', '.pagination-user-client-button-table', function (event) {
   
    event.preventDefault();	
    var $this2 = $(this);
		
    var targetPage = parseInt($this2.attr('at'));
   
    var urlPagination = "/Admin/Client/GetFiveUsersForClientById?currPage=" + targetPage + "&clientId=" + clientIdFromUserManagePage;

		$.get(urlPagination)
            .done(function (data) {
                $('#clientUsers-list-table-holder').empty();

                $('#clientUsers-list-table-holder').append(data);
            }).fail(function (failResponse) {
               toastr.options = {
                   "debug": false,
                   "positionClass": "toast-top-center",
                   "onclick": null,
                   "fadeIn": 300,
                   "fadeOut": 1000,
                   "timeOut": 3000,
                   "extendedTimeOut": 3000,
                   "closeButton": true
               }
               toastr.error(failResponse.responseText);
			});  		
		});
	