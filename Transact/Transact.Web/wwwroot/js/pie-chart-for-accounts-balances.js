var allAccounts = $('#all-accounts-for-user');
var allUserAccounts = allAccounts[0].value;
var chartAccountNames = [];
var chartAccuntBalances = [];


if(jsonAccounts.length > 0)

for(var i = 0; i < jsonAccounts.length; i++)

{
    for(var i = 0; i < jsonAccounts.length; i++)
    {
        chartAccountNames[i] = jsonAccounts[i].nickName;
        chartAccuntBalances[i] = jsonAccounts[i].balance;
    }
    
    var options = {
        chart: {
            width: 250,
            type: 'pie',
        },
        labels: chartAccountNames,
        series: chartAccuntBalances,
        legend: {
            show: false
          }
    }
    
    var chart = new ApexCharts(
        document.querySelector("#chart"),
        options
    );
    
    chart.render();
}
