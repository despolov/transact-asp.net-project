$('#login-form').on('submit', function(eventLogin)
{
    eventLogin.preventDefault();    
    $thisLoginForm = $(this);
    var loginInputs = $thisLoginForm.find('input');
    var serializedInputssForLogin = loginInputs.serialize();
    var redirectHref = $thisLoginForm.attr('redirectPath'); 
    var urlToLoginOn = "/AppUser/Identity/Login";
    $.post(urlToLoginOn, serializedInputssForLogin)
        .done(function(datafromLogin)
        {
            console.log(redirectHref);
            debugger;

            toastr.options =
             {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 200,
                "fadeOut": 1000,
                "timeOut": 1000,
                "extendedTimeOut": 1000,
                "onHidden": function()
                {
                    window.location.href = redirectHref;
                }
            }

            $(':input','#login-form')
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .prop('checked', false)
            .prop('selected', false);

            toastr.success(datafromLogin);

        }).fail(function(datafromLogin)
        {
            toastr.options = 
            {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000
            }

            console.log(datafromLogin);
            debugger;

            toastr.error(datafromLogin.responseText);
        });    
})