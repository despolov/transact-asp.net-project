$('#userAccount-manage-holder').on('submit', '#assign-account-for-user-form', function (event) {

    event.preventDefault();

        var $this = $(this);
		if($($this).valid()){
			
            var url = "/Admin/User/AssignAccountToUser";
            var inputs = $this.find('input');
            var serializedInput = inputs.serialize();		
            var userId = inputs[1].value;

        $.post(url, serializedInput)
            .done(function (response) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }

                toastr.success(response);               
                var getUserAccountsTableUrlAfterAdding = "/Admin/User/GetFiveAccountsForUserById?currPage=1&userId=" + userId;
       
                $.get(getUserAccountsTableUrlAfterAdding)
                    .done(function (data)
                    {
                        $('#userAccounts-list-table-holder').empty();

                        $('#userAccounts-list-table-holder').append(data);
                    })
                    .fail(function (failResponse) {
                        toastr.options = {
                            "debug": false,
                            "positionClass": "toast-top-center",
                            "onclick": null,
                            "fadeIn": 300,
                            "fadeOut": 1000,
                            "timeOut": 3000,
                            "extendedTimeOut": 3000,
                            "closeButton": true
                        }
                        toastr.error(failResponse.responseText);
                    });                   

            }).fail(function (response) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(response.responseText);
            });			
		};      
});