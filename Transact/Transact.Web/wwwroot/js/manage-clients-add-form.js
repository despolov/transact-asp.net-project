 $('#submit-client-form').submit(function (event) {
        event.preventDefault();
        var $this = $(this);
		if($($this).valid()){
			
	    var url = "/Admin/Client/SaveClient";
        var inputs = $this.find('input');
        var serializedInput = inputs.serialize();		
		
        $.post(url, serializedInput)
            .done(function (response) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }

                toastr.success(response);               

                $.get("/Admin/Client/GetFiveClientsById")
                    .done(function (data) {
                        $('#target-client-list-table').empty();

                        $('#target-client-list-table').append(data);

                        $this[0].reset();
                    })
                    .fail(function (failResponse) {
                        toastr.options = {
                            "debug": false,
                            "positionClass": "toast-top-center",
                            "onclick": null,
                            "fadeIn": 300,
                            "fadeOut": 1000,
                            "timeOut": 3000,
                            "extendedTimeOut": 3000,
                            "closeButton": true
                        }
                        toastr.error(failResponse.responseText);
                    });                   

            }).fail(function (response) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(response.responseText);
            });			
		};      
});