$('#submit-account-form').submit(function (event) {
 
        event.preventDefault();
        var $this = $(this);
		if($($this).valid()){
			
	    var url = "/Admin/Account/CreateAccount";
            var inputs = $this.find('input');
            var serializedInput = inputs.serialize();		
            var clientId = inputs[1].value;

        $.post(url, serializedInput)
            .done(function (response) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }

                toastr.success(response);               
                var getAccountsTableUrlAfterAdding = "/Admin/Client/GetFiveAccountsForClientById?currPage=1&clientId=" + clientId;
       
                $.get(getAccountsTableUrlAfterAdding)
                    .done(function (data) {
                        $('#target-account-list-table').empty();

                        $('#target-account-list-table').append(data);
                    })
                    .fail(function (failResponse) {
                        toastr.options = {
                            "debug": false,
                            "positionClass": "toast-top-center",
                            "onclick": null,
                            "fadeIn": 300,
                            "fadeOut": 1000,
                            "timeOut": 3000,
                            "extendedTimeOut": 3000,
                            "closeButton": true
                        }
                        toastr.error(failResponse.responseText);
                    });                   

            }).fail(function (response) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(response.responseText);
            });			
		};      
});