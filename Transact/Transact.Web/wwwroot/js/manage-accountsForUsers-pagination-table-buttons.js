var userIdFromUserAccountsManagePartial = ($('#user-id-for-assigning-account-hidden'))[0].value;

$('#userAccount-manage-holder').on('click', '.pagination-account-user-button-table', function (event) {
   
    event.preventDefault();	
    var $this2 = $(this);
		
    var targetPage = parseInt($this2.attr('at'));
   
    var urlPagination = "/Admin/User/GetFiveAccountsForUserById?currPage=" + targetPage + "&userId=" + userIdFromUserAccountsManagePartial;

		$.get(urlPagination)
            .done(function (data) {
                $('#userAccounts-list-table-holder').empty();

                $('#userAccounts-list-table-holder').append(data);
            }).fail(function (failResponse) {
               toastr.options = {
                   "debug": false,
                   "positionClass": "toast-top-center",
                   "onclick": null,
                   "fadeIn": 300,
                   "fadeOut": 1000,
                   "timeOut": 3000,
                   "extendedTimeOut": 3000,
                   "closeButton": true
               }
               toastr.error(failResponse.responseText);
			});  		
		});
	