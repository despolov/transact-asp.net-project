$(document).ready(function(){  

    $(function () {

                var startDate = new Date($('#startingDate-picker').val());
                var endDate = new Date($('#endingDate-picker').val());

                $('#startingDate-picker').datetimepicker({
                    locale: 'bg',
                    format: 'MM-DD-YYYY'
                });

                $('#endingDate-picker').datetimepicker({
                    locale: 'bg',
                    format: 'MM-DD-YYYY'
                });     

                $("#startingDate-picker").val($.format.date(startDate, 'MM-dd-yyyy'));
                $("#endingDate-picker").val($.format.date(endDate, 'MM-dd-yyyy'));
                
    });

});