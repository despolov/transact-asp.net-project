var clientManageHoler = $('#client-manage-holder');
var urlForAccountManagement = "/Admin/Client/ManageAccountsForClient?clientId=";
var urlForUserManagement = "/Admin/Client/ManageUsersForClient?clientId=";

$('#toggle-account-management-button').on('click', function (accountClickEvent)
{ 
    accountClickEvent.preventDefault();
    var $thisAccountButton = $(this);   

    var clientId = parseInt($thisAccountButton.attr('clientId'));

    var urlToGetAccountManagement = urlForAccountManagement + clientId;

    $.get(urlToGetAccountManagement)
        .done(function (accountResponseData)
        {
             clientManageHoler.empty();
             clientManageHoler.append(accountResponseData);

        }).fail(function (accountResponseData)
        {
            toastr.options =
                {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(accountResponseData.responseText);
        });

});

$('#toggle-user-management-button').on('click', function (userClickEvent) {

    userClickEvent.preventDefault();  

    var $thisUserButton = $(this); 

    var clientId = parseInt($thisUserButton.attr('clientId'));
    var urlToGetUserManagement = urlForUserManagement + clientId;

    $.get(urlToGetUserManagement)
        .done(function (userResponseData)
        {
            clientManageHoler.empty();
            clientManageHoler.append(userResponseData);

        }).fail(function (userResponseData)
        {
            toastr.options =
            {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(userResponseData.responseText);
        });
});
