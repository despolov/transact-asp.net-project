$('#target-banners-list-table').on('click', '.banner-pagination-buttons', function (eventBannerPagination) 
{
        eventBannerPagination.preventDefault();
        var $thisBannersPage = $(this);		
        var targetPage = parseInt( $thisBannersPage.attr('at'));
        var urlPagination = "/Admin/Banner/GetFiveBanners?currPage="+targetPage;
  
		$.get(urlPagination)
            .done(function (data) 
            {                
                $('#target-banners-list-table').empty();
                $('#target-banners-list-table').append(data);
            }).fail(function (failResponse) 
            {          
                        toastr.options = 
                        {
                            "debug": false,
                            "positionClass": "toast-top-center",
                            "onclick": null,
                            "fadeIn": 300,
                            "fadeOut": 1000,
                            "timeOut": 3000,
                            "extendedTimeOut": 3000,
                            "closeButton": true                
                        }           
                
               toastr.error(failResponse.responseText);
    
             });  	 		
});