    $('#input-for-receiver-account').autocomplete({
        source: function (request, response) {
            var dataToSend = parseInt($('#sender-account-selector option:selected').attr('value'));

            $.ajax({
                url: "/AppUser/Account/FindSingleByAccName",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term, ignored: dataToSend },
                success: function (data) {
                    response($.map(data, function (item) {
                        var itemToSave = Number(item.id);
                        $("#save-target-id").val(itemToSave);
                        var completeAccountListing = item.accountNumber;
                        $('#receiver-client-name-div').empty();
                        $('#receiver-client-name-div').append("Client Name: " + item.clientName);
                        return { label: completeAccountListing };
                    }));

                }
            });
        },
        minLength: 3,
        delay: 500,
            minLength: 3,
            response: function(event, ui) 
            {
                if (!ui.content.length) {
                    var noResult = { value:"",label:"No results found" };
                    ui.content.push(noResult);
                    $("#message").text("No results found");
                } else {
                    $("#message").empty();
                }
            }               
    });
