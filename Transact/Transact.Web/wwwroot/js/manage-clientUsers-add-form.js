$('#client-manage-holder').on('submit', '#assign-user-for-client-form', function (event) {

    event.preventDefault();

        var $this = $(this);
		if($($this).valid()){
			
            var url = "/Admin/Client/AssignUserToClient";
            var inputs = $this.find('input');
            var serializedInput = inputs.serialize();		
            var clientId = inputs[1].value;

        $.post(url, serializedInput)
            .done(function (response) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }

                toastr.success(response);               
                var getUsersTableUrlAfterAdding = "/Admin/Client/GetFiveUsersForClientById?currPage=1&clientId=" + clientId;
       
                $.get(getUsersTableUrlAfterAdding)
                    .done(function (data)
                    {
                        $('#clientUsers-list-table-holder').empty();

                        $('#clientUsers-list-table-holder').append(data);
                    })
                    .fail(function (failResponse) {
                        toastr.options = {
                            "debug": false,
                            "positionClass": "toast-top-center",
                            "onclick": null,
                            "fadeIn": 300,
                            "fadeOut": 1000,
                            "timeOut": 3000,
                            "extendedTimeOut": 3000,
                            "closeButton": true
                        }
                        toastr.error(failResponse.responseText);
                    });                   

            }).fail(function (response) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(response.responseText);
            });			
		};      
});