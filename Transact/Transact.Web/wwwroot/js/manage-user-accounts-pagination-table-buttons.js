$(document).ready(function() 
{
    $('#user-client-selector').select2(); 
});

$('#accounts-for-user-table-holder').on('click', '.user-acounts-pagination-table-button', function (event) 
{
        event.preventDefault();
        var $thisAccountPage = $(this);		
        var targetPage = parseInt( $thisAccountPage.attr('at'));
        var preSelectedOption = $('#user-client-selector option:selected').attr('clientId');;

        if(preSelectedOption)
        {
            var preSelectedClientIdPaging = parseInt(preSelectedOption);
            var urlPagination = "/AppUser/Account/GetFiveAccounts?currPage="+targetPage+"&preSelectedClient="+preSelectedClientIdPaging;
        }

        else
        {
            var urlPagination = "/AppUser/Account/GetFiveAccounts?currPage="+targetPage;
        } 
  
		$.get(urlPagination)
            .done(function (data) 
            {    

                $('#accounts-for-user-table-holder').empty();
                $('#accounts-for-user-table-holder').append(data);

            }).fail(function (failResponse) 
            {
                if(failResponse.getResponseHeader('customError') == "ClientNotAvailable")
                {            
                        toastr.options = 
                        {
                            "debug": false,
                            "positionClass": "toast-top-center",
                            "onclick": null,
                            "fadeIn": 300,
                            "fadeOut": 1000,
                            "timeOut": 2000,
                            "extendedTimeOut": 2000,
                            "closeButton": true,
                            "onHidden" : function()
                            {
                                // this will be executed after fadeout, i.e. 2secs after notification has been show
                                location.reload();
                            }
                        }           
                }
                else
                {
                        toastr.options = 
                        {
                            "debug": false,
                            "positionClass": "toast-top-center",
                            "onclick": null,
                            "fadeIn": 300,
                            "fadeOut": 1000,
                            "timeOut": 3000,
                            "extendedTimeOut": 3000,
                            "closeButton": true                
                        }             
                }
    
               toastr.error(failResponse.responseText);
    
             });  	 		
});


$('#user-client-selector').on('select2:select', function (eventSelectAccount) 
{
    var selectedClientId = $('#user-client-selector option:selected').attr('clientId'); 
    var clientId = parseInt(selectedClientId);   
    var currPageFromClientSelect = parseInt($('#current-page-active-button').attr('at'));
    $thisAccountsTable = $('#accounts-for-user-table-holder');

    var urlSelectAccPagination = "/AppUser/Account/GetFiveAccounts?currPage="+currPageFromClientSelect+"&preSelectedAccount="+clientId;

    $.get(urlSelectAccPagination)
        .done(function (data) 
        {
            $('#accounts-for-user-table-holder').empty();
            $('#accounts-for-user-table-holder').append(data);
         })         
         .fail(function (failResponse) 
        {     
            if(failResponse.getResponseHeader('customError') == "ClientNotAvailable")
            {    
                    toastr.options = 
                    {
                        "debug": false,
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 2000,
                        "extendedTimeOut": 2000,
                        "closeButton": true,
                        "onHidden" : function()
                        {
                            // this will be executed after fadeout, i.e. 2secs after notification has been show
                            location.reload();
                        }
                    }           
            }
            else
            {
                    toastr.options = 
                    {
                        "debug": false,
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true                
                    }             
            }

           toastr.error(failResponse.responseText);

         });  	
});