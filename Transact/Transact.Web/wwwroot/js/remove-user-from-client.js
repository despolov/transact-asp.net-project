$('#client-manage-holder').on('click', '.unassign-user-from-client-button', function(clickedRemoveBtnEvent)
{
    $this = $(this);
    clickedRemoveBtnEvent.preventDefault();
    if (window.confirm("Are you sure?"))
    {       
        var clientId = $this.attr('clientId');

        var userId = $this.attr('userId');
        var antiforgeryspan = $('#anti-forgery-for-user-unassign').find('input');
        var antifogerytoken = antiforgeryspan[0];    
        var url = "/Admin/Client/RemoveUserForClient"
        var params = "clientId=" + clientId + "&userId=" + userId + "&" + antifogerytoken.name + "=" + antifogerytoken.value;

        $.post(url, params).done(function(data)
        {
            var getUsersTableUrlAfterAdding = "/Admin/Client/GetFiveUsersForClientById?currPage=1&clientId=" + clientId;
            $.get(getUsersTableUrlAfterAdding)
            .done(function (data) {
                $('#clientUsers-list-table-holder').empty();

                $('#clientUsers-list-table-holder').append(data);
            })
            
        }).fail(function(data)
        {
        });
    }
});