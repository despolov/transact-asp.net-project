$(document).ready(function(){  

    $(function () {

                var startDate = new Date($('#startingDate-picker').val());
                var endDate = new Date($('#endingDate-picker').val());

                $('#startingDate-picker').datetimepicker({
                    locale: 'bg',
                    format: 'MM-DD-YYYY'
                });

                $('#endingDate-picker').datetimepicker({
                    locale: 'bg',
                    format: 'MM-DD-YYYY'
                });                     
    });

});