$(document).ready(function() 
{
    $('#sender-account-selector').select2();    
});


$('#sender-account-selector').on('select2:select', function (e) 
{
    var selectedClientName = $('#sender-account-selector option:selected').attr('att');   
    $('#sender-client-name-div').empty();
    $('#sender-client-name-div').append("Client Name: " + selectedClientName);
});

