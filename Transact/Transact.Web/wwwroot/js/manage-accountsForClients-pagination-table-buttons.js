var clientIdFromAccountManagePage = ($('#client-id-input-hidden'))[0].value;

$('#client-manage-holder').on('click', '.pagination-account-table-button', function (event) {

        var $this2 = $(this);	
		event.preventDefault();		
		
        var targetPage = parseInt($this2.attr('at'));

    var urlPagination = "/Admin/Client/GetFiveAccountsForClientById?currPage=" + targetPage + "&clientId=" + clientIdFromAccountManagePage;

		$.get(urlPagination)
            .done(function (data) {
                $('#target-account-list-table').empty();

                $('#target-account-list-table').append(data);
            }).fail(function (failResponse) {
               toastr.options = {
                   "debug": false,
                   "positionClass": "toast-top-center",
                   "onclick": null,
                   "fadeIn": 300,
                   "fadeOut": 1000,
                   "timeOut": 3000,
                   "extendedTimeOut": 3000,
                   "closeButton": true
               }
               toastr.error(failResponse.responseText);
			});  		
});