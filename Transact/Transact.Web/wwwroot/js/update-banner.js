 $('#update-banner-form').submit(function (eventUpdateBanner) 
 {
   eventUpdateBanner.preventDefault();
   var $thisBannerEvent = $(this);
   
    if($($thisBannerEvent).valid())
    {	 
       var urlToSend = "/Admin/Banner/UpdateBanner";
       var inputs = $thisBannerEvent.find('input');

       var startDate = inputs[1].value;
       var endDate = inputs[2].value;
       
       if(startDate < endDate)
       {           
            var urlOfbanner = inputs[0].value;    
            var file = inputs[3];
            var image = file.files[0];            
            var bannerId = parseInt(inputs[4].value);
            var bannerImageName = inputs[5].value;
            var IsImageEdited = inputs[6].value;
            var antiForgeryToken = inputs[7];

            var fdata = new FormData();
            fdata.append("Url", urlOfbanner);
            fdata.append("StartDate", startDate);
            fdata.append("EndDate", endDate);
            fdata.append("Image", image);
            fdata.append(antiForgeryToken.name, antiForgeryToken.value); 
            fdata.append("Id", bannerId);  
            fdata.append("ImageName", bannerImageName);  
            fdata.append("IsNewImage", IsImageEdited);  

            $.ajax({
                type: 'post',
                url: urlToSend ,
                data: fdata,
                processData: false,
                contentType: false
                }).done(function (responseBanner)
                {          
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true,
                        "onHidden": function(){                           
                            window.location.replace("/Admin/Banner/AddBanner");
                        }
                    }

                    toastr.success(responseBanner);                     

                }).fail(function (errorResponse) 
                {
                        toastr.options = {
                            "debug": false,
                            "positionClass": "toast-top-center",
                            "onclick": null,
                            "fadeIn": 300,
                            "fadeOut": 1000,
                            "timeOut": 3000,
                            "extendedTimeOut": 3000,
                            "closeButton": true
                        }
                        toastr.error(errorResponse.responseText);                                             
                });
                   
            } 
       }
       else
       {
            toastr.options = 
            {
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }

            toastr.error("Please, enter valid Banner Activity Period!");   

       }   
      
});