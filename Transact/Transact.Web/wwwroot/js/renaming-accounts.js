$('#accounts-listing-holder').on('click', '.rename-nickname-for-concrete-account-button', function()
{
    $this = $(this);
    var idEditButton = $this.attr('id');
  
    var idOfAcountNicknameSpan = "#account-listing-nickname-"+idEditButton;
    var spanToEdit = $(idOfAcountNicknameSpan);
    var oldNickName = spanToEdit.text();
    var saveButton = `<button id="save-account-nick-edit-${idEditButton}" class="d-inline btn btn-auto btn-sm btn-success col-sm-3 ml-1" type="submit">Edit</button>`
    var newItem = `<span id="span-to-replace-after-edit-${idEditButton}"><input id="input-to-edit-${idEditButton}" class="d-inline input-bordered col-sm-8" type="text" value="${oldNickName}"/>` + saveButton + "</span>";


    // var buttonIdToEdit = ``;

    $(idOfAcountNicknameSpan).replaceWith(newItem);

    $('#accounts-listing-holder').on('click', `#save-account-nick-edit-${idEditButton}`, function(eventEdit)
    {
        eventEdit.preventDefault();      

        var antiForgerySpan = $('#anti-forgery-span-for-acc-dash').find('input');
        var antiForgeryToken = antiForgerySpan[0];    
        var urlToEdit = "/AppUser/Account/EditAccountNickname";
       
        var newNickname = $(`#input-to-edit-${idEditButton}`)[0].value;
        var editAccountParams = `accountId=${idEditButton}&newNickname=${newNickname}&${antiForgeryToken.name}=${antiForgeryToken.value}`;

        $.post(urlToEdit, editAccountParams)
            .done(function(responseData)
            {
                var newNickSpanOnSuccess = `<span id="account-listing-nickname-${idEditButton}" class="lead user-name">${newNickname}</span>`;
                $(`#span-to-replace-after-edit-${idEditButton}`).replaceWith(newNickSpanOnSuccess);
                
                toastr.options = 
                {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true                
                }
                
                toastr.success(responseData);
           

            }).fail(function(responseData)
            {
                var oldNickSpanOnSuccess = `<span id="account-listing-nickname-${idEditButton}" class="lead user-name">${oldNickName}</span>`;
                $(`#span-to-replace-after-edit-${idEditButton}`).replaceWith(oldNickSpanOnSuccess);

                toastr.options = 
                {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true                
                }
                
                toastr.error(responseData);
            });
    });
});