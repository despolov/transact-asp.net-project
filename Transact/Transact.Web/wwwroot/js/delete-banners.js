$('#target-banners-list-table').on('click', '.banner-delete-button-class', function(deleteBannerEvent)
{
    deleteBannerEvent.preventDefault();
    $thisDelete = $(this);

    var confirmPay=confirm('Are you sure you to delete the banner?');

    if(confirmPay)
    {
   
    var bannerIdToDelte =  $thisDelete.attr('bannerId');
    var antiForgeryToken = ($('#anti-forgery-token-banners').find('input'))[0];
    var urlToDeleteBanner = "/Admin/Banner/DeleteBanner";
    var params = "id=" + bannerIdToDelte+  "&" + antiForgeryToken.name + "=" + antiForgeryToken.value;
    $.post(urlToDeleteBanner, params)
    .done(function(deletedBannerData)
    {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-center",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }

        toastr.success(deletedBannerData); 

        $.get("/Admin/Banner/GetFiveBanners")
            .done(function (bannerData)
             {
                $('#target-banners-list-table').empty();
                $('#target-banners-list-table').append(bannerData);
            })
            .fail(function (failResponseBanner) 
            {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(failResponseBanner.responseText);
            });                   

    }).fail(function(deletedBannerData)
    {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-center",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error(deletedBannerData.responseText);     

    });
}


});