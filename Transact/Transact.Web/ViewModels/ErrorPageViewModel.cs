﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transact.Web.ViewModels
{
    public class ErrorPageViewModel
    {
        public int StatusCode { get; set; }
    }
}
