﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Transact.Services.Contracts;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.Mapper.Contracts;
using Transact.Web.Utills;

namespace Transact.Web.Areas.AppUser.Controllers
{
    [Area("AppUser")]
    [CustomAuthorize("User")]
    public class AccountController : Controller
    {
        private readonly IAccountServices accountServices;
        private readonly IAccountViewModelMapper accountMapper;
        private readonly IHttpContextAccessor contextAccessor;
        private readonly IAuthorizationManager authorizationManager;
        private readonly IClientServices clientServices;

        public AccountController(
            IAccountServices accountServices,
            IClientServices clientServices,
            IAccountViewModelMapper accountMapper,
            IHttpContextAccessor contextAccessor,
            IAuthorizationManager authorizationManager)
        {
            this.accountServices = accountServices;
            this.clientServices = clientServices;
            this.accountMapper = accountMapper;
            this.contextAccessor = contextAccessor;
            this.authorizationManager = authorizationManager;
        }

        [HttpGet]
        public async Task<IActionResult> AccountDash()
        {
            var currentUserId = authorizationManager.GetLoggedUserId();
            var allUserAccounts = await accountServices.GetAllUserAvailableAccountsAsync(currentUserId);

            var allAccountsForUser = await accountServices.GetAllUserAvailableAccountsAsync(currentUserId);
      
            var mappedAccountsForUser = accountMapper.MapFrom(allAccountsForUser);

            var viewModel = new AccountDashViewModel()
            {
                AllUserAccounts = mappedAccountsForUser
            };
          
            return View(viewModel);
        }

        [HttpPost]
        public async Task<JsonResult> FindSingleByAccName(string prefix, long ignored)
        {
              if (string.IsNullOrEmpty(prefix))
              {
                  return Json(string.Empty);
              }
             
              var allAccountsContaining = await accountServices.FindAccountsContainingAsync(prefix, ignored);
                          
              return Json(allAccountsContaining);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAccountNickname(long? accountId, string newNickname)
        {
            if (accountId == null)
            {
                return BadRequest("Invalid selected account!");
            }
            else if (string.IsNullOrEmpty(newNickname))
            {
                return BadRequest("New Name cannot be empty!");
            }

            var currentUserId = authorizationManager.GetLoggedUserId();

            var editedAccountDto = await accountServices.RenameAccountNicknameAsync((long)accountId, newNickname, currentUserId);

            return Ok($"Account Nickname succesfully changed to: {editedAccountDto.NickName}!");
        }
    }
}