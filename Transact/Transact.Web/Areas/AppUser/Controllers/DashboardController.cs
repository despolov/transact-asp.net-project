﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Transact.Services.Contracts;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.Mapper.Contracts;

namespace Transact.Web.Areas.AppUser.Controllers
{
    [Area("AppUser")]
    [AllowAnonymous]
    public class DashboardController : Controller
    {
        private readonly IBannerServices bannerServices;
        private readonly IBannerViewModelMapper bannerViewModelMapper;

        public DashboardController(IBannerServices bannerServices, IBannerViewModelMapper bannerViewModelMapper)
        {
            this.bannerServices = bannerServices;
            this.bannerViewModelMapper = bannerViewModelMapper;
        }

        [HttpGet]
        public async Task<IActionResult> LoginPage()
        {
            var allActiveBanners = await bannerServices.GetActiveBannersImagePathsAsync(DateTime.Now);

            var allActiveBannersViewModeList = bannerViewModelMapper.MapFrom(allActiveBanners);

            var viewModel = new LoginViewModel()
            {
                ActiveBanners = allActiveBannersViewModeList
            };            

            return View("LoginMain", viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> About()
        {
            return View();
        }
    }
}