﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Transact.Services.Contracts;
using Transact.Web.Areas.AppUser.ViewModels;

namespace Transact.Web.Areas.AppUser.Controllers
{
    [Area("AppUser")]
    [AllowAnonymous]
    public class IdentityController : Controller
    {
        private readonly ITokenManager tokenManager;
        private readonly IUserServices userServices;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly ICookieManager cookieManager;

        public IdentityController(
            IUserServices userServices, 
            ITokenManager tokenManager,
            IHttpContextAccessor httpContextAccessor,
            ICookieManager cookieManager)
        {
            this.userServices = userServices;
            this.tokenManager = tokenManager;
            this.httpContextAccessor = httpContextAccessor;
            this.cookieManager = cookieManager;
        }

        [HttpPost]      
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Please enter valid login form!");
            }

            var user = await userServices.GetUserAsync(userModel.UserName, userModel.Password);

            var token = tokenManager.GenerateToken(user.UserName, user.RoleName, user.Id.ToString());

            cookieManager.AddSessionCookieForToken(token, user.UserName);


            return Ok("Successfully logged in!");                
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            cookieManager.DeleteSessionCookies();           

            return Ok("Succesfully logged out!");
        }
    }
}
