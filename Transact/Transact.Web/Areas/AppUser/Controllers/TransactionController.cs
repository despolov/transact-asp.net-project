﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.Mapper.Contracts;
using Transact.Web.Utills;

namespace Transact.Web.Areas.AppUser.Controllers
{
    [Area("AppUser")]
    [CustomAuthorize("User")]
    public class TransactionController : Controller
    {
        private readonly IUserServices userServices;
        private readonly IAccountServices accountServices;
        private readonly ITransactionServices transactionServices;
        private readonly IAuthorizationManager authorizationManager;
        private readonly IHttpContextAccessor contextAccessor;
        private readonly ITransactionVeiwModelMapper transactionMapper;
        private readonly IAccountViewModelMapper accountMapper;

        public TransactionController(
            IUserServices userServices,
            IAccountServices accountServices,
            IAccountViewModelMapper accountMapper,
            ITransactionServices transactionServices,
            IAuthorizationManager authorizationManager,
            IHttpContextAccessor contextAccessor,
            ITransactionVeiwModelMapper transactionMapper)
        {
            this.transactionMapper = transactionMapper;
            this.userServices = userServices;
            this.accountServices = accountServices;
            this.accountMapper = accountMapper;
            this.transactionServices = transactionServices;
            this.authorizationManager = authorizationManager;
            this.contextAccessor = contextAccessor;
        }

        [HttpGet]
        public async Task<IActionResult> AddTransaction(long? preSelectedAccount)
        {            
             var currentUserId = authorizationManager.GetLoggedUserId();
             var allUserAccounts = await accountServices.GetAllUserAvailableAccountsAsync(currentUserId);

             var allMappedUserAccounts = accountMapper.MapFrom(allUserAccounts);

             var viewModel = new TransactionViewModel()
             {
                 AllAvailableAccounts = allMappedUserAccounts,

             };

             if (preSelectedAccount != null)
             {
                 var mappedAccount = allMappedUserAccounts.FirstOrDefault(x => x.Id == preSelectedAccount);
                 viewModel.PreSelectedAccount = mappedAccount;
                 viewModel.SenderAccountId = mappedAccount.Id;
             }

             return View(viewModel);                                  
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditTransaction(long transactionId)
        {          
            var currentUserId = authorizationManager.GetLoggedUserId();
            var allUserAccounts = await accountServices.GetAllUserAvailableAccountsAsync(currentUserId);

            var allMappedUserAccounts = accountMapper.MapFrom(allUserAccounts);
           
            var transactionToEdit = await transactionServices.GetSingleTransactionForEditingAsync(transactionId);
           
            var viewModel = transactionMapper.MapFrom(transactionToEdit);
            viewModel.AllAvailableAccounts = new List<AccountViewModel>(allMappedUserAccounts);
           
            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> ListTransactions(long preSelectedAccount)
        {
            var currentUserId = authorizationManager.GetLoggedUserId();
            var allUserAccounts = await accountServices.GetAllUserAvailableAccountsAsync(currentUserId);

            List<TransactionDto> fiveSelectedTransactions;
            List<TransactionViewModel> mappedFiveTransactions;
            int totalPages;

            var allMappedUserAccounts = accountMapper.MapFrom(allUserAccounts);

            var viewModel = new ListTransactionsViewModel()
            {
                AllAvailableAccounts = new List<AccountViewModel>(allMappedUserAccounts)
            };

            totalPages = await transactionServices
                .GetPageCountForTransactionsAsync(5, preSelectedAccount, null);

            viewModel.PreSelectedAccount = allMappedUserAccounts
                 .SingleOrDefault(x => x.Id == preSelectedAccount);

            if (preSelectedAccount > 0)
            {
                fiveSelectedTransactions = new List<TransactionDto>(await transactionServices
                  .GetFiveTransactionsForAccountDescAsync(
                  1, preSelectedAccount, currentUserId));

                mappedFiveTransactions = new List<TransactionViewModel>(transactionMapper.MapFromWithAccount(fiveSelectedTransactions, preSelectedAccount));
            }
            else
            {
                mappedFiveTransactions = new List<TransactionViewModel>();
            }

            viewModel.FiveTransactionsList = new FiveTransactionListViewModel()
            {
                FiveSelectedTransactions = new List<TransactionViewModel>(mappedFiveTransactions),
                CurrPage = 1
            };

            if (totalPages > 1)
            {
                viewModel.FiveTransactionsList.NextPage = 2;
            }

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetFiveTransactions(int currPage, long preSelectedAccount)
        {
            var currentUserId = authorizationManager.GetLoggedUserId();

            if (currPage == 0)
            {
                currPage = 1;
            }          
           
            var currentNewPage = currPage;         
     
             List<TransactionDto> fiveSelectedTransactions;
             List<TransactionViewModel> mappedFiveTransactions;
             int totalPages;
            
             var allUserAccounts = await accountServices.GetAllUserAvailableAccountsAsync(currentUserId);
   
             totalPages = await transactionServices.GetPageCountForTransactionsAsync(
                 5,
                 preSelectedAccount,
                 null);
            
             if (totalPages == 1 || totalPages <= currentNewPage)
             {
                 currentNewPage = totalPages;
             }
            
            
             fiveSelectedTransactions = new List<TransactionDto>(await transactionServices
                 .GetFiveTransactionsForAccountDescAsync(
                 currentNewPage, 
                 preSelectedAccount,
                 currentUserId));
            
             mappedFiveTransactions = new List<TransactionViewModel>(transactionMapper.MapFromWithAccount(fiveSelectedTransactions, preSelectedAccount));
                    
            
             var viewModel = new FiveTransactionListViewModel()
             {
                 FiveSelectedTransactions = mappedFiveTransactions,
                 CurrPage = currentNewPage
             };
            
             if (totalPages > currentNewPage)
             {
                 viewModel.NextPage = currentNewPage + 1;                
             }
            
             if (currentNewPage > 1)
             {
                 viewModel.PrevPage = currentNewPage - 1;
             }               
            
             return PartialView("_FiveTransactionsPartial", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveTransaction(TransactionViewModel transactionToSave)
        {          
              if (!ModelState.IsValid)
              {
                  return BadRequest("Please submit valid form!");
              }
          
              var transactionDto = transactionMapper.MapFrom(transactionToSave);
          
              var result = await transactionServices.SaveTransactionAsync(transactionDto);
          
              return Ok($"Transaction #{result.Id} for: {result.Amount}bgn was SAVED succesfully!");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateTransaction(TransactionViewModel transactionToSave)
        {
              if (!ModelState.IsValid)
              {
                  return BadRequest("Please submit valid form!");
              }
             
              var transactionDto = transactionMapper.MapFrom(transactionToSave);
             
              var result = await transactionServices.UpdateTransactionAsync(transactionDto);
             
              return Ok($"Transaction #{result.Id} for: {result.Amount}bgn was UPDATED succesfully!");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendUpdatedTransaction(TransactionViewModel transactionToSave)
        {
             if (!ModelState.IsValid)
             {
                 return BadRequest("Please submit valid form!");
             }

             var transactionDto = transactionMapper.MapFrom(transactionToSave);

             var result = await transactionServices.SendUpdatedTransactionAsync(transactionDto);

             return Ok($"Transaction #{result.Id} for: {result.Amount}bgn was UPDATED and SENT succesfully!");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendTransaction(long transactionId)
        {
              if (!ModelState.IsValid)
              {
                  return BadRequest("Please submit valid form!");
              }

              var result = await transactionServices.SendTransactionAsync(transactionId);

              return Ok($"Transaction #{result.Id} for: {result.Amount}bgn was SENT succesfully!");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendJustCreatedTransaction(TransactionViewModel transactionToDirectlySend)
        {
             if (!ModelState.IsValid)
             {
                 return BadRequest("Please submit valid form!");
             }

             var transactionDto = transactionMapper.MapFrom(transactionToDirectlySend);

             var result = await transactionServices.AddTransactionAsync(transactionDto);

             return Ok($"Transaction #{result.Id} for: {result.Amount}bgn was SENT succesfully!");
        }
    }
}