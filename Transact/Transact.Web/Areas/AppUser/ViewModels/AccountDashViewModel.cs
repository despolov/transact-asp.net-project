﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transact.Web.Areas.AppUser.ViewModels
{
    public class AccountDashViewModel
    {
        public IList<AccountViewModel> AllUserAccounts { get; set; }
    }
}
