﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Transact.Web.Areas.AppUser.ViewModels
{
    public class ClientViewModel
    {
        public long Id { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(35)]
        public string Name { get; set; }

        public ICollection<AccountViewModel> Accounts { get; set; }
    }
}
