﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Transact.Web.Areas.Admin.ViewModels;

namespace Transact.Web.Areas.AppUser.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [MinLength(5)]
        [MaxLength(16)]
        public string UserName { get; set; }

        [Required]
        [MinLength(8)]
        [MaxLength(32)]
        public string Password { get; set; }

        public ICollection<BannerViewModel> ActiveBanners { get; set; }

    }
}
