﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transact.Web.ViewModels;

namespace Transact.Web.Areas.AppUser.ViewModels
{
    public class ListTransactionsViewModel
    {
        public AccountViewModel PreSelectedAccount { get; set; }

        public IList<AccountViewModel> AllAvailableAccounts { get; set; }

        public FiveTransactionListViewModel FiveTransactionsList { get; set; }
    }
}
