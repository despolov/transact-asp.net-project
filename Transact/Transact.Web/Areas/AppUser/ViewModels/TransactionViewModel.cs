﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Transact.Models;
using Transact.Web.Utils;
using Transact.Web.ViewModels;

namespace Transact.Web.Areas.AppUser.ViewModels
{
    public class TransactionViewModel
    {
        public long Id { get; set; }

        public long SenderAccountId { get; set; }

        public string SenderAccountNumber { get; set; }

        public string SenderAccountNickName { get; set; }

        public string SenderClientName { get; set; }

        public string ReceiverAccountNickName { get; set; }

        public string ReceiverClientName { get; set; }

        public long StatusId { get; set; }

        public IList<AccountViewModel> AllAvailableAccounts { get; set; }

        public long ReceiverAccountId { get; set; }

        [Required(ErrorMessage = "You must choose Receiver Account#")]
        [StringLength(10, MinimumLength = 10)]        
        public string ReceiverAccountNumber { get; set; }

        [MaxLength(35)]
        public string Description { get; set; }

        [Range(0, Double.PositiveInfinity)]
        public decimal Amount { get; set; }

        public DateTime TimeStamp { get; set; }

        public string StatusName { get; set; }

        public TransactionIOStatus IOstatus { get; set; }

        public AccountViewModel PreSelectedAccount { get; set; }
    }
}
