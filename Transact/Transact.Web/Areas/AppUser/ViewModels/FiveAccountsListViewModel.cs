﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transact.Web.Areas.AppUser.ViewModels
{
    public class FiveAccountsListViewModel
    {
        public int? PrevPage { get; set; }

        public int CurrPage { get; set; }

        public int? NextPage { get; set; }

        public IList<AccountViewModel> FiveSelectedAccounts { get; set; }
    }
}
