﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transact.Web.Areas.AppUser.ViewModels
{
    public class AccountDashViewModel3
    {
        public ClientViewModel PreSelectedClient { get; set; }

        public IList<ClientViewModel> AllAvailableClients { get; set; }

        public FiveAccountsListViewModel FiveAccountsList { get; set; }
    }
}
