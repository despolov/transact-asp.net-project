﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transact.Web.Areas.Admin.ViewModels
{
    public class ManageClientViewModel
    {
        public ClientViewModel ClientToManage { get; set; }
        public ManageClientAccountsViewModel ManageClientAccounts { get; set; }
    }
}
