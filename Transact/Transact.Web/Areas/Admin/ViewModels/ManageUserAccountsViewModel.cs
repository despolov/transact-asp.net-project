﻿using System.ComponentModel.DataAnnotations;

namespace Transact.Web.Areas.Admin.ViewModels
{
    public class ManageUserAccountsViewModel
    {
        public long UserId { get; set; }

        public long AccountId { get; set; }

        [Required(ErrorMessage = "You must choose Account")]
        public string AccountNumber { get; set; }

        public FiveAccountsViewModel FiveAccountsList { get; set; }
    }
}
