﻿using System.Collections.Generic;

namespace Transact.Web.Areas.Admin.ViewModels
{
    public class FiveUsersViewModel
    {
        public int? PrevPage { get; set; }

        public int CurrPage { get; set; }

        public int? NextPage { get; set; }

        public int TotalPages { get; set; }

        public long ClientId { get; set; }

        public IList<UserViewModel> FiveUsersList { get; set; }
    }
}
