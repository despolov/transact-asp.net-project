﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transact.Web.Areas.Admin.ViewModels
{
    public class BannerManagementViewModel
    {
        public BannerViewModel BannerForm { get; set; }

        public FiveBannersViewModel FiveBanners { get; set; }
    }
}
