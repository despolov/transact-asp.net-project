﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace Transact.Web.Areas.Admin.ViewModels
{
    public class EditBannerViewModel
    {
        public long Id { get; set; }

        [Url(ErrorMessage = "Please put valid Url!")]
        [Required(ErrorMessage = "Banner Url is required!")]
        public string Url { get; set; }

        [Required(ErrorMessage = "Starting date is required!")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "Ending date is required!")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
        public DateTime EndDate { get; set; }

        public IFormFile Image { get; set; }

        public string ImagePath { get; set; }

        public string ImageName { get; set; }

        public bool IsNewImage { get; set; }
    }
}
