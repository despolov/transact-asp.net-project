﻿using System.ComponentModel.DataAnnotations;

namespace Transact.Web.Areas.Admin.ViewModels
{
    public class ManageClientUsersViewModel
    {
        public long ClientId { get; set; }

        public long UserId { get; set; }

        [Required(ErrorMessage = "You must choose User")]
        public string UserName { get; set; }

        public FiveUsersViewModel FiveUsersList { get; set; }
    }
}
