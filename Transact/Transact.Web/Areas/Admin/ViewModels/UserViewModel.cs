﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Transact.Web.Areas.Admin.ViewModels
{
    public class UserViewModel
    {
        public long Id { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(35)]
        public string Name { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(16)]
        public string UserName { get; set; }

        [Required]
        [MinLength(8)]
        [MaxLength(32)]
        public string Password { get; set; }

        public long RoleId { get; set; }

        public string Role { get; set; }       
    }
}
