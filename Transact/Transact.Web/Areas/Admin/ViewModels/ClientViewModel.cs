﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Transact.Models;

namespace Transact.Web.Areas.Admin.ViewModels
{
    public class ClientViewModel
    {
        public long Id { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(35)]
        public string Name { get; set; }

        public int NumberOfAccounts { get; set; }

        public ICollection<Account> Accounts { get; set; }

        public ICollection<UsersClients> UsersClients { get; set; }
    }
}