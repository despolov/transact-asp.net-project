﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transact.Web.Areas.Admin.ViewModels
{
    public class AdminDashViewModel
    {
        public int TotalUsers { get; set; }

        public ICollection<UserViewModel> FiveLatestUsers { get; set; }

        public ICollection<ClientViewModel> SevenLatestClients { get; set; }
    }
}
