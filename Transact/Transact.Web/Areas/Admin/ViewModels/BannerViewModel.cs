﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Transact.Web.Areas.Admin.ViewModels
{
    public class BannerViewModel
    {
        public long Id { get; set; }

        [Url(ErrorMessage = "Please put valid Url!")]
        [Required(ErrorMessage = "Banner Url is required!")]
        public string Url { get; set; }

        [Required(ErrorMessage = "Starting date is required!")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
        public DateTime StartDate { get; set; }        

        [Required(ErrorMessage = "Ending date is required!")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
        public DateTime EndDate { get; set; }

        [Required(ErrorMessage = "Image for banner is required!")]
        public IFormFile Image { get; set; }

        public string ImagePath { get; set; }

        public string ImageName { get; set; }

        public bool IsNewImage { get; set; }
    }
}
