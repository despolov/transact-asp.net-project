﻿namespace Transact.Web.Areas.Admin.ViewModels
{
    public class AddUserViewModel
    {
        public UserViewModel UserForm { get; set; }

        public UserListViewModel UsersList { get; set; }
    }
}
