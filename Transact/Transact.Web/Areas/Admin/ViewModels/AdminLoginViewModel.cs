﻿using System.ComponentModel.DataAnnotations;

namespace Transact.Web.Areas.Admin.ViewModels
{
    public class AdminLoginViewModel
    {
        [Required]
        [MinLength(5)]
        [MaxLength(16)]
        public string UserName { get; set; }

        [Required]
        [MinLength(8)]
        [MaxLength(32)]
        public string Password { get; set; }
    }
}
