﻿namespace Transact.Web.Areas.Admin.ViewModels
{
    public class ManageClientAccountsViewModel
    {
        public long ClientId { get; set; }

        public decimal Balance { get; set; }

        public FiveAccountsViewModel FiveAccountsList { get; set; }

        public string DefaultAccountBalance { get; set; }
    }
}
