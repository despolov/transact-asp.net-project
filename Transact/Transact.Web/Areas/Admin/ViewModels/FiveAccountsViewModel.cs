﻿using System.Collections.Generic;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.ViewModels;

namespace Transact.Web.Areas.Admin.ViewModels
{
    public class FiveAccountsViewModel
    {
        public int? PrevPage { get; set; }

        public int CurrPage { get; set; }

        public int? NextPage { get; set; }

        public int TotalPages { get; set; }

        public long ClientId { get; set; }

        public long UserId { get; set; }

        public long AccountId { get; set; }

        public IList<AccountViewModel> FiveAccountsList { get; set; }
    }
}
