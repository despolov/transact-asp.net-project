﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transact.Web.Areas.Admin.ViewModels
{
    public class AddClientsViewModel
    {
        public ClientViewModel ClientForm { get; set; }

        public ClientListViewModel ClientsList { get; set; }
    }
}
