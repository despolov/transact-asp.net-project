﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transact.Web.Areas.Admin.ViewModels
{
    public class ManageUserViewModel
    {
        public UserViewModel UserToManage { get; set; }
        public ManageUserAccountsViewModel ManageUserAccounts { get; set; }
    }
}
