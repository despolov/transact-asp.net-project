﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Mapper.Contracts;
using Transact.Web.Utills;

namespace Transact.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [CustomAuthorize("Admin")]
    public class DashboardController : Controller
    {
        private readonly IUserServices userServices;
        private readonly IClientServices clientServices;
        private readonly IUserViewModelMapper userVieModelMapper;
        private readonly IClientViewModelMapper clientViewModelMapper;
        private readonly IMemoryCache memoryCache;

        public DashboardController(
            IUserServices userServices, 
            IClientServices clientServices,
            IUserViewModelMapper userVieModelMapper,
            IClientViewModelMapper clientViewModelMapper,
            IMemoryCache memoryCache)
        {
            this.userServices = userServices;
            this.clientServices = clientServices;
            this.userVieModelMapper = userVieModelMapper;
            this.clientViewModelMapper = clientViewModelMapper;
            this.memoryCache = memoryCache;
        }

        public async Task<IActionResult> Index()
        {
            var totalUsers = await userServices.GetTotalUsersCountAsync();           

            var cachedLatestUsers = await memoryCache.GetOrCreateAsync<IList<UserDto>>("LatestFiveUsers", async (cacheEntry) =>
            {
                var cachedUsers = await userServices.GetFiveUsersByIdAsync(1);
                cacheEntry.SlidingExpiration = TimeSpan.FromHours(1);
                return cachedUsers;
            });

            var cachedLatestClients = await memoryCache.GetOrCreateAsync<ICollection<ClientDto>>("LatestSevenClients", async (cacheEntry) =>
            {
                var cachedClients = await clientServices.GetSevenLatestClientsAsync();
                cacheEntry.SlidingExpiration = TimeSpan.FromHours(1);
                return cachedClients;
            });

            var fiveMappedUsers = userVieModelMapper.MapFrom(cachedLatestUsers);
            var sevenMappedClients = clientViewModelMapper.MapFrom(cachedLatestClients);

            var viewModel = new AdminDashViewModel()
            {
                TotalUsers = totalUsers,
                SevenLatestClients = sevenMappedClients,
                FiveLatestUsers = fiveMappedUsers
            };

            return View(viewModel);
        }

        public async Task<IActionResult> About()
        {            
            return View();
        }
    }
}