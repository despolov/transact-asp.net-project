﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Transact.Services.Contracts;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Mapper.Contracts;
using Transact.Web.Utills;

namespace Transact.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [CustomAuthorize("Admin")]
    public class ClientController : Controller
    {
        private readonly IClientServices clientServices;
        private readonly IAccountServices accountServices;
        private readonly IAccountViewModelMapper accountMapper;
        private readonly IClientViewModelMapper clientMapper;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IUserServices userServices;
        private readonly IUserViewModelMapper userMapper;
        private readonly IConfiguration configuration;

        public ClientController(
            IClientServices clientServices, 
            IAccountServices accountServices, 
            IAccountViewModelMapper accountMapper,
            IClientViewModelMapper clientMapper, 
            IHttpContextAccessor httpContextAccessor, 
            IUserServices userServices, 
            IUserViewModelMapper userMapper,
            IConfiguration configuration)
        {
            this.clientServices = clientServices;
            this.accountServices = accountServices;
            this.accountMapper = accountMapper;
            this.clientMapper = clientMapper;
            this.httpContextAccessor = httpContextAccessor;
            this.userServices = userServices;
            this.userMapper = userMapper;
            this.configuration = configuration;
        }

        [HttpGet]
        public async Task<IActionResult> AddClient()
        {
                var fiveClientsById = await clientServices.GetFiveClientsByIdAsync(1);
                var mappedFiveClientsById = clientMapper.MapFrom(fiveClientsById);
                var totalPages = await clientServices.GetPageCountForClientsAsync(5);

                var viewModel = new AddClientsViewModel()
                {
                    ClientForm = new ClientViewModel(),
                    ClientsList = new ClientListViewModel()
                    {
                        CurrPage = 1,
                        TotalPages = totalPages,
                        FiveClientsById = mappedFiveClientsById
                    }
                };

                if (totalPages > 1)
                {
                    viewModel.ClientsList.NextPage = 2;
                }

                return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> SaveClient(AddClientsViewModel manageViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Please submit valid form!");
            }

            var clientDto = clientMapper.MapFrom(manageViewModel.ClientForm);

            var client = await clientServices.AddClientAsync(clientDto);

            return Ok($"Client with name: \"{client.Name}\" addedd succesfully!");
            
        }

        [HttpGet]
        public async Task<IActionResult> GetFiveClientsById(int? currPage)
        {
           if (!ModelState.IsValid)
           {
               return BadRequest();
           }

           var currentPage = currPage ?? 1;

           var totalPages = await clientServices.GetPageCountForClientsAsync(5);
           var fiveClientsById = await clientServices.GetFiveClientsByIdAsync(currentPage);
           var mappedFiveClientsById = clientMapper.MapFrom(fiveClientsById);


           var viewModel = new ClientListViewModel()
           {
               CurrPage = currentPage,
               TotalPages = totalPages,
               FiveClientsById = mappedFiveClientsById
           };

           if (totalPages > currentPage)
           {
               viewModel.NextPage = currentPage + 1;
           }

           if (currentPage > 1)
           {
               viewModel.PrevPage = currentPage - 1;
           }

           return PartialView("_ClientListTable", viewModel);
          
        }

        [HttpGet]
        public async Task<IActionResult> ManageAccountsForClient(long clientId)
        {
            var currentPage = 1;

            var fiveAccountsDtos = await accountServices.GetFiveAccountsForClientByIdAsync(currentPage, clientId);

            var totalPages = await accountServices.GetPageCountForAccountsAsync(5, clientId);

            var fiveMappedAccounts = accountMapper.MapFrom(fiveAccountsDtos);

            var defaultAccountBalance = configuration.GetSection("DefaultAccountAmount").Value;

            var fiveAccounts = new FiveAccountsViewModel()
            {
                CurrPage = currentPage,
                TotalPages = totalPages,
                FiveAccountsList = fiveMappedAccounts,
                ClientId = clientId
            };

            var viewModel = new ManageClientAccountsViewModel()
            {
                ClientId = clientId,
                FiveAccountsList = fiveAccounts,
                DefaultAccountBalance = defaultAccountBalance
            };

            if (totalPages > currentPage)
            {
                fiveAccounts.NextPage = currentPage + 1;
            }

            if (currentPage > 1)
            {
                fiveAccounts.PrevPage = currentPage - 1;
            }

            return PartialView("_AccountsManagePartial", viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetFiveAccountsForClientById(int? currPage, long clientId)
        {
           if (!ModelState.IsValid)
           {
               return BadRequest();
           }

           var currentPage = currPage ?? 1;

           var totalPages = await accountServices.GetPageCountForAccountsAsync(5, clientId);
           var fiveAccForClientsById = await accountServices.GetFiveAccountsForClientByIdAsync(currentPage, clientId);
           var mappedFiveAccForClientsById = accountMapper.MapFrom(fiveAccForClientsById);


           var viewModel = new FiveAccountsViewModel()
           {
               CurrPage = currentPage,
               TotalPages = totalPages,
               FiveAccountsList = mappedFiveAccForClientsById,
               ClientId = clientId
           };

           if (totalPages > currentPage)
           {
               viewModel.NextPage = currentPage + 1;
           }

           if (currentPage > 1)
           {
               viewModel.PrevPage = currentPage - 1;
           }

           return PartialView("_AccountsListTable", viewModel);            
        }

        [HttpGet]
        public async Task<IActionResult> ManageClient(long clientId)
        {
           var currentPage = 1;

           var fiveAccountsDtos = await accountServices.GetFiveAccountsForClientByIdAsync(1, clientId);

           var fiveMappedAccounts = accountMapper.MapFrom(fiveAccountsDtos);

           var clientDto = await clientServices.GetClientByIdAsync(clientId);

           var clientViewModel = clientMapper.MapFrom(clientDto);

           var totalPages = await accountServices.GetPageCountForAccountsAsync(5, clientId);

            var defaultAccountBalance = configuration.GetSection("DefaultAccountAmount").Value;

            var fiveAccounts = new FiveAccountsViewModel()
           {
               CurrPage = currentPage,
               TotalPages = totalPages,
               FiveAccountsList = fiveMappedAccounts,
               ClientId = clientViewModel.Id
           };

           if (totalPages > currentPage)
           {
               fiveAccounts.NextPage = currentPage + 1;
           }

           if (totalPages > 1)
           {
               fiveAccounts.NextPage = currentPage + 1;
           }

           if (currentPage > 1)
           {
               fiveAccounts.PrevPage = currentPage - 1;
           }

           var manageClientViewModel = new ManageClientAccountsViewModel()
           {
               ClientId = clientViewModel.Id,
               FiveAccountsList = fiveAccounts,
               DefaultAccountBalance = defaultAccountBalance
           };

           var viewModel = new ManageClientViewModel()
           {
               ClientToManage = clientViewModel,
               ManageClientAccounts = manageClientViewModel
           };

           return View(viewModel);          
        }

        [HttpGet]
        public async Task<IActionResult> ManageUsersForClient(long clientId)
        {
            var currentPage = 1;

            var fiveUsersDtos = await userServices.GetFiveUsersForClientByIdAsync(currentPage, clientId);

            var totalPages = await userServices.GetPageCountForUsersWithClientIdAsync(5, clientId);

            var fiveMappedUsers = userMapper.MapFrom(fiveUsersDtos);

            var fiveUsers = new FiveUsersViewModel()
            {
                CurrPage = currentPage,
                TotalPages = totalPages,
                FiveUsersList = fiveMappedUsers,
                ClientId = clientId
            };

            var viewModel = new ManageClientUsersViewModel()
            {
                ClientId = clientId,

                FiveUsersList = fiveUsers
            };

            if (totalPages > 1)
            {
                fiveUsers.NextPage = currentPage + 1;
            }

            if (currentPage > 1)
            {
                fiveUsers.PrevPage = currentPage - 1;
            }

            return PartialView("_UsersManagePartial", viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetFiveUsersForClientById(int? currPage, long clientId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var currentPage = currPage ?? 1;

            var totalPages = await userServices.GetPageCountForUsersWithClientIdAsync(5, clientId);
            var fiveUsersForClientsById = await userServices.GetFiveUsersForClientByIdAsync(currentPage, clientId);
            var mappedFiveUsersForClientsById = userMapper.MapFrom(fiveUsersForClientsById);


            var viewModel = new FiveUsersViewModel()
            {
                CurrPage = currentPage,
                TotalPages = totalPages,
                FiveUsersList = mappedFiveUsersForClientsById,
                ClientId = clientId
            };

            if (totalPages > currentPage)
            {
                viewModel.NextPage = currentPage + 1;
            }

            if (currentPage > 1)
            {
                viewModel.PrevPage = currentPage - 1;
            }

            return PartialView("_ClientUsersListTable", viewModel);        
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AssignUserToClient(string userName, long clientId)
        {
            var user = await clientServices.AssignUserToClientAsync(userName, clientId);

            return Ok($"User with name: \"{user.Name}\" assigned succesfully!");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveUserForClient(long clientId, long userId)
        {
            var user = await userServices.RemoveUserForClient(clientId, userId);

            return Ok($"User \"{user.Name}\" removed from client!");
        }
    }
}