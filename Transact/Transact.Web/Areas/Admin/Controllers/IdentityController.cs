﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Transact.Services.Contracts;
using Transact.Web.Areas.Admin.ViewModels;

namespace Transact.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [AllowAnonymous]
    public class IdentityController : Controller
    {
        private readonly ITokenManager tokenManager;
        private readonly IAdminServices adminServices;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly ICookieManager cookieManager;

        public IdentityController(
            IAdminServices adminServices,
            ITokenManager tokenManager,
            IHttpContextAccessor httpContextAccessor,
            ICookieManager cookieManager)
        {
            this.adminServices = adminServices;
            this.tokenManager = tokenManager;
            this.httpContextAccessor = httpContextAccessor;
            this.cookieManager = cookieManager;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(AdminLoginViewModel adminModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Please enter valid login form!");
            }

            var admin = await adminServices.GetAdminAsync(adminModel.UserName, adminModel.Password);

            var token = tokenManager.GenerateToken(admin.UserName, admin.RoleName, admin.Id.ToString());

            cookieManager.AddSessionCookieForToken(token, admin.UserName);

            return Ok("Successfully logged in!");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            cookieManager.DeleteSessionCookies();

            return Ok("Succesfully logged out!");
        }
    }
}