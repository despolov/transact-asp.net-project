﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Transact.FileManager.Contracts;
using Transact.Services.Contracts;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Mapper.Contracts;
using Transact.Web.Utills;

namespace Transact.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [CustomAuthorize("Admin")]
    public class BannerController : Controller
    {
        private readonly IBannerServices bannerServices;
        private readonly IBannerViewModelMapper bannerViewModelMapper;
        private readonly ILocalStorageManager localStorageManager;

        public BannerController
            (IBannerServices bannerServices,
             IBannerViewModelMapper bannerViewModelMapper,
             ILocalStorageManager localStorageManager)
        {
            this.bannerServices = bannerServices;
            this.bannerViewModelMapper = bannerViewModelMapper;
            this.localStorageManager = localStorageManager;
        }

        public IActionResult ManageBanners()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> AddBanner()
        {
            var fiveBannerDtos = await bannerServices.GetFiveBannersByExpirationDateAsync();

            var totalPages = await bannerServices.GetPageCountForBanersAsync(5);

            var fiveMappedBanners = bannerViewModelMapper.MapFrom(fiveBannerDtos);

            var viewModel = new BannerManagementViewModel()
            {
                FiveBanners = new FiveBannersViewModel()
                {
                    FiveBannersList = fiveMappedBanners,
                    CurrPage = 1,
                    TotalPages = totalPages                    
                },
                BannerForm = new BannerViewModel()
            };

            if (totalPages > 1)
            {
                viewModel.FiveBanners.NextPage = viewModel.FiveBanners.CurrPage + 1;
            }

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetFiveBanners(int? currPage)
        {
            currPage = currPage ?? 1;

            var fiveBannerDtos = await bannerServices.GetFiveBannersByExpirationDateAsync((int)currPage);

            var totalPages = await bannerServices.GetPageCountForBanersAsync(5);

            var fiveMappedBanners = bannerViewModelMapper.MapFrom(fiveBannerDtos);

            var viewModel = new FiveBannersViewModel()
            {
                FiveBannersList = fiveMappedBanners,
                CurrPage = (int)currPage,
                TotalPages = totalPages
            };

            if (totalPages > currPage)
            {
                viewModel.NextPage = currPage + 1;
            }

            if (currPage > 1)
            {
                viewModel.PrevPage = currPage - 1;
            }

            return PartialView("_BannerListTable", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveBanner(BannerViewModel bannerToSave)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Please enter valid form!");
            }

            var bannerDtoToSave = bannerViewModelMapper.MapFrom(bannerToSave);

            var imageName = await localStorageManager.SaveFileAsync(bannerToSave.Image);

            bannerDtoToSave.ImageName = imageName;

            var savedBannerDto = await bannerServices.SaveBannerAsync(bannerDtoToSave);

            return Ok("Banner succesfuly added!");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateBanner(EditBannerViewModel bannerToUpdate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Please enter valid form!");
            }

            var bannerDtoToEdit = bannerViewModelMapper.MapFromForEditing(bannerToUpdate);

            if (bannerToUpdate.IsNewImage)
            {
                var newImageName = await localStorageManager.SaveFileAsync(bannerToUpdate.Image);

                bannerDtoToEdit.ImageName = newImageName;

                var oldImageName = bannerToUpdate.ImageName;

                localStorageManager.DeleteFile(oldImageName);
            }                     

            var editedBannerDto = await bannerServices.UpdateBannerAsync(bannerDtoToEdit);

            return Ok("Banner succesfuly edited!");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBanner(long id)
        {
            var bannerToEdit = await bannerServices.GetSingleBannerForEditingAsync(id);

            var viewModel = bannerViewModelMapper.MapFromForEditing(bannerToEdit);

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteBanner(long id)
        {
            var bannerToDelete = await bannerServices.DeleteBannerAsync(id);

            return Ok("Banner succesfuly deleted!");
        }      
    }
}