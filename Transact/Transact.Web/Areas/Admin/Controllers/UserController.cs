﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Transact.Services.Contracts;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Mapper.Contracts;
using Transact.Web.Utills;

namespace Transact.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [CustomAuthorize("Admin")]
    public class UserController : Controller
    {
        private readonly IUserServices userServices;
        private readonly IUserViewModelMapper userMapper;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IAccountServices accountServices;
        private readonly IAccountViewModelMapper accountMapper;

        public UserController(
            IUserServices userServices,
            IUserViewModelMapper userMapper,
            IHttpContextAccessor httpContextAccessor,
            IAccountServices accountServices,
            IAccountViewModelMapper accountMapper)
        {
            this.userServices = userServices;
            this.userMapper = userMapper;
            this.httpContextAccessor = httpContextAccessor;
            this.accountServices = accountServices;
            this.accountMapper = accountMapper;
        }


        [HttpGet]
        public async Task<IActionResult> AddUser()
        {
            var fiveUsersById = await userServices.GetFiveUsersByIdAsync(1);
            var mappedFiveUsersById = userMapper.MapFrom(fiveUsersById);
            var totalPages = await userServices.GetPageCountForUsersAsync(5);

            var viewModel = new AddUserViewModel()
            {
                UserForm = new UserViewModel(),
                UsersList = new UserListViewModel()
                {
                    CurrPage = 1,
                    TotalPages = totalPages,
                    FiveUsersById = mappedFiveUsersById
                }
            };

            if (totalPages > 1)
            {
                viewModel.UsersList.NextPage = 2;
            }

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetFiveUsersById(int? currPage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var currentPage = currPage ?? 1;

            var totalPages = await userServices.GetPageCountForUsersAsync(5);
            var fiveUsersById = await userServices.GetFiveUsersByIdAsync(currentPage);
            var mappedFiveUsersById = userMapper.MapFrom(fiveUsersById);


            var viewModel = new UserListViewModel()
            {
                CurrPage = currentPage,
                TotalPages = totalPages,
                FiveUsersById = mappedFiveUsersById
            };

            if (totalPages > currentPage)
            {
                viewModel.NextPage = currentPage + 1;
            }

            if (currentPage > 1)
            {
                viewModel.PrevPage = currentPage - 1;
            }

            return PartialView("_UserListTable", viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> SaveUser(AddUserViewModel manageViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Please submit valid form!");
            }

            var userDto = userMapper.MapFrom(manageViewModel.UserForm);

            var user = await userServices.
                AddUserAsync(userDto);

            return Ok($"User with Username: \"{user.UserName}\" addedd succesfully!");
        }

        [HttpPost]
        public async Task<JsonResult> FindSingleByUserName(string userNameString, long clientId)
        {
            if (string.IsNullOrEmpty(userNameString))
            {
                return Json(string.Empty);
            }

            var allUsersContaining = await userServices.FindUsersContainingAsync(userNameString, clientId);

            var allUsersModels = userMapper.MapFrom(allUsersContaining);

            return Json(allUsersModels);
        }

        [HttpGet]
        public async Task<IActionResult> ManageUser(long userId)
        {
            var currentPage = 1;

            var fiveAccountsDtos = await accountServices.GetFiveAccountsForUserByIdAsync(currentPage, userId);

            var fiveMappedAccounts = accountMapper.MapFrom(fiveAccountsDtos);

            var userDto = await userServices.GetUserByIdAsync(userId);
            var userViewModel = userMapper.MapFrom(userDto);

            var totalPages = await accountServices.GetPageCountForAccountsWithUsersIdAsync(5, userId);


            var fiveAccounts = new FiveAccountsViewModel()
            {
                CurrPage = currentPage,
                TotalPages = totalPages,
                FiveAccountsList = fiveMappedAccounts,
                UserId = userViewModel.Id
            };

            if (totalPages > currentPage)
            {
                fiveAccounts.NextPage = currentPage + 1;
            }

            if (totalPages > 1)
            {
                fiveAccounts.NextPage = currentPage + 1;
            }

            if (currentPage > 1)
            {
                fiveAccounts.PrevPage = currentPage - 1;
            }

            var manageUserViewModel = new ManageUserAccountsViewModel()
            {
                UserId = userViewModel.Id,
                FiveAccountsList = fiveAccounts
            };

            var viewModel = new ManageUserViewModel()
            {
                UserToManage = userViewModel,
                ManageUserAccounts = manageUserViewModel
            };

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetFiveAccountsForUserById(int? currPage, long userId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var currentPage = currPage ?? 1;

            var totalPages = await accountServices.GetPageCountForAccountsWithUsersIdAsync(5, userId);
            var fiveAccountsForUsersById = await accountServices.GetFiveAccountsForUserByIdAsync(currentPage, userId);
            var mappedFiveAccountsForUsersById = accountMapper.MapFrom(fiveAccountsForUsersById);


            var viewModel = new FiveAccountsViewModel()
            {
                CurrPage = currentPage,
                TotalPages = totalPages,
                FiveAccountsList = mappedFiveAccountsForUsersById,
                UserId = userId
            };

            if (totalPages > currentPage)
            {
                viewModel.NextPage = currentPage + 1;
            }

            if (currentPage > 1)
            {
                viewModel.PrevPage = currentPage - 1;
            }

            return PartialView("_UserAccountsListTable", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AssignAccountToUser(string AccountNumber, long userId)
        {
            var account = await accountServices.AssignAccountToUserAsync(AccountNumber, userId);

            return Ok($"User with name: \"{account.AccountNumber}\" assigned succesfully!");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveUserForClient(long userId, long accountId)
        {
            var account = await accountServices.RemoveAccountForUser(userId, accountId);

            return Ok($"Account \"{account}\" removed from user!");
        }

        [HttpPost]
        public async Task<JsonResult> FindSingleByAccountNumberForUser(string accountNumberString, long userId)
        {
            if (string.IsNullOrEmpty(accountNumberString))
            {
                return Json(string.Empty);
            }

            var allAccountsContaining = await accountServices.FindAccountsForUserContainingAsync(accountNumberString, userId);

            var allAccontsModels = accountMapper.MapFrom(allAccountsContaining);

            return Json(allAccontsModels);
        }
    }
}