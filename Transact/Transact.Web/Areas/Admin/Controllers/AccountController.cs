﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Transact.Services.Contracts;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.Mapper.Contracts;
using Transact.Web.Utills;

namespace Transact.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [CustomAuthorize("Admin")]
    public class AccountController : Controller
    {
        private readonly IAccountServices accountServices;
        private readonly IAccountViewModelMapper accountMapper;
        private readonly IHttpContextAccessor httpContextAccessor;

        public AccountController(IAccountServices accountServices, IAccountViewModelMapper accountMapper, IHttpContextAccessor httpContextAccessor)
        {
            this.accountServices = accountServices;
            this.accountMapper = accountMapper;
            this.httpContextAccessor = httpContextAccessor;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAccount(AccountViewModel accountToBeAdded)
        {
           if (!ModelState.IsValid)
           {
               return BadRequest("Please submit valid form!");
           }

           var accountDto = accountMapper.MapFrom(accountToBeAdded);

           var result = await accountServices.AddAccountToClientAsync(accountDto);

           return Ok($"Account #{result.AccountNumber} was added succesfully!");           
        }
    }
}