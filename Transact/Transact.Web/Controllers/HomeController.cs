﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Transact.Services.Contracts;
using Transact.Web.Models;
using Transact.Web.Utills;

namespace Transact.Web.Controllers
{
    [ResponseCache(Location = ResponseCacheLocation.Client, Duration = 86400)]
    public class HomeController : Controller
    {
        public async Task<IActionResult> Index()
        {
            return View("LandingPage");            
        }

        public async Task<IActionResult> About()
        {
            return View();
        }
    }
}
