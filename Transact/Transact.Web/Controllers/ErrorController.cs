﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Transact.Web.ViewModels;

namespace Transact.Web.Controllers
{
    public class ErrorController : Controller
    {
        public async Task<IActionResult> GeneralError(int? code)
        {
            var viewModel = new ErrorPageViewModel();

            if (code.HasValue && code > 400 && code < 600)
            {
                viewModel.StatusCode = code ?? (int)code;

                return View("GeneralErrorPage", viewModel);
            }            

            return View("GeneralErrorPage", viewModel);
        }
    }
}