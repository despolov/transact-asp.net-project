﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Transact.Data;
using Transact.FileManager;
using Transact.FileManager.Contracts;
using Transact.Services;
using Transact.Services.Contracts;
using Transact.Services.Mapper;
using Transact.Services.Mapper.Contracts;
using Transact.Web.Areas.Admin.Mapper;
using Transact.Web.Mapper;
using Transact.Web.Mapper.Contracts;
using Transact.Web.Utils.Middlewares;
using ICookieManager = Transact.Services.Contracts.ICookieManager;

namespace Transact.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {           
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie();

            services.AddDbContext<TransactContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("SmarterASPNetConnection")));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);        

            services.AddScoped<ITransactionServices, TransactionServices>();
            services.AddScoped<IUserServices, UserServices>();
            services.AddScoped<IClientServices, ClientServices>();
            services.AddScoped<IAccountServices, AccountServices>();
            services.AddScoped<IAdminServices, AdminServices>();
            services.AddScoped<IBannerServices, BannerServices>();
            services.AddScoped<IRandomizer, Randomizer>();

            services.AddScoped<IAuthorizationManager, AuthorizationManager>();
            services.AddTransient<ICookieManager, CookieManager>();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<ITokenManager, TokenManager>();
            services.AddSingleton<IDateTimeProvider, DateTimeProvider>();

            services.AddScoped<ITransactionVeiwModelMapper, TransactionViewModelMapper>();
            services.AddScoped<ITransactionDtoMapper, TransactionDtoMapper>();

            services.AddScoped<IUserViewModelMapper, UserViewModelMapper>();
            services.AddScoped<IUserDtoMapper, UserDtoMapper>();

            services.AddScoped<IAccountViewModelMapper, AccountViewModelMapper>();
            services.AddScoped<IAccountDtoMapper, AccountDtoMapper>();

            services.AddScoped<IClientViewModelMapper, ClientViewModelMapper>();
            services.AddScoped<IClientDtoMapper, ClientDtoMapper>();

            services.AddScoped<IBannerViewModelMapper, BannerViewModelMapper>();
            services.AddScoped<IBannerDtoMapper, BannerDtoMapper>();

            services.AddScoped<IFileWrapper, FileWrapper>();

            services.AddScoped<IAdminViewModelMapper, AdminViewModelMapper>();
            services.AddScoped<IAdminDtoMapper, AdminDtoMapper>();

            services.AddScoped<ILocalStorageManager, LocalStorageManager>();

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {          
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                //app.UseStatusCodePagesWithRedirects("/Error/GeneralError?code={0}");
                //app.UseGeneralExceptionHandlerMiddleware();            
            }
            else
            {
                app.UseStatusCodePagesWithRedirects("/Error/GeneralError?code={0}");
                app.UseGeneralExceptionHandlerMiddleware();

                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }           

            app.UseHttpsRedirection();
            app.UseStaticFiles();            

            app.UseAuthentication();

            //app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areas",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
