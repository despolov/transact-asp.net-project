﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transact.Web.Utils
{
    public enum TransactionIOStatus
    {
        Incoming = 0,
        Outgoing = 1,
        IncomingAndOutgoing = 2
    }
}
