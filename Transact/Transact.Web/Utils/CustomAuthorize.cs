﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Transact.Services.Contracts;

namespace Transact.Web.Utills
{
    public class CustomAuthorize : ActionFilterAttribute
    {
        private readonly string roleToCheckAgainst;

        public CustomAuthorize() : base()
        {
        }

        public CustomAuthorize(string roleToCheckAgainst) : base()
        {
            this.roleToCheckAgainst = roleToCheckAgainst;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var token = context.HttpContext.Request.Cookies["SecurityToken"];

            var manager = CheckForAuthentication(context);

            if (string.IsNullOrEmpty(roleToCheckAgainst))
            {
                manager.CheckIfLogged(token);
            }
            else
            {
                manager.CheckForRole(token, roleToCheckAgainst);
            }
        }

        protected IAuthorizationManager CheckForAuthentication(ActionExecutingContext context)
        {
            return context.HttpContext.RequestServices.GetService<IAuthorizationManager>();    
        }
    }
}
