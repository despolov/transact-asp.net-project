﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Transact.Services;
using Transact.Services.CustomExceptions;

namespace Transact.Web.Utils.Middlewares
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project

    public class GeneralErrorHandlerMiddleware
    {
        private readonly RequestDelegate next;

        public GeneralErrorHandlerMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {             
                await this.next.Invoke(context);
            }
            catch (BusinessLogicException ex)
            {
                if (context.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.StatusCode = 500;
                    await context.Response.WriteAsync(ex.Message);                    
                }
                           
            }
            catch (AccountNotAvailableException ex)
            {
                if (context.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.StatusCode = 500;
                    context.Response.Headers.Add("customError", "AccountNotAvailable");
                    await context.Response.WriteAsync(ex.Message);
                }                     
            }
            catch (Exception ex)
            {
                if (context.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.StatusCode = 500;
                    await context.Response.WriteAsync(ExceptionMessages.GeneralOopsMessage);
                }
                else
                {                    
                   context.Response.Redirect("error/generalerror");
                }
            }
        }      
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class GeneralExceptionHandlerMiddlewareExtensions
    {
        public static IApplicationBuilder UseGeneralExceptionHandlerMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<GeneralErrorHandlerMiddleware>();
        }
    }
}
