﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Transact.FileManager.Contracts
{
    public interface IFileWrapper
    {
        FileInfo GetNewFileInfo(string fileName);

        Stream GetNewFileStream(string inputFileName, FileMode fileMode);
    }
}
