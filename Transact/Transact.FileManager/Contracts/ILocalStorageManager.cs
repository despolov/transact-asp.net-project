﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Transact.FileManager.Contracts
{
    public interface ILocalStorageManager
    {
        void DeleteFile(string imageName);

        Task<string> SaveFileAsync(IFormFile imageToSave);
    }
}
