﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Transact.FileManager.Contracts;

namespace Transact.FileManager
{
    public class FileWrapper : IFileWrapper
    {
        public FileInfo GetNewFileInfo(string fileName)
        {
            return new FileInfo(fileName);
        }

        public Stream GetNewFileStream(string inputFileName, FileMode fileMode)
        {
            return new FileStream(inputFileName, fileMode);
        }
    }
}
