﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Transact.FileManager.Contracts;

namespace Transact.FileManager
{
    public class LocalStorageManager : ILocalStorageManager
    {
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly IConfiguration configuration;
        private readonly IFileWrapper fileWrapper;

        public LocalStorageManager(IHostingEnvironment hostingEnvironment, IConfiguration configuration, IFileWrapper fileWrapper)
        {
            this.hostingEnvironment = hostingEnvironment;
            this.configuration = configuration;
            this.fileWrapper = fileWrapper;
        }

        public void DeleteFile(string imageName)
        {
            var imageFolder = configuration.GetSection("DefaultAzureFolder").Value;

            var fullPathOfFileToDelete = Path.Combine(hostingEnvironment.WebRootPath, imageFolder) + imageName;

            var fileToDelete = fileWrapper.GetNewFileInfo(fullPathOfFileToDelete);

            fileToDelete.Delete();
        }

        public async Task<string> SaveFileAsync(IFormFile imageToSave)
        {
            var imageName = Guid.NewGuid() + ".jpg";

            var imageFolder = configuration.GetSection("DefaultAzureFolder").Value;

            var inputFileName = Path.Combine(hostingEnvironment.WebRootPath, imageFolder) + imageName;

            await imageToSave.CopyToAsync(fileWrapper.GetNewFileStream(inputFileName, FileMode.Create));

            return imageName;
        }
    }
}
