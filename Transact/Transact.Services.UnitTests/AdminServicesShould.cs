﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;
using Transact.Data;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.CustomExceptions;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services.UnitTests
{
    [TestClass]
    public class AdminServicesShould
    {
        [TestMethod]
        public async Task GetAdminAsync_Should_ReturnCorrectAdmin()
        {
            var options = TestUtils.GetOptions(nameof(GetAdminAsync_Should_ReturnCorrectAdmin));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAdminHash = new Admin()
                {
                    Id = 1,
                    UserName = "ExampleUserName1",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "Admin" },
                    RoleId = 2
                };

                await arrangeContext.UserRoles.AddAsync(new UserRole() { RoleName = "Admin" });
                var adminToAdd = await arrangeContext.Admins.AddAsync(exampleAdminHash);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAdminDto = new AdminDto()
                {
                    Id = 1,
                    RoleId = 2,
                    UserName = "ExampleUserName1",
                    RoleName = "Example"
                };

                var exampleAdmin = new Admin()
                {
                    Id = 1,
                    UserName = "ExampleUserName1",
                    Password = "ExamplePassword1",
                    Role = new UserRole { RoleName = "Admin" },
                    RoleId = 2
                };

                var fullAdminMapperMock = new Mock<IAdminDtoMapper>();
                fullAdminMapperMock.Setup(x => x.MapFrom(It.IsAny<Admin>())).Returns(exampleAdminDto);

                var sut = new AdminServices(assertContext, fullAdminMapperMock.Object);

                var result = await sut.GetAdminAsync(exampleAdmin.UserName, exampleAdmin.Password);

                Assert.AreEqual(result.Id, exampleAdmin.Id);
                Assert.AreEqual(result.UserName, exampleAdminDto.UserName);
                Assert.AreEqual(result.RoleName, exampleAdminDto.RoleName);
                Assert.AreEqual(result.UserName, exampleAdmin.UserName);
                Assert.IsInstanceOfType(result, typeof(AdminDto));
            }
        }

        [TestMethod]
        public async Task GetAdminAsync_Should_ThrowException_WhenNoSuchAdmin()
        {
            var options = TestUtils.GetOptions(nameof(GetAdminAsync_Should_ThrowException_WhenNoSuchAdmin));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleadminHash = new Admin()
                {
                    Id = 1,
                    UserName = "Example",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "Admin" },
                    RoleId = 2
                };

                var adminToAdd = await arrangeContext.Admins.AddAsync(exampleadminHash);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAdminDto = new AdminDto()
                {
                    Id = 1,
                    RoleId = 2,
                    UserName = "Example",
                    RoleName = "Example"
                };

                var exampleAdmin2 = new Admin()
                {
                    Id = 2,
                    UserName = "Example2",
                    Password = "ExamplePassword2",
                    Role = new UserRole { RoleName = "Admin" }
                };

                var fullAdminMapperMock = new Mock<IAdminDtoMapper>();
                fullAdminMapperMock.Setup(x => x.MapFrom(It.IsAny<Admin>())).Returns(exampleAdminDto);

                var sut = new AdminServices(assertContext, fullAdminMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.GetAdminAsync(exampleAdmin2.UserName, exampleAdmin2.Password));

                Assert.AreEqual(ex.Message, ExceptionMessages.AdminNull);
            }
        }

        [TestMethod]
        public async Task GetAdminAsync_Should_ThrowBusinessLogicException_WhenWrongUsernamePassCombo()
        {
            var options = TestUtils.GetOptions(nameof(GetAdminAsync_Should_ThrowBusinessLogicException_WhenWrongUsernamePassCombo));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAdmin = new Admin()
                {
                    Id = 2,
                    UserName = "ExampleUserName2",
                    Password = "ExamplePassword2",
                    Role = new UserRole { RoleName = "Admin" }
                };

                await arrangeContext.UserRoles.AddAsync(new UserRole() { RoleName = "Admin" });

                var adminToAdd = await arrangeContext.Admins.AddAsync(exampleAdmin);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAdminDto = new AdminDto()
                {
                    Id = 1,
                    RoleId = 1,
                    UserName = "exampleAdminDto",
                    RoleName = "Example"
                };

                var exampleAdmin = new Admin()
                {
                    Id = 1,
                    UserName = "ExampleUserName2",
                    Password = "ExamplePassword2",
                    Role = new UserRole { RoleName = "Admin" }
                };

                var exampleAdmin2 = new Admin()
                {
                    Id = 2,
                    UserName = "ExampleUserName2",
                    Password = "ExamplePassword2",
                    Role = new UserRole { RoleName = "Admin" }
                };

                var fullAdminMapperMock = new Mock<IAdminDtoMapper>();
                fullAdminMapperMock.Setup(x => x.MapFrom(It.IsAny<AdminDto>())).Returns(exampleAdmin);

                var sut = new AdminServices(assertContext, fullAdminMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.GetAdminAsync(exampleAdmin.UserName, exampleAdmin2.Password));

                Assert.AreEqual(ex.Message, ExceptionMessages.IncorrectUserPasswordCombo);
            }
        }

        [TestMethod]
        public async Task GetAdminAsync_Should_CallAdminMapperMock_MapFrom_Once()
        {

            var options = TestUtils.GetOptions(nameof(GetAdminAsync_Should_CallAdminMapperMock_MapFrom_Once));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleadminHash = new Admin()
                {
                    Id = 1,
                    UserName = "Example",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "Admin" },
                    RoleId = 2
                };

                await arrangeContext.UserRoles.AddAsync(new UserRole() { RoleName = "Admin" });

                var adminToAdd = await arrangeContext.Admins.AddAsync(exampleadminHash);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAdminDto = new AdminDto()
                {
                    Id = 1,
                    RoleId = 2,
                    UserName = "Example",
                    RoleName = "Admin"
                };

                var exampleAdmin = new Admin()
                {
                    Id = 1,
                    UserName = "Example",
                    Password = "ExamplePassword1",
                    Role = new UserRole { RoleName = "Admin" },
                    RoleId = 2
                };

                var fullAdminMapperMock = new Mock<IAdminDtoMapper>();
                fullAdminMapperMock.Setup(x => x.MapFrom(It.IsAny<Admin>())).Returns(exampleAdminDto);

                var sut = new AdminServices(assertContext, fullAdminMapperMock.Object);

                var result = await sut.GetAdminAsync(exampleAdmin.UserName, exampleAdmin.Password);

                fullAdminMapperMock.Verify(x => x.MapFrom(It.IsAny<Admin>()));
            }
        }

        [TestMethod]
        public void GetHashedString_Should_ReturnString()
        {
            var options = TestUtils.GetOptions(nameof(GetHashedString_Should_ReturnString));

            using (var assertContext = new TransactContext(options))
            {
                var exampleAdminDto = new AdminDto()
                {
                    Id = 1,
                    RoleId = 1,
                    UserName = "exampleAdminDto",
                    RoleName = "Example"
                };

                var fullAdminMapperMock = new Mock<IAdminDtoMapper>();
                fullAdminMapperMock.Setup(x => x.MapFrom(It.IsAny<Admin>())).Returns(exampleAdminDto);

                var sut = new AdminServices(assertContext, fullAdminMapperMock.Object);

                var result = sut.GetHashedString("Example");

                Assert.IsInstanceOfType(result, typeof(string));
            }
        }
    }
}
