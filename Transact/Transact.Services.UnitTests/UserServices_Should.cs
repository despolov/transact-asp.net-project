﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transact.Data;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.CustomExceptions;
using Transact.Services.Mapper;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services.UnitTests
{
    [TestClass]
    public class UserServices_Should
    {
        [TestMethod]
        public async Task AddUserAsync_Should_CorrectlyAddUser()
        {
            var options = TestUtils.GetOptions(nameof(AddUserAsync_Should_CorrectlyAddUser));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    RoleId = 1,
                    Password = "ExamplePassword1",
                    UserName = "exampleUserDto",
                    RoleName = "Example"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "ExamplePassword1",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<UserDto>())).Returns(exampleUser);
                var sut = new UserServices(arrangeContext, fullUserMapperMock.Object);

                var result = sut.AddUserAsync(exampleUserDto);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                Assert.AreEqual(assertContext.Users.Count(), 1);
                Assert.AreEqual(assertContext.Users.First().Id, exampleUser.Id);
                Assert.AreEqual(assertContext.Users.First().Name, exampleUser.Name);
                Assert.AreEqual(assertContext.Users.First().UserName, exampleUser.UserName);
                Assert.AreEqual(assertContext.Users.First().Password, exampleUser.Password);
                Assert.AreEqual(assertContext.Users.First().RoleId, exampleUser.RoleId);
                Assert.IsInstanceOfType(assertContext.Users.First(), typeof(User));
            }
        }

        [TestMethod]
        public async Task AddUserAsync_Should_ReturnCorrectUser()
        {
            var options = TestUtils.GetOptions(nameof(AddUserAsync_Should_ReturnCorrectUser));

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    RoleId = 1,
                    Password = "ExamplePassword1",
                    UserName = "exampleUserDto",
                    RoleName = "Example"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "ExamplePassword1",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<UserDto>())).Returns(exampleUser);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var result = await sut.AddUserAsync(exampleUserDto);

                Assert.AreEqual(result.Id, exampleUser.Id);
                Assert.AreEqual(result.UserName, exampleUserDto.UserName);
                Assert.AreEqual(result.RoleName, exampleUserDto.RoleName);
                Assert.AreEqual(result.Name, exampleUser.Name);
                Assert.IsInstanceOfType(result, typeof(UserDto));
            }
        }

        [TestMethod]
        public async Task AddUserAsync_Should_CallUserMapperMock_MapFrom_Once()
        {
            var options = TestUtils.GetOptions(nameof(AddUserAsync_Should_CallUserMapperMock_MapFrom_Once));

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    RoleId = 1,
                    Password = "SomePass",
                    UserName = "exampleUserDto",
                    RoleName = "Example"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "ExamplePassword1",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<UserDto>())).Returns(exampleUser);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var result = await sut.AddUserAsync(exampleUserDto);

                fullUserMapperMock.Verify(x => x.MapFrom(It.IsAny<UserDto>()));
            }
        }

        [TestMethod]
        public async Task AddUserAsync_Should_ThrowBusinessLogicException_UserAlreadyExists()
        {
            var options = TestUtils.GetOptions(nameof(AddUserAsync_Should_ThrowBusinessLogicException_UserAlreadyExists));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUserHash = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUserHash);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    RoleId = 1,
                    Password = "ExamplePassword1",
                    UserName = "ExampleUserName1",
                    RoleName = "Example"
                };

                var exampleUser = new User()
                {
                    Id = 2,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "ExamplePassword1",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<UserDto>())).Returns(exampleUser);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.AddUserAsync(exampleUserDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.UserNameExists);
            }
        }

        [TestMethod]
        public async Task GetUserAsync_Should_ReturnCorrectUser()
        {
            var options = TestUtils.GetOptions(nameof(GetUserAsync_Should_ReturnCorrectUser));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUserHash = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                await arrangeContext.UserRoles.AddAsync(new UserRole() { RoleName = "User" });
                var userToAdd = await arrangeContext.Users.AddAsync(exampleUserHash);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    RoleId = 1,
                    UserName = "exampleUserDto",
                    RoleName = "Example"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "ExamplePassword1",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var result = await sut.GetUserAsync(exampleUser.UserName, exampleUser.Password);

                Assert.AreEqual(result.Id, exampleUser.Id);
                Assert.AreEqual(result.UserName, exampleUserDto.UserName);
                Assert.AreEqual(result.RoleName, exampleUserDto.RoleName);
                Assert.AreEqual(result.Name, exampleUser.Name);
                Assert.IsInstanceOfType(result, typeof(UserDto));
            }
        }

        [TestMethod]
        public async Task GetUserAsync_Should_ThrowBusinessLogicException_WhenNoSuchUser()
        {
            var options = TestUtils.GetOptions(nameof(GetUserAsync_Should_ThrowBusinessLogicException_WhenNoSuchUser));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUserHash = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUserHash);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    RoleId = 1,
                    UserName = "exampleUserDto",
                    RoleName = "Example"
                };

                var exampleUser2 = new User()
                {
                    Id = 2,
                    Name = "ExampleUser2",
                    UserName = "ExampleUserName2",
                    Password = "ExamplePassword2",
                    Role = new UserRole { RoleName = "User" },
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.GetUserAsync(exampleUser2.UserName, exampleUser2.Password));

                Assert.AreEqual(ex.Message, ExceptionMessages.UserNull);
            }
        }

        [TestMethod]
        public async Task GetUserAsync_Should_ThrowBusinessLogicException_WhenWrongUsernamePassCombo()
        {
            var options = TestUtils.GetOptions(nameof(GetUserAsync_Should_ThrowBusinessLogicException_WhenWrongUsernamePassCombo));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUserHash = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                await arrangeContext.UserRoles.AddAsync(new UserRole() { RoleName = "User" });

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUserHash);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    RoleId = 1,
                    UserName = "exampleUserDto",
                    RoleName = "Example"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "ExamplePassword1",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUser2 = new User()
                {
                    Id = 2,
                    Name = "ExampleUser2",
                    UserName = "ExampleUserName2",
                    Password = "ExamplePassword2",
                    Role = new UserRole { RoleName = "User" },
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<UserDto>())).Returns(exampleUser);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.GetUserAsync(exampleUser.UserName, exampleUser2.Password));

                Assert.AreEqual(ex.Message, ExceptionMessages.IncorrectUserPasswordCombo);
            }
        }

        [TestMethod]
        public async Task GetUserAsync_Should_CallUserMapperMock_MapFrom_Once()
        {

            var options = TestUtils.GetOptions(nameof(GetUserAsync_Should_CallUserMapperMock_MapFrom_Once));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUserHash = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                await arrangeContext.UserRoles.AddAsync(new UserRole() { RoleName = "User" });

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUserHash);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    RoleId = 1,
                    UserName = "exampleUserDto",
                    RoleName = "Example"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "ExamplePassword1",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var result = await sut.GetUserAsync(exampleUser.UserName, exampleUser.Password);

                fullUserMapperMock.Verify(x => x.MapFrom(It.IsAny<User>()));
            }
        }

        [TestMethod]
        public async Task GetPageCountForUsersAsync_Should_ReturnProperPageCount()
        {
            var options = TestUtils.GetOptions(nameof(GetPageCountForUsersAsync_Should_ReturnProperPageCount));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUserHash = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash2 = new User()
                {
                    Id = 2,
                    Name = "ExampleUser2",
                    UserName = "ExampleUserName2",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash3 = new User()
                {
                    Id = 3,
                    Name = "ExampleUser3",
                    UserName = "ExampleUserName3",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash4 = new User()
                {
                    Id = 4,
                    Name = "ExampleUser4",
                    UserName = "ExampleUserName4",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash5 = new User()
                {
                    Id = 5,
                    Name = "ExampleUser5",
                    UserName = "ExampleUserName5",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUserHash);
                var userToAdd2 = await arrangeContext.Users.AddAsync(exampleUserHash2);
                var userToAdd3 = await arrangeContext.Users.AddAsync(exampleUserHash3);
                var userToAdd4 = await arrangeContext.Users.AddAsync(exampleUserHash4);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    RoleId = 1,
                    UserName = "exampleUserDto",
                    RoleName = "Example"
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var result = await sut.GetPageCountForUsersAsync(2);

                Assert.AreEqual(result, 2);
            }
        }

        [TestMethod]
        public async Task GetFiveUsersById_Should_ReturnCorrectUsers()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveUsersById_Should_ReturnCorrectUsers));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUserHash = new User()
                {
                    Id = 1,
                    Name = "ExampleUser3",
                    UserName = "ExampleUserName3",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash2 = new User()
                {
                    Id = 2,
                    Name = "ExampleUser3",
                    UserName = "ExampleUserName3",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash3 = new User()
                {
                    Id = 3,
                    Name = "ExampleUser3",
                    UserName = "ExampleUserName3",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash4 = new User()
                {
                    Id = 4,
                    Name = "ExampleUser4",
                    UserName = "ExampleUserName4",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash5 = new User()
                {
                    Id = 5,
                    Name = "ExampleUser5",
                    UserName = "ExampleUserName5",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash6 = new User()
                {
                    Id = 6,
                    Name = "ExampleUser6",
                    UserName = "ExampleUserName6",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUserHash);
                var userToAdd2 = await arrangeContext.Users.AddAsync(exampleUserHash2);
                var userToAdd3 = await arrangeContext.Users.AddAsync(exampleUserHash3);
                var userToAdd4 = await arrangeContext.Users.AddAsync(exampleUserHash4);
                var userToAdd5 = await arrangeContext.Users.AddAsync(exampleUserHash5);
                var userToAdd6 = await arrangeContext.Users.AddAsync(exampleUserHash6);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDtoList = new List<UserDto>()
            {
                new UserDto { Id = 6 },
                new UserDto { Id = 5 },
                new UserDto { Id = 4 },
                new UserDto { Id = 3 },
                new UserDto { Id = 2 }
            };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<List<User>>())).Returns(exampleUserDtoList);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var result = await sut.GetFiveUsersByIdAsync(2);

                Assert.AreEqual(result.Count, 5);
                Assert.AreEqual(result.First().Id, 6);
                Assert.AreEqual(result.Skip(1).First().Id, 5);
                Assert.AreEqual(result.Skip(2).First().Id, 4);
                Assert.AreEqual(result.Skip(3).First().Id, 3);
                Assert.AreEqual(result.Skip(4).First().Id, 2);

            }
        }

        [TestMethod]
        public async Task GetFiveUsersById_Should_CallUserMapper_MapFrom_Once()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveUsersById_Should_CallUserMapper_MapFrom_Once));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUserHash = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash2 = new User()
                {
                    Id = 2,
                    Name = "ExampleUser2",
                    UserName = "ExampleUserName2",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash3 = new User()
                {
                    Id = 3,
                    Name = "ExampleUser3",
                    UserName = "ExampleUserName3",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash4 = new User()
                {
                    Id = 4,
                    Name = "ExampleUser4",
                    UserName = "ExampleUserName4",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash5 = new User()
                {
                    Id = 5,
                    Name = "ExampleUser5",
                    UserName = "ExampleUserName5",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash6 = new User()
                {
                    Id = 6,
                    Name = "ExampleUser6",
                    UserName = "ExampleUserName6",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUserHash);
                var userToAdd2 = await arrangeContext.Users.AddAsync(exampleUserHash2);
                var userToAdd3 = await arrangeContext.Users.AddAsync(exampleUserHash3);
                var userToAdd4 = await arrangeContext.Users.AddAsync(exampleUserHash4);
                var userToAdd5 = await arrangeContext.Users.AddAsync(exampleUserHash5);
                var userToAdd6 = await arrangeContext.Users.AddAsync(exampleUserHash6);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    RoleId = 1,
                    UserName = "exampleUserDto",
                    RoleName = "Example"
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var result = await sut.GetFiveUsersByIdAsync(1);

                fullUserMapperMock.Verify(x => x.MapFrom(It.IsAny<List<User>>()), Times.Once);

            }
        }

        [TestMethod]
        public async Task GetTotalUsersCountAsync_Should_GetCorrectUserCount()
        {
            var options = TestUtils.GetOptions(nameof(GetTotalUsersCountAsync_Should_GetCorrectUserCount));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUserHash = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash2 = new User()
                {
                    Id = 2,
                    Name = "ExampleUser2",
                    UserName = "ExampleUserName2",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash3 = new User()
                {
                    Id = 3,
                    Name = "ExampleUser3",
                    UserName = "ExampleUserName3",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash4 = new User()
                {
                    Id = 4,
                    Name = "ExampleUser4",
                    UserName = "ExampleUserName4",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash5 = new User()
                {
                    Id = 5,
                    Name = "ExampleUser5",
                    UserName = "ExampleUserName5",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash6 = new User()
                {
                    Id = 6,
                    Name = "ExampleUser6",
                    UserName = "ExampleUserName6",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUserHash);
                var userToAdd2 = await arrangeContext.Users.AddAsync(exampleUserHash2);
                var userToAdd3 = await arrangeContext.Users.AddAsync(exampleUserHash3);
                var userToAdd4 = await arrangeContext.Users.AddAsync(exampleUserHash4);
                var userToAdd5 = await arrangeContext.Users.AddAsync(exampleUserHash5);
                var userToAdd6 = await arrangeContext.Users.AddAsync(exampleUserHash6);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var fullUserMapperMock = new Mock<IUserDtoMapper>();

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var result = await sut.GetTotalUsersCountAsync();

                Assert.AreEqual(result, 6);
            }
        }

        [TestMethod]
        public void GetHashedString_Should_ReturnString()
        {
            var options = TestUtils.GetOptions(nameof(GetHashedString_Should_ReturnString));

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    RoleId = 1,
                    UserName = "exampleUserDto",
                    RoleName = "Example"
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var result = sut.GetHashedString("Example");

                Assert.IsInstanceOfType(result, typeof(string));
            }
        }

        [TestMethod]
        public async Task GetPageCountForUsersWithClientId_Should_ReturnProperPageCount()
        {
            var options = TestUtils.GetOptions(nameof(GetPageCountForUsersWithClientId_Should_ReturnProperPageCount));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUserHash = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash2 = new User()
                {
                    Id = 2,
                    Name = "ExampleUser2",
                    UserName = "ExampleUserName2",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash3 = new User()
                {
                    Id = 3,
                    Name = "ExampleUser3",
                    UserName = "ExampleUserName3",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash4 = new User()
                {
                    Id = 4,
                    Name = "ExampleUser4",
                    UserName = "ExampleUserName4",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash5 = new User()
                {
                    Id = 5,
                    Name = "ExampleUser5",
                    UserName = "ExampleUserName5",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUserHash);
                var userToAdd2 = await arrangeContext.Users.AddAsync(exampleUserHash2);
                var userToAdd3 = await arrangeContext.Users.AddAsync(exampleUserHash3);
                var userToAdd4 = await arrangeContext.Users.AddAsync(exampleUserHash4);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    RoleId = 1,
                    UserName = "exampleUserDto",
                    RoleName = "Example"
                };

                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "Client",
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var result = await sut.GetPageCountForUsersWithClientIdAsync(2, exampleClient.Id);

                Assert.AreEqual(result, 1);
            }
        }

        [TestMethod]
        public async Task FindUsersContaining_Should_GetCorrectUsers()
        {
            var options = TestUtils.GetOptions(nameof(FindUsersContaining_Should_GetCorrectUsers));

            using (var arrangeContext = new TransactContext(options))
            {

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "exampleUser1",
                    UserName = "exampleUserName1",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUser2 = new User()
                {
                    Id = 2,
                    Name = "exampleUser2",
                    UserName = "exampleUserName2",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleName"
                };

                arrangeContext.Add(exampleClient);
                arrangeContext.Add(exampleUser);
                arrangeContext.Add(exampleUser2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "exampleUser1",
                    RoleId = 1,
                    UserName = "exampleUserName1",
                    RoleName = "User"
                };

                var exampleUserDto2 = new UserDto()
                {
                    Id = 2,
                    Name = "exampleUser2",
                    RoleId = 1,
                    UserName = "exampleUserName2",
                    RoleName = "User"
                };

                var exampleUsereDtoList = new List<UserDto>()
                {
                    new UserDto()
                    {
                        Id = 1,
                        Name = "exampleUser1",
                        RoleId = 1,
                        UserName = "exampleUserName1",
                        RoleName = "User"
                    },
                    new UserDto()
                    {
                        Id = 2,
                        Name = "exampleUser2",
                        RoleId = 1,
                        UserName = "exampleUserName2",
                        RoleName = "User"
                    }
                };

                var userMapper = new UserDtoMapper();

                var sut = new UserServices(assertContext, userMapper);

                var result = await sut.FindUsersContainingAsync("exa", 1);

                Assert.IsInstanceOfType(result, typeof(List<UserDto>));

                Assert.AreEqual(result.Count, 2);
                Assert.AreEqual(result[0].Id, exampleUserDto.Id);
                Assert.AreEqual(result[0].Name, exampleUserDto.Name);
                Assert.AreEqual(result[0].UserName, exampleUserDto.UserName);
                Assert.AreEqual(result[0].RoleId, exampleUserDto.RoleId);

                Assert.AreEqual(result[1].Id, exampleUserDto2.Id);
                Assert.AreEqual(result[1].UserName, exampleUserDto2.UserName);
                Assert.AreEqual(result[1].Name, exampleUserDto2.Name);
                Assert.AreEqual(result[1].RoleId, exampleUserDto2.RoleId);
            }
        }

        [TestMethod]
        public async Task GetUserById_Should_ReturnCorrectUser()
        {
            var options = TestUtils.GetOptions(nameof(GetUserById_Should_ReturnCorrectUser));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    RoleId = 1,
                    UserName = "exampleUserDto",
                    RoleName = "Example"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "ExamplePassword1",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var result = await sut.GetUserByIdAsync(exampleUser.Id);

                Assert.AreEqual(result.Id, exampleUser.Id);
                Assert.AreEqual(result.UserName, exampleUserDto.UserName);
                Assert.AreEqual(result.RoleName, exampleUserDto.RoleName);
                Assert.AreEqual(result.Name, exampleUser.Name);
                Assert.IsInstanceOfType(result, typeof(UserDto));
            }
        }

        [TestMethod]
        public async Task GetUserById_Should_ThrowBusinessLogicException_WhenNoSuchUser()
        {
            var options = TestUtils.GetOptions(nameof(GetUserById_Should_ThrowBusinessLogicException_WhenNoSuchUser));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExampleUserName1",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    RoleId = 1,
                    UserName = "exampleUserDto",
                    RoleName = "Example"
                };

                var exampleUser2 = new User()
                {
                    Id = 2,
                    Name = "ExampleUser2",
                    UserName = "ExampleUserName2",
                    Password = "ExamplePassword2",
                    Role = new UserRole { RoleName = "User" },
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.GetUserByIdAsync(exampleUser2.Id));

                Assert.AreEqual(ex.Message, ExceptionMessages.UserNull);
            }
        }

        [TestMethod]
        public async Task GetFiveUsersForClientById_Should_ReturnCorrectUsersIfCurrPageIs1()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveUsersForClientById_Should_ReturnCorrectUsersIfCurrPageIs1));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUserHash = new User()
                {
                    Id = 1,
                    Name = "ExampleUser3",
                    UserName = "ExampleUserName3",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash2 = new User()
                {
                    Id = 2,
                    Name = "ExampleUser3",
                    UserName = "ExampleUserName3",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash3 = new User()
                {
                    Id = 3,
                    Name = "ExampleUser3",
                    UserName = "ExampleUserName3",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash4 = new User()
                {
                    Id = 4,
                    Name = "ExampleUser4",
                    UserName = "ExampleUserName4",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash5 = new User()
                {
                    Id = 5,
                    Name = "ExampleUser5",
                    UserName = "ExampleUserName5",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash6 = new User()
                {
                    Id = 6,
                    Name = "ExampleUser6",
                    UserName = "ExampleUserName6",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUserHash);
                var userToAdd2 = await arrangeContext.Users.AddAsync(exampleUserHash2);
                var userToAdd3 = await arrangeContext.Users.AddAsync(exampleUserHash3);
                var userToAdd4 = await arrangeContext.Users.AddAsync(exampleUserHash4);
                var userToAdd5 = await arrangeContext.Users.AddAsync(exampleUserHash5);
                var userToAdd6 = await arrangeContext.Users.AddAsync(exampleUserHash6);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDtoList = new List<UserDto>()
            {
                new UserDto { Id = 6 },
                new UserDto { Id = 5 },
                new UserDto { Id = 4 },
                new UserDto { Id = 3 },
                new UserDto { Id = 2 }
            };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<List<User>>())).Returns(exampleUserDtoList);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var result = await sut.GetFiveUsersForClientByIdAsync(1, 0);

                Assert.AreEqual(result.Count, 5);
                Assert.AreEqual(result.First().Id, 6);
                Assert.AreEqual(result.Skip(1).First().Id, 5);
                Assert.AreEqual(result.Skip(2).First().Id, 4);
                Assert.AreEqual(result.Skip(3).First().Id, 3);
                Assert.AreEqual(result.Skip(4).First().Id, 2);
            }
        }

        [TestMethod]
        public async Task GetFiveUsersForClientById_Should_ReturnCorrectUsersIfCurrPageIs2()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveUsersForClientById_Should_ReturnCorrectUsersIfCurrPageIs2));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleUserHash = new User()
                {
                    Id = 1,
                    Name = "ExampleUser3",
                    UserName = "ExampleUserName3",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash2 = new User()
                {
                    Id = 2,
                    Name = "ExampleUser3",
                    UserName = "ExampleUserName3",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash3 = new User()
                {
                    Id = 3,
                    Name = "ExampleUser3",
                    UserName = "ExampleUserName3",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash4 = new User()
                {
                    Id = 4,
                    Name = "ExampleUser4",
                    UserName = "ExampleUserName4",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash5 = new User()
                {
                    Id = 5,
                    Name = "ExampleUser5",
                    UserName = "ExampleUserName5",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var exampleUserHash6 = new User()
                {
                    Id = 6,
                    Name = "ExampleUser6",
                    UserName = "ExampleUserName6",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUserHash);
                var userToAdd2 = await arrangeContext.Users.AddAsync(exampleUserHash2);
                var userToAdd3 = await arrangeContext.Users.AddAsync(exampleUserHash3);
                var userToAdd4 = await arrangeContext.Users.AddAsync(exampleUserHash4);
                var userToAdd5 = await arrangeContext.Users.AddAsync(exampleUserHash5);
                var userToAdd6 = await arrangeContext.Users.AddAsync(exampleUserHash6);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUserDtoList = new List<UserDto>()
            {
                new UserDto { Id = 6 },
                new UserDto { Id = 5 },
                new UserDto { Id = 4 },
                new UserDto { Id = 3 },
                new UserDto { Id = 2 }
            };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<List<User>>())).Returns(exampleUserDtoList);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var result = await sut.GetFiveUsersForClientByIdAsync(2, 0);

                Assert.AreEqual(result.Count, 5);
                Assert.AreEqual(result.First().Id, 6);
                Assert.AreEqual(result.Skip(1).First().Id, 5);
                Assert.AreEqual(result.Skip(2).First().Id, 4);
                Assert.AreEqual(result.Skip(3).First().Id, 3);
                Assert.AreEqual(result.Skip(4).First().Id, 2);
            }
        }

        [TestMethod]
        public async Task RemoveUserForClient_Should_ReturnCorrectUser()
        {
            var options = TestUtils.GetOptions(nameof(RemoveUserForClient_Should_ReturnCorrectUser));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var clientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var exampleUsersClients = new UsersClients()
                {
                    UserId = 1,
                    ClientId = 1
                };


                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);
                var userClients = await arrangeContext.UsersClients.AddAsync(exampleUsersClients);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var result = await sut.RemoveUserForClient(exampleClient.Id, exampleUser.Id);

                Assert.AreEqual(result.Id, exampleUser.Id);
                Assert.AreEqual(result.Name, exampleUser.Name);
                Assert.IsInstanceOfType(result, typeof(UserDto));
            }
        }

        [TestMethod]
        public async Task RemoveUserForClient_Should_ThrowWhenNoSuchUser()
        {
            var options = TestUtils.GetOptions(nameof(RemoveUserForClient_Should_ThrowWhenNoSuchUser));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var clientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser",
                    UserName = "ExmplUserName"
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleUser2 = new User()
                {
                    Id = 2,
                    Name = "ExampleUser2",
                    UserName = "ExmplUserName2"
                };

                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleClient1",
                    UserName = "ExmplUserName"
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                         await sut.RemoveUserForClient(exampleClient.Id, exampleUser2.Id));

                Assert.AreEqual(ex.Message, ExceptionMessages.UserNull);
            }
        }

        [TestMethod]
        public async Task RemoveUserForClient_Should_ThrowWhenNoSuchClient()
        {
            var options = TestUtils.GetOptions(nameof(RemoveUserForClient_Should_ThrowWhenNoSuchClient));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var clientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser",
                    UserName = "ExmplUserName"
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClient2 = new Client()
                {
                    Id = 2,
                    Name = "ExampleClient2"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser",
                    UserName = "ExmplUserName"
                };

                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleClient1",
                    UserName = "ExmplUserName"
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                         await sut.RemoveUserForClient(exampleClient2.Id, exampleUser.Id));

                Assert.AreEqual(ex.Message, ExceptionMessages.ClientNull);
            }
        }

        [TestMethod]
        public async Task RemoveUserForClient_Should_ThrowWhenNoSuchUsersClients()
        {
            var options = TestUtils.GetOptions(nameof(RemoveUserForClient_Should_ThrowWhenNoSuchUsersClients));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var clientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser",
                    UserName = "ExmplUserName"
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser",
                    UserName = "ExmplUserName"
                };

                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleClient1",
                    UserName = "ExmplUserName"
                };

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var sut = new UserServices(assertContext, fullUserMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                         await sut.RemoveUserForClient(exampleClient.Id, exampleUser.Id));

                Assert.AreEqual(ex.Message, ExceptionMessages.UsersClientsNull);
            }
        }
    }
}
