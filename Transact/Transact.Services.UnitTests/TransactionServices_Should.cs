﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Transact.Data;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Services.CustomExceptions;
using Transact.Services.Mapper;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services.UnitTests
{
    [TestClass]
    public class TransactionServices_Should
    {
        [TestMethod]
        public async Task AddTransactionAsync_Should_CorrectlyAddTransaction()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                         .UseInMemoryDatabase(nameof(AddTransactionAsync_Should_CorrectlyAddTransaction))
                         .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                         .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                arrangeContext.Accounts.Add(accountOne);
                arrangeContext.Accounts.Add(accountTwo);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    ReceiverAccountNumber = "1233367890",
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15)                  
                };

                var dtMock1 = new Mock<IDateTimeProvider>();
                dtMock1.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();
                transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionDto>())).Returns(exampleSavedTransaction);


                var sut = new TransactionServices(assertContext, dtMock1.Object, transactionMapperMock.Object);

                var result = await sut.AddTransactionAsync(exampleTransactionDto);

                Assert.AreEqual(assertContext.Transactions.Count(), 1);
                Assert.AreEqual(assertContext.Transactions.First().Id, exampleSavedTransaction.Id);
                Assert.AreEqual(assertContext.Transactions.First().SenderAccountId, exampleSavedTransaction.SenderAccountId);
                Assert.AreEqual(assertContext.Transactions.First().ReceiverAccountId, exampleSavedTransaction.ReceiverAccountId);
                Assert.AreEqual(assertContext.Transactions.First().ReceiverAccount.AccountNumber, exampleSavedTransaction.ReceiverAccount.AccountNumber);
                Assert.AreEqual(assertContext.Transactions.First().Amount, exampleSavedTransaction.Amount);
                Assert.AreEqual(assertContext.Transactions.First().Description, exampleSavedTransaction.Description);
                Assert.AreEqual(assertContext.Transactions.First().TimeStamp, exampleSavedTransaction.TimeStamp);
                Assert.AreEqual(assertContext.Transactions.First().StatusId, 2);
                Assert.AreEqual(assertContext.Accounts.First().Balance, (exampleAccount.Balance - exampleSavedTransaction.Amount));
                Assert.AreEqual(assertContext.Accounts.Skip(1).First().Balance, (exampleAccount.Balance + exampleSavedTransaction.Amount));
                Assert.IsInstanceOfType(assertContext.Transactions.First(), typeof(Transaction));
            }
        }

        [TestMethod]
        public async Task AddTransactionAsync_ThrowsBusinessLogicException_IfSenderAccountIsNull()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                 .UseInMemoryDatabase(nameof(AddTransactionAsync_ThrowsBusinessLogicException_IfSenderAccountIsNull))
                 .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                 .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                arrangeContext.Accounts.Add(exampleAccount2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 2,
                    Amount = 10000,
                    SenderAccountId = 3,
                    ReceiverAccountId = 20,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 4, 10),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 2,
                    Amount = 10000,
                    SenderAccountId = 3,
                    ReceiverAccountId = 20,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 4, 10),
                    ReceiverAccountNumber = "1133367890",
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.AddTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ExceptionMessages.SenderAccountNull, ex.Message);
            }
        }

        [TestMethod]
        public async Task AddTransactionAsync_Should_ThrowBusinessLogicException_IfReceiverAccountIsNull()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                      .UseInMemoryDatabase(nameof(AddTransactionAsync_Should_ThrowBusinessLogicException_IfReceiverAccountIsNull))
                      .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                      .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                arrangeContext.Accounts.Add(exampleAccount);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 3,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 5, 10),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 3,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 5, 10),
                    ReceiverAccountNumber = "1133367890",
                };


                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
              var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>

                await sut.AddTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.ReceiverAccountNull);
            }
        }

        [TestMethod]
        public async Task AddTransactionAsync_Should_ThrowBusinessLogicException_IfSenderAndReceiverSame()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                    .UseInMemoryDatabase(nameof(AddTransactionAsync_Should_ThrowBusinessLogicException_IfSenderAndReceiverSame))
                    .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                    .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                arrangeContext.Accounts.Add(account);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 9,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 1,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 1, AccountNumber = "1234567890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 9,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 1,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    ReceiverAccountNumber = "1234567890",
                };


                var dtMock4 = new Mock<IDateTimeProvider>();
                dtMock4.Setup(x => x.GetDateTimeNow()).Returns(transaction.TimeStamp);
              var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock4.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>

                await sut.AddTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.SameAccountsForTransaction);
            }
        }

        [TestMethod]
        public async Task AddTransactionAsync_Should_ReturnCorrectTransaction()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                         .UseInMemoryDatabase(nameof(AddTransactionAsync_Should_ReturnCorrectTransaction))
                         .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                         .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                arrangeContext.Accounts.Add(exampleAccount);
                arrangeContext.Accounts.Add(exampleAccount2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountNumber = "1234567890",
                    SenderAccountNickName = "ExampleNickName",
                    SenderClientName = "ExampleSenderClient",
                    ReceiverAccountNumber = "1233367890",
                    ReceiverClientName = "ExampleReceiverClient",
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusName = "Saved",
                };                 

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();
                transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionDto>())).Returns(exampleSavedTransaction);

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var result = await sut.AddTransactionAsync(exampleTransactionDto);

                Assert.AreEqual(result.Id, exampleSavedTransaction.Id);
                Assert.AreEqual(result.Amount, exampleSavedTransaction.Amount);
                Assert.AreEqual(result.Description, exampleSavedTransaction.Description);
                Assert.AreEqual(result.TimeStamp, exampleSavedTransaction.TimeStamp);
                Assert.AreEqual(result.SenderAccountId, exampleSavedTransaction.SenderAccountId);
                Assert.AreEqual(result.ReceiverAccountId, exampleSavedTransaction.ReceiverAccountId);
                Assert.AreEqual(result.ReceiverAccountNumber, exampleSavedTransaction.ReceiverAccount.AccountNumber);
                Assert.IsInstanceOfType(result, typeof(TransactionDto));
            }
        }

        [TestMethod]
        public async Task AddTransactionAsync_Should_CallTransactionDtoMapper_Once()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                   .UseInMemoryDatabase(nameof(AddTransactionAsync_Should_CallTransactionDtoMapper_Once))
                                   .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                   .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                arrangeContext.Accounts.Add(exampleAccount);
                arrangeContext.Accounts.Add(exampleAccount2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    ReceiverAccountNumber = "1233367890",
                };


                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();
                transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionDto>())).Returns(exampleSavedTransaction);

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var result = await sut.AddTransactionAsync(exampleTransactionDto);

                transactionMapperMock.Verify(x => x.MapFrom(It.IsAny<TransactionDto>()), Times.Once);
            }
        }

        [TestMethod]
        public async Task AddTransactionAsync_Should_ThrowBusinessLogicException_When_SenderHasInsufficientFunds()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                    .UseInMemoryDatabase(nameof(AddTransactionAsync_Should_ThrowBusinessLogicException_When_SenderHasInsufficientFunds))
                                    .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                    .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 10,
                    AccountNumber = "6258567871",
                    Balance = 12,
                    ClientId = 1,
                    NickName = "ExampleNickName10"
                };

                var accountTwo = new Account()
                {
                    Id = 11,
                    AccountNumber = "1133367890",
                    Balance = 1020,
                    ClientId = 1,
                    NickName = "ExampleNickName11"
                };

                arrangeContext.Accounts.Add(accountOne);
                arrangeContext.Accounts.Add(accountTwo);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 8,
                    Amount = 18800,
                    SenderAccountId = 10,
                    ReceiverAccountId = 11,
                    Description = "example Description8",
                    TimeStamp = new DateTime(2000, 1, 3),
                    SenderAccount = new Account { Id = 10 },
                    ReceiverAccount = new Account { Id = 11, AccountNumber = "1133367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 8,
                    Amount = 18800,
                    SenderAccountId = 10,
                    ReceiverAccountId = 11,
                    Description = "example Description8",
                    TimeStamp = new DateTime(2000, 1, 3),
                    ReceiverAccountNumber = "1133367890",
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2000, 1, 3));
              var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                   await sut.AddTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.NotEnoughFromSenderBalance);
            }
        }

        [TestMethod]
        public async Task UpdateTransactionAsync_Should_CorrectlyUpdateTransaction()
        {
            var options = TestUtils.GetOptions(nameof(UpdateTransactionAsync_Should_CorrectlyUpdateTransaction));

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    ReceiverAccountNumber = "1233367890",
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15)
                };

                var dtMock1 = new Mock<IDateTimeProvider>();
                dtMock1.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();
                transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionDto>())).Returns(exampleSavedTransaction);

                var sut = new TransactionServices(assertContext, dtMock1.Object, transactionMapperMock.Object);

                var result = await sut.UpdateTransactionAsync(exampleTransactionDto);

                Assert.AreEqual(assertContext.Transactions.Count(), 1);
                Assert.AreEqual(assertContext.Transactions.First().Id, exampleSavedTransaction.Id);
                Assert.AreEqual(assertContext.Transactions.First().SenderAccountId, exampleSavedTransaction.SenderAccountId);
                Assert.AreEqual(assertContext.Transactions.First().ReceiverAccountId, exampleSavedTransaction.ReceiverAccountId);
                Assert.AreEqual(assertContext.Transactions.First().ReceiverAccount.AccountNumber, exampleSavedTransaction.ReceiverAccount.AccountNumber);
                Assert.AreEqual(assertContext.Transactions.First().Amount, exampleSavedTransaction.Amount);
                Assert.AreEqual(assertContext.Transactions.First().Description, exampleSavedTransaction.Description);
                Assert.AreEqual(assertContext.Transactions.First().TimeStamp, exampleSavedTransaction.TimeStamp);
                Assert.AreEqual(assertContext.Transactions.First().StatusId, 1);
                Assert.AreEqual(assertContext.Accounts.First().Balance, exampleAccount.Balance);
                Assert.AreEqual(assertContext.Accounts.Skip(1).First().Balance, exampleAccount.Balance);
                Assert.IsInstanceOfType(assertContext.Transactions.First(), typeof(Transaction));
            }
        }

        [TestMethod]
        public async Task UpdateTransactionAsync_Should_ReturnCorrectTransaction()
        {
            var options = TestUtils.GetOptions(nameof(UpdateTransactionAsync_Should_ReturnCorrectTransaction));

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountNumber = "1234567890",
                    SenderAccountNickName = "ExampleNickName",
                    SenderClientName = "ExampleSenderClient",
                    ReceiverAccountNumber = "1233367890",
                    ReceiverClientName = "ExampleReceiverClient",
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusName = "Saved",
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();
                transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionDto>())).Returns(exampleSavedTransaction);

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var result = await sut.UpdateTransactionAsync(exampleTransactionDto);

                Assert.AreEqual(result.Id, exampleSavedTransaction.Id);
                Assert.AreEqual(result.Amount, exampleSavedTransaction.Amount);
                Assert.AreEqual(result.Description, exampleSavedTransaction.Description);
                Assert.AreEqual(result.TimeStamp, exampleSavedTransaction.TimeStamp);
                Assert.AreEqual(result.SenderAccountId, exampleSavedTransaction.SenderAccountId);
                Assert.AreEqual(result.ReceiverAccountId, exampleSavedTransaction.ReceiverAccountId);
                Assert.AreEqual(result.ReceiverAccountNumber, exampleSavedTransaction.ReceiverAccount.AccountNumber);
                Assert.IsInstanceOfType(result, typeof(TransactionDto));
            }
        }

        [TestMethod]
        public async Task UpdateTransactionAsync_ThrowsBusinessLogicException_IfSenderAccountIsNull()
        {
            var options = TestUtils.GetOptions(nameof(UpdateTransactionAsync_ThrowsBusinessLogicException_IfSenderAccountIsNull));

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 2,
                    Amount = 10000,
                    SenderAccountId = 3,
                    ReceiverAccountId = 20,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 4, 10),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 2,
                    Amount = 10000,
                    SenderAccountId = 3,
                    ReceiverAccountId = 20,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 4, 10),
                    ReceiverAccountNumber = "1133367890",
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.UpdateTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ExceptionMessages.SenderAccountNull, ex.Message);
            }
        }

        [TestMethod]
        public async Task UpdateTransactionAsync_Should_ThrowBusinessLogicException_IfReceiverAccountIsNull()
        {
            var options = TestUtils.GetOptions(nameof(UpdateTransactionAsync_Should_ThrowBusinessLogicException_IfReceiverAccountIsNull));

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 3,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 5, 10),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 3,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 5, 10),
                    ReceiverAccountNumber = "1133367890",
                };


                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.UpdateTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.ReceiverAccountNull);
            }
        }

        [TestMethod]
        public async Task UpdateTransactionAsync_Should_ThrowBusinessLogicException_IfSenderAndReceiverSame()
        {
            var options = TestUtils.GetOptions(nameof(UpdateTransactionAsync_Should_ThrowBusinessLogicException_IfSenderAndReceiverSame));

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 9,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 1,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 1, AccountNumber = "1234567890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 9,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 1,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    ReceiverAccountNumber = "1234567890",
                };


                var dtMock4 = new Mock<IDateTimeProvider>();
                dtMock4.Setup(x => x.GetDateTimeNow()).Returns(transaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock4.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>

                await sut.UpdateTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.SameAccountsForTransaction);
            }
        }

        [TestMethod]
        public async Task UpdateTransactionAsync_Should_ThrowBusinessLogicException_IfNoSuchTransaction()
        {
            var options = TestUtils.GetOptions(nameof(UpdateTransactionAsync_Should_ThrowBusinessLogicException_IfNoSuchTransaction));

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "2233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo                   
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1234567890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 22,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    ReceiverAccountNumber = "2233367890",
                };


                var dtMock4 = new Mock<IDateTimeProvider>();
                dtMock4.Setup(x => x.GetDateTimeNow()).Returns(transaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock4.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                 await sut.UpdateTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.TransactionNull);
            }
        }

        [TestMethod]
        public async Task UpdateTransactionAsync_Should_ThrowBusinessLogicException_IfTransactionAlreadySent()
        {
            var options = TestUtils.GetOptions(nameof(UpdateTransactionAsync_Should_ThrowBusinessLogicException_IfTransactionAlreadySent));

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "2233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 2,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1234567890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    ReceiverAccountNumber = "2233367890",
                };


                var dtMock4 = new Mock<IDateTimeProvider>();
                dtMock4.Setup(x => x.GetDateTimeNow()).Returns(transaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock4.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                 await sut.UpdateTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.CannotEditSentTransaction);
            }
        }

        [TestMethod]
        public async Task SendUpdatedTransactionAsync_Should_CorrectlyUpdateTransaction()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                  .UseInMemoryDatabase(nameof(SendUpdatedTransactionAsync_Should_CorrectlyUpdateTransaction))
                                  .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                  .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    ReceiverAccountNumber = "1233367890",
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15)
                };

                var dtMock1 = new Mock<IDateTimeProvider>();
                dtMock1.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();
                transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionDto>())).Returns(exampleSavedTransaction);

                var sut = new TransactionServices(assertContext, dtMock1.Object, transactionMapperMock.Object);

                var result = await sut.SendUpdatedTransactionAsync(exampleTransactionDto);

                Assert.AreEqual(assertContext.Transactions.Count(), 1);
                Assert.AreEqual(assertContext.Transactions.First().Id, exampleSavedTransaction.Id);
                Assert.AreEqual(assertContext.Transactions.First().SenderAccountId, exampleSavedTransaction.SenderAccountId);
                Assert.AreEqual(assertContext.Transactions.First().ReceiverAccountId, exampleSavedTransaction.ReceiverAccountId);
                Assert.AreEqual(assertContext.Transactions.First().ReceiverAccount.AccountNumber, exampleSavedTransaction.ReceiverAccount.AccountNumber);
                Assert.AreEqual(assertContext.Transactions.First().Amount, exampleSavedTransaction.Amount);
                Assert.AreEqual(assertContext.Transactions.First().Description, exampleSavedTransaction.Description);
                Assert.AreEqual(assertContext.Transactions.First().TimeStamp, exampleSavedTransaction.TimeStamp);
                Assert.AreEqual(assertContext.Transactions.First().StatusId, 2);
                Assert.AreEqual(assertContext.Accounts.First().Balance, (exampleAccount.Balance - exampleSavedTransaction.Amount));
                Assert.AreEqual(assertContext.Accounts.Skip(1).First().Balance, (exampleAccount.Balance + exampleSavedTransaction.Amount));
                Assert.IsInstanceOfType(assertContext.Transactions.First(), typeof(Transaction));
            }
        }

        [TestMethod]
        public async Task SendUpdatedTransactionAsync_Should_ReturnCorrectTransaction()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                 .UseInMemoryDatabase(nameof(SendUpdatedTransactionAsync_Should_ReturnCorrectTransaction))
                                 .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                 .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountNumber = "1234567890",
                    SenderAccountNickName = "ExampleNickName",
                    SenderClientName = "ExampleSenderClient",
                    ReceiverAccountNumber = "1233367890",
                    ReceiverClientName = "ExampleReceiverClient",
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusName = "Saved",
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();
                transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionDto>())).Returns(exampleSavedTransaction);

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var result = await sut.SendUpdatedTransactionAsync(exampleTransactionDto);

                Assert.AreEqual(result.Id, exampleSavedTransaction.Id);
                Assert.AreEqual(result.Amount, exampleSavedTransaction.Amount);
                Assert.AreEqual(result.Description, exampleSavedTransaction.Description);
                Assert.AreEqual(result.TimeStamp, exampleSavedTransaction.TimeStamp);
                Assert.AreEqual(result.SenderAccountId, exampleSavedTransaction.SenderAccountId);
                Assert.AreEqual(result.ReceiverAccountId, exampleSavedTransaction.ReceiverAccountId);
                Assert.AreEqual(result.ReceiverAccountNumber, exampleSavedTransaction.ReceiverAccount.AccountNumber);
                Assert.IsInstanceOfType(result, typeof(TransactionDto));
            }
        }

        [TestMethod]
        public async Task SendUpdatedTransactionAsync_ThrowsBusinessLogicException_IfSenderAccountIsNull()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                 .UseInMemoryDatabase(nameof(SendUpdatedTransactionAsync_ThrowsBusinessLogicException_IfSenderAccountIsNull))
                                 .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                 .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 2,
                    Amount = 10000,
                    SenderAccountId = 3,
                    ReceiverAccountId = 20,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 4, 10),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 2,
                    Amount = 10000,
                    SenderAccountId = 3,
                    ReceiverAccountId = 20,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 4, 10),
                    ReceiverAccountNumber = "1133367890",
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.SendUpdatedTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ExceptionMessages.SenderAccountNull, ex.Message);
            }
        }

        [TestMethod]
        public async Task SendUpdatedTransactionAsync_Should_ThrowBusinessLogicException_IfReceiverAccountIsNull()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                .UseInMemoryDatabase(nameof(SendUpdatedTransactionAsync_Should_ThrowBusinessLogicException_IfReceiverAccountIsNull))
                                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 3,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 5, 10),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 3,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 5, 10),
                    ReceiverAccountNumber = "1133367890",
                };


                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.SendUpdatedTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.ReceiverAccountNull);
            }
        }

        [TestMethod]
        public async Task SendUpdatedTransactionAsync_Should_ThrowBusinessLogicException_IfSenderAndReceiverSame()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                 .UseInMemoryDatabase(nameof(SendUpdatedTransactionAsync_Should_ThrowBusinessLogicException_IfSenderAndReceiverSame))
                                 .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                 .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 9,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 1,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 1, AccountNumber = "1234567890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 9,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 1,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    ReceiverAccountNumber = "1234567890",
                };


                var dtMock4 = new Mock<IDateTimeProvider>();
                dtMock4.Setup(x => x.GetDateTimeNow()).Returns(transaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock4.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>

                await sut.SendUpdatedTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.SameAccountsForTransaction);
            }
        }

        [TestMethod]
        public async Task SendUpdatedTransactionAsync_Should_ThrowBusinessLogicException_IfNoSuchTransaction()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                 .UseInMemoryDatabase(nameof(SendUpdatedTransactionAsync_Should_ThrowBusinessLogicException_IfNoSuchTransaction))
                                 .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                 .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "2233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1234567890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 22,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    ReceiverAccountNumber = "2233367890",
                };


                var dtMock4 = new Mock<IDateTimeProvider>();
                dtMock4.Setup(x => x.GetDateTimeNow()).Returns(transaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock4.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                 await sut.SendUpdatedTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.TransactionNull);
            }
        }

        [TestMethod]
        public async Task SendUpdatedTransactionAsync_Should_ThrowBusinessLogicException_IfTransactionAlreadySent()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                  .UseInMemoryDatabase(nameof(SendUpdatedTransactionAsync_Should_ThrowBusinessLogicException_IfTransactionAlreadySent))
                                  .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                  .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "2233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 2,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1234567890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    ReceiverAccountNumber = "2233367890",
                };


                var dtMock4 = new Mock<IDateTimeProvider>();
                dtMock4.Setup(x => x.GetDateTimeNow()).Returns(transaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock4.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                 await sut.SendUpdatedTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.CannotEditSentTransaction);
            }
        }

        [TestMethod]
        public async Task SendUpdatedTransactionAsync_Should_ThrowBusinessLogicException_IfInsufficientBalance()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                .UseInMemoryDatabase(nameof(SendUpdatedTransactionAsync_Should_ThrowBusinessLogicException_IfInsufficientBalance))
                                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var accountTwo = new Account()
                {
                    Id = 2,
                    AccountNumber = "2233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 2,
                    SenderAccount = accountOne,
                    ReceiverAccount = accountTwo
                };

                var accountToAdd = arrangeContext.Accounts.Add(accountOne);
                var accountToAdd2 = arrangeContext.Accounts.Add(accountTwo);
                var transactionToAdd = arrangeContext.Transactions.Add(exampleTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1234567890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 102033,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    ReceiverAccountNumber = "2233367890",
                };

                var dtMock4 = new Mock<IDateTimeProvider>();
                dtMock4.Setup(x => x.GetDateTimeNow()).Returns(transaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock4.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                 await sut.SendUpdatedTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.NotEnoughFromSenderBalance);
            }
        }

        [TestMethod]
        public async Task SaveTransactionAsync_Should_CorrectlySaveTransaction()
        {
            var options = TestUtils.GetOptions(nameof(SaveTransactionAsync_Should_CorrectlySaveTransaction));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                arrangeContext.Accounts.Add(exampleAccount);
                arrangeContext.Accounts.Add(exampleAccount2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    ReceiverAccountNumber = "1233367890",
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();
                transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionDto>())).Returns(exampleSavedTransaction);

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var result = await sut.SaveTransactionAsync(exampleTransactionDto);

                Assert.AreEqual(assertContext.Transactions.Count(), 1);
                Assert.AreEqual(assertContext.Transactions.First().Id, exampleSavedTransaction.Id);
                Assert.AreEqual(assertContext.Transactions.First().SenderAccountId, exampleSavedTransaction.SenderAccountId);
                Assert.AreEqual(assertContext.Transactions.First().ReceiverAccountId, exampleSavedTransaction.ReceiverAccountId);
                Assert.AreEqual(assertContext.Transactions.First().Amount, exampleSavedTransaction.Amount);
                Assert.AreEqual(assertContext.Transactions.First().Description, exampleSavedTransaction.Description);
                Assert.AreEqual(assertContext.Transactions.First().TimeStamp, exampleSavedTransaction.TimeStamp);
                Assert.AreEqual(assertContext.Transactions.First().StatusId, 1);
                Assert.IsInstanceOfType(assertContext.Transactions.First(), typeof(Transaction));
            }
        }

        [TestMethod]
        public async Task SaveTransactionAsync_Should_ReturnCorrectTransaction()
        {
            var options = TestUtils.GetOptions(nameof(SaveTransactionAsync_Should_ReturnCorrectTransaction));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                arrangeContext.Accounts.Add(exampleAccount);
                arrangeContext.Accounts.Add(exampleAccount2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountNumber = "1234567890",
                    SenderAccountNickName = "ExampleNickName",
                    SenderClientName = "ExampleSenderClient",
                    ReceiverAccountNumber = "1233367890",
                    ReceiverClientName = "ExampleReceiverClient",
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusName = "Saved",
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();
                transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionDto>())).Returns(exampleSavedTransaction);

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var result = await sut.SaveTransactionAsync(exampleTransactionDto);

                Assert.AreEqual(result.Id, exampleSavedTransaction.Id);
                Assert.AreEqual(result.Amount, exampleSavedTransaction.Amount);
                Assert.AreEqual(result.Description, exampleSavedTransaction.Description);
                Assert.AreEqual(result.TimeStamp, exampleSavedTransaction.TimeStamp);
                Assert.AreEqual(result.SenderAccountId, exampleSavedTransaction.SenderAccountId);
                Assert.AreEqual(result.ReceiverAccountId, exampleSavedTransaction.ReceiverAccountId);
                Assert.AreEqual(result.ReceiverAccountNumber, exampleSavedTransaction.ReceiverAccount.AccountNumber);
                Assert.IsInstanceOfType(result, typeof(TransactionDto));
            }
        }

        [TestMethod]
        public async Task SaveTransactionAsync_Should_CallTransactionDtoMapper_Once()
        {
            var options = TestUtils.GetOptions(nameof(SaveTransactionAsync_Should_CallTransactionDtoMapper_Once));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                arrangeContext.Accounts.Add(exampleAccount);
                arrangeContext.Accounts.Add(exampleAccount2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    ReceiverAccountNumber = "1233367890",
                };


                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();
                transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionDto>())).Returns(exampleSavedTransaction);

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var result = await sut.SaveTransactionAsync(exampleTransactionDto);

                transactionMapperMock.Verify(x => x.MapFrom(It.IsAny<TransactionDto>()), Times.Once);
            }
        }

        [TestMethod]
        public async Task SaveTransactionAsync_Should_ThrowBusinessLogicException_IfSenderAccount_IsNull()
        {
            var options = TestUtils.GetOptions(nameof(SaveTransactionAsync_Should_ThrowBusinessLogicException_IfSenderAccount_IsNull));

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 9,
                    AccountNumber = "6258567870",
                    Balance = 102,
                    ClientId = 1,
                    NickName = "ExampleNickName9"
                };

                arrangeContext.Accounts.Add(accountOne);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    ReceiverAccountNumber = "1233367890",
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();
                transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionDto>())).Returns(exampleSavedTransaction);

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.SaveTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.SenderAccountNull);
            }
        }

        [TestMethod]
        public async Task SaveTransactionAsync_Should_ThrowBusinessLogicException_IfSenderAndReceiverSame()
        {
            var options = TestUtils.GetOptions(nameof(SaveTransactionAsync_Should_ThrowBusinessLogicException_IfSenderAndReceiverSame));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var account = exampleAccount;
                arrangeContext.Accounts.Add(account);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 9,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 1,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 1, AccountNumber = "1234567890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 9,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 1,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    ReceiverAccountNumber = "1234567890",
                };


                var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(transaction.TimeStamp);

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>

                await sut.SaveTransactionAsync(exampleTransactionDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.SameAccountsForTransaction);
            }
        }

        [TestMethod]
        public async Task SaveTransactionAsync_Should_ThrowBusinessLogicException_IfReceiverAccountIsNull()
        {
            var options = TestUtils.GetOptions(nameof(SaveTransactionAsync_Should_ThrowBusinessLogicException_IfReceiverAccountIsNull));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                arrangeContext.Accounts.Add(exampleAccount);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    ReceiverAccountNumber = "1233367890",
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>

                await sut.SaveTransactionAsync(exampleTransactionDto));
                Assert.AreEqual(ex.Message, ExceptionMessages.ReceiverAccountNull);
            }
        }

        [TestMethod]
        public async Task SendTransactionAsync_Should_CorrectlySendTransaction()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                .UseInMemoryDatabase(nameof(SendTransactionAsync_Should_CorrectlySendTransaction))
                                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var accountToAdd = arrangeContext.Accounts.Add(exampleAccount);
                var accountToAdd2 = arrangeContext.Accounts.Add(exampleAccount2);
                arrangeContext.Transactions.Add(exampleSavedTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
                var transactionMapperMock = new Mock<ITransactionDtoMapper>();
                transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionDto>())).Returns(exampleSavedTransaction);

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var result = await sut.SendTransactionAsync(1);

                Assert.AreEqual(assertContext.Transactions.Count(), 1);
                Assert.AreEqual(assertContext.Transactions.First().StatusId, 2);
                Assert.AreEqual(assertContext.Transactions.First().Id, exampleSavedTransaction.Id);
                Assert.AreEqual(assertContext.Transactions.First().SenderAccountId, exampleSavedTransaction.SenderAccountId);
                Assert.AreEqual(assertContext.Transactions.First().ReceiverAccountId, exampleSavedTransaction.ReceiverAccountId);
                Assert.AreEqual(assertContext.Transactions.First().Amount, exampleSavedTransaction.Amount);
                Assert.AreEqual(assertContext.Transactions.First().Description, exampleSavedTransaction.Description);
                Assert.AreEqual(assertContext.Transactions.First().TimeStamp, exampleSavedTransaction.TimeStamp);
                Assert.AreEqual(assertContext.Accounts.First().Balance, (exampleAccount.Balance - exampleSavedTransaction.Amount));
                Assert.AreEqual(assertContext.Accounts.Skip(1).First().Balance, (exampleAccount.Balance + exampleSavedTransaction.Amount));
                Assert.IsInstanceOfType(assertContext.Transactions.First(), typeof(Transaction));
            }
        }

        [TestMethod]
        public async Task SendTransactionAsync_Should_ReturnCorrectTransaction()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                 .UseInMemoryDatabase(nameof(SendTransactionAsync_Should_ReturnCorrectTransaction))
                                 .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                 .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                arrangeContext.Accounts.Add(exampleAccount);
                arrangeContext.Accounts.Add(exampleAccount2);

                arrangeContext.Transactions.Add(exampleSavedTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountNumber = "1234567890",
                    SenderAccountNickName = "ExampleNickName",
                    SenderClientName = "ExampleSenderClient",
                    ReceiverAccountNumber = "1233367890",
                    ReceiverClientName = "ExampleReceiverClient",
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusName = "Saved",
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
              var transactionMapperMock = new Mock<ITransactionDtoMapper>();
                transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<Transaction>())).Returns(exampleTransactionDto);


                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var result = await sut.SendTransactionAsync(1);

                Assert.AreEqual(result.Id, exampleSavedTransaction.Id);
                Assert.AreEqual(result.Amount, exampleSavedTransaction.Amount);
                Assert.AreEqual(result.Description, exampleSavedTransaction.Description);
                Assert.AreEqual(result.TimeStamp, exampleSavedTransaction.TimeStamp);
                Assert.AreEqual(result.SenderAccountId, exampleSavedTransaction.SenderAccountId);
                Assert.AreEqual(result.ReceiverAccountId, exampleSavedTransaction.ReceiverAccountId);
                Assert.AreEqual(result.ReceiverAccountNumber, exampleSavedTransaction.ReceiverAccount.AccountNumber);
                Assert.IsInstanceOfType(result, typeof(TransactionDto));
            }
        }

        [TestMethod]
        public async Task SendTransactionAsync_Should_CallTransactionDtoMapperMapFrom_Once()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                               .UseInMemoryDatabase(nameof(SendTransactionAsync_Should_CallTransactionDtoMapperMapFrom_Once))
                               .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                               .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                arrangeContext.Accounts.Add(exampleAccount);
                arrangeContext.Accounts.Add(exampleAccount2);

                arrangeContext.Transactions.Add(exampleSavedTransaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
              var transactionMapperMock = new Mock<ITransactionDtoMapper>();
                transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<Transaction>())).Verifiable();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var result = await sut.SendTransactionAsync(1);

                transactionMapperMock.Verify(x => x.MapFrom(It.IsAny<Transaction>()), Times.Once);
            }
        }

        [TestMethod]
        public async Task SendTransactionAsync_Should_ThrowBusinessLogicException_When_TransactionToSendIsNull()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                .UseInMemoryDatabase(nameof(SendTransactionAsync_Should_ThrowBusinessLogicException_When_TransactionToSendIsNull))
                                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                arrangeContext.Accounts.Add(exampleAccount);
                arrangeContext.Accounts.Add(exampleAccount2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(exampleSavedTransaction.TimeStamp);
              var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                   await sut.SendTransactionAsync(7));

                Assert.AreEqual(ex.Message, ExceptionMessages.TransactionNull);
            }
        }

        [TestMethod]
        public async Task SendTransactionAsync_Should_BusinessLogicException_When_SenderAccountIsNull()
        {

            var options = new DbContextOptionsBuilder<TransactContext>()
                                 .UseInMemoryDatabase(nameof(SendTransactionAsync_Should_BusinessLogicException_When_SenderAccountIsNull))
                                 .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                 .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleSavedTransaction = new Transaction()
                {
                    Id = 40,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 4, 15),
                    StatusId = 1                 
                };

                arrangeContext.Transactions.Add(exampleSavedTransaction);               

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {              
                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2016, 4, 15));
              var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                   await sut.SendTransactionAsync(40));

                Assert.AreEqual(ex.Message, ExceptionMessages.SenderAccountNull);
            }
        }

        [TestMethod]
        public async Task SendTransactionAsync_Should_BusinessLogicException_When_ReceiverAccountIsNull()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                 .UseInMemoryDatabase(nameof(SendTransactionAsync_Should_BusinessLogicException_When_ReceiverAccountIsNull))
                                 .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                 .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var account = new Account()
                {
                    Id = 5,
                    AccountNumber = "2255567890",
                    Balance = 1200,
                    ClientId = 1,
                    NickName = "ExampleNickName5"
                };

                var transaction = new Transaction()
                {
                    Id = 5,
                    Amount = 104,
                    SenderAccountId = 5,
                    ReceiverAccountId = 40,
                    Description = "example Description5",
                    TimeStamp = new DateTime(2012, 2, 9)              
                };

                arrangeContext.Accounts.Add(account);
                arrangeContext.Transactions.Add(transaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2012, 2, 9));
              var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                   await sut.SendTransactionAsync(5));

                Assert.AreEqual(ex.Message, ExceptionMessages.ReceiverAccountNull);
            }
        }

        [TestMethod]
        public async Task SendTransactionAsync_Should_BusinessLogicException_When_SenderHasInsufficientFunds()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                .UseInMemoryDatabase(nameof(SendTransactionAsync_Should_BusinessLogicException_When_SenderHasInsufficientFunds))
                                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 6,
                    AccountNumber = "6255567890",
                    Balance = 100,
                    ClientId = 1,
                    NickName = "ExampleNickName6"
                };

                var accountTwo = new Account()
                {
                    Id = 7,
                    AccountNumber = "6255567870",
                    Balance = 1070,
                    ClientId = 1,
                    NickName = "ExampleNickName7"
                };

                var transaction = new Transaction()
                {
                    Id = 6,
                    Amount = 102400,
                    SenderAccountId = 6,
                    ReceiverAccountId = 7,
                    Description = "example Description6",
                    TimeStamp = new DateTime(2010, 3, 3),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890" }
                };

                arrangeContext.Accounts.Add(accountOne);
                arrangeContext.Accounts.Add(accountTwo);
                arrangeContext.Transactions.Add(transaction);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2010, 3, 3));
              var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                   await sut.SendTransactionAsync(6));

                Assert.AreEqual(ex.Message, ExceptionMessages.NotEnoughFromSenderBalance);
            }
        }

        [TestMethod]
        public async Task SendTransactionAsync_Should_ThrowBusinessLogicException_IfSenderAndReceiverSame()
        {
            var options = new DbContextOptionsBuilder<TransactContext>()
                                 .UseInMemoryDatabase(nameof(SendTransactionAsync_Should_ThrowBusinessLogicException_IfSenderAndReceiverSame))
                                 .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                 .Options;

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var transaction = new Transaction()
                {
                    Id = 9,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 1,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 1, AccountNumber = "1234567890" }
                };

                arrangeContext.Accounts.Add(exampleAccount);
                arrangeContext.Transactions.Add(transaction);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 9,
                    Amount = 1020,
                    SenderAccountId = 1,
                    ReceiverAccountId = 1,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2012, 7, 15),
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 1, AccountNumber = "1234567890" }
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(transaction.TimeStamp);
              var transactionMapperMock = new Mock<ITransactionDtoMapper>();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.SendTransactionAsync(9));

                Assert.AreEqual(ex.Message, ExceptionMessages.SameAccountsForTransaction);
            }
        }

        [TestMethod]
        public async Task GetPageCountForTransactionsAsync_Should_ReturnProperPageCount_ForAllAccounts()
        {
            var options = TestUtils.GetOptions(nameof(GetPageCountForTransactionsAsync_Should_ReturnProperPageCount_ForAllAccounts));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction3 = new Transaction()
                {
                    Id = 3,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description3",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction4 = new Transaction()
                {
                    Id = 4,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description4",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction5 = new Transaction()
                {
                    Id = 5,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description5",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction6 = new Transaction()
                {
                    Id = 6,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description6",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var transactionToAdd = await arrangeContext.Transactions.AddAsync(exampleTransaction);
                var transactionToAdd2 = await arrangeContext.Transactions.AddAsync(exampleTransaction2);
                var transactionToAdd3 = await arrangeContext.Transactions.AddAsync(exampleTransaction3);
                var transactionToAdd4 = await arrangeContext.Transactions.AddAsync(exampleTransaction4);
                var transactionToAdd5 = await arrangeContext.Transactions.AddAsync(exampleTransaction5);
                var transactionToAdd6 = await arrangeContext.Transactions.AddAsync(exampleTransaction6);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleTransactionDto = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 1,
                    Description = "example Description",
                    TimeStamp = new DateTime(2012, 7, 15),
                    ReceiverAccountNumber = "1234567890",
                };

                var exampleAccountIds = new List<long>() { 1, 2 };

                var transactionMock = new Mock<ITransactionDtoMapper>();
                transactionMock.Setup(x => x.MapFrom(It.IsAny<Transaction>())).Returns(exampleTransactionDto);
                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2000, 1, 3));
                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMock.Object);

                var result = await sut.GetPageCountForTransactionsAsync(5, null, exampleAccountIds);

                Assert.AreEqual(result, 2);
            }
        }

        [TestMethod]
        public async Task GetPageCountForTransactionsAsync_Should_ReturnProperPageCount_ForSpecificAccount()
        {
            var options = TestUtils.GetOptions(nameof(GetPageCountForTransactionsAsync_Should_ReturnProperPageCount_ForSpecificAccount));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleTransaction5 = new Transaction()
                {
                    Id = 5,
                    Amount = 100,
                    SenderAccountId = 5,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 5 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction6 = new Transaction()
                {
                    Id = 6,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction7 = new Transaction()
                {
                    Id = 7,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description3",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };              

                var transactionToAdd = await arrangeContext.Transactions.AddAsync(exampleTransaction5);
                var transactionToAdd2 = await arrangeContext.Transactions.AddAsync(exampleTransaction6);
                var transactionToAdd3 = await arrangeContext.Transactions.AddAsync(exampleTransaction7);
             
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleTransactionDto5 = new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 5,
                    ReceiverAccountId = 1,
                    Description = "example Description",
                    TimeStamp = new DateTime(2022, 7, 15),
                    ReceiverAccountNumber = "1234567890",
                };

                var transactionMock = new Mock<ITransactionDtoMapper>();
                transactionMock.Setup(x => x.MapFrom(It.IsAny<Transaction>())).Returns(exampleTransactionDto5);
                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2002, 7, 5));
                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMock.Object);

                var result = await sut.GetPageCountForTransactionsAsync(2, 5, null);

                Assert.AreEqual(result, 1);
            }
        }

        [TestMethod]
        public async Task GetSingleTransactionAsync_Should_GetCorrectTransaction()
        {
            var options = TestUtils.GetOptions(nameof(GetSingleTransactionAsync_Should_GetCorrectTransaction));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleStatus = new Status()
                {
                    Id = 1,
                    StatusName = "Example"
                };

                var exampleStatus2 = new Status()
                {
                    Id = 2,
                    StatusName = "Example2"
                };

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = exampleAccount,
                    ReceiverAccount = exampleAccount2
                };

                var exampleTransaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = exampleAccount,
                    ReceiverAccount = exampleAccount2
                };

                var transactionToAdd = await arrangeContext.Transactions.AddAsync(exampleTransaction);
                var transactionToAdd2 = await arrangeContext.Transactions.AddAsync(exampleTransaction2);               
                var statusToAdd = await arrangeContext.Statuses.AddAsync(exampleStatus);
                var statusToAdd2 = await arrangeContext.Statuses.AddAsync(exampleStatus2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = exampleAccount,
                    ReceiverAccount = exampleAccount2
                };

                var exampleStatus = new Status()
                {
                    Id = 1,
                    StatusName = "Example"
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2002, 7, 5));

                var transactionDtoMapper = new TransactionDtoMapper();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionDtoMapper);

                var result = await sut.GetSingleTransactionAsync(1);

                Assert.IsInstanceOfType(result, typeof(TransactionDto));

                Assert.AreEqual(result.Id, exampleTransaction.Id);
                Assert.AreEqual(result.Amount, exampleTransaction.Amount);
                Assert.AreEqual(result.Description, exampleTransaction.Description);
                Assert.AreEqual(result.SenderAccountId, exampleAccount.Id);
                Assert.AreEqual(result.SenderAccountId, exampleAccount.Id);
                Assert.AreEqual(result.ReceiverAccountId, exampleAccount2.Id);
                Assert.AreEqual(result.StatusName, exampleStatus.StatusName);
                Assert.AreEqual(result.TimeStamp, exampleTransaction.TimeStamp);
            }
        }

        [TestMethod]
        public async Task GetSingleTransactionAsync_Should_ThrowBusinessLogicException_IfNoSuchTransaction()
        {
            var options = TestUtils.GetOptions(nameof(GetSingleTransactionAsync_Should_ThrowBusinessLogicException_IfNoSuchTransaction));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleStatus = new Status()
                {
                    Id = 1,
                    StatusName = "Example"
                };

                var exampleStatus2 = new Status()
                {
                    Id = 2,
                    StatusName = "Example2"
                };

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = exampleAccount,
                    ReceiverAccount = exampleAccount2
                };

                var exampleTransaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = exampleAccount,
                    ReceiverAccount = exampleAccount2
                };

                var transactionToAdd = await arrangeContext.Transactions.AddAsync(exampleTransaction);
                var transactionToAdd2 = await arrangeContext.Transactions.AddAsync(exampleTransaction2);
                var statusToAdd = await arrangeContext.Statuses.AddAsync(exampleStatus);
                var statusToAdd2 = await arrangeContext.Statuses.AddAsync(exampleStatus2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2002, 7, 5));

                var transactionDtoMapper = new TransactionDtoMapper();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionDtoMapper);


                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.GetSingleTransactionAsync(4));

                Assert.AreEqual(ExceptionMessages.TransactionNull, ex.Message);
            }
        }

        [TestMethod]
        public async Task GetSingleTransactionForEditingAsync_Should_GetCorrectTransaction()
        {
            var options = TestUtils.GetOptions(nameof(GetSingleTransactionForEditingAsync_Should_GetCorrectTransaction));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleStatus = new Status()
                {
                    Id = 1,
                    StatusName = "Example"
                };

                var exampleStatus2 = new Status()
                {
                    Id = 2,
                    StatusName = "Example2"
                };

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = exampleAccount,
                    ReceiverAccount = exampleAccount2
                };

                var exampleTransaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = exampleAccount,
                    ReceiverAccount = exampleAccount2
                };

                var transactionToAdd = await arrangeContext.Transactions.AddAsync(exampleTransaction);
                var transactionToAdd2 = await arrangeContext.Transactions.AddAsync(exampleTransaction2);
                var statusToAdd = await arrangeContext.Statuses.AddAsync(exampleStatus);
                var statusToAdd2 = await arrangeContext.Statuses.AddAsync(exampleStatus2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = exampleAccount,
                    ReceiverAccount = exampleAccount2
                };

                var exampleStatus = new Status()
                {
                    Id = 1,
                    StatusName = "Example"
                };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2002, 7, 5));

                var transactionDtoMapper = new TransactionDtoMapper();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionDtoMapper);

                var result = await sut.GetSingleTransactionForEditingAsync(1);

                Assert.IsInstanceOfType(result, typeof(TransactionDto));

                Assert.AreEqual(result.Id, exampleTransaction.Id);
                Assert.AreEqual(result.Amount, exampleTransaction.Amount);
                Assert.AreEqual(result.Description, exampleTransaction.Description);              
                Assert.AreEqual(result.SenderAccountId, exampleAccount.Id);
                Assert.AreEqual(result.ReceiverAccountId, exampleAccount2.Id);
                Assert.AreEqual(result.TimeStamp, exampleTransaction.TimeStamp);
            }
        }

        [TestMethod]
        public async Task GetSingleTransactionForEditingAsync_Should_ThrowBusinessLogicException_IfNoSuchTransaction()
        {
            var options = TestUtils.GetOptions(nameof(GetSingleTransactionForEditingAsync_Should_ThrowBusinessLogicException_IfNoSuchTransaction));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleStatus = new Status()
                {
                    Id = 1,
                    StatusName = "Example"
                };

                var exampleStatus2 = new Status()
                {
                    Id = 2,
                    StatusName = "Example2"
                };

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = exampleAccount,
                    ReceiverAccount = exampleAccount2
                };

                var exampleTransaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = exampleAccount,
                    ReceiverAccount = exampleAccount2
                };

                var transactionToAdd = await arrangeContext.Transactions.AddAsync(exampleTransaction);
                var transactionToAdd2 = await arrangeContext.Transactions.AddAsync(exampleTransaction2);
                var statusToAdd = await arrangeContext.Statuses.AddAsync(exampleStatus);
                var statusToAdd2 = await arrangeContext.Statuses.AddAsync(exampleStatus2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2002, 7, 5));

                var transactionDtoMapper = new TransactionDtoMapper();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionDtoMapper);


                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.GetSingleTransactionForEditingAsync(4));

                Assert.AreEqual(ExceptionMessages.TransactionNull, ex.Message);
            }
        }


        [TestMethod]
        public async Task GetSingleTransactionForEditingAsync_Should_ThrowArgumentException_IfTransactionAlreadySent()
        {
            var options = TestUtils.GetOptions(nameof(GetSingleTransactionForEditingAsync_Should_ThrowArgumentException_IfTransactionAlreadySent));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleStatus = new Status()
                {
                    Id = 1,
                    StatusName = "Example"
                };

                var exampleStatus2 = new Status()
                {
                    Id = 2,
                    StatusName = "Example2"
                };

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 2,
                    SenderAccount = exampleAccount,
                    ReceiverAccount = exampleAccount2
                };

                var exampleTransaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = exampleAccount,
                    ReceiverAccount = exampleAccount2
                };

                var transactionToAdd = await arrangeContext.Transactions.AddAsync(exampleTransaction);
                var transactionToAdd2 = await arrangeContext.Transactions.AddAsync(exampleTransaction2);
                var statusToAdd = await arrangeContext.Statuses.AddAsync(exampleStatus);
                var statusToAdd2 = await arrangeContext.Statuses.AddAsync(exampleStatus2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2002, 7, 5));

                var transactionDtoMapper = new TransactionDtoMapper();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionDtoMapper);


                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.GetSingleTransactionForEditingAsync(1));

                Assert.AreEqual(ExceptionMessages.CannotEditSentTransaction, ex.Message);
            }
        }

        [TestMethod]
        public async Task GetFiveUserTransactionsDescAsync_Should_ReturnCorrectTransactions()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveUserTransactionsDescAsync_Should_ReturnCorrectTransactions));

            var exampleTransaction = new Transaction()
            {
                Id = 1,
                Amount = 100,
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                Description = "example Description",
                TimeStamp = new DateTime(2016, 7, 15),
                StatusId = 1,               
                SenderAccount = new Account { Id = 1, AccountNumber = "1111111111" },
                ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890", ClientId = 1 }

            };

            var exampleClient = new Client()
            {
                Id = 1,
                Name = "ExampleClient"
            };

            var exampleUsersAccounts = new UsersAccounts()
            {
                UserId = 1,
                UserAccountNickname = "exampleNick",
                AccountId = 1
            };

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleStatus = new Status()
                {
                    Id = 1,
                    StatusName = "Example"
                };               

                var exampleStatus2 = new Status()
                {
                    Id = 2,
                    StatusName = "Example2"
                };               

                var exampleTransaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account
                    {
                        Id = 1,
                        AccountNumber = "1111111111",
                        Client = new Client()
                        {
                            Id = 1, Name = "exampleClientName"
                        }
                    },
                    ReceiverAccount = new Account
                    {
                        Id = 2,
                        AccountNumber = "1233367890",
                        Client = new Client()
                        {
                            Id = 2,
                            Name = "exampleClientName2"
                        }
                    }                    
                };

                var exampleTransaction3 = new Transaction()
                {
                    Id = 3,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description3",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction4 = new Transaction()
                {
                    Id = 4,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description4",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction5 = new Transaction()
                {
                    Id = 5,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description5",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction6 = new Transaction()
                {
                    Id = 6,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description6",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleClientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);
                var exampleUsersAccountsToAdd = await arrangeContext.UsersAccounts.AddAsync(exampleUsersAccounts);
                var transactionToAdd = await arrangeContext.Transactions.AddAsync(exampleTransaction);
                var transactionToAdd2 = await arrangeContext.Transactions.AddAsync(exampleTransaction2);
                var transactionToAdd3 = await arrangeContext.Transactions.AddAsync(exampleTransaction3);
                var transactionToAdd4 = await arrangeContext.Transactions.AddAsync(exampleTransaction4);
                var transactionToAdd5 = await arrangeContext.Transactions.AddAsync(exampleTransaction5);
                var transactionToAdd6 = await arrangeContext.Transactions.AddAsync(exampleTransaction6);
                var statusToAdd = await arrangeContext.Statuses.AddAsync(exampleStatus);
                var statusToAdd2 = await arrangeContext.Statuses.AddAsync(exampleStatus2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var accountsList = new List<long>() { 1, 2 };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2002, 7, 5));

                var transactionDtoMapper = new TransactionDtoMapper();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionDtoMapper);

                var result = await sut.GetFiveTransactionsForAccountDescAsync(1, 1, 1);

                Assert.AreEqual(result.Count, 5);
                Assert.AreEqual(result[0].Id, exampleTransaction.Id);
                Assert.AreEqual(result[0].SenderAccountNickName, exampleUsersAccounts.UserAccountNickname);
                Assert.AreEqual(result[0].SenderAccountNumber, exampleTransaction.SenderAccount.AccountNumber);
                Assert.AreEqual(result[0].ReceiverAccountNumber, exampleTransaction.ReceiverAccount.AccountNumber);
                Assert.AreEqual(result[0].Description, exampleTransaction.Description);
                Assert.AreEqual(result[0].Amount, exampleTransaction.Amount);
                Assert.AreEqual(result[0].StatusId, exampleTransaction.StatusId);
                Assert.AreEqual(result[1].Id, 2);
                Assert.AreEqual(result[2].Id, 3);
                Assert.AreEqual(result[3].Id, 4);
                Assert.AreEqual(result[4].Id, 5);

                Assert.IsInstanceOfType(result, typeof(IList<TransactionDto>));
            }
        }

        [TestMethod]
        public async Task GetFiveUserTransactionsDescAsync_Should_CallDtoMapperOnce()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveUserTransactionsDescAsync_Should_CallDtoMapperOnce));

            var exampleTransaction = new Transaction()
            {
                Id = 1,
                Amount = 100,
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                Description = "example Description",
                TimeStamp = new DateTime(2016, 7, 15),
                StatusId = 1,
                SenderAccount = new Account { Id = 1, AccountNumber = "1111111111" },
                ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890", ClientId = 1 }

            };

            var exampleClient = new Client()
            {
                Id = 1,
                Name = "ExampleClient"
            };

            var exampleUsersAccounts = new UsersAccounts()
            {
                UserId = 1,
                UserAccountNickname = "exampleNick",
                AccountId = 1
            };

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleStatus = new Status()
                {
                    Id = 1,
                    StatusName = "Example"
                };

                var exampleStatus2 = new Status()
                {
                    Id = 2,
                    StatusName = "Example2"
                };

                var exampleTransaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account
                    {
                        Id = 1,
                        AccountNumber = "1111111111",
                        Client = new Client()
                        {
                            Id = 1,
                            Name = "exampleClientName"
                        }
                    },
                    ReceiverAccount = new Account
                    {
                        Id = 2,
                        AccountNumber = "1233367890",
                        Client = new Client()
                        {
                            Id = 2,
                            Name = "exampleClientName2"
                        }
                    }
                };

                var exampleTransaction3 = new Transaction()
                {
                    Id = 3,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description3",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction4 = new Transaction()
                {
                    Id = 4,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description4",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction5 = new Transaction()
                {
                    Id = 5,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description5",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction6 = new Transaction()
                {
                    Id = 6,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description6",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleClientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);
                var exampleUsersAccountsToAdd = await arrangeContext.UsersAccounts.AddAsync(exampleUsersAccounts);
                var transactionToAdd = await arrangeContext.Transactions.AddAsync(exampleTransaction);
                var transactionToAdd2 = await arrangeContext.Transactions.AddAsync(exampleTransaction2);
                var transactionToAdd3 = await arrangeContext.Transactions.AddAsync(exampleTransaction3);
                var transactionToAdd4 = await arrangeContext.Transactions.AddAsync(exampleTransaction4);
                var transactionToAdd5 = await arrangeContext.Transactions.AddAsync(exampleTransaction5);
                var transactionToAdd6 = await arrangeContext.Transactions.AddAsync(exampleTransaction6);
                var statusToAdd = await arrangeContext.Statuses.AddAsync(exampleStatus);
                var statusToAdd2 = await arrangeContext.Statuses.AddAsync(exampleStatus2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleTransactionDtoList = new List<TransactionDto>()
                {
                    new TransactionDto { Id = 6 },
                    new TransactionDto { Id = 5 },
                    new TransactionDto { Id = 4 },
                    new TransactionDto { Id = 3 },
                    new TransactionDto { Id = 2 }
                };

                var accountsList = new List<long>() { 1, 2 };

                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2002, 7, 5));

                var transactionMock = new Mock<ITransactionDtoMapper>();
                transactionMock.Setup(x => x.MapFromWithPersonalNick(It.IsAny<List<Transaction>>())).Returns(exampleTransactionDtoList);

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionMock.Object);

                var result = await sut.GetFiveTransactionsForAccountDescAsync(1, 1, 1);

                transactionMock.Verify(x => x.MapFromWithPersonalNick(It.IsAny<List<Transaction>>()), Times.Once);
            }
        }      

        //[TestMethod]
        //public async Task GetFiveTransactionsForAccountDescAsync_Should_CallFromSqlMethod()
        //{
        //    SpAsyncEnumerableQueryable<Transaction> transactionsContext = new SpAsyncEnumerableQueryable<Transaction>
        //    {
        //        new Transaction()
        //        {
        //            Id = 1,
        //            Amount = 100,
        //            SenderAccountId = 1,
        //            ReceiverAccountId = 2,
        //            Description = "example Description",
        //            TimeStamp = new DateTime(2016, 7, 15),
        //            StatusId = 1,
        //            Status = new Status { Id = 1, StatusName = "Saved" },
        //            SenderAccount = new Account { Id = 1 },
        //            ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
        //        },

        //        new Transaction()
        //        {
        //            Id = 2,
        //            Amount = 100,
        //            SenderAccountId = 1,
        //            ReceiverAccountId = 2,
        //            Description = "example Description2",
        //            TimeStamp = new DateTime(2016, 7, 15),
        //            StatusId = 1,
        //            Status = new Status { Id = 1, StatusName = "Saved" },
        //            SenderAccount = new Account { Id = 1 },
        //            ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
        //        },

        //        new Transaction()
        //        {
        //            Id = 3,
        //            Amount = 100,
        //            SenderAccountId = 1,
        //            ReceiverAccountId = 2,
        //            Description = "example Description3",
        //            TimeStamp = new DateTime(2016, 7, 15),
        //            StatusId = 1,
        //            Status = new Status { Id = 1, StatusName = "Saved" },
        //            SenderAccount = new Account { Id = 1 },
        //            ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
        //        },
        //        new Transaction()
        //        {
        //            Id = 4,
        //            Amount = 100,
        //            SenderAccountId = 1,
        //            ReceiverAccountId = 2,
        //            Description = "example Description4",
        //            TimeStamp = new DateTime(2016, 7, 15),
        //            StatusId = 1,
        //            Status = new Status { Id = 1, StatusName = "Saved" },
        //            SenderAccount = new Account { Id = 1 },
        //            ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
        //        },
        //        new Transaction()
        //        {
        //            Id = 5,
        //            Amount = 100,
        //            SenderAccountId = 3,
        //            ReceiverAccountId = 4,
        //            Description = "example Description5",
        //            TimeStamp = new DateTime(2016, 7, 15),
        //            StatusId = 1,
        //            Status = new Status { Id = 1, StatusName = "Saved" },
        //            SenderAccount = new Account { Id = 3 },
        //            ReceiverAccount = new Account { Id = 4, AccountNumber = "1233367890" }
        //        }
        //    };

        //    var options = new DbContextOptionsBuilder<TransactContext>()
        //        .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
        //        .Options;

        //    var transactionDtoMapper = new TransactionDtoMapper();

        //    var dtMock = new Mock<IDateTimeProvider>();

        //    TransactContext context = new TransactContext(options);

        //    context.Transactions = context.Transactions.MockFromSql(transactionsContext);

        //    TransactionServices sut = new TransactionServices(context, dtMock.Object, transactionDtoMapper);

        //    var result = new List<TransactionDto>(await sut
        //        .GetFiveTransactionsForAccountDescAsync
        //        (1, 0, 1));


        //    Assert.AreEqual(result.First().Id, 1);
        //    Assert.AreEqual(result.Count(), 5);
        //    Assert.AreEqual(result[0].Id, 1);
        //    Assert.AreEqual(result[1].Id, 2);
        //    Assert.AreEqual(result[2].Id, 3);
        //    Assert.AreEqual(result[3].Id, 4);
        //    Assert.IsInstanceOfType(result, typeof(IList<TransactionDto>));            
        //}

        //[TestMethod]
        //public async Task GetFiveTransactionsForAccountDescAsync_Should_CallTransactionDtoMapper_MapFromWithPersonalNick_Once()
        //{
        //    SpAsyncEnumerableQueryable<Transaction> transactionsContext = new SpAsyncEnumerableQueryable<Transaction>
        //    {
        //        new Transaction()
        //        {
        //            Id = 1,
        //            Amount = 100,
        //            SenderAccountId = 1,
        //            ReceiverAccountId = 2,
        //            Description = "example Description",
        //            TimeStamp = new DateTime(2016, 7, 15),
        //            StatusId = 1,
        //            Status = new Status { Id = 1, StatusName = "Saved" },
        //            SenderAccount = new Account { Id = 1 },
        //            ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
        //        },

        //        new Transaction()
        //        {
        //            Id = 2,
        //            Amount = 100,
        //            SenderAccountId = 1,
        //            ReceiverAccountId = 2,
        //            Description = "example Description2",
        //            TimeStamp = new DateTime(2016, 7, 15),
        //            StatusId = 1,
        //            Status = new Status { Id = 1, StatusName = "Saved" },
        //            SenderAccount = new Account { Id = 1 },
        //            ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
        //        },

        //        new Transaction()
        //        {
        //            Id = 3,
        //            Amount = 100,
        //            SenderAccountId = 1,
        //            ReceiverAccountId = 2,
        //            Description = "example Description3",
        //            TimeStamp = new DateTime(2016, 7, 15),
        //            StatusId = 1,
        //            Status = new Status { Id = 1, StatusName = "Saved" },
        //            SenderAccount = new Account { Id = 1 },
        //            ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
        //        },
        //        new Transaction()
        //        {
        //            Id = 4,
        //            Amount = 100,
        //            SenderAccountId = 1,
        //            ReceiverAccountId = 2,
        //            Description = "example Description4",
        //            TimeStamp = new DateTime(2016, 7, 15),
        //            StatusId = 1,
        //            Status = new Status { Id = 1, StatusName = "Saved" },
        //            SenderAccount = new Account { Id = 1 },
        //            ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
        //        },
        //        new Transaction()
        //        {
        //            Id = 5,
        //            Amount = 100,
        //            SenderAccountId = 3,
        //            ReceiverAccountId = 4,
        //            Description = "example Description5",
        //            TimeStamp = new DateTime(2016, 7, 15),
        //            StatusId = 1,
        //            Status = new Status { Id = 1, StatusName = "Saved" },
        //            SenderAccount = new Account { Id = 3 },
        //            ReceiverAccount = new Account { Id = 4, AccountNumber = "1233367890" }
        //        }
        //    };

        //    var exampleTransactionDtoList = new List<TransactionDto>()
        //        {
        //            new TransactionDto { Id = 5 },
        //            new TransactionDto { Id = 4 },
        //            new TransactionDto { Id = 3 },
        //            new TransactionDto { Id = 2 },
        //            new TransactionDto { Id = 1 }
        //        };

        //    var options = new DbContextOptionsBuilder<TransactContext>()
        //        .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
        //        .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
        //        .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
        //        .Options;

        //    var transactionDtoMapperMock = new Mock<ITransactionDtoMapper>();

        //    transactionDtoMapperMock.Setup(x => x.MapFromWithPersonalNick(It.IsAny<List<Transaction>>())).Returns(exampleTransactionDtoList);

        //    var dtMock = new Mock<IDateTimeProvider>();

        //    TransactContext context = new TransactContext(options);

        //    context.Transactions = context.Transactions.MockFromSql(transactionsContext);

        //    TransactionServices sut = new TransactionServices(context, dtMock.Object, transactionDtoMapperMock.Object);

        //    var result = new List<TransactionDto>(await sut
        //        .GetFiveTransactionsForAccountDescAsync
        //        (1, 0, 1));


        //    transactionDtoMapperMock.Verify(x => x.MapFromWithPersonalNick(It.IsAny<List<Transaction>>()), Times.Once);
        //}
       

        [TestMethod]
        public async Task GetFiveTransactionsForAccountDescAsync_Should_ThrowAccountNotAvailableException_IfUserDoesNotOwnAccount()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveTransactionsForAccountDescAsync_Should_ThrowAccountNotAvailableException_IfUserDoesNotOwnAccount));

            using (var arrangeContext = new TransactContext(options))
            {            
                var exampleStatus = new Status()
                {
                    Id = 1,
                    StatusName = "Example"
                };

                var exampleStatus2 = new Status()
                {
                    Id = 2,
                    StatusName = "Example2"
                };

                var exampleTransaction = new Transaction()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction3 = new Transaction()
                {
                    Id = 3,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description3",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction4 = new Transaction()
                {
                    Id = 4,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description4",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 1 },
                    ReceiverAccount = new Account { Id = 2, AccountNumber = "1233367890" }
                };

                var exampleTransaction5 = new Transaction()
                {
                    Id = 5,
                    Amount = 100,
                    SenderAccountId = 3,
                    ReceiverAccountId = 4,
                    Description = "example Description5",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 3 },
                    ReceiverAccount = new Account { Id = 4, AccountNumber = "1233367890" }
                };

                var exampleTransaction6 = new Transaction()
                {
                    Id = 6,
                    Amount = 100,
                    SenderAccountId = 3,
                    ReceiverAccountId = 4,
                    Description = "example Description6",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusId = 1,
                    SenderAccount = new Account { Id = 3 },
                    ReceiverAccount = new Account { Id = 4, AccountNumber = "1233367890" }
                };

                var transactionToAdd = await arrangeContext.Transactions.AddAsync(exampleTransaction);
                var transactionToAdd2 = await arrangeContext.Transactions.AddAsync(exampleTransaction2);
                var transactionToAdd3 = await arrangeContext.Transactions.AddAsync(exampleTransaction3);
                var transactionToAdd4 = await arrangeContext.Transactions.AddAsync(exampleTransaction4);
                var transactionToAdd5 = await arrangeContext.Transactions.AddAsync(exampleTransaction5);
                var transactionToAdd6 = await arrangeContext.Transactions.AddAsync(exampleTransaction6);
                var statusToAdd = await arrangeContext.Statuses.AddAsync(exampleStatus);
                var statusToAdd2 = await arrangeContext.Statuses.AddAsync(exampleStatus2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var dtMock = new Mock<IDateTimeProvider>();
                dtMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2002, 7, 5));

                var transactionDtoMapper = new TransactionDtoMapper();

                var sut = new TransactionServices(assertContext, dtMock.Object, transactionDtoMapper);

                var ex = await Assert.ThrowsExceptionAsync<AccountNotAvailableException>(async () =>
                 await sut.GetFiveTransactionsForAccountDescAsync(1, 1, 1));

                Assert.AreEqual(ExceptionMessages.AccountNoLongerAvailable, ex.Message);
            }
        }       
    }
}
