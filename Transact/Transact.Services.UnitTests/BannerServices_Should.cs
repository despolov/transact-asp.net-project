﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transact.Data;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Services.CustomExceptions;
using Transact.Services.Mapper;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services.UnitTests
{
    [TestClass]
    public class BannerServices_Should
    {
        [TestMethod]
        public async Task SaveBannerAsyncAsync_Should_CorrectlyAddBanner()
        {
            var options = TestUtils.GetOptions(nameof(SaveBannerAsyncAsync_Should_CorrectlyAddBanner));
            var exampleImageName = new Guid().ToString();

            var randomizerMock = new Mock<IRandomizer>();

            using (var assertContext = new TransactContext(options))
            {
                var exampleBannerDto = new BannerDto()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };              

                var bannerMapper = new BannerDtoMapper();

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var result = await sut.SaveBannerAsync(exampleBannerDto);

                Assert.AreEqual(assertContext.Banners.Count(), 1);
                Assert.AreEqual(assertContext.Banners.First().Id, exampleBannerDto.Id);
                Assert.AreEqual(assertContext.Banners.First().Url, exampleBannerDto.Url);
                Assert.AreEqual(assertContext.Banners.First().StartDate, exampleBannerDto.StartDate);
                Assert.AreEqual(assertContext.Banners.First().EndDate, exampleBannerDto.EndDate);
                Assert.AreEqual(assertContext.Banners.First().ImageName, exampleBannerDto.ImageName);
                Assert.IsInstanceOfType(assertContext.Banners.First(), typeof(Banner));
            }
        }

        [TestMethod]
        public async Task SaveBannerAsyncAsync_Should_CorrectlyReturnBannerDto()
        {
            var options = TestUtils.GetOptions(nameof(SaveBannerAsyncAsync_Should_CorrectlyReturnBannerDto));
            var exampleImageName = new Guid().ToString();
            var randomizerMock = new Mock<IRandomizer>();

            using (var assertContext = new TransactContext(options))
            {
                var exampleBannerDto = new BannerDto()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };

                var bannerMapper = new BannerDtoMapper();

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var result = await sut.SaveBannerAsync(exampleBannerDto);

                Assert.AreEqual(result.Id, exampleBannerDto.Id);
                Assert.AreEqual(result.Url, exampleBannerDto.Url);
                Assert.AreEqual(result.StartDate, exampleBannerDto.StartDate);
                Assert.AreEqual(result.EndDate, exampleBannerDto.EndDate);
                Assert.AreEqual(result.ImageName, exampleBannerDto.ImageName);
                Assert.IsInstanceOfType(result, typeof(BannerDto));
            }
        }

        [TestMethod]
        public async Task SaveBannerAsyncAsync_ThrowBusinessException_IfStartDateIsAfterEndDate()
        {
            var options = TestUtils.GetOptions(nameof(SaveBannerAsyncAsync_ThrowBusinessException_IfStartDateIsAfterEndDate));
            var exampleImageName = new Guid().ToString();
            var randomizerMock = new Mock<IRandomizer>();

            using (var assertContext = new TransactContext(options))
            {
                var exampleBannerDto = new BannerDto()
                {
                    Id = 1,
                    StartDate = new DateTime(2018, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };

                var bannerMapper = new BannerDtoMapper();

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                    await sut.SaveBannerAsync(exampleBannerDto));

                Assert.AreEqual(ExceptionMessages.BannerDateInvalid, ex.Message);
            }
        }

        [TestMethod]
        public async Task DeleteBanner_Should_CorrectlyDeleteBanner()
        {
            var options = TestUtils.GetOptions(nameof(DeleteBanner_Should_CorrectlyDeleteBanner));
            var exampleImageName = new Guid().ToString();
            var randomizerMock = new Mock<IRandomizer>();

            using (var arrangeContext = new TransactContext(options))
            {

                var exampleBanner = new Banner()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };              

                arrangeContext.Banners.Add(exampleBanner);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {

                var bannerMapper = new BannerDtoMapper();

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var result = await sut.DeleteBannerAsync(1);

                Assert.AreEqual(assertContext.Transactions.Count(), 0);               
            }
        }

        [TestMethod]
        public async Task DeleteBanner_Should_CorrectlyReturnBannerDto()
        {
            var options = TestUtils.GetOptions(nameof(DeleteBanner_Should_CorrectlyReturnBannerDto));
            var exampleImageName = new Guid().ToString();
            var randomizerMock = new Mock<IRandomizer>();

            using (var arrangeContext = new TransactContext(options))
            {

                var exampleBanner = new Banner()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };

                arrangeContext.Banners.Add(exampleBanner);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleBannerDto = new BannerDto()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };

                var bannerMapper = new BannerDtoMapper();

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var result = await sut.DeleteBannerAsync(1);

                Assert.AreEqual(result.Id, exampleBannerDto.Id);
                Assert.AreEqual(result.Url, exampleBannerDto.Url);
                Assert.AreEqual(result.StartDate, exampleBannerDto.StartDate);
                Assert.AreEqual(result.EndDate, exampleBannerDto.EndDate);
                Assert.AreEqual(result.ImageName, exampleBannerDto.ImageName);
                Assert.IsInstanceOfType(result, typeof(BannerDto));
            }
        }

        [TestMethod]
        public async Task DeleteBanner_ThrowsBusinessLogicException_IfNoSuchBanner()
        {
            var options = TestUtils.GetOptions(nameof(DeleteBanner_Should_CorrectlyDeleteBanner));
            var exampleImageName = new Guid().ToString();
            var randomizerMock = new Mock<IRandomizer>();

            using (var arrangeContext = new TransactContext(options))
            {

                var exampleBanner = new Banner()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };

                arrangeContext.Banners.Add(exampleBanner);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleBannerDto = new BannerDto()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };

                var bannerMapper = new BannerDtoMapper();

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                     await sut.DeleteBannerAsync(2));

                Assert.AreEqual(ExceptionMessages.BannerNull, ex.Message);
            }
        }

        [TestMethod]
        public async Task UpdateBanner_Should_CorrectlyUpdateBanner()
        {
            var options = TestUtils.GetOptions(nameof(UpdateBanner_Should_CorrectlyUpdateBanner));
            var exampleImageName = new Guid().ToString();
            var randomizerMock = new Mock<IRandomizer>();

            using (var arrangeContext = new TransactContext(options))
            {

                var exampleBanner = new Banner()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };

                arrangeContext.Banners.Add(exampleBanner);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleBannerDto2 = new BannerDto()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2019, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://exa33ple.com"
                };

                var bannerMapper = new BannerDtoMapper();

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var result = await sut.UpdateBannerAsync(exampleBannerDto2);

                Assert.AreEqual(assertContext.Banners.Count(), 1);
                Assert.AreEqual(assertContext.Banners.First().Id, exampleBannerDto2.Id);
                Assert.AreEqual(assertContext.Banners.First().Url, exampleBannerDto2.Url);
                Assert.AreEqual(assertContext.Banners.First().StartDate, exampleBannerDto2.StartDate);
                Assert.AreEqual(assertContext.Banners.First().EndDate, exampleBannerDto2.EndDate);
                Assert.AreEqual(assertContext.Banners.First().ImageName, exampleBannerDto2.ImageName);
                Assert.IsInstanceOfType(assertContext.Banners.First(), typeof(Banner));
            }
        }

        [TestMethod]
        public async Task UpdateBanner_Should_CorrectlyReturnBannerDto()
        {
            var options = TestUtils.GetOptions(nameof(UpdateBanner_Should_CorrectlyReturnBannerDto));
            var exampleImageName = new Guid().ToString();
            var randomizerMock = new Mock<IRandomizer>();

            using (var arrangeContext = new TransactContext(options))
            {

                var exampleBanner = new Banner()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };

                arrangeContext.Banners.Add(exampleBanner);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleBannerDto2 = new BannerDto()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2020, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://examp22le.com"
                };

                var bannerMapper = new BannerDtoMapper();

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var result = await sut.UpdateBannerAsync(exampleBannerDto2);

                Assert.AreEqual(result.Id, exampleBannerDto2.Id);
                Assert.AreEqual(result.Url, exampleBannerDto2.Url);
                Assert.AreEqual(result.StartDate, exampleBannerDto2.StartDate);
                Assert.AreEqual(result.EndDate, exampleBannerDto2.EndDate);
                Assert.AreEqual(result.ImageName, exampleBannerDto2.ImageName);
                Assert.IsInstanceOfType(result, typeof(BannerDto));
            }
        }

        [TestMethod]
        public async Task UpdateBanner_ThrowsBusinessLogicException_IfNoSuchBanner()
        {
            var options = TestUtils.GetOptions(nameof(UpdateBanner_ThrowsBusinessLogicException_IfNoSuchBanner));
            var exampleImageName = new Guid().ToString();
            var randomizerMock = new Mock<IRandomizer>();

            using (var arrangeContext = new TransactContext(options))
            {

                var exampleBanner = new Banner()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };

                arrangeContext.Banners.Add(exampleBanner);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleBannerDto2 = new BannerDto()
                {
                    Id = 2,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };

                var bannerMapper = new BannerDtoMapper();

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                     await sut.UpdateBannerAsync(exampleBannerDto2));

                Assert.AreEqual(ExceptionMessages.BannerNull, ex.Message);
            }
        }


        [TestMethod]
        public async Task UpdateBanner_ThrowsBusinessLogicException_IfStartDateAfterEndDate()
        {
            var options = TestUtils.GetOptions(nameof(UpdateBanner_ThrowsBusinessLogicException_IfStartDateAfterEndDate));
            var exampleImageName = new Guid().ToString();
            var randomizerMock = new Mock<IRandomizer>();

            using (var arrangeContext = new TransactContext(options))
            {

                var exampleBanner = new Banner()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };

                arrangeContext.Banners.Add(exampleBanner);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleBannerDto2 = new BannerDto()
                {
                    Id = 1,
                    StartDate = new DateTime(2019, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };

                var bannerMapper = new BannerDtoMapper();

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                     await sut.UpdateBannerAsync(exampleBannerDto2));

                Assert.AreEqual(ExceptionMessages.BannerDateInvalid, ex.Message);
            }
        }

        [TestMethod]
        public async Task GetActiveBannersImagePathsAsync_Should_Randomizer_GetInRandomOrder()
        {
            var options = TestUtils.GetOptions(nameof(GetActiveBannersImagePathsAsync_Should_Randomizer_GetInRandomOrder));
            var exampleImageName = new Guid().ToString();
            var randomizerMock = new Mock<IRandomizer>();

           

            var exampleBanner = new Banner()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2020, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner2 = new Banner()
            {
                Id = 2,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2016, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner3 = new Banner()
            {
                Id = 3,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2020, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner4 = new Banner()
            {
                Id = 4,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2020, 7, 16),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner5 = new Banner()
            {
                Id = 5,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2020, 7, 16),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner6 = new Banner()
            {
                Id = 6,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2020, 7, 16),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner7 = new Banner()
            {
                Id = 7,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2020, 7, 16),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner8 = new Banner()
            {
                Id = 8,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2020, 7, 16),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBannerList = new List<Banner>()
            {
                exampleBanner, exampleBanner2, exampleBanner3, exampleBanner4, exampleBanner5
            };

            using (var arrangeContext = new TransactContext(options))
            {
                arrangeContext.Banners.Add(exampleBanner);
                arrangeContext.Banners.Add(exampleBanner2);
                arrangeContext.Banners.Add(exampleBanner3);
                arrangeContext.Banners.Add(exampleBanner4);
                arrangeContext.Banners.Add(exampleBanner5);
                arrangeContext.Banners.Add(exampleBanner6);
                arrangeContext.Banners.Add(exampleBanner7);
                arrangeContext.Banners.Add(exampleBanner8);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var bannerMapper = new BannerDtoMapper();

                randomizerMock.Setup(x => x.GetInRandomOrder(It.IsAny<IQueryable<Banner>>())).ReturnsAsync(exampleBannerList);

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var result = new List<BannerDto>(await sut.GetActiveBannersImagePathsAsync(DateTime.Now));

                randomizerMock.Verify(x=>x.GetInRandomOrder(It.IsAny<IQueryable<Banner>>()), Times.Once);
            }
        }

        [TestMethod]
        public async Task GetFiveBannersByExpirationDateAsync_Should_CorrectlyGetBannerDtos()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveBannersByExpirationDateAsync_Should_CorrectlyGetBannerDtos));
            var exampleImageName = new Guid().ToString();
            var randomizerMock = new Mock<IRandomizer>();

            var exampleBanner = new Banner()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner2 = new Banner()
            {
                Id = 2,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2018, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner3 = new Banner()
            {
                Id = 3,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2019, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner4 = new Banner()
            {
                Id = 4,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2016, 7, 16),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner5 = new Banner()
            {
                Id = 5,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2020, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner6 = new Banner()
            {
                Id = 6,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2021, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            using (var arrangeContext = new TransactContext(options))
            {              
                arrangeContext.Banners.Add(exampleBanner);
                arrangeContext.Banners.Add(exampleBanner2);
                arrangeContext.Banners.Add(exampleBanner3);
                arrangeContext.Banners.Add(exampleBanner4);
                arrangeContext.Banners.Add(exampleBanner5);
                arrangeContext.Banners.Add(exampleBanner6);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var bannerMapper = new BannerDtoMapper();

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var result = await sut.GetFiveBannersByExpirationDateAsync(1);

                Assert.AreEqual(result.Count, 5);

                Assert.AreEqual(result[0].Id, exampleBanner6.Id);
                Assert.AreEqual(result[0].Url, exampleBanner6.Url);
                Assert.AreEqual(result[0].StartDate, exampleBanner6.StartDate);
                Assert.AreEqual(result[0].EndDate, exampleBanner6.EndDate);
                Assert.AreEqual(result[0].ImageName, exampleBanner6.ImageName);
                Assert.IsInstanceOfType(result, typeof(List<BannerDto>));

                Assert.AreEqual(result[1].Id, exampleBanner5.Id);
                Assert.AreEqual(result[1].Url, exampleBanner5.Url);
                Assert.AreEqual(result[1].StartDate, exampleBanner5.StartDate);
                Assert.AreEqual(result[1].EndDate, exampleBanner5.EndDate);
                Assert.AreEqual(result[1].ImageName, exampleBanner5.ImageName);
                Assert.IsInstanceOfType(result[1], typeof(BannerDto));

                Assert.AreEqual(result[2].Id, exampleBanner3.Id);
                Assert.AreEqual(result[3].Id, exampleBanner2.Id);
                Assert.AreEqual(result[4].Id, exampleBanner.Id);
            }
        }

        [TestMethod]
        public async Task GetPageCountForBanersAsync_Should_CorrectlyGetPageCount()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveBannersByExpirationDateAsync_Should_CorrectlyGetBannerDtos));
            var exampleImageName = new Guid().ToString();
            var randomizerMock = new Mock<IRandomizer>();

            var exampleBanner = new Banner()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner2 = new Banner()
            {
                Id = 2,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2018, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner3 = new Banner()
            {
                Id = 3,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2019, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner4 = new Banner()
            {
                Id = 4,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2016, 7, 16),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner5 = new Banner()
            {
                Id = 5,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2020, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner6 = new Banner()
            {
                Id = 6,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2021, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            using (var arrangeContext = new TransactContext(options))
            {
                arrangeContext.Banners.Add(exampleBanner);
                arrangeContext.Banners.Add(exampleBanner2);
                arrangeContext.Banners.Add(exampleBanner3);
                arrangeContext.Banners.Add(exampleBanner4);
                arrangeContext.Banners.Add(exampleBanner5);
                arrangeContext.Banners.Add(exampleBanner6);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var bannerMapper = new BannerDtoMapper();

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var result = await sut.GetPageCountForBanersAsync(5);

                Assert.AreEqual(result, 2);               
            }
        }

        [TestMethod]
        public async Task GetSingleBannerForEditingAsync_Should_CorrectlyGetBannerDto()
        {
            var options = TestUtils.GetOptions(nameof(GetSingleBannerForEditingAsync_Should_CorrectlyGetBannerDto));
            var exampleImageName = new Guid().ToString();
            var randomizerMock = new Mock<IRandomizer>();

            var exampleBanner = new Banner()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBanner2 = new Banner()
            {
                Id = 2,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2018, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            using (var arrangeContext = new TransactContext(options))
            {
                arrangeContext.Banners.Add(exampleBanner);
                arrangeContext.Banners.Add(exampleBanner2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleBannerDto = new BannerDto()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };

                var bannerMapper = new BannerDtoMapper();

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var result = await sut.GetSingleBannerForEditingAsync(1);

                Assert.AreEqual(result.Id, exampleBannerDto.Id);
                Assert.AreEqual(result.Url, exampleBannerDto.Url);
                Assert.AreEqual(result.StartDate, exampleBannerDto.StartDate);
                Assert.AreEqual(result.EndDate, exampleBannerDto.EndDate);
                Assert.AreEqual(result.ImageName, exampleBannerDto.ImageName);
                Assert.IsInstanceOfType(result, typeof(BannerDto));
            }
        }

        [TestMethod]
        public async Task GetSingleBannerForEditingAsync_ThrowsBusinessLogicException_IfBannerNull()
        {
            var options = TestUtils.GetOptions(nameof(GetSingleBannerForEditingAsync_ThrowsBusinessLogicException_IfBannerNull));
            var exampleImageName = new Guid().ToString();
            var randomizerMock = new Mock<IRandomizer>();

            var exampleBanner = new Banner()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            using (var arrangeContext = new TransactContext(options))
            {
                arrangeContext.Banners.Add(exampleBanner);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleBannerDto = new BannerDto()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                };

                var bannerMapper = new BannerDtoMapper();

                var sut = new BannerServices(assertContext, bannerMapper, randomizerMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                    await sut.GetSingleBannerForEditingAsync(2));

                Assert.AreEqual(ExceptionMessages.BannerNull, ex.Message);
            }
        }
    }
}
