﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Transact.Services.UnitTests
{
    [TestClass]
    public class DateTimeProvider_Should
    {
        [TestMethod]
        public void GetDateTimeNow_Should_ReturnCorrectDateTimeType()
        {
            var sut = new DateTimeProvider();
            var result = sut.GetDateTimeNow();

            Assert.IsInstanceOfType(result, typeof(DateTime));
        }
    }
}
