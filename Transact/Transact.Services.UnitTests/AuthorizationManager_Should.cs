﻿using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Transact.Data;
using Transact.Services.Contracts;

namespace Transact.Services.UnitTests
{
    [TestClass]
    public class AuthorizationManager_Should
    {
        [TestMethod]
        public void CheckIfLogged_Should_ThrowUnathorizedAccessException_WhenTokenIsNull()
        {
            var exampleToken = @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            var tokenManagerMock = new Mock<ITokenManager>();

            var contextMock = new Mock<IHttpContextAccessor>();
            contextMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));


            var sut = new AuthorizationManager(tokenManagerMock.Object, contextMock.Object);

            var ex = Assert.ThrowsException<UnauthorizedAccessException>(() =>
                sut.CheckIfLogged(exampleToken));

            Assert.AreEqual(ex.Message, ExceptionMessages.NotLogged);
        }

        [TestMethod]
        public void CheckForRole_Should_ThrowUnathorizedAccessException_WhenTokenIsNull()
        {
            var exampleToken = @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
            var contextMock = new Mock<IHttpContextAccessor>();
            contextMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));
            var tokenManagerMock = new Mock<ITokenManager>();

            var sut = new AuthorizationManager(tokenManagerMock.Object, contextMock.Object);

            var ex = Assert.ThrowsException<UnauthorizedAccessException>(() =>
                sut.CheckIfLogged(exampleToken));

            Assert.AreEqual(ex.Message, ExceptionMessages.NotLogged);
        }

        [TestMethod]
        public void CheckForRole_Should_ThrowUnathorizedAccessException_WhenUserRoleIsNotAuthorized()
        {
            var exampleToken = @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
            var contextMock = new Mock<IHttpContextAccessor>();
            contextMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));
            var tokenManagerMock = new Mock<ITokenManager>();

            tokenManagerMock.Setup(x => x.GetPrincipal(exampleToken)).Returns(TestSamples.exampleClaimPrincipal);       

            var sut = new AuthorizationManager(tokenManagerMock.Object, contextMock.Object);

            var ex = Assert.ThrowsException<UnauthorizedAccessException>(() =>
                sut.CheckForRole(exampleToken, "Admin"));

            Assert.AreEqual(ex.Message, ExceptionMessages.NotAuthorized);
        }

        [TestMethod]
        public void GetUserId_Should_GetCorrectUserId()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var contextMock = new Mock<IHttpContextAccessor>();
            var eaxmpleClaimCollection = new List<Claim>
            {
                new Claim("userRole", "User"),
                new Claim("userId", "1")
            };

            var exampleClaimsIdentity = new ClaimsIdentity(eaxmpleClaimCollection);

            var exampleClaimPrincipal = new ClaimsPrincipal(exampleClaimsIdentity);

            var exampleToken = @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
            contextMock.Setup(X => X.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);

            tokenManagerMock.Setup(x => x.GetPrincipal(It.IsAny<string>())).Returns(exampleClaimPrincipal);

            var sut = new AuthorizationManager(tokenManagerMock.Object, contextMock.Object);

            var result = sut.GetLoggedUserId();

            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void GetUserId_Should_ThrowUnathorizedAccessException_WhenNoUserLogged()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var exampleToken = @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            var contextMock = new Mock<IHttpContextAccessor>();
            //tokenManagerMock.Setup(x => x.ValidateToken(TestSamples.exampleToken)).Returns(value: false);
            contextMock.Setup(X => X.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);

            var sut = new AuthorizationManager(tokenManagerMock.Object, contextMock.Object);

            var ex = Assert.ThrowsException<UnauthorizedAccessException>(() =>
                sut.GetLoggedUserId());

            Assert.AreEqual(ex.Message, ExceptionMessages.NotLogged);
        }       
    }
}
