﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Mapper;

namespace Transact.Services.UnitTests
{
    [TestClass]
    public class TransactionDtoMapper_Should
    {
        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_Transaction_To_TransactionDto()
        {
            var sut = new TransactionDtoMapper();

            var exampleTransaction = new Transaction()
            {
                Id = 1,
                Amount = 10000,
                SenderAccountId = 3,
                ReceiverAccountId = 20,
                Description = "example Description",
                TimeStamp = new DateTime(2016, 4, 10),
                SenderAccount = new Account { Id = 1, Client = new Client() { Id = 1, Name = "Example1" } },
                Status = new Status { Id = 1, StatusName = "Saved"},
                ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890", Client = new Client() { Id = 1, Name = "Example2" } },
               
            };

            var result = sut.MapFrom(exampleTransaction);

            Assert.IsInstanceOfType(result, typeof(TransactionDto));
            Assert.AreEqual(result.Id, exampleTransaction.Id);
            Assert.AreEqual(result.Amount, exampleTransaction.Amount);
            Assert.AreEqual(result.SenderAccountId, exampleTransaction.SenderAccountId);
            Assert.AreEqual(result.SenderAccountNumber, exampleTransaction.SenderAccount.AccountNumber);
            Assert.AreEqual(result.SenderAccountNickName, exampleTransaction.SenderAccount.NickName);
            Assert.AreEqual(result.ReceiverAccountId, exampleTransaction.ReceiverAccountId);
            Assert.AreEqual(result.ReceiverAccountNumber, exampleTransaction.ReceiverAccount.AccountNumber);
            Assert.AreEqual(result.Description, exampleTransaction.Description);
            Assert.AreEqual(result.TimeStamp, exampleTransaction.TimeStamp);
            Assert.AreEqual(result.StatusName, exampleTransaction.Status.StatusName);
            Assert.AreEqual(result.ReceiverClientName, exampleTransaction.ReceiverAccount.Client.Name);
            Assert.AreEqual(result.ReceiverAccountNickName, exampleTransaction.ReceiverAccount.NickName);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_TransactionDto_To_Transaction()
        {
            var sut = new TransactionDtoMapper();

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Amount = 100,
                SenderAccountNumber = "1234567890",
                SenderAccountNickName = "ExampleNickName",
                SenderClientName = "ExampleSenderClient",
                ReceiverAccountNumber = "1233367890",
                ReceiverClientName = "ExampleReceiverClient",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                Description = "example Description",
                TimeStamp = new DateTime(2016, 7, 15),
                StatusName = "Saved",
            };

            var result = sut.MapFrom(exampleTransactionDto);

            Assert.IsInstanceOfType(result, typeof(Transaction));
            Assert.AreEqual(result.Id, exampleTransactionDto.Id);
            Assert.AreEqual(result.Amount, exampleTransactionDto.Amount);
            Assert.AreEqual(result.SenderAccountId, exampleTransactionDto.SenderAccountId);
            Assert.AreEqual(result.ReceiverAccountId, exampleTransactionDto.ReceiverAccountId);
            Assert.AreEqual(result.Description, exampleTransactionDto.Description);
            Assert.AreEqual(result.TimeStamp, exampleTransactionDto.TimeStamp);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfTransactions_To_ListOfTransactionDtos()
        {
            var exampleTransactionList = new List<Transaction>()
            {
                new Transaction()
                {
                    Id = 1,
                    Amount = 10000,
                    SenderAccountId = 1,
                    ReceiverAccountId = 3,
                    Description = "example Description1",
                    TimeStamp = new DateTime(2016, 3, 10),
                    SenderAccount = new Account { Id = 1, AccountNumber = "1234567890", NickName = "ExampleSenderNick" },
                    Status = new Status { Id = 1, StatusName = "Saved"},
                    ReceiverAccount = new Account { Id = 3, AccountNumber = "1133367890", NickName = "ExampleReceiverNick" }
                },
                new Transaction()
                {
                    Id = 2,
                    Amount = 1200,
                    SenderAccountId = 1,
                    ReceiverAccountId = 5,
                    Description = "example Description1",
                    TimeStamp = new DateTime(2016, 5, 10),
                    SenderAccount = new Account { Id = 1 },
                    Status = new Status { Id = 1, StatusName = "Saved"},
                    ReceiverAccount = new Account { Id = 5, AccountNumber = "1133367890" }
                }
            };

            var sut = new TransactionDtoMapper();

            var result = sut.MapFrom(exampleTransactionList);

            Assert.IsInstanceOfType(result, typeof(List<TransactionDto>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleTransactionList[0].Id);
            Assert.AreEqual(result[0].Amount, exampleTransactionList[0].Amount);
            Assert.AreEqual(result[0].SenderAccountId, exampleTransactionList[0].SenderAccountId);
            Assert.AreEqual(result[0].SenderAccountNumber, exampleTransactionList[0].SenderAccount.AccountNumber);
            Assert.AreEqual(result[0].SenderAccountNickName, exampleTransactionList[0].SenderAccount.NickName);
            Assert.AreEqual(result[0].ReceiverAccountId, exampleTransactionList[0].ReceiverAccountId);
            Assert.AreEqual(result[0].ReceiverAccountNumber, exampleTransactionList[0].ReceiverAccount.AccountNumber);
            Assert.AreEqual(result[0].Description, exampleTransactionList[0].Description);
            Assert.AreEqual(result[0].TimeStamp, exampleTransactionList[0].TimeStamp);
            Assert.AreEqual(result[0].StatusName, exampleTransactionList[0].Status.StatusName);

            Assert.AreEqual(result[1].Id, exampleTransactionList[1].Id);
            Assert.AreEqual(result[1].Amount, exampleTransactionList[1].Amount);
            Assert.AreEqual(result[1].SenderAccountId, exampleTransactionList[1].SenderAccountId);
            Assert.AreEqual(result[1].SenderAccountNumber, exampleTransactionList[1].SenderAccount.AccountNumber);
            Assert.AreEqual(result[1].SenderAccountNickName, exampleTransactionList[1].SenderAccount.NickName);
            Assert.AreEqual(result[1].ReceiverAccountId, exampleTransactionList[1].ReceiverAccountId);
            Assert.AreEqual(result[1].ReceiverAccountNumber, exampleTransactionList[1].ReceiverAccount.AccountNumber);
            Assert.AreEqual(result[1].Description, exampleTransactionList[1].Description);
            Assert.AreEqual(result[1].TimeStamp, exampleTransactionList[1].TimeStamp);
            Assert.AreEqual(result[1].StatusName, exampleTransactionList[1].Status.StatusName);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfTransactionsDto_To_ListOfTransactions()
        {
            var exampleTransactionDtoList = new List<TransactionDto>()
            {
                new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    ReceiverAccountNumber = "1233367890",
                    StatusName = "Saved"
                },
                new TransactionDto()
                {
                    Id = 2,
                    Amount = 200,
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description2",
                    TimeStamp = new DateTime(2016, 4, 15),
                    ReceiverAccountNumber = "1233367890",
                    StatusName = "Saved"
                }
            };

            var sut = new TransactionDtoMapper();

            var result = sut.MapFrom(exampleTransactionDtoList);

            Assert.IsInstanceOfType(result, typeof(List<Transaction>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleTransactionDtoList[0].Id);
            Assert.AreEqual(result[0].Amount, exampleTransactionDtoList[0].Amount);
            Assert.AreEqual(result[0].SenderAccountId, exampleTransactionDtoList[0].SenderAccountId);
            Assert.AreEqual(result[0].ReceiverAccountId, exampleTransactionDtoList[0].ReceiverAccountId);
            Assert.AreEqual(result[0].Description, exampleTransactionDtoList[0].Description);
            Assert.AreEqual(result[0].TimeStamp, exampleTransactionDtoList[0].TimeStamp);
            Assert.AreEqual(result[1].Id, exampleTransactionDtoList[1].Id);
            Assert.AreEqual(result[1].Amount, exampleTransactionDtoList[1].Amount);
            Assert.AreEqual(result[1].SenderAccountId, exampleTransactionDtoList[1].SenderAccountId);
            Assert.AreEqual(result[1].ReceiverAccountId, exampleTransactionDtoList[1].ReceiverAccountId);
            Assert.AreEqual(result[1].Description, exampleTransactionDtoList[1].Description);
            Assert.AreEqual(result[1].TimeStamp, exampleTransactionDtoList[1].TimeStamp);
        }
    }
}
