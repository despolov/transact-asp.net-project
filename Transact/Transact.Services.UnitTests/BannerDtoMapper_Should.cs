﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Mapper;

namespace Transact.Services.UnitTests
{
    [TestClass]
    public class BannerDtoMapper_Should
    {
        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_Banner_To_BannerDto()
        {
            var sut = new BannerDtoMapper();

            var exampleImageName = new Guid().ToString();

            var exampleBanner = new Banner()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var result = sut.MapFrom(exampleBanner);
         
            Assert.AreEqual(result.Id, exampleBanner.Id);
            Assert.AreEqual(result.Url, exampleBanner.Url);
            Assert.AreEqual(result.StartDate, exampleBanner.StartDate);
            Assert.AreEqual(result.EndDate, exampleBanner.EndDate);
            Assert.AreEqual(result.ImageName, exampleBanner.ImageName);
            Assert.IsInstanceOfType(result, typeof(BannerDto));
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_BannerDto_To_Banner()
        {
            var sut = new BannerDtoMapper();

            var exampleImageName = new Guid().ToString();

            var exampleBannerDto = new BannerDto()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var result = sut.MapFrom(exampleBannerDto);

            Assert.AreEqual(result.Id, exampleBannerDto.Id);
            Assert.AreEqual(result.Url, exampleBannerDto.Url);
            Assert.AreEqual(result.StartDate, exampleBannerDto.StartDate);
            Assert.AreEqual(result.EndDate, exampleBannerDto.EndDate);
            Assert.AreEqual(result.ImageName, exampleBannerDto.ImageName);
            Assert.IsInstanceOfType(result, typeof(Banner));
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfBannerDtos_To_Banners()
        {
            var sut = new BannerDtoMapper();

            var exampleImageName = new Guid().ToString();

            var exampleBannerDtoList = new List<BannerDto>
            {
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            var result = sut.MapFrom(exampleBannerDtoList);

            Assert.AreEqual(result.Count, 2);

            Assert.AreEqual(result[0].Id, exampleBannerDtoList[0].Id);
            Assert.AreEqual(result[0].Url, exampleBannerDtoList[0].Url);
            Assert.AreEqual(result[0].StartDate, exampleBannerDtoList[0].StartDate);
            Assert.AreEqual(result[0].EndDate, exampleBannerDtoList[0].EndDate);
            Assert.AreEqual(result[0].ImageName, exampleBannerDtoList[0].ImageName);

            Assert.AreEqual(result[1].Id, exampleBannerDtoList[1].Id);
            Assert.AreEqual(result[1].Url, exampleBannerDtoList[1].Url);
            Assert.AreEqual(result[1].StartDate, exampleBannerDtoList[1].StartDate);
            Assert.AreEqual(result[1].EndDate, exampleBannerDtoList[1].EndDate);
            Assert.AreEqual(result[1].ImageName, exampleBannerDtoList[1].ImageName);

            Assert.IsInstanceOfType(result, typeof(List<Banner>));
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfBanners_To_BannerDtos()
        {
            var sut = new BannerDtoMapper();

            var exampleImageName = new Guid().ToString();

            var exampleBannerList = new List<Banner>
            {
                new Banner()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new Banner()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            var result = sut.MapFrom(exampleBannerList);

            Assert.AreEqual(result.Count, 2);

            Assert.AreEqual(result[0].Id, exampleBannerList[0].Id);
            Assert.AreEqual(result[0].Url, exampleBannerList[0].Url);
            Assert.AreEqual(result[0].StartDate, exampleBannerList[0].StartDate);
            Assert.AreEqual(result[0].EndDate, exampleBannerList[0].EndDate);
            Assert.AreEqual(result[0].ImageName, exampleBannerList[0].ImageName);

            Assert.AreEqual(result[1].Id, exampleBannerList[1].Id);
            Assert.AreEqual(result[1].Url, exampleBannerList[1].Url);
            Assert.AreEqual(result[1].StartDate, exampleBannerList[1].StartDate);
            Assert.AreEqual(result[1].EndDate, exampleBannerList[1].EndDate);
            Assert.AreEqual(result[1].ImageName, exampleBannerList[1].ImageName);

            Assert.IsInstanceOfType(result, typeof(List<BannerDto>));
        }
    }

}
