﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using Transact.Models;
using Transact.Models.Dtos;

namespace Transact.Services.UnitTests
{
    public static class TestSamples
    {
        public static readonly string exampleToken = @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

        public static readonly string exampleToken2 = @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6InN0YWthdGEiLCJ1c2VyUm9sZSI6IkFkbWluIiwibmJmIjoxNTU4MzM2OTcxLCJleHAiOjE1NTg0NDQ5NzAsImlhdCI6MTU1ODMzNjk3MX0.LUz95iTGgKvKqwVaWmRJSG_gnuZ0yS7DPVTkj9FC4Sw";

        public static readonly string exampleSecret = "d8bec9dfa11de0c600a88b0d5dec0acce9deb0cc812f0865671ca2c2cfa40da8";

        //public static readonly string exampleUserHashPassword = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811";

        public static readonly IList<Claim> eaxmpleClaimCollection = new List<Claim>
        {
            new Claim("userRole", "User")
        };

        public static readonly string exampleReceiverAccountNumber = "1234567890";

        public static readonly ClaimsIdentity exampleClaimsIdentity = new ClaimsIdentity(eaxmpleClaimCollection);

        public static readonly ClaimsPrincipal exampleClaimPrincipal = new ClaimsPrincipal(exampleClaimsIdentity);

        public static Client exampleClient7 = new Client()
        {
            Id = 1,
            Name = "exampleClient"
        };

        public static Client exampleClient8 = new Client()
        {
            Id = 2,
            Name = "exampleClient2"
        };

        public static Client exampleClient9 = new Client()
        {
            Id = 3,
            Name = "exampleClient3"
        };

        public static Client exampleClient4 = new Client()
        {
            Id = 4,
            Name = "exampleClient4"
        };

        public static Client exampleClient5 = new Client()
        {
            Id = 5,
            Name = "exampleClient5"
        };

        public static Client exampleClient6 = new Client()
        {
            Id = 6,
            Name = "exampleClient6"
        };

        //public static Transaction exampleSavedTransaction = new Transaction()
        //{
        //    Id = 1,
        //    Amount = 100,
        //    SenderAccountId = 1,
        //    ReceiverAccountId = 2,
        //    Description = "example Description",
        //    TimeStamp = new DateTime(2016, 7, 15),
        //    StatusId = 1,
        //    SenderAccount = new Account { Id = 1 },
        //    ReceiverAccount = new Account{ Id = 2, AccountNumber = "1233367890" }
        //};

       
        public static Transaction exampleSavedTransaction2 = new Transaction()
        {
            Id = 2,
            Amount = 10000,
            SenderAccountId = 3,
            ReceiverAccountId = 20,
            Description = "example Description",
            TimeStamp = new DateTime(2016, 4, 10),
            SenderAccount = new Account { Id = 1 },
            ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890" }
        };

        //public static Transaction exampleSavedTransaction3 = new Transaction()
        //{
        //    Id = 3,
        //    Amount = 100,
        //    SenderAccountId = 1,
        //    ReceiverAccountId = 2,
        //    Description = "example Description",
        //    TimeStamp = new DateTime(2016, 5, 10),
        //    SenderAccount = new Account { Id = 1 },
        //    ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890" }
        //};

        public static Transaction exampleSavedTransaction4 = new Transaction()
        {
            Id = 4,
            Amount = 10,
            SenderAccountId = 10,
            ReceiverAccountId = 4,
            Description = "example Description2",
            TimeStamp = new DateTime(2015, 7, 10),
            SenderAccount = new Account { Id = 1 },
            ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890" }
        };

        //public static Transaction exampleSavedTransaction5 = new Transaction()
        //{
        //    Id = 5,
        //    Amount = 104,
        //    SenderAccountId = 5,
        //    ReceiverAccountId = 40,
        //    Description = "example Description5",
        //    TimeStamp = new DateTime(2012, 2, 9),
        //    SenderAccount = new Account { Id = 1 },
        //    ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890" }
        //};

        //public static Transaction exampleSavedTransaction6 = new Transaction()
        //{
        //    Id = 6,
        //    Amount = 102400,
        //    SenderAccountId = 6,
        //    ReceiverAccountId = 7,
        //    Description = "example Description6",
        //    TimeStamp = new DateTime(2010, 3, 3),
        //    SenderAccount = new Account { Id = 1 },
        //    ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890" }
        //};

        //public static Transaction exampleSavedTransaction7 = new Transaction()
        //{
        //    Id = 7,
        //    Amount = 1400,
        //    SenderAccountId = 8,
        //    ReceiverAccountId = 9,
        //    Description = "example Description7",
        //    TimeStamp = new DateTime(2010, 7, 3),
        //    SenderAccount = new Account { Id = 1 },
        //    ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890" }
        //};

        //public static Transaction exampleSavedTransaction8 = new Transaction()
        //{
        //    Id = 8,
        //    Amount = 18800,
        //    SenderAccountId = 10,
        //    ReceiverAccountId = 11,
        //    Description = "example Description8",
        //    TimeStamp = new DateTime(2000, 1, 3),
        //    SenderAccount = new Account { Id = 1 },
        //    ReceiverAccount = new Account { Id = 2, AccountNumber = "1133367890" }
        //};

        //public static Transaction exampleSavedTransactionSameAccounts = new Transaction()
        //{
        //    Id = 9,
        //    Amount = 1020,
        //    SenderAccountId = 1,
        //    ReceiverAccountId = 1,
        //    Description = "example Description2",
        //    TimeStamp = new DateTime(2012, 7, 15),
        //    SenderAccount = new Account { Id = 1 },
        //    ReceiverAccount = new Account { Id = 1, AccountNumber = "1234567890" }
        //};


        //public static Account exampleAccount = new Account()
        //{
        //    Id = 1,
        //    AccountNumber = "1234567890",
        //    Balance = 10000,
        //    ClientId = 1,
        //    NickName = "ExampleNickName"
        //};

        //public static Account exampleAccount2 = new Account()
        //{
        //    Id = 2,
        //    AccountNumber = "1233367890",
        //    Balance = 10000,
        //    ClientId = 2,
        //    NickName = "ExampleNickName2"
        //};

        public static Account exampleAccount3 = new Account()
        {
            Id = 3,
            AccountNumber = "3333367890",
            Balance = 10,
            ClientId = 3,
            NickName = "ExampleNickName2"
        };

        public static Account exampleAccount4 = new Account()
        {
            Id = 4,
            AccountNumber = "1255567890",
            Balance = 100,
            ClientId = 1,
            NickName = "ExampleNickName4"
        };

        //public static Account exampleAccount5 = new Account()
        //{
        //    Id = 5,
        //    AccountNumber = "2255567890",
        //    Balance = 1200,
        //    ClientId = 1,
        //    NickName = "ExampleNickName5"
        //};

        //public static Account exampleAccount6 = new Account()
        //{
        //    Id = 6,
        //    AccountNumber = "6255567890",
        //    Balance = 100,
        //    ClientId = 1,
        //    NickName = "ExampleNickName6"
        //};

        //public static Account exampleAccount7 = new Account()
        //{
        //    Id = 7,
        //    AccountNumber = "6255567870",
        //    Balance = 1070,
        //    ClientId = 1,
        //    NickName = "ExampleNickName7"
        //};

        //public static Account exampleAccount8 = new Account()
        //{
        //    Id = 8,
        //    AccountNumber = "6259567870",
        //    Balance = 101,
        //    ClientId = 1,
        //    NickName = "ExampleNickName8"
        //};

        //public static Account exampleAccount9 = new Account()
        //{
        //    Id = 9,
        //    AccountNumber = "6258567870",
        //    Balance = 102,
        //    ClientId = 1,
        //    NickName = "ExampleNickName9"
        //};

        //public static Account exampleAccount10 = new Account()
        //{
        //    Id = 10,
        //    AccountNumber = "6258567871",
        //    Balance = 12,
        //    ClientId = 1,
        //    NickName = "ExampleNickName10"
        //};

        //public static Account exampleAccount11 = new Account()
        //{
        //    Id = 11,
        //    AccountNumber = "6258567890",
        //    Balance = 1020,
        //    ClientId = 1,
        //    NickName = "ExampleNickName11"
        //};

        public static Account exampleAccount12 = new Account()
        {
            Id = 12,
            AccountNumber = "2258567890",
            Balance = 120,
            ClientId = 1,
            NickName = "ExampleNickName12"
        };

        public static Account exampleAccount13 = new Account()
        {
            Id = 13,
            AccountNumber = "2253567890",
            Balance = 123,
            ClientId = 1,
            NickName = "ExampleNickName13"
        };

        public static Account exampleAccount14 = new Account()
        {
            Id = 14,
            AccountNumber = "2253567894",
            Balance = 1130,
            ClientId = 3,
            NickName = "ExampleNickName14"
        };

        public static Account exampleAccount15 = new Account()
        {
            Id = 15,
            AccountNumber = "2253567895",
            Balance = 1231,
            ClientId = 3,
            NickName = "ExampleNickName15"
        };

        //public static User exampleUser = new User()
        //{
        //    Id = 1,
        //    Name = "ExampleUser1",
        //    UserName = "ExampleUserName1",
        //    Password = "ExamplePassword1",
        //    Role = new UserRole { RoleName = "User" },
        //    RoleId = 1,
        //    UsersAccounts = new List<UsersAccounts>(),
        //    UsersClients = new List<UsersClients>(),
        //};

        //public static User exampleUserHash = new User()
        //{
        //    Id = 1,
        //    Name = "ExampleUser1",
        //    UserName = "ExampleUserName1",
        //    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
        //    Role = new UserRole { RoleName = "User" },
        //    RoleId = 1,
        //    UsersAccounts = new List<UsersAccounts>(),
        //    UsersClients = new List<UsersClients>(),
        //};

        public static User exampleUserHash2 = new User()
        {
            Id = 2,
            Name = "ExampleUser2",
            UserName = "ExampleUserName2",
            Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
            Role = new UserRole { RoleName = "User" },
            RoleId = 1,
            UsersAccounts = new List<UsersAccounts>(),
            UsersClients = new List<UsersClients>(),
        };

        public static User exampleUserHash3 = new User()
        {
            Id = 3,
            Name = "ExampleUser3",
            UserName = "ExampleUserName3",
            Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
            Role = new UserRole { RoleName = "User" },
            RoleId = 1,
            UsersAccounts = new List<UsersAccounts>(),
            UsersClients = new List<UsersClients>(),
        };

        public static User exampleUserHash4 = new User()
        {
            Id = 4,
            Name = "ExampleUser4",
            UserName = "ExampleUserName4",
            Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
            Role = new UserRole { RoleName = "User" },
            RoleId = 1,
            UsersAccounts = new List<UsersAccounts>(),
            UsersClients = new List<UsersClients>(),
        };

        public static User exampleUserHash5 = new User()
        {
            Id = 5,
            Name = "ExampleUser6",
            UserName = "ExampleUserName5",
            Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
            Role = new UserRole { RoleName = "User" },
            RoleId = 1,
            UsersAccounts = new List<UsersAccounts>(),
            UsersClients = new List<UsersClients>(),
        };

        public static User exampleUserHash6 = new User()
        {
            Id = 6,
            Name = "ExampleUser6",
            UserName = "ExampleUserName6",
            Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
            Role = new UserRole { RoleName = "User" },
            RoleId = 1,
            UsersAccounts = new List<UsersAccounts>(),
            UsersClients = new List<UsersClients>(),
        };

        public static User exampleUserHash7 = new User()
        {
            Id = 7,
            Name = "ExampleUser7",
            UserName = "ExampleUserName7",
            Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
            Role = new UserRole { RoleName = "User" },
            RoleId = 1,
            UsersAccounts = new List<UsersAccounts>(),
            UsersClients = new List<UsersClients>(),
        };

        //public static User exampleUser2 = new User()
        //{
        //    Id = 2,
        //    Name = "ExampleUser2",
        //    UserName = "ExampleUserName2",
        //    Password = "ExamplePassword2",
        //    Role = new UserRole { RoleName = "User" },
        //    UsersAccounts = new List<UsersAccounts>(),
        //    UsersClients = new List<UsersClients>(),
        //};

        public static User exampleUser3 = new User()
        {
            Id = 3,
            Name = "stakata",
            UserName = "ExampleUserName3",
            Password = "ExamplePassword3",
            Role = new UserRole { RoleName = "User" },
            UsersAccounts = new List<UsersAccounts>(),
            UsersClients = new List<UsersClients>()
        };

        public static User exampleUser4 = new User()
        {
            Id = 4,
            Name = "exampleUser4",
            UserName = "ExampleUserName4",
            Password = "ExamplePassword4",
            Role = new UserRole { RoleName = "User" },
            UsersAccounts = new List<UsersAccounts>(),
            UsersClients = new List<UsersClients>(),
        };

        public static User exampleUser5 = new User()
        {
            Id = 5,
            Name = "exampleUser5",
            UserName = "ExampleUserName5",
            Password = "ExamplePassword5",
            Role = new UserRole { RoleName = "User" },
            UsersAccounts = new List<UsersAccounts>(),
            UsersClients = new List<UsersClients>(),
        };

        public static User exampleUser6 = new User()
        {
            Id = 6,
            Name = "exampleUser6",
            UserName = "ExampleUserName6",
            Password = "ExamplePassword6",
            Role = new UserRole { RoleName = "User" },
            UsersAccounts = new List<UsersAccounts>(),
            UsersClients = new List<UsersClients>(),
        };       

        public static Client exampleClient = new Client()
        {
            Id = 2,
            Name = "exampleClient"
        };

        public static Client exampleClient3 = new Client()
        {
            Id = 3,
            Name = "exampleClient3"
        };

        public static UsersAccounts exampleUsersAccounts = new UsersAccounts()
        {
            UserId = 1,
            AccountId = 1
        };

        public static UsersAccounts exampleUsersAccounts2 = new UsersAccounts()
        {
            UserId = 2,
            AccountId = 2
        };

        public static UsersAccounts exampleUsersAccounts3 = new UsersAccounts()
        {
            UserId = 7,
            AccountId = 14
        };

        public static UsersAccounts exampleUsersAccounts4 = new UsersAccounts()
        {
            UserId = 7,
            AccountId = 15
        };

        public static Admin exampleAdmin = new Admin()
        {
            Id = 1,
            UserName = "ExampleUserName1",
            Password = "ExamplePassword1"
        };

        //public static TransactionDto exampleTransactionDto = new TransactionDto()
        //{
        //    Id = 1,
        //    Amount = 100,
        //    SenderAccountId = 1,
        //    ReceiverAccountId = 2,
        //    Description = "example Description",
        //    TimeStamp = new DateTime(2016, 7, 15),
        //    ReceiverAccountNumber = "1233367890",
        //    Status = new Status { Id = 1 }          
        //};

        //public static UserDto exampleUserDto = new UserDto()
        //{
        //    Id = 1,
        //    Name = "ExampleUser1",
        //    RoleId = 1,
        //    UserName = "exampleUserDto",
        //    RoleName = "Example"
        //};

        //public static List<UserDto> exampleUserDtoList = new List<UserDto>()
        //{
        //    new UserDto()
        //    {
        //        Id = 6
        //    },
        //    new UserDto()
        //    {
        //        Id = 5
        //    },
        //    new UserDto()
        //    {
        //        Id = 4
        //    },
        //    new UserDto()
        //    {
        //        Id = 3
        //    },
        //    new UserDto()
        //    {
        //        Id = 2
        //    }
        //};
    }
}
