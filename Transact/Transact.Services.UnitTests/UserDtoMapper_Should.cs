﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Mapper;

namespace Transact.Services.UnitTests
{
    [TestClass]
    public class UserDtoMapper_Should
    {
          [TestMethod]
          public void MapFrom_Should_CorrectlyMap_User_To_UserDto()
          {
              var sut = new UserDtoMapper();                

              var exampleUser = new User()
              {
                  Id = 1,
                  Name = "ExampleUser1",
                  UserName = "ExampleUserName1",
                  Password = "ExamplePassword1",
                  Role = new UserRole { RoleName = "User" },
                  RoleId = 1                   
              };

              var result = sut.MapFrom(exampleUser);

              Assert.IsInstanceOfType(result, typeof(UserDto));
              Assert.AreEqual(result.Id, exampleUser.Id);
              Assert.AreEqual(result.Name, exampleUser.Name);
              Assert.AreEqual(result.UserName, exampleUser.UserName);
              Assert.AreEqual(result.RoleId, exampleUser.RoleId);
          }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_UserTo_To_User()
        {
            var sut = new UserDtoMapper();

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                Name = "ExampleUser1",
                UserName = "ExampleUserName1",
                Password = "ExamplePassword1",
                RoleId = 1
            };

            var result = sut.MapFrom(exampleUserDto);

            Assert.IsInstanceOfType(result, typeof(User));
            Assert.AreEqual(result.Id, exampleUserDto.Id);
            Assert.AreEqual(result.Name, exampleUserDto.Name);
            Assert.AreEqual(result.UserName, exampleUserDto.UserName);
            Assert.AreEqual(result.RoleId, exampleUserDto.RoleId);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfUsers_To_ListOfUserDtos()
        {
            var exampleUserDtoList = new List<UserDto>()
            {
                new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUserDto1",
                    UserName = "ExampleUserName2",
                    RoleId = 1
                },
                new UserDto()
                {
                    Id = 2,
                    Name = "ExampleUserDto2",
                    UserName = "ExampleUserName2",
                    RoleId = 1
                }               
            };

            var sut = new UserDtoMapper();

            var result = sut.MapFrom(exampleUserDtoList);

            Assert.IsInstanceOfType(result, typeof(List<User>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleUserDtoList[0].Id);
            Assert.AreEqual(result[0].Name, exampleUserDtoList[0].Name);
            Assert.AreEqual(result[0].UserName, exampleUserDtoList[0].UserName);
            Assert.AreEqual(result[0].RoleId, exampleUserDtoList[0].RoleId);
            Assert.AreEqual(result[1].Id, exampleUserDtoList[1].Id);
            Assert.AreEqual(result[1].Name, exampleUserDtoList[1].Name);
            Assert.AreEqual(result[1].UserName, exampleUserDtoList[1].UserName);
            Assert.AreEqual(result[1].RoleId, exampleUserDtoList[1].RoleId);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfUsersDtos_To_ListOfUsers()
        {
            var exampleUserList = new List<User>()
            {
                new User()
                {
                    Id = 1,
                    Name = "ExampleUserDto1",
                    UserName = "ExampleUserName2",
                    RoleId = 1,
                    Role = new UserRole()
                    {
                        Id = 1,
                        RoleName = "User"
                    }
                },
                new User()
                {
                    Id = 2,
                    Name = "ExampleUserDto2",
                    UserName = "ExampleUserName2",
                    RoleId = 1,
                    Role = new UserRole()
                    {
                        Id = 1,
                        RoleName = "User"
                    }
                }
            };

            var sut = new UserDtoMapper();

            var result = sut.MapFrom(exampleUserList);

            Assert.IsInstanceOfType(result, typeof(List<UserDto>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleUserList[0].Id);
            Assert.AreEqual(result[0].Name, exampleUserList[0].Name);
            Assert.AreEqual(result[0].UserName, exampleUserList[0].UserName);
            Assert.AreEqual(result[0].RoleId, exampleUserList[0].RoleId);
            Assert.AreEqual(result[0].RoleName, exampleUserList[0].Role.RoleName);
            Assert.AreEqual(result[1].Id, exampleUserList[1].Id);
            Assert.AreEqual(result[1].Name, exampleUserList[1].Name);
            Assert.AreEqual(result[1].UserName, exampleUserList[1].UserName);
            Assert.AreEqual(result[1].RoleId, exampleUserList[1].RoleId);
            Assert.AreEqual(result[1].RoleName, exampleUserList[1].Role.RoleName);
        }
    }
}

