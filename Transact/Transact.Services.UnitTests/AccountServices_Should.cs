﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transact.Data;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.CustomExceptions;
using Transact.Services.Mapper;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services.UnitTests
{
    [TestClass]
    public class AccountServices_Should
    {
        [TestMethod]
        public async Task GetAccountAsync_Should_GetCorrectAccount()
        {
            var options = TestUtils.GetOptions(nameof(GetAccountAsync_Should_GetCorrectAccount));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "ExampleNickName"
                };

                var accountToAdd = await arrangeContext.Accounts.AddAsync(exampleAccount);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "ExampleNickName"
                };

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "ExampleNickName"
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<Account>())).Returns(exampleAccountDto);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var result = await sut.GetAccountAsync(exampleAccount.Id);

                Assert.AreEqual(result.Id, exampleAccount.Id);
                Assert.AreEqual(result.AccountNumber, exampleAccount.AccountNumber);
                Assert.AreEqual(result.Balance, exampleAccount.Balance);
                Assert.AreEqual(result.NickName, exampleAccount.NickName);
                Assert.IsInstanceOfType(result, typeof(AccountDto));
            }
        }

        [TestMethod]
        public async Task GetAccountAsync_Should_ThrowArgumentExceptionIfAccountIsNull()
        {
            var options = TestUtils.GetOptions(nameof(GetAccountAsync_Should_ThrowArgumentExceptionIfAccountIsNull));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "ExampleNickName"
                };

                var accountToAdd = await arrangeContext.Accounts.AddAsync(exampleAccount);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "ExampleNickName"
                };

                var exampleAccount = new Account()
                {
                    Id = 2,
                    AccountNumber = "1238567890",
                    Balance = 10000,
                    NickName = "ExamplekickName"
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<Account>())).Returns(exampleAccountDto);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.GetAccountAsync(exampleAccount.Id));

                Assert.AreEqual(ex.Message, ExceptionMessages.AccountNull);
            }
        }

        [TestMethod]
        public async Task GetAccountAsync_Should_CallAccountMapperMock_MapFrom_Once()
        {

            var options = TestUtils.GetOptions(nameof(GetAccountAsync_Should_CallAccountMapperMock_MapFrom_Once));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "ExampleNickName"
                };

                var accountToAdd = await arrangeContext.Accounts.AddAsync(exampleAccount);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "ExampleNickName"
                };

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1238567890",
                    Balance = 10000,
                    NickName = "ExamplekickName"
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<Account>())).Returns(exampleAccountDto);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var result = await sut.GetAccountAsync(exampleAccount.Id);

                fullAccountMapperMock.Verify(x => x.MapFrom(It.IsAny<Account>()));
            }
        }

        [TestMethod]
        public async Task FindAccountsContainingAsync_Should_GetCorrectAccounts()
        {
            var options = TestUtils.GetOptions(nameof(FindAccountsContainingAsync_Should_GetCorrectAccounts));

            using (var arrangeContext = new TransactContext(options))
            {

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1234567892",
                    Balance = 20000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };

                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleName"
                };

                arrangeContext.Add(exampleClient);
                arrangeContext.Add(exampleAccount);
                arrangeContext.Add(exampleAccount2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccountDto2 = new AccountDto()
                {
                    Id = 2,
                    AccountNumber = "1234567892",
                    Balance = 20000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };

                var exampleAccountDtoList = new List<AccountDto>()
                {
                    new AccountDto()
                    {
                        Id = 1,
                        AccountNumber = "1234567890",
                        Balance = 10000,
                        NickName = "ExampleNickName"
                    },
                    new AccountDto()
                    {
                        Id = 2,
                        AccountNumber = "1234567892",
                        Balance = 20000,
                        NickName = "ExampleNickName2"
                    }
                };

                var accountMapper = new AccountDtoMapper();

                var sut = new AccountServices(assertContext, accountMapper);

                var result = await sut.FindAccountsContainingAsync("123", 0);

                Assert.IsInstanceOfType(result, typeof(List<AccountDto>));

                Assert.AreEqual(result.Count, 2);
                Assert.AreEqual(result[0].Id, exampleAccountDto.Id);
                Assert.AreEqual(result[0].NickName, exampleAccountDto.NickName);
                Assert.AreEqual(result[0].Balance, exampleAccountDto.Balance);
                Assert.AreEqual(result[0].AccountNumber, exampleAccountDto.AccountNumber);
                Assert.AreEqual(result[0].ClientId, exampleAccountDto.ClientId);

                Assert.AreEqual(result[1].Id, exampleAccountDto2.Id);
                Assert.AreEqual(result[1].NickName, exampleAccountDto2.NickName);
                Assert.AreEqual(result[1].Balance, exampleAccountDto2.Balance);
                Assert.AreEqual(result[1].AccountNumber, exampleAccountDto2.AccountNumber);
                Assert.AreEqual(result[1].ClientId, exampleAccountDto2.ClientId);
            }
        }

        [TestMethod]
        public async Task FindAccountsContainingAsync_Should_GetCorrectAccounts_WithIgnoredAccount()
        {
            var options = TestUtils.GetOptions(nameof(FindAccountsContainingAsync_Should_GetCorrectAccounts_WithIgnoredAccount));

            using (var arrangeContext = new TransactContext(options))
            {

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1234567892",
                    Balance = 20000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };

                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleName"
                };

                arrangeContext.Add(exampleClient);
                arrangeContext.Add(exampleAccount);
                arrangeContext.Add(exampleAccount2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccountDto2 = new AccountDto()
                {
                    Id = 2,
                    AccountNumber = "1234567892",
                    Balance = 20000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };

                var exampleAccountDtoList = new List<AccountDto>()
                {
                    new AccountDto()
                    {
                        Id = 1,
                        AccountNumber = "1234567890",
                        Balance = 10000,
                        NickName = "ExampleNickName"
                    },
                    new AccountDto()
                    {
                        Id = 2,
                        AccountNumber = "1234567892",
                        Balance = 20000,
                        NickName = "ExampleNickName2"
                    }
                };

                var accountMapper = new AccountDtoMapper();

                var sut = new AccountServices(assertContext, accountMapper);

                var result = await sut.FindAccountsContainingAsync("123", 2);

                Assert.IsInstanceOfType(result, typeof(List<AccountDto>));

                Assert.AreEqual(result.Count, 1);
                Assert.AreEqual(result[0].Id, exampleAccountDto.Id);
                Assert.AreEqual(result[0].NickName, exampleAccountDto.NickName);
                Assert.AreEqual(result[0].Balance, exampleAccountDto.Balance);
                Assert.AreEqual(result[0].AccountNumber, exampleAccountDto.AccountNumber);
                Assert.AreEqual(result[0].ClientId, exampleAccountDto.ClientId);
            }
        }

        [TestMethod]
        public async Task FindAccountsContainingAsync_Should_ReturnExactlyTenMatches()
        {
            var options = TestUtils.GetOptions(nameof(FindAccountsContainingAsync_Should_ReturnExactlyTenMatches));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1234567892",
                    Balance = 20000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };

                var exampleAccount3 = new Account()
                {
                    Id = 3,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccount4 = new Account()
                {
                    Id = 4,
                    AccountNumber = "1234567892",
                    Balance = 20000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };

                var exampleAccount5 = new Account()
                {
                    Id = 5,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                };

                var exampleAccount6 = new Account()
                {
                    Id = 6,
                    AccountNumber = "1234567892",
                    Balance = 20000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };

                var exampleAccount7 = new Account()
                {
                    Id = 7,
                    AccountNumber = "1234567892",
                    Balance = 20000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };
                var exampleAccount8 = new Account()
                {
                    Id = 8,
                    AccountNumber = "1234567892",
                    Balance = 20000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };
                var exampleAccount9 = new Account()
                {
                    Id = 9,
                    AccountNumber = "1234567892",
                    Balance = 20000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };
                var exampleAccount10 = new Account()
                {
                    Id = 10,
                    AccountNumber = "9999999999",
                    Balance = 20000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };
                var exampleAccount11 = new Account()
                {
                    Id = 11,
                    AccountNumber = "1234567892",
                    Balance = 20000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };
                var exampleAccount12 = new Account()
                {
                    Id = 12,
                    AccountNumber = "1234567892",
                    Balance = 20000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };

                var exampleClient = new Client() { Id = 1, Name = "ExampleClient" };

                arrangeContext.Add(exampleClient);
                arrangeContext.Add(exampleAccount);
                arrangeContext.Add(exampleAccount2);
                arrangeContext.Add(exampleAccount3);
                arrangeContext.Add(exampleAccount4);
                arrangeContext.Add(exampleAccount5);
                arrangeContext.Add(exampleAccount6);
                arrangeContext.Add(exampleAccount7);
                arrangeContext.Add(exampleAccount8);
                arrangeContext.Add(exampleAccount9);
                arrangeContext.Add(exampleAccount10);
                arrangeContext.Add(exampleAccount11);
                arrangeContext.Add(exampleAccount12);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var accountMapper = new AccountDtoMapper();

                var sut = new AccountServices(assertContext, accountMapper);

                var result = await sut.FindAccountsContainingAsync("123", 0);

                Assert.IsInstanceOfType(result, typeof(List<AccountDto>));
                Assert.AreEqual(10, result.Count);
            }
        }

        [TestMethod]
        public async Task GetAllUserAvailableAccountsAsync_Should_GetCorrectAccountsForUser()
        {
            var options = TestUtils.GetOptions(nameof(GetAllUserAvailableAccountsAsync_Should_GetCorrectAccountsForUser));

            using (var arrangeContext = new TransactContext(options))
            {
                var accountOne = new Account()
                {
                    Id = 14,
                    AccountNumber = "2253567894",
                    Balance = 1130,
                    ClientId = 3,
                    NickName = "ExampleNickName14"
                };
                var accountTwo = new Account()
                {
                    Id = 15,
                    AccountNumber = "2253567895",
                    Balance = 1231,
                    ClientId = 3,
                    NickName = "ExampleNickName15"
                };
                var usersAccountsOne = new UsersAccounts()
                {
                    UserId = 7,
                    AccountId = 14
                };
                var usersAccountsTwo = new UsersAccounts()
                {
                    UserId = 7,
                    AccountId = 15
                };
                var clientOne = new Client()
                {
                    Id = 3,
                    Name = "exampleClient3"
                };
                var customUserOne = new User()
                {
                    Id = 7,
                    Name = "ExampleUser7",
                    UserName = "ExampleUserName7",
                    Password = "500E0B7BFD3540CED17D8EFBAB9AADAD2C86D3ADC39E848A8A52EA914A335811",
                    Role = new UserRole { RoleName = "User" },
                    RoleId = 1,
                    UsersAccounts = new List<UsersAccounts>(),
                    UsersClients = new List<UsersClients>(),
                };

                customUserOne.UsersAccounts.Add(usersAccountsTwo);

                arrangeContext.UsersAccounts.Add(usersAccountsOne);
                arrangeContext.UsersAccounts.Add(usersAccountsTwo);

                arrangeContext.Accounts.Add(accountOne);
                arrangeContext.Accounts.Add(accountTwo);

                arrangeContext.Clients.Add(clientOne);

                arrangeContext.Users.Add(customUserOne);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccountDtoList = new List<AccountDto>()
                {
                new AccountDto { Id = 14 },
                new AccountDto { Id = 15 }
                };

                var accountMapper = new AccountDtoMapper();

                var sut = new AccountServices(assertContext, accountMapper);

                var result = await sut.GetAllUserAvailableAccountsAsync(7);

                Assert.AreEqual(result.Count, 2);
                Assert.AreEqual(result.First().Id, 14);
                Assert.AreEqual(result.Skip(1).First().Id, 15);
            }
        }

        [TestMethod]
        public async Task RenameAccountNicknameAsync_Should_CorrectlyRenameAccount()
        {
            var options = TestUtils.GetOptions(nameof(RenameAccountNicknameAsync_Should_CorrectlyRenameAccount));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser"
                };

                var exampleUsersAccounts = new UsersAccounts()
                {
                    UserId = 1,
                    AccountId = 1,
                    UserAccountNickname = "Test1"
                };

                arrangeContext.Accounts.Add(exampleAccount);
                arrangeContext.Users.Add(exampleUser);
                arrangeContext.UsersAccounts.Add(exampleUsersAccounts);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var accountDtoMapperMock = new Mock<IAccountDtoMapper>();

                var sut = new AccountServices(assertContext, accountDtoMapperMock.Object);

                var result = await sut.RenameAccountNicknameAsync(1, "NewNick", 1);

                Assert.AreEqual(assertContext.UsersAccounts.First().UserAccountNickname, "NewNick");
            }
        }

        [TestMethod]
        public async Task RenameAccountNicknameAsync_Should_ReturnCorrectInfo()
        {
            var options = TestUtils.GetOptions(nameof(RenameAccountNicknameAsync_Should_ReturnCorrectInfo));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser"
                };

                var exampleUsersAccounts = new UsersAccounts()
                {
                    UserId = 1,
                    AccountId = 1,
                    UserAccountNickname = "Test1"
                };

                arrangeContext.Accounts.Add(exampleAccount);
                arrangeContext.Users.Add(exampleUser);
                arrangeContext.UsersAccounts.Add(exampleUsersAccounts);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var accountDtoMapper = new AccountDtoMapper();

                var sut = new AccountServices(assertContext, accountDtoMapper);

                var result = await sut.RenameAccountNicknameAsync(1, "NewNick", 1);

                Assert.IsInstanceOfType(result, typeof(AccountDto));
                Assert.AreEqual(result.Id, 1);
                Assert.AreEqual(result.NickName, "NewNick");
            }
        }

        [TestMethod]
        public async Task GetPageCountForAccounts_Should_ReturnProperPageCount()
        {
            var options = TestUtils.GetOptions(nameof(GetPageCountForAccounts_Should_ReturnProperPageCount));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName1"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367892",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };

                var exampleAccount3 = new Account()
                {
                    Id = 3,
                    AccountNumber = "1233367893",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName3"
                };

                var exampleAccount4 = new Account()
                {
                    Id = 4,
                    AccountNumber = "1233367894",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName4"
                };

                var exampleAccount5 = new Account()
                {
                    Id = 5,
                    AccountNumber = "1233367895",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName5"
                };

                var accountToAdd = await arrangeContext.Accounts.AddAsync(exampleAccount);
                var accountToAdd2 = await arrangeContext.Accounts.AddAsync(exampleAccount2);
                var accountToAdd3 = await arrangeContext.Accounts.AddAsync(exampleAccount3);
                var accountToAdd4 = await arrangeContext.Accounts.AddAsync(exampleAccount4);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName1"
                };

                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "Client",
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var result = await sut.GetPageCountForAccountsAsync(2, exampleClient.Id);

                Assert.AreEqual(result, 2);
            }
        }

        [TestMethod]
        public async Task GetPageCountForAccountsWithUsersId_Should_ReturnProperPageCount()
        {
            var options = TestUtils.GetOptions(nameof(GetPageCountForAccountsWithUsersId_Should_ReturnProperPageCount));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName1"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367892",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };

                var exampleAccount3 = new Account()
                {
                    Id = 3,
                    AccountNumber = "1233367893",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName3"
                };

                var exampleAccount4 = new Account()
                {
                    Id = 4,
                    AccountNumber = "1233367894",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName4"
                };

                var exampleAccount5 = new Account()
                {
                    Id = 5,
                    AccountNumber = "1233367895",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName5"
                };

                var accountToAdd = await arrangeContext.Accounts.AddAsync(exampleAccount);
                var accountToAdd2 = await arrangeContext.Accounts.AddAsync(exampleAccount2);
                var accountToAdd3 = await arrangeContext.Accounts.AddAsync(exampleAccount3);
                var accountToAdd4 = await arrangeContext.Accounts.AddAsync(exampleAccount4);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName1"
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var result = await sut.GetPageCountForAccountsWithUsersIdAsync(2, 0);

                Assert.AreEqual(result, 1);
            }
        }

        [TestMethod]
        public async Task FindAccountsForUserContaining_Should_GetCorrectAccounts()
        {
            var options = TestUtils.GetOptions(nameof(FindAccountsForUserContaining_Should_GetCorrectAccounts));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName1"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367892",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "Name",
                    UserName = "ExampleUserName3",
                    Password = "ExamplePassword3",
                    Role = new UserRole { RoleName = "User" }                   
                };

                var exampleUsersAccounts = new UsersAccounts()
                {
                    UserId = 1,
                    AccountId = 1                   
                };

                var exampleUsersAccounts2 = new UsersAccounts()
                {
                    UserId = 1,
                    AccountId = 2
                };

                var exampleUsersClients = new UsersClients()
                {
                    UserId = 1,
                    ClientId = 1
                };

                var exampleClient = new Client()
                {
                    Id = 1
                };

                arrangeContext.Add(exampleUser);
                arrangeContext.Add(exampleAccount);
                arrangeContext.Add(exampleAccount2);
                arrangeContext.Add(exampleUsersAccounts);
                arrangeContext.Add(exampleUsersAccounts);
                arrangeContext.Add(exampleClient);
                arrangeContext.Add(exampleUsersClients);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName1"
                };

                var exampleAccountDto2 = new AccountDto()
                {
                    Id = 2,
                    AccountNumber = "1233367892",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName2"
                };

                var exampleAccountDtoList = new List<AccountDto>()
                {
                    new AccountDto()
                    {
                        Id = 1,
                        AccountNumber = "1233367891",
                        Balance = 10000,
                        ClientId = 1,
                        NickName = "1233367891"
                    },
                    new AccountDto()
                    {
                        Id = 2,
                        AccountNumber = "1233367892",
                        Balance = 10000,
                        ClientId = 1,
                        NickName = "1233367892"
                    }
                };

                var accountMapper = new AccountDtoMapper();

                var sut = new AccountServices(assertContext, accountMapper);

                var result = await sut.FindAccountsForUserContainingAsync("123", 1);

                Assert.IsInstanceOfType(result, typeof(List<AccountDto>));

                Assert.AreEqual(result.Count, 2);
                Assert.AreEqual(result[0].Id, exampleAccountDto.Id);
                Assert.AreEqual(result[0].AccountNumber, exampleAccountDto.AccountNumber);
                Assert.AreEqual(result[0].Balance, exampleAccountDto.Balance);
                Assert.AreEqual(result[0].NickName, exampleAccountDto.NickName);

                Assert.AreEqual(result[1].Id, exampleAccountDto2.Id);
                Assert.AreEqual(result[1].AccountNumber, exampleAccountDto2.AccountNumber);
                Assert.AreEqual(result[1].Balance, exampleAccountDto2.Balance);
                Assert.AreEqual(result[1].NickName, exampleAccountDto2.NickName);
            }
        }

        [TestMethod]
        public async Task GetFiveAccountsForClientById_Should_ReturnCorrectUsersIfCurrPageIs1()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveAccountsForClientById_Should_ReturnCorrectUsersIfCurrPageIs1));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount1 = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367893",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367892"
                };

                var exampleAccount3 = new Account()
                {
                    Id = 3,
                    AccountNumber = "1233367893",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367893"
                };

                var exampleAccount4 = new Account()
                {
                    Id = 4,
                    AccountNumber = "1233367894",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367894"
                };

                var exampleAccount5 = new Account()
                {
                    Id = 5,
                    AccountNumber = "1233367895",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367895"
                };

                var exampleAccount6 = new Account()
                {
                    Id = 6,
                    AccountNumber = "1233367896",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367896"
                };

                var AccountToAdd1 = await arrangeContext.Accounts.AddAsync(exampleAccount1);
                var AccountToAdd2 = await arrangeContext.Accounts.AddAsync(exampleAccount2);
                var AccountToAdd3 = await arrangeContext.Accounts.AddAsync(exampleAccount3);
                var AccountToAdd4 = await arrangeContext.Accounts.AddAsync(exampleAccount4);
                var AccountToAdd5 = await arrangeContext.Accounts.AddAsync(exampleAccount5);
                var AccountToAdd6 = await arrangeContext.Accounts.AddAsync(exampleAccount6);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccountDtoList = new List<AccountDto>()
                {
                new AccountDto { Id = 6 },
                new AccountDto { Id = 5 },
                new AccountDto { Id = 4 },
                new AccountDto { Id = 3 },
                new AccountDto { Id = 2 }
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<Account>>())).Returns(exampleAccountDtoList);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var result = await sut.GetFiveAccountsForClientByIdAsync(1, 0);

                Assert.AreEqual(result.Count, 5);
                Assert.AreEqual(result.First().Id, 6);
                Assert.AreEqual(result.Skip(1).First().Id, 5);
                Assert.AreEqual(result.Skip(2).First().Id, 4);
                Assert.AreEqual(result.Skip(3).First().Id, 3);
                Assert.AreEqual(result.Skip(4).First().Id, 2);
            }
        }

        [TestMethod]
        public async Task GetFiveAccountsForClientById_Should_ReturnCorrectUsersIfCurrPageIs2()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveAccountsForClientById_Should_ReturnCorrectUsersIfCurrPageIs2));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount1 = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367893",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367892"
                };

                var exampleAccount3 = new Account()
                {
                    Id = 3,
                    AccountNumber = "1233367893",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367893"
                };

                var exampleAccount4 = new Account()
                {
                    Id = 4,
                    AccountNumber = "1233367894",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367894"
                };

                var exampleAccount5 = new Account()
                {
                    Id = 5,
                    AccountNumber = "1233367895",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367895"
                };

                var exampleAccount6 = new Account()
                {
                    Id = 6,
                    AccountNumber = "1233367896",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367896"
                };

                var AccountToAdd1 = await arrangeContext.Accounts.AddAsync(exampleAccount1);
                var AccountToAdd2 = await arrangeContext.Accounts.AddAsync(exampleAccount2);
                var AccountToAdd3 = await arrangeContext.Accounts.AddAsync(exampleAccount3);
                var AccountToAdd4 = await arrangeContext.Accounts.AddAsync(exampleAccount4);
                var AccountToAdd5 = await arrangeContext.Accounts.AddAsync(exampleAccount5);
                var AccountToAdd6 = await arrangeContext.Accounts.AddAsync(exampleAccount6);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccountDtoList = new List<AccountDto>()
                {
                new AccountDto { Id = 6 },
                new AccountDto { Id = 5 },
                new AccountDto { Id = 4 },
                new AccountDto { Id = 3 },
                new AccountDto { Id = 2 }
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<Account>>())).Returns(exampleAccountDtoList);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var result = await sut.GetFiveAccountsForClientByIdAsync(2, 0);

                Assert.AreEqual(result.Count, 5);
                Assert.AreEqual(result.First().Id, 6);
                Assert.AreEqual(result.Skip(1).First().Id, 5);
                Assert.AreEqual(result.Skip(2).First().Id, 4);
                Assert.AreEqual(result.Skip(3).First().Id, 3);
                Assert.AreEqual(result.Skip(4).First().Id, 2);
            }
        }

        [TestMethod]
        public async Task GetFiveAccountsForUserById_Should_ReturnCorrectUsersIfCurrPageIs1()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveAccountsForUserById_Should_ReturnCorrectUsersIfCurrPageIs1));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount1 = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367893",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367892"
                };

                var exampleAccount3 = new Account()
                {
                    Id = 3,
                    AccountNumber = "1233367893",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367893"
                };

                var exampleAccount4 = new Account()
                {
                    Id = 4,
                    AccountNumber = "1233367894",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367894"
                };

                var exampleAccount5 = new Account()
                {
                    Id = 5,
                    AccountNumber = "1233367895",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367895"
                };

                var exampleAccount6 = new Account()
                {
                    Id = 6,
                    AccountNumber = "1233367896",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367896"
                };

                var AccountToAdd1 = await arrangeContext.Accounts.AddAsync(exampleAccount1);
                var AccountToAdd2 = await arrangeContext.Accounts.AddAsync(exampleAccount2);
                var AccountToAdd3 = await arrangeContext.Accounts.AddAsync(exampleAccount3);
                var AccountToAdd4 = await arrangeContext.Accounts.AddAsync(exampleAccount4);
                var AccountToAdd5 = await arrangeContext.Accounts.AddAsync(exampleAccount5);
                var AccountToAdd6 = await arrangeContext.Accounts.AddAsync(exampleAccount6);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccountDtoList = new List<AccountDto>()
                {
                new AccountDto { Id = 6 },
                new AccountDto { Id = 5 },
                new AccountDto { Id = 4 },
                new AccountDto { Id = 3 },
                new AccountDto { Id = 2 }
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<UsersAccounts>>())).Returns(exampleAccountDtoList);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var result = await sut.GetFiveAccountsForUserByIdAsync(1, 0);

                Assert.AreEqual(result.Count, 5);
                Assert.AreEqual(result.First().Id, 6);
                Assert.AreEqual(result.Skip(1).First().Id, 5);
                Assert.AreEqual(result.Skip(2).First().Id, 4);
                Assert.AreEqual(result.Skip(3).First().Id, 3);
                Assert.AreEqual(result.Skip(4).First().Id, 2);
            }
        }

        [TestMethod]
        public async Task GetFiveAccountsForUserById_Should_ReturnCorrectUsersIfCurrPageIs2()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveAccountsForUserById_Should_ReturnCorrectUsersIfCurrPageIs2));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount1 = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367893",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367892"
                };

                var exampleAccount3 = new Account()
                {
                    Id = 3,
                    AccountNumber = "1233367893",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367893"
                };

                var exampleAccount4 = new Account()
                {
                    Id = 4,
                    AccountNumber = "1233367894",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367894"
                };

                var exampleAccount5 = new Account()
                {
                    Id = 5,
                    AccountNumber = "1233367895",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367895"
                };

                var exampleAccount6 = new Account()
                {
                    Id = 6,
                    AccountNumber = "1233367896",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367896"
                };

                var AccountToAdd1 = await arrangeContext.Accounts.AddAsync(exampleAccount1);
                var AccountToAdd2 = await arrangeContext.Accounts.AddAsync(exampleAccount2);
                var AccountToAdd3 = await arrangeContext.Accounts.AddAsync(exampleAccount3);
                var AccountToAdd4 = await arrangeContext.Accounts.AddAsync(exampleAccount4);
                var AccountToAdd5 = await arrangeContext.Accounts.AddAsync(exampleAccount5);
                var AccountToAdd6 = await arrangeContext.Accounts.AddAsync(exampleAccount6);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccountDtoList = new List<AccountDto>()
                {
                new AccountDto { Id = 6 },
                new AccountDto { Id = 5 },
                new AccountDto { Id = 4 },
                new AccountDto { Id = 3 },
                new AccountDto { Id = 2 }
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<UsersAccounts>>())).Returns(exampleAccountDtoList);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var result = await sut.GetFiveAccountsForUserByIdAsync(2, 0);

                Assert.AreEqual(result.Count, 5);
                Assert.AreEqual(result.First().Id, 6);
                Assert.AreEqual(result.Skip(1).First().Id, 5);
                Assert.AreEqual(result.Skip(2).First().Id, 4);
                Assert.AreEqual(result.Skip(3).First().Id, 3);
                Assert.AreEqual(result.Skip(4).First().Id, 2);
            }
        }

        [TestMethod]
        public async Task AssignAccountToUser_Should_ReturnCorrectUser()
        {
            var options = TestUtils.GetOptions(nameof(AssignAccountToUser_Should_ReturnCorrectUser));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var accToAdd = await arrangeContext.Accounts.AddAsync(exampleAccount);

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();

                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<Account>())).Returns(exampleAccountDto);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var result = await sut.AssignAccountToUserAsync(exampleAccount.AccountNumber, exampleUser.Id);

                Assert.AreEqual(result.Id, exampleAccount.Id);
                Assert.AreEqual(result.AccountNumber, exampleAccount.AccountNumber);
                Assert.IsInstanceOfType(result, typeof(AccountDto));
            }
        }

        [TestMethod]
        public async Task AssignAccountToUser_Should_ThrowWhenNoSuchUser()
        {
            var options = TestUtils.GetOptions(nameof(AssignAccountToUser_Should_ReturnCorrectUser));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var accToAdd = await arrangeContext.Accounts.AddAsync(exampleAccount);

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var exampleUser2 = new User()
                {
                    Id = 2,
                    Name = "ExampleUser2",
                    UserName = "ExmplUserName2"
                };

                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();

                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<Account>())).Returns(exampleAccountDto);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                         await sut.AssignAccountToUserAsync(exampleAccount.AccountNumber, exampleUser2.Id));

                Assert.AreEqual(ex.Message, ExceptionMessages.UserNull);
            }
        }

        [TestMethod]
        public async Task AssignAccountToUser_Should_ThrowWhenNoSuchAccount()
        {
            var options = TestUtils.GetOptions(nameof(AssignAccountToUser_Should_ThrowWhenNoSuchAccount));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var accToAdd = await arrangeContext.Accounts.AddAsync(exampleAccount);

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367892",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367892"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();

                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<Account>())).Returns(exampleAccountDto);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                         await sut.AssignAccountToUserAsync(exampleAccount2.AccountNumber, exampleUser.Id));

                Assert.AreEqual(ex.Message, ExceptionMessages.AccountNull);
            }
        }

        [TestMethod]
        public async Task RemoveAccountForUser_Should_ReturnCorrectUser()
        {
            var options = TestUtils.GetOptions(nameof(RemoveAccountForUser_Should_ReturnCorrectUser));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };
               
                var accToAdd = await arrangeContext.Accounts.AddAsync(exampleAccount);

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                var exampleUsersAccounts = new UsersAccounts()
                {
                    UserId = 1,
                    AccountId = 1
                };

                var userClients = await arrangeContext.UsersAccounts.AddAsync(exampleUsersAccounts);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();

                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<Account>())).Returns(exampleAccountDto);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var result = await sut.RemoveAccountForUser(exampleUser.Id, exampleAccount.Id);

                Assert.AreEqual(result.Id, exampleAccount.Id);
                Assert.AreEqual(result.AccountNumber, exampleAccount.AccountNumber);
                Assert.IsInstanceOfType(result, typeof(AccountDto));
            }
        }

        [TestMethod]
        public async Task RemoveAccountForUser_Should_ThrowWhenNoSuchUser()
        {
            var options = TestUtils.GetOptions(nameof(RemoveAccountForUser_Should_ThrowWhenNoSuchUser));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var accToAdd = await arrangeContext.Accounts.AddAsync(exampleAccount);

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var exampleUser2 = new User()
                {
                    Id = 2,
                    Name = "ExampleUser2",
                    UserName = "ExmplUserName2"
                };

                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();

                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<Account>())).Returns(exampleAccountDto);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                         await sut.RemoveAccountForUser(exampleUser2.Id, exampleAccount.Id));

                Assert.AreEqual(ex.Message, ExceptionMessages.UserNull);
            }
        }

        [TestMethod]
        public async Task RemoveAccountForUser_Should_ThrowWhenNoSuchAccount()
        {
            var options = TestUtils.GetOptions(nameof(RemoveAccountForUser_Should_ThrowWhenNoSuchAccount));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var accToAdd = await arrangeContext.Accounts.AddAsync(exampleAccount);

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccount2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1233367892",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367892"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();

                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<Account>())).Returns(exampleAccountDto);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                         await sut.RemoveAccountForUser(exampleUser.Id, exampleAccount2.Id));

                Assert.AreEqual(ex.Message, ExceptionMessages.AccountNull);
            }
        }

        [TestMethod]
        public async Task RemoveAccountForUser_Should_ThrowWhenNoSuchUserAccounts()
        {
            var options = TestUtils.GetOptions(nameof(RemoveAccountForUser_Should_ThrowWhenNoSuchUserAccounts));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var accToAdd = await arrangeContext.Accounts.AddAsync(exampleAccount);

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();

                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<Account>())).Returns(exampleAccountDto);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                         await sut.RemoveAccountForUser(exampleUser.Id, exampleAccount.Id));

                Assert.AreEqual(ex.Message, ExceptionMessages.UsersAccountsNull);
            }
        }

        [TestMethod]
        public async Task AddAccountToClient_Should_CorrectlyAddAccount()
        {
            var options = TestUtils.GetOptions(nameof(AddAccountToClient_Should_CorrectlyAddAccount));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient"
                };

              

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                var clientToAdd = arrangeContext.Add(exampleClient);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();

                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    ClientName = "ExampleClient",
                    NickName = "1233367891"
                };

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<AccountDto>())).Returns(exampleAccount);              

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var result = sut.AddAccountToClientAsync(exampleAccountDto);

                Assert.AreEqual(assertContext.Accounts.Count(), 1);
                Assert.AreEqual(assertContext.Accounts.First().Id, exampleAccount.Id);
                Assert.AreEqual(assertContext.Accounts.First().AccountNumber, exampleAccount.AccountNumber);
                Assert.AreEqual(assertContext.Accounts.First().Balance, exampleAccount.Balance);
                Assert.AreEqual(assertContext.Accounts.First().NickName, exampleAccount.NickName);
                Assert.AreEqual(assertContext.Accounts.First().ClientId, exampleAccount.ClientId);
                Assert.IsInstanceOfType(assertContext.Accounts.First(), typeof(Account));
            }
        }

        [TestMethod]
        public async Task AddAccountToClient_Should_ThrowWhenNoSuchClient()
        {
            var options = TestUtils.GetOptions(nameof(AddAccountToClient_Should_ThrowWhenNoSuchClient));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();

                var exampleAccountDto = new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    ClientName = "ExampleClient",
                    NickName = "1233367891"
                };

                var exampleAccount = new Account()
                {
                    Id = 1,
                    AccountNumber = "1233367891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1233367891"
                };

                fullAccountMapperMock.Setup(x => x.MapFrom(It.IsAny<AccountDto>())).Returns(exampleAccount);

                var sut = new AccountServices(assertContext, fullAccountMapperMock.Object);

                var result = sut.AddAccountToClientAsync(exampleAccountDto);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                         await sut.AddAccountToClientAsync(exampleAccountDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.ClientNull);
            }
        }
    }
}