﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Mapper;

namespace Transact.Services.UnitTests
{
    [TestClass]
    public class AdminDtoMapper_Should
    {
        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_Admin_To_AdminDto()
        {
            var sut = new AdminDtoMapper();

            var exampleAdmin = new Admin()
            {
                Id = 1,
                UserName = "ExampleUserName1",
                Password = "ExamplePassword1",
                Role = new UserRole { RoleName = "Admin" },
                RoleId = 2
            };

            var result = sut.MapFrom(exampleAdmin);

            Assert.IsInstanceOfType(result, typeof(AdminDto));
            Assert.AreEqual(result.Id, exampleAdmin.Id);
            Assert.AreEqual(result.UserName, exampleAdmin.UserName);
            Assert.AreEqual(result.RoleId, exampleAdmin.RoleId);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_AdminTo_To_Admin()
        {
            var sut = new AdminDtoMapper();

            var exampleAdminDto = new AdminDto()
            {
                Id = 1,
                RoleId = 2,
                UserName = "ExampleUserName1",
                Password = "ExamplePassword1",
                RoleName = "Example"
            };

            var result = sut.MapFrom(exampleAdminDto);

            Assert.IsInstanceOfType(result, typeof(Admin));
            Assert.AreEqual(result.Id, exampleAdminDto.Id);
            Assert.AreEqual(result.RoleId, exampleAdminDto.RoleId);
            Assert.AreEqual(result.UserName, exampleAdminDto.UserName);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfAdmins_To_ListOfAdminsDtos()
        {
            var exampleAdminDtoList = new List<AdminDto>()
            {
                new AdminDto()
                {
                Id = 1,
                RoleId = 2,
                UserName = "ExampleUserName1",
                RoleName = "Example"
                },
                new AdminDto()
                {
                Id = 1,
                RoleId = 2,
                UserName = "ExampleUserName1",
                RoleName = "Example"
                }
            };

            var sut = new AdminDtoMapper();

            var result = sut.MapFrom(exampleAdminDtoList);

            Assert.IsInstanceOfType(result, typeof(List<Admin>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleAdminDtoList[0].Id);
            Assert.AreEqual(result[0].RoleId, exampleAdminDtoList[0].RoleId);
            Assert.AreEqual(result[0].UserName, exampleAdminDtoList[0].UserName);

            Assert.AreEqual(result[1].Id, exampleAdminDtoList[1].Id);
            Assert.AreEqual(result[1].RoleId, exampleAdminDtoList[1].RoleId);
            Assert.AreEqual(result[1].UserName, exampleAdminDtoList[1].UserName);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfAdminsDtos_To_ListOfAdmins()
        {
            var exampleAdminList = new List<Admin>()
            {
                new Admin()
                {
                Id = 1,
                UserName = "ExampleUserName1",
                Role = new UserRole { RoleName = "Admin" },
                RoleId = 2
                },
                new Admin()
                {
                Id = 1,
                UserName = "ExampleUserName1",
                Role = new UserRole { RoleName = "Admin" },
                RoleId = 2
                }
            };

            var sut = new AdminDtoMapper();

            var result = sut.MapFrom(exampleAdminList);

            Assert.IsInstanceOfType(result, typeof(List<AdminDto>));

            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleAdminList[0].Id);
            Assert.AreEqual(result[0].RoleId, exampleAdminList[0].RoleId);
            Assert.AreEqual(result[0].UserName, exampleAdminList[0].UserName);

            Assert.AreEqual(result[1].Id, exampleAdminList[1].Id);
            Assert.AreEqual(result[1].RoleId, exampleAdminList[1].RoleId);
            Assert.AreEqual(result[1].UserName, exampleAdminList[1].UserName);
        }
    }
}