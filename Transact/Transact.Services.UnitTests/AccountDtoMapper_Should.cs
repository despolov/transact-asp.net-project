﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Mapper;

namespace Transact.Services.UnitTests
{
    [TestClass]
    public class AccountDtoMapper_Should
    {
        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_Account_To_AccountDto()
        {
            var sut = new AccountDtoMapper();

            var exampleAccount = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                NickName = "ExampleNickName1",
                Balance = 10000,
                ClientId = 2,
                Client = new Client() { Id = 2, Name = "ExampleClientName"}
                
            };

            var result = sut.MapFrom(exampleAccount);

            Assert.IsInstanceOfType(result, typeof(AccountDto));
            Assert.AreEqual(result.Id, exampleAccount.Id);
            Assert.AreEqual(result.AccountNumber, exampleAccount.AccountNumber);
            Assert.AreEqual(result.NickName, exampleAccount.NickName);
            Assert.AreEqual(result.Balance, exampleAccount.Balance);
            Assert.AreEqual(result.ClientName, exampleAccount.Client.Name);
            Assert.AreEqual(result.ClientId, exampleAccount.Client.Id);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_AccountTo_To_Account()
        {
            var sut = new AccountDtoMapper();

            var exampleAccountDto = new AccountDto()
            {
                Id = 1,
                AccountNumber = "1234567890",
                NickName = "ExampleNickName1",
                Balance = 10000
            };

            var result = sut.MapFrom(exampleAccountDto);

            Assert.IsInstanceOfType(result, typeof(Account));
            Assert.AreEqual(result.Id, exampleAccountDto.Id);
            Assert.AreEqual(result.AccountNumber, exampleAccountDto.AccountNumber);
            Assert.AreEqual(result.NickName, exampleAccountDto.NickName);
            Assert.AreEqual(result.Balance, exampleAccountDto.Balance);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfAccounts_To_ListOfAccountsDtos()
        {
            var exampleAccountDtoList = new List<AccountDto>()
            {
                new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    NickName = "ExampleNickName1",
                    Balance = 10000
                },
                new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    NickName = "ExampleNickName1",
                    Balance = 10000
                }
            };

            var sut = new AccountDtoMapper();

            var result = sut.MapFrom(exampleAccountDtoList);

            Assert.IsInstanceOfType(result, typeof(List<Account>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleAccountDtoList[0].Id);
            Assert.AreEqual(result[0].AccountNumber, exampleAccountDtoList[0].AccountNumber);
            Assert.AreEqual(result[0].NickName, exampleAccountDtoList[0].NickName);
            Assert.AreEqual(result[0].Balance, exampleAccountDtoList[0].Balance);

            Assert.AreEqual(result[1].Id, exampleAccountDtoList[1].Id);
            Assert.AreEqual(result[1].AccountNumber, exampleAccountDtoList[1].AccountNumber);
            Assert.AreEqual(result[1].NickName, exampleAccountDtoList[1].NickName);
            Assert.AreEqual(result[1].Balance, exampleAccountDtoList[1].Balance);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfAccountsDtos_To_ListOfAccounts()
        {
            var exampleAccountList = new List<Account>()
            {
                new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    NickName = "ExampleNickName1",
                    Balance = 10000,
                    ClientId = 2,
                    Client = new Client() { Id = 2, Name = "ExampleClientName"}
                },
                new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    NickName = "ExampleNickName1",
                    Balance = 10000,
                    ClientId = 3,
                    Client = new Client() { Id = 3, Name = "ExampleClientName2"}
                }
            };

            var sut = new AccountDtoMapper();

            var result = sut.MapFrom(exampleAccountList);

            Assert.IsInstanceOfType(result, typeof(List<AccountDto>));

            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleAccountList[0].Id);
            Assert.AreEqual(result[0].AccountNumber, exampleAccountList[0].AccountNumber);
            Assert.AreEqual(result[0].NickName, exampleAccountList[0].NickName);
            Assert.AreEqual(result[0].Balance, exampleAccountList[0].Balance);
            Assert.AreEqual(result[0].ClientName, exampleAccountList[0].Client.Name);
            Assert.AreEqual(result[0].ClientId, exampleAccountList[0].Client.Id);

            Assert.AreEqual(result[1].Id, exampleAccountList[1].Id);
            Assert.AreEqual(result[1].AccountNumber, exampleAccountList[1].AccountNumber);
            Assert.AreEqual(result[1].NickName, exampleAccountList[1].NickName);
            Assert.AreEqual(result[1].Balance, exampleAccountList[1].Balance);
            Assert.AreEqual(result[1].ClientName, exampleAccountList[1].Client.Name);
            Assert.AreEqual(result[1].ClientId, exampleAccountList[1].Client.Id);
        }
    }
}
