﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transact.Data;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.CustomExceptions;
using Transact.Services.Mapper;
using Transact.Services.Mapper.Contracts;

namespace Transact.Services.UnitTests
{
    [TestClass]
    public class ClientServices_Should
    {
        [TestMethod]
        public async Task AddClientAsync_Should_CorrectlyAddClient()
        {
            var options = TestUtils.GetOptions(nameof(AddClientAsync_Should_CorrectlyAddClient));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClientDto = new ClientDto()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                fullClientMapperMock.Setup(x => x.MapFrom(It.IsAny<ClientDto>())).Returns(exampleClient);

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(arrangeContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var result = sut.AddClientAsync(exampleClientDto);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                Assert.AreEqual(assertContext.Clients.Count(), 1);
                Assert.AreEqual(assertContext.Clients.First().Id, exampleClient.Id);
                Assert.AreEqual(assertContext.Clients.First().Name, exampleClient.Name);
                Assert.IsInstanceOfType(assertContext.Clients.First(), typeof(Client));
            }
        }

        [TestMethod]
        public async Task AddClientAsync_Should_ReturnCorrectClient()
        {
            var options = TestUtils.GetOptions(nameof(AddClientAsync_Should_ReturnCorrectClient));

            using (var assertContext = new TransactContext(options))
            {
                var exampleClientDto = new ClientDto()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                fullClientMapperMock.Setup(x => x.MapFrom(It.IsAny<ClientDto>())).Returns(exampleClient);
                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var result = await sut.AddClientAsync(exampleClientDto);

                Assert.AreEqual(result.Id, exampleClient.Id);
                Assert.AreEqual(result.Name, exampleClientDto.Name);        //Maybe exmplClient
                Assert.IsInstanceOfType(result, typeof(ClientDto));
            }
        }

        [TestMethod]
        public async Task AddClientAsync_Should_ThrowArgumentException_ClientAlreadyExists()
        {
            var options = TestUtils.GetOptions(nameof(AddClientAsync_Should_ThrowArgumentException_ClientAlreadyExists));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };
                var clientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClientDto = new ClientDto()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                fullClientMapperMock.Setup(x => x.MapFrom(It.IsAny<ClientDto>())).Returns(exampleClient);
                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.AddClientAsync(exampleClientDto));

                Assert.AreEqual(ex.Message, ExceptionMessages.ClientNameExists);
            }
        }

        [TestMethod]
        public async Task AddClientAsync_Should_CallClientMapperMock_MapFrom_Once()
        {
            var options = TestUtils.GetOptions(nameof(AddClientAsync_Should_CallClientMapperMock_MapFrom_Once));

            using (var assertContext = new TransactContext(options))
            {
                var exampleClientDto = new ClientDto()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                fullClientMapperMock.Setup(x => x.MapFrom(It.IsAny<ClientDto>())).Returns(exampleClient);

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var result = await sut.AddClientAsync(exampleClientDto);

                fullClientMapperMock.Verify(x => x.MapFrom(It.IsAny<ClientDto>()));
            }
        }

        [TestMethod]
        public async Task GetClientAsync_Should_ReturnCorrectClient()
        {
            var options = TestUtils.GetOptions(nameof(GetClientAsync_Should_ReturnCorrectClient));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var clientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClientDto = new ClientDto()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                fullClientMapperMock.Setup(x => x.MapFrom(It.IsAny<Client>())).Returns(exampleClientDto);

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var result = await sut.GetClientAsync(exampleClient.Name);

                Assert.AreEqual(result.Id, exampleClient.Id);
                Assert.AreEqual(result.Name, exampleClient.Name);           //maybe exmplClientDto
                Assert.IsInstanceOfType(result, typeof(ClientDto));
            }
        }

        [TestMethod]
        public async Task GetClientAsync_Should_ThrowArgumentException_WhenNoSuchClient()
        {
            var options = TestUtils.GetOptions(nameof(GetClientAsync_Should_ThrowArgumentException_WhenNoSuchClient));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var clientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClientDto = new ClientDto()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleClient2 = new Client()
                {
                    Id = 2,
                    Name = "ExampleClient2"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                fullClientMapperMock.Setup(x => x.MapFrom(It.IsAny<Client>())).Returns(exampleClientDto);

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.GetClientAsync(exampleClient2.Name));

                Assert.AreEqual(ex.Message, ExceptionMessages.ClientNull);
            }
        }

        [TestMethod]
        public async Task GetClientAsync_Should_CallClientMapperMock_MapFrom_Once()
        {

            var options = TestUtils.GetOptions(nameof(GetClientAsync_Should_CallClientMapperMock_MapFrom_Once));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var clientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClientDto = new ClientDto()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                fullClientMapperMock.Setup(x => x.MapFrom(It.IsAny<Client>())).Returns(exampleClientDto);

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var result = await sut.GetClientAsync(exampleClient.Name);

                fullClientMapperMock.Verify(x => x.MapFrom(It.IsAny<Client>()));
            }
        }

        [TestMethod]
        public async Task GetPageCountForClientsAsync_Should_ReturnProperPageCount()
        {
            var options = TestUtils.GetOptions(nameof(GetPageCountForClientsAsync_Should_ReturnProperPageCount));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleClient2 = new Client()
                {
                    Id = 2,
                    Name = "ExampleClient2"
                };

                var exampleClient3 = new Client()
                {
                    Id = 3,
                    Name = "ExampleClient3"
                };

                var exampleClient4 = new Client()
                {
                    Id = 4,
                    Name = "ExampleClient4"
                };

                var exampleClient5 = new Client()
                {
                    Id = 5,
                    Name = "ExampleClient5"
                };

                var userToAdd = await arrangeContext.Clients.AddAsync(exampleClient);
                var userToAdd2 = await arrangeContext.Clients.AddAsync(exampleClient2);
                var userToAdd3 = await arrangeContext.Clients.AddAsync(exampleClient3);
                var userToAdd4 = await arrangeContext.Clients.AddAsync(exampleClient4);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClientDto = new ClientDto()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                fullClientMapperMock.Setup(x => x.MapFrom(It.IsAny<Client>())).Returns(exampleClientDto);

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var result = await sut.GetPageCountForClientsAsync(2);

                Assert.AreEqual(result, 2);
            }
        }

        [TestMethod]
        public async Task GetFiveClientsById_Should_ReturnCorrectClients()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveClientsById_Should_ReturnCorrectClients));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleClient2 = new Client()
                {
                    Id = 2,
                    Name = "ExampleClient2"
                };

                var exampleClient3 = new Client()
                {
                    Id = 3,
                    Name = "ExampleClient3"
                };

                var exampleClient4 = new Client()
                {
                    Id = 4,
                    Name = "ExampleClient4"
                };

                var exampleClient5 = new Client()
                {
                    Id = 5,
                    Name = "ExampleClient5"
                };

                var exampleClient6 = new Client()
                {
                    Id = 6,
                    Name = "ExampleClient6"
                };

                var userToAdd = await arrangeContext.Clients.AddAsync(exampleClient);
                var userToAdd2 = await arrangeContext.Clients.AddAsync(exampleClient2);
                var userToAdd3 = await arrangeContext.Clients.AddAsync(exampleClient3);
                var userToAdd4 = await arrangeContext.Clients.AddAsync(exampleClient4);
                var userToAdd5 = await arrangeContext.Clients.AddAsync(exampleClient5);
                var userToAdd6 = await arrangeContext.Clients.AddAsync(exampleClient6);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClientDtoList = new List<ClientDto>()
                {
                //new ClientDto { Id = 6 },
                //new ClientDto { Id = 5 },
                //new ClientDto { Id = 4 },
                //new ClientDto { Id = 3 },
                new ClientDto { Id = 1 }
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                fullClientMapperMock.Setup(x => x.MapFrom(It.IsAny<List<Client>>())).Returns(exampleClientDtoList);

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var result = await sut.GetFiveClientsByIdAsync(2);

                Assert.AreEqual(result.Count, 1);
                Assert.AreEqual(result.First().Id, 1);
                //Assert.AreEqual(result.Skip(1).First().Id, 5);
                //Assert.AreEqual(result.Skip(2).First().Id, 4);
                //Assert.AreEqual(result.Skip(3).First().Id, 3);
                //Assert.AreEqual(result.Skip(4).First().Id, 2);
            }
        }

        [TestMethod]
        public async Task GetFiveClientsById_Should_CallClientMapper_MapFrom_Once()
        {
            var options = TestUtils.GetOptions(nameof(GetFiveClientsById_Should_CallClientMapper_MapFrom_Once));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleClient2 = new Client()
                {
                    Id = 2,
                    Name = "ExampleClient2"
                };

                var exampleClient3 = new Client()
                {
                    Id = 3,
                    Name = "ExampleClient3"
                };

                var exampleClient4 = new Client()
                {
                    Id = 4,
                    Name = "ExampleClient4"
                };

                var exampleClient5 = new Client()
                {
                    Id = 5,
                    Name = "ExampleClient5"
                };

                var exampleClient6 = new Client()
                {
                    Id = 6,
                    Name = "ExampleClient6"
                };

                var userToAdd = await arrangeContext.Clients.AddAsync(exampleClient);
                var userToAdd2 = await arrangeContext.Clients.AddAsync(exampleClient2);
                var userToAdd3 = await arrangeContext.Clients.AddAsync(exampleClient3);
                var userToAdd4 = await arrangeContext.Clients.AddAsync(exampleClient4);
                var userToAdd5 = await arrangeContext.Clients.AddAsync(exampleClient5);
                var userToAdd6 = await arrangeContext.Clients.AddAsync(exampleClient6);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClientDto = new ClientDto()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                fullClientMapperMock.Setup(x => x.MapFrom(It.IsAny<Client>())).Returns(exampleClientDto);

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var result = await sut.GetFiveClientsByIdAsync(1);

                fullClientMapperMock.Verify(x => x.MapFrom(It.IsAny<List<Client>>()), Times.Once);

            }
        }

        [TestMethod]
        public async Task GetSevenLatestClients_Should_GetCorrectClientDtos()
        {
            var options = TestUtils.GetOptions(nameof(GetSevenLatestClients_Should_GetCorrectClientDtos));

            var exampleClient = new Client()
            {
                Id = 1,
                Name = "ExampleClient1",
                Accounts = new List<Account>
                    {
                        new Account() { Id = 1 }
                    }
            };

            using (var arrangeContext = new TransactContext(options))
            {
               

                var exampleClient2 = new Client()
                {
                    Id = 2,
                    Name = "ExampleClient1",
                    Accounts = new List<Account>
                    {
                        new Account() { Id = 2 }
                    }
                };

                var exampleClient3 = new Client()
                {
                    Id = 3,
                    Name = "ExampleClient1",
                    Accounts = new List<Account>
                    {
                        new Account() { Id = 3 }
                    }
                };

                var exampleClient4 = new Client()
                {
                    Id = 4,
                    Name = "ExampleClient1",
                    Accounts = new List<Account>
                    {
                        new Account() { Id = 4 }
                    }
                };

                var exampleClient5 = new Client()
                {
                    Id = 5,
                    Name = "ExampleClient1",
                    Accounts = new List<Account>
                    {
                        new Account() { Id = 5 }
                    }
                };

                var exampleClient6 = new Client()
                {
                    Id = 6,
                    Name = "ExampleClient1",
                    Accounts = new List<Account>
                    {
                        new Account() { Id = 6 }
                    }
                };

                var exampleClient7 = new Client()
                {
                    Id = 7,
                    Name = "ExampleClient1",
                    Accounts = new List<Account>
                    {
                        new Account() { Id = 7 }
                    }
                };

                var exampleClient8 = new Client()
                {
                    Id = 8,
                    Name = "ExampleClient1",
                    Accounts = new List<Account>
                    {
                        new Account() { Id = 8 }
                    }
                };

                await arrangeContext.Clients.AddAsync(exampleClient);
                await arrangeContext.Clients.AddAsync(exampleClient2);
                await arrangeContext.Clients.AddAsync(exampleClient3);
                await arrangeContext.Clients.AddAsync(exampleClient4);
                await arrangeContext.Clients.AddAsync(exampleClient5);
                await arrangeContext.Clients.AddAsync(exampleClient6);
                await arrangeContext.Clients.AddAsync(exampleClient7);
                await arrangeContext.Clients.AddAsync(exampleClient8);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var fullClientMapper = new ClientDtoMapper();

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();

                var sut = new ClientServices(assertContext, fullClientMapper, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var result =  new List<ClientDto>(await sut.GetSevenLatestClientsAsync());

                Assert.AreEqual(result.Count, 7);
                Assert.IsInstanceOfType(result, typeof(List<ClientDto>));

                Assert.AreEqual(result[0].Id, 8);
                Assert.AreEqual(result[0].NumberOfAccounts, 1);
                Assert.AreEqual(result[0].Name, exampleClient.Name);

                Assert.AreEqual(result[1].Id, 7);
                Assert.AreEqual(result[2].Id, 6);
                Assert.AreEqual(result[3].Id, 5);
                Assert.AreEqual(result[4].Id, 4);
                Assert.AreEqual(result[5].Id, 3);
                Assert.AreEqual(result[6].Id, 2);             
            }
        }

        [TestMethod]
        public async Task GetSevenLatestClients_Should_CallClientDtoMapper_Once()
        {
            var options = TestUtils.GetOptions(nameof(GetSevenLatestClients_Should_CallClientDtoMapper_Once));

            var exampleClient = new Client()
            {
                Id = 1,
                Name = "ExampleClient1",
                Accounts = new List<Account>
                    {
                        new Account() { Id = 1 }
                    }
            };

            using (var arrangeContext = new TransactContext(options))
            {


                var exampleClient2 = new Client()
                {
                    Id = 2,
                    Name = "ExampleClient1",
                    Accounts = new List<Account>
                    {
                        new Account() { Id = 2 }
                    }
                };

                var exampleClient3 = new Client()
                {
                    Id = 3,
                    Name = "ExampleClient1",
                    Accounts = new List<Account>
                    {
                        new Account() { Id = 3 }
                    }
                };

                var exampleClient4 = new Client()
                {
                    Id = 4,
                    Name = "ExampleClient1",
                    Accounts = new List<Account>
                    {
                        new Account() { Id = 4 }
                    }
                };

                var exampleClient5 = new Client()
                {
                    Id = 5,
                    Name = "ExampleClient1",
                    Accounts = new List<Account>
                    {
                        new Account() { Id = 5 }
                    }
                };

                var exampleClient6 = new Client()
                {
                    Id = 6,
                    Name = "ExampleClient1",
                    Accounts = new List<Account>
                    {
                        new Account() { Id = 6 }
                    }
                };

                var exampleClient7 = new Client()
                {
                    Id = 7,
                    Name = "ExampleClient1",
                    Accounts = new List<Account>
                    {
                        new Account() { Id = 7 }
                    }
                };

                var exampleClient8 = new Client()
                {
                    Id = 8,
                    Name = "ExampleClient1",
                    Accounts = new List<Account>
                    {
                        new Account() { Id = 8 }
                    }
                };

                await arrangeContext.Clients.AddAsync(exampleClient);
                await arrangeContext.Clients.AddAsync(exampleClient2);
                await arrangeContext.Clients.AddAsync(exampleClient3);
                await arrangeContext.Clients.AddAsync(exampleClient4);
                await arrangeContext.Clients.AddAsync(exampleClient5);
                await arrangeContext.Clients.AddAsync(exampleClient6);
                await arrangeContext.Clients.AddAsync(exampleClient7);
                await arrangeContext.Clients.AddAsync(exampleClient8);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                fullClientMapperMock.Setup(x => x.MapFrom(It.IsAny<List<Client>>())).Returns(new List<ClientDto>());

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();

                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var result = new List<ClientDto>(await sut.GetSevenLatestClientsAsync());

                fullClientMapperMock.Verify(x => x.MapFrom(It.IsAny<List<Client>>()), Times.Once);
            }
        }

        [TestMethod]
        public async Task GetClientById_Should_ReturnCorrectClient()
        {
            var options = TestUtils.GetOptions(nameof(GetClientById_Should_ReturnCorrectClient));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var clientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClientDto = new ClientDto()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                fullClientMapperMock.Setup(x => x.MapFrom(It.IsAny<Client>())).Returns(exampleClientDto);

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var result = await sut.GetClientByIdAsync(exampleClient.Id);

                Assert.AreEqual(result.Id, exampleClient.Id);
                Assert.AreEqual(result.Name, exampleClient.Name);
                Assert.IsInstanceOfType(result, typeof(ClientDto));
            }
        }

        [TestMethod]
        public async Task GetClientById_Should_ThrowArgumentException_WhenNoSuchClient()
        {
            var options = TestUtils.GetOptions(nameof(GetClientById_Should_ThrowArgumentException_WhenNoSuchClient));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var clientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClientDto = new ClientDto()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleClient2 = new Client()
                {
                    Id = 2,
                    Name = "ExampleClient2"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                fullClientMapperMock.Setup(x => x.MapFrom(It.IsAny<Client>())).Returns(exampleClientDto);

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                await sut.GetClientByIdAsync(exampleClient2.Id));

                Assert.AreEqual(ex.Message, ExceptionMessages.ClientNull);
            }
        }

        [TestMethod]
        public async Task GetClientById_Should_CallClientMapperMock_MapFrom_Once()
        {

            var options = TestUtils.GetOptions(nameof(GetClientById_Should_CallClientMapperMock_MapFrom_Once));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var clientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClientDto = new ClientDto()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                fullClientMapperMock.Setup(x => x.MapFrom(It.IsAny<Client>())).Returns(exampleClientDto);

                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var result = await sut.GetClientByIdAsync(exampleClient.Id);

                fullClientMapperMock.Verify(x => x.MapFrom(It.IsAny<Client>()));
            }
        }

        [TestMethod]
        public async Task AssignUserToClient_Should_ReturnCorrectUser()
        {
            var options = TestUtils.GetOptions(nameof(AssignUserToClient_Should_ReturnCorrectUser));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var clientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleUser1",
                    UserName = "ExmplUserName"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var result = await sut.AssignUserToClientAsync(exampleUser.UserName, exampleClient.Id);

                Assert.AreEqual(result.Id, exampleUser.Id);
                Assert.AreEqual(result.Name, exampleUser.Name);
                Assert.IsInstanceOfType(result, typeof(UserDto));
            }
        }

        [TestMethod]
        public async Task AssignUserToClient_Should_ThrowWhenNoSuchUser()
        {
            var options = TestUtils.GetOptions(nameof(AssignUserToClient_Should_ReturnCorrectUser));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var clientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser",
                    UserName = "ExmplUserName"
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleUser2 = new User()
                {
                    Id = 2,
                    Name = "ExampleUser2",
                    UserName = "ExmplUserName2"
                };

                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleClient1",
                    UserName = "ExmplUserName"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () => 
                         await sut.AssignUserToClientAsync(exampleUser2.UserName, exampleClient.Id));

                Assert.AreEqual(ex.Message, ExceptionMessages.UserNull);
            }
        }

        [TestMethod]
        public async Task AssignUserToClient_Should_ThrowWhenNoSuchClient()
        {
            var options = TestUtils.GetOptions(nameof(AssignUserToClient_Should_ThrowWhenNoSuchClient));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var clientToAdd = await arrangeContext.Clients.AddAsync(exampleClient);

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser",
                    UserName = "ExmplUserName"
                };

                var userToAdd = await arrangeContext.Users.AddAsync(exampleUser);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleClient2 = new Client()
                {
                    Id = 2,
                    Name = "ExampleClient2"
                };

                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser",
                    UserName = "ExmplUserName"
                };

                var exampleUserDto = new UserDto()
                {
                    Id = 1,
                    Name = "ExampleClient1",
                    UserName = "ExmplUserName"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                fullUserMapperMock.Setup(x => x.MapFrom(It.IsAny<User>())).Returns(exampleUserDto);

                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var ex = await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () =>
                         await sut.AssignUserToClientAsync(exampleUser.UserName, exampleClient2.Id));

                Assert.AreEqual(ex.Message, ExceptionMessages.ClientNull);
            }
        }

        [TestMethod]
        public async Task GetAllUserAvailableClientsIds_Should_ReturnCorrectClients()
        {
            var options = TestUtils.GetOptions(nameof(GetAllUserAvailableClientsIds_Should_ReturnCorrectClients));

            using (var arrangeContext = new TransactContext(options))
            {
                var exampleClient = new Client()
                {
                    Id = 1,
                    Name = "ExampleClient1"
                };

                var exampleClient2 = new Client()
                {
                    Id = 2,
                    Name = "ExampleClient2"
                };

                var exampleClient3 = new Client()
                {
                    Id = 3,
                    Name = "ExampleClient3"
                };

                var exampleClient4 = new Client()
                {
                    Id = 4,
                    Name = "ExampleClient4"
                };

                var exampleClient5 = new Client()
                {
                    Id = 5,
                    Name = "ExampleClient5"
                };

                var exampleClient6 = new Client()
                {
                    Id = 6,
                    Name = "ExampleClient6"
                };

                var userToAdd = await arrangeContext.Clients.AddAsync(exampleClient);
                var userToAdd2 = await arrangeContext.Clients.AddAsync(exampleClient2);
                var userToAdd3 = await arrangeContext.Clients.AddAsync(exampleClient3);
                var userToAdd4 = await arrangeContext.Clients.AddAsync(exampleClient4);
                var userToAdd5 = await arrangeContext.Clients.AddAsync(exampleClient5);
                var userToAdd6 = await arrangeContext.Clients.AddAsync(exampleClient6);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new TransactContext(options))
            {
                var exampleUser = new User()
                {
                    Id = 1,
                    Name = "ExampleUser",
                    UserName = "ExmplUserName"
                };

                var fullClientMapperMock = new Mock<IClientDtoMapper>();
                var fullUserMapperMock = new Mock<IUserDtoMapper>();
                var fullAccountMapperMock = new Mock<IAccountDtoMapper>();
                var sut = new ClientServices(assertContext, fullClientMapperMock.Object, fullUserMapperMock.Object, fullAccountMapperMock.Object);

                var result = await sut.GetAllUserAvailableClientsIdsAsync(exampleUser.Id);

                Assert.IsInstanceOfType(result, typeof(ICollection<long>));
            }
        }
    }
}