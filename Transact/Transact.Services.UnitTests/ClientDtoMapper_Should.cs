﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Mapper;

namespace Transact.Services.UnitTests
{
    [TestClass]
    public class ClientDtoMapper_Should
    {
        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_Client_To_ClientDto()
        {
            var sut = new ClientDtoMapper();

            var exampleClient = new Client()
            {
                Id = 1,
                Name = "ExampleClient1"
            };

            var result = sut.MapFrom(exampleClient);

            Assert.IsInstanceOfType(result, typeof(ClientDto));
            Assert.AreEqual(result.Id, exampleClient.Id);
            Assert.AreEqual(result.Name, exampleClient.Name);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_ClientTo_To_Client()
        {
            var sut = new ClientDtoMapper();

            var exampleClientDto = new ClientDto()
            {
                Id = 1,
                Name = "ExampleClient1"
            };

            var result = sut.MapFrom(exampleClientDto);

            Assert.IsInstanceOfType(result, typeof(Client));
            Assert.AreEqual(result.Id, exampleClientDto.Id);
            Assert.AreEqual(result.Name, exampleClientDto.Name);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfClients_To_ListOfClientsDtos()
        {
            var exampleClientDtoList = new List<ClientDto>()
            {
                new ClientDto()
                {
                    Id = 1,
                    Name = "ExampleClientDto1"
                },
                new ClientDto()
                {
                    Id = 2,
                    Name = "ExampleClientDto2"
                }
            };

            var sut = new ClientDtoMapper();

            var result = sut.MapFrom(exampleClientDtoList);

            Assert.IsInstanceOfType(result, typeof(List<Client>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleClientDtoList[0].Id);
            Assert.AreEqual(result[0].Name, exampleClientDtoList[0].Name);
            Assert.AreEqual(result[1].Id, exampleClientDtoList[1].Id);
            Assert.AreEqual(result[1].Name, exampleClientDtoList[1].Name);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfClientsDtos_To_ListOfClients()
        {
            var exampleClientList = new List<Client>()
            {
                new Client()
                {
                    Id = 1,
                    Name = "ExampleClientDto1"
                },
                new Client()
                {
                    Id = 2,
                    Name = "ExampleClientDto2"
                }
            };

            var sut = new ClientDtoMapper();

            var result = sut.MapFrom(exampleClientList);

            Assert.IsInstanceOfType(result, typeof(List<ClientDto>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleClientList[0].Id);
            Assert.AreEqual(result[0].Name, exampleClientList[0].Name);
            Assert.AreEqual(result[1].Id, exampleClientList[1].Id);
            Assert.AreEqual(result[1].Name, exampleClientList[1].Name);
        }
    }
}