﻿//using Microsoft.AspNetCore.Hosting;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Http.Internal;
//using Microsoft.Extensions.Configuration;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using System.IO;
//using System.Threading;
//using System.Threading.Tasks;
//using Transact.FileManager;
//using Transact.FileManager.Contracts;

//namespace Transact.Services.UnitTests
//{
//    [TestClass]
//    public class LocalStorageManager_Should
//    {
//        [TestMethod]
//        public async Task SaveFileAsync_Should_CorrectlyCopyToAsync()
//        {
//            var hostingEnvironmentMock = new Mock<IHostingEnvironment>();

//            var fileWrapperMock = new Mock<IFileWrapper>();

//            var fileInfo = new FileInfo("test");

//            var stream = new FileStream("test", FileMode.Create);

//            fileWrapperMock.Setup(x => x.GetNewFileInfo(It.IsAny<string>())).Returns(fileInfo);

//            fileWrapperMock.Setup(x => x.GetNewFileStream(It.IsAny<string>(), FileMode.Create)).Returns(stream);

//            var configureMock = new Mock<IConfiguration>();

//            var formFileMock = new Mock<IFormFile>();

//            hostingEnvironmentMock.Setup(x => x.WebRootPath).Returns("bannerImages\\");

//            configureMock.Setup(x => x.GetSection("DisplayImageFolder").Value).Returns("/bannerImages/");

//            var sut = new LocalStorageManager(hostingEnvironmentMock.Object, configureMock.Object, fileWrapperMock.Object);

//            var result = sut.SaveFileAsync(formFileMock.Object);

//            formFileMock.Verify((x => x.CopyToAsync(It.IsAny<Stream>(), CancellationToken.None)), Times.Once);
//        }

//        [TestMethod]
//        public async Task DeleteFile_Should_CorrectlyCopyToAsync()
//        {
//        }
//    }
//}
