﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Transact.Web.Controllers;
using Transact.Web.ViewModels;

namespace Transact.Web.UnitTests
{
    [TestClass]
    public class ErrorController_Should
    {
        [TestMethod]
        public async Task GeneralErrorPage_ShouldReturnCorrectView_IfNoErrors()
        {           
            var sut = new ErrorController();

            var actionResult = await sut.GeneralError(403);

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));

            var viewResult = (ViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(ErrorPageViewModel));
        }
    }
}
