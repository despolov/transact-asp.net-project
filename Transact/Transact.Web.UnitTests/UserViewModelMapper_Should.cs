﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Web.Areas.Admin.ViewModels;

namespace Transact.Web.UnitTests
{
    [TestClass]
    public class UserViewModelMapper_Should
    {
        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_UserDto_To_UserViewModel()
        {
            var sut = new UserViewModelMapper();

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                Name = "ExampleUser1",
                UserName = "ExampleUserName1",
                RoleId = 1
            };

            var result = sut.MapFrom(exampleUserDto);

            Assert.IsInstanceOfType(result, typeof(UserViewModel));
            Assert.AreEqual(result.Id, exampleUserDto.Id);
            Assert.AreEqual(result.Name, exampleUserDto.Name);
            Assert.AreEqual(result.UserName, exampleUserDto.UserName);
            Assert.AreEqual(result.RoleId, exampleUserDto.RoleId);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_UserViwModel_To_UserDto()
        {
            var sut = new UserViewModelMapper();

            var exampleUserViewModel = new UserViewModel()
            {
                Id = 1,
                Name = "ExampleUser1",
                UserName = "ExampleUserName1",
                Password = "ExamplePassword",
                RoleId = 1
            };

            var result = sut.MapFrom(exampleUserViewModel);

            Assert.IsInstanceOfType(result, typeof(UserDto));
            Assert.AreEqual(result.Id, exampleUserViewModel.Id);
            Assert.AreEqual(result.Name, exampleUserViewModel.Name);
            Assert.AreEqual(result.UserName, exampleUserViewModel.UserName);
            Assert.AreEqual(result.Password, exampleUserViewModel.Password);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfUsersDtos_To_ListOfUserViwModels()
        {
            var exampleUserDtoList = new List<UserDto>()
                {
                    new UserDto()
                    {
                        Id = 1,
                        Name = "ExampleUserDto1",
                        UserName = "ExampleUserName2",
                        RoleId = 1
                    },
                    new UserDto()
                    {
                        Id = 2,
                        Name = "ExampleUserDto2",
                        UserName = "ExampleUserName2",
                        RoleId = 1
                    }
                };

            var sut = new UserViewModelMapper();

            var result = sut.MapFrom(exampleUserDtoList);

            Assert.IsInstanceOfType(result, typeof(List<UserViewModel>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleUserDtoList[0].Id);
            Assert.AreEqual(result[0].Name, exampleUserDtoList[0].Name);
            Assert.AreEqual(result[0].UserName, exampleUserDtoList[0].UserName);
            Assert.AreEqual(result[0].RoleId, exampleUserDtoList[0].RoleId);
            Assert.AreEqual(result[1].Id, exampleUserDtoList[1].Id);
            Assert.AreEqual(result[1].Name, exampleUserDtoList[1].Name);
            Assert.AreEqual(result[1].UserName, exampleUserDtoList[1].UserName);
            Assert.AreEqual(result[1].RoleId, exampleUserDtoList[1].RoleId);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfUsersViewModels_To_ListOfUserDtos()
        {
            var exampleUserViewModelList = new List<UserViewModel>()
                {
                    new UserViewModel()
                    {
                        Id = 1,
                        Name = "ExampleUserDto1",
                        UserName = "ExampleUserName2",
                        Password = "ExamplePassword1"
                    },
                    new UserViewModel()
                    {
                        Id = 2,
                        Name = "ExampleUserDto2",
                        UserName = "ExampleUserName2",
                        Password = "ExamplePassword2"
                    }
                };

            var sut = new UserViewModelMapper();

            var result = sut.MapFrom(exampleUserViewModelList);

            Assert.IsInstanceOfType(result, typeof(List<UserDto>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleUserViewModelList[0].Id);
            Assert.AreEqual(result[0].Name, exampleUserViewModelList[0].Name);
            Assert.AreEqual(result[0].UserName, exampleUserViewModelList[0].UserName);
            Assert.AreEqual(result[0].Password, exampleUserViewModelList[0].Password);
            Assert.AreEqual(result[1].Id, exampleUserViewModelList[1].Id);
            Assert.AreEqual(result[1].Name, exampleUserViewModelList[1].Name);
            Assert.AreEqual(result[1].UserName, exampleUserViewModelList[1].UserName);
            Assert.AreEqual(result[1].Password, exampleUserViewModelList[1].Password);
        }        
    }
}
