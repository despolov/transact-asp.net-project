﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Transact.Models.Dtos;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Mapper;

namespace Transact.Web.UnitTests
{
    [TestClass]
    public class AdminViewModelMapper_Should
    {
        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_AdminDto_To_AdminLoginViewModel()
        {
            var sut = new AdminViewModelMapper();

            var exampleAdminDto = new AdminDto()
            {
                Id = 1,
                UserName = "ExampleUserName1",
                RoleId = 2,
                RoleName = "Admin"
            };

            var result = sut.MapFrom(exampleAdminDto);

            Assert.IsInstanceOfType(result, typeof(AdminLoginViewModel));
            Assert.AreEqual(result.UserName, exampleAdminDto.UserName);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_AdminLoginViewModel_To_AdminDto()
        {
            var sut = new AdminViewModelMapper();

            var exampleAdminViewModel = new AdminLoginViewModel()
            {
                UserName = "ExampleUserName1",
                Password = "ExamplePassword"
            };

            var result = sut.MapFrom(exampleAdminViewModel);

            Assert.IsInstanceOfType(result, typeof(AdminDto));
            Assert.AreEqual(result.UserName, exampleAdminViewModel.UserName);
            Assert.AreEqual(result.Password, exampleAdminViewModel.Password);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfAdminsDtos_To_ListOfAdminLoginViewModels()
        {
            var exampleAdminDtoList = new List<AdminDto>()
                {
                    new AdminDto()
                    {
                        Id = 1,
                        UserName = "ExampleUserName2"
                    },
                    new AdminDto()
                    {
                        Id = 2,
                        UserName = "ExampleUserName2"
                    }
                };

            var sut = new AdminViewModelMapper();

            var result = sut.MapFrom(exampleAdminDtoList);

            Assert.IsInstanceOfType(result, typeof(List<AdminLoginViewModel>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].UserName, exampleAdminDtoList[0].UserName);
            Assert.AreEqual(result[1].UserName, exampleAdminDtoList[1].UserName);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfAdminsViewModels_To_ListOfAdminDtos()
        {
            var exampleAdminViewModelList = new List<AdminLoginViewModel>()
                {
                    new AdminLoginViewModel()
                    {
                        UserName = "ExampleUserName2",
                        Password = "ExamplePassword1"
                    },
                    new AdminLoginViewModel()
                    {
                        UserName = "ExampleUserName2",
                        Password = "ExamplePassword2"
                    }
                };

            var sut = new AdminViewModelMapper();

            var result = sut.MapFrom(exampleAdminViewModelList);

            Assert.IsInstanceOfType(result, typeof(List<AdminDto>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].UserName, exampleAdminViewModelList[0].UserName);
            Assert.AreEqual(result[0].Password, exampleAdminViewModelList[0].Password);
            Assert.AreEqual(result[1].UserName, exampleAdminViewModelList[1].UserName);
            Assert.AreEqual(result[1].Password, exampleAdminViewModelList[1].Password);
        }
    }
}
