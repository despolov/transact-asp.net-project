﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Transact.Web.Controllers;

namespace Transact.Web.UnitTests
{
    [TestClass]
    public class HomeController_Should
    {
        [TestMethod]
        public async Task Index_ShouldReturnCorrectView_IfNoErrors()
        {
            var sut = new HomeController();

            var actionResult = await sut.Index();

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
        }

        [TestMethod]
        public async Task About_ShouldReturnCorrectView_IfNoErrors()
        {
            var sut = new HomeController();

            var actionResult = await sut.About();

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
        }
    }
}
