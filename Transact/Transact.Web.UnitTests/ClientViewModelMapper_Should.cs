﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Transact.Models.Dtos;
using Transact.Web.Areas.Admin.Mapper;
using Transact.Web.Areas.Admin.ViewModels;

namespace Transact.Web.UnitTests
{
    [TestClass]
    public class ClientViewModelMapper_Should
    {
        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_ClientDto_To_ClientViewModel()
        {
            var sut = new ClientViewModelMapper();

            var exampleClientDto = new ClientDto()
            {
                Id = 1,
                Name = "ExampleClient1"
            };

            var result = sut.MapFrom(exampleClientDto);

            Assert.IsInstanceOfType(result, typeof(ClientViewModel));
            Assert.AreEqual(result.Id, exampleClientDto.Id);
            Assert.AreEqual(result.Name, exampleClientDto.Name);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_ClientViwModel_To_ClientDto()
        {
            var sut = new ClientViewModelMapper();

            var exampleClientViewModel = new ClientViewModel()
            {
                Id = 1,
                Name = "ExamplClient1"
            };

            var result = sut.MapFrom(exampleClientViewModel);

            Assert.IsInstanceOfType(result, typeof(ClientDto));
            Assert.AreEqual(result.Id, exampleClientViewModel.Id);
            Assert.AreEqual(result.Name, exampleClientViewModel.Name);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfUsersDtos_To_ListOfUserViwModels()
        {
            var exampleClientDtoList = new List<ClientDto>()
                {
                    new ClientDto()
                    {
                        Id = 1,
                        Name = "ExampleClient1"
                    },
                    new ClientDto()
                    {
                        Id = 2,
                        Name = "ExampleClient2"
                    },
                };

            var sut = new ClientViewModelMapper();

            var result = sut.MapFrom(exampleClientDtoList);

            Assert.IsInstanceOfType(result, typeof(List<ClientViewModel>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleClientDtoList[0].Id);
            Assert.AreEqual(result[0].Name, exampleClientDtoList[0].Name);
            Assert.AreEqual(result[1].Id, exampleClientDtoList[1].Id);
            Assert.AreEqual(result[1].Name, exampleClientDtoList[1].Name);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfClientsViewModels_To_ListOfClientsDtos()
        {
            var exampleClientViewModelList = new List<ClientViewModel>()
                {
                    new ClientViewModel()
                    {
                        Id = 1,
                        Name = "ExampleClientDto1"
                    },
                    new ClientViewModel()
                    {
                        Id = 2,
                        Name = "ExampleClientDto2"
                    }
                };

            var sut = new ClientViewModelMapper();

            var result = sut.MapFrom(exampleClientViewModelList);

            Assert.IsInstanceOfType(result, typeof(List<ClientDto>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleClientViewModelList[0].Id);
            Assert.AreEqual(result[0].Name, exampleClientViewModelList[0].Name);
            Assert.AreEqual(result[1].Id, exampleClientViewModelList[1].Id);
            Assert.AreEqual(result[1].Name, exampleClientViewModelList[1].Name);
        }
    }
}