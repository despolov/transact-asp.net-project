﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Transact.Models.Dtos;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.Mapper;
using Transact.Web.ViewModels;

namespace Transact.Web.UnitTests
{
    [TestClass]
    public class AccountViewModelMapper_Should
    {
        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_AccountDto_To_AccountViewModel()
        {
            var sut = new AccountViewModelMapper();

            var exampleAccountDto = new AccountDto()
            {
                Id = 1,
                AccountNumber = "1234567890",
                NickName = "ExampleNickName1",
                Balance = 10000
            };

            var result = sut.MapFrom(exampleAccountDto);

            Assert.IsInstanceOfType(result, typeof(AccountViewModel));
            Assert.AreEqual(result.Id, exampleAccountDto.Id);
            Assert.AreEqual(result.AccountNumber, exampleAccountDto.AccountNumber);
            Assert.AreEqual(result.NickName, exampleAccountDto.NickName);
            Assert.AreEqual(result.Balance, exampleAccountDto.Balance);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_AccountViwModel_To_AccountDto()
        {
            var sut = new AccountViewModelMapper();

            var exampleAccountViewModel = new AccountViewModel()
            {
                Id = 1,
                AccountNumber = "1234567890",
                NickName = "ExampleNickName1",
                Balance = 10000
            };

            var result = sut.MapFrom(exampleAccountViewModel);

            Assert.IsInstanceOfType(result, typeof(AccountDto));
            Assert.AreEqual(result.Id, exampleAccountViewModel.Id);
            Assert.AreEqual(result.ClientId, exampleAccountViewModel.ClientId);
            Assert.AreEqual(result.Balance, exampleAccountViewModel.Balance);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfAccountsDtos_To_ListOfAccountViwModels()
        {
            var exampleAccountDtoList = new List<AccountDto>()
                {
                    new AccountDto()
                    {
                        Id = 1,
                        AccountNumber = "1234567899",
                        NickName = "ExampleNickName2",
                        Balance = 10000
                    },
                    new AccountDto()
                    {
                        Id = 2,
                        AccountNumber = "1234567800",
                        NickName = "ExampleNickName2",
                        Balance = 10000
                    },
                };

            var sut = new AccountViewModelMapper();

            var result = sut.MapFrom(exampleAccountDtoList);

            Assert.IsInstanceOfType(result, typeof(List<AccountViewModel>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleAccountDtoList[0].Id);
            Assert.AreEqual(result[0].AccountNumber, exampleAccountDtoList[0].AccountNumber);
            Assert.AreEqual(result[0].NickName, exampleAccountDtoList[0].NickName);
            Assert.AreEqual(result[0].Balance, exampleAccountDtoList[0].Balance);
            Assert.AreEqual(result[1].Id, exampleAccountDtoList[1].Id);
            Assert.AreEqual(result[1].AccountNumber, exampleAccountDtoList[1].AccountNumber);
            Assert.AreEqual(result[1].Balance, exampleAccountDtoList[1].Balance);
            Assert.AreEqual(result[1].NickName, exampleAccountDtoList[1].NickName);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfAccountsViewModels_To_ListOfAccountsDtos()
        {
            var exampleAccountViewModelList = new List<AccountViewModel>()
                {
                    new AccountViewModel()
                    {
                        Id = 1,
                        AccountNumber = "1234567899",
                        NickName = "ExampleNickName2",
                        Balance = 10000
                    },
                    new AccountViewModel()
                    {
                        Id = 2,
                        AccountNumber = "1234567800",
                        NickName = "ExampleNickName2",
                        Balance = 10000
                    }
                };

            var sut = new AccountViewModelMapper();

            var result = sut.MapFrom(exampleAccountViewModelList);

            Assert.IsInstanceOfType(result, typeof(List<AccountDto>));
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Id, exampleAccountViewModelList[0].Id);
            Assert.AreEqual(result[0].ClientId, exampleAccountViewModelList[0].ClientId);
            Assert.AreEqual(result[0].Balance, exampleAccountViewModelList[0].Balance);
            Assert.AreEqual(result[1].Id, exampleAccountViewModelList[1].Id);
            Assert.AreEqual(result[1].ClientId, exampleAccountViewModelList[1].ClientId);
            Assert.AreEqual(result[1].Balance, exampleAccountViewModelList[1].Balance);
        }
    }
}
