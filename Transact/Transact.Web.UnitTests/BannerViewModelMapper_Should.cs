﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Transact.Models.Dtos;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Mapper;

namespace Transact.Web.UnitTests
{
    [TestClass]
    public class BannerViewModelMapper_Should
    {
        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_BannerDto_To_BannerViewModel()
        {
            var configureMock = new Mock<IConfiguration>();

            configureMock.Setup(x => x.GetSection("DisplayImageFolder").Value).Returns("/bannerImages/");

            var sut = new BannerViewModelMapper(configureMock.Object);

            var exampleImageName = new Guid().ToString();

            var exampleBannerDto = new BannerDto()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var result = sut.MapFrom(exampleBannerDto);

            Assert.IsInstanceOfType(result, typeof(BannerViewModel));
            Assert.AreEqual(result.Id, exampleBannerDto.Id);
            Assert.AreEqual(result.StartDate, exampleBannerDto.StartDate);
            Assert.AreEqual(result.EndDate, exampleBannerDto.EndDate);
            Assert.AreEqual(result.ImageName, exampleBannerDto.ImageName);
            Assert.AreEqual(result.Url, exampleBannerDto.Url);
            Assert.AreEqual(result.ImagePath, "/bannerImages/"+ exampleBannerDto.ImageName);
        }

        [TestMethod]
        public void MapFromForEditing_Should_CorrectlyMap_BannerDto_To_EditBannerViewModel()
        {
            var configureMock = new Mock<IConfiguration>();

            configureMock.Setup(x => x.GetSection("DisplayImageFolder").Value).Returns("/bannerImages/");

            var sut = new BannerViewModelMapper(configureMock.Object);

            var exampleImageName = new Guid().ToString();

            var exampleBannerDto = new BannerDto()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var result = sut.MapFromForEditing(exampleBannerDto);

            Assert.IsInstanceOfType(result, typeof(EditBannerViewModel));
            Assert.AreEqual(result.Id, exampleBannerDto.Id);
            Assert.AreEqual(result.StartDate, exampleBannerDto.StartDate);
            Assert.AreEqual(result.EndDate, exampleBannerDto.EndDate);
            Assert.AreEqual(result.ImageName, exampleBannerDto.ImageName);
            Assert.AreEqual(result.Url, exampleBannerDto.Url);
            Assert.AreEqual(result.ImagePath, "/bannerImages/" + exampleBannerDto.ImageName);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_BannerViewModel_To_BannerDto()
        {
            var configureMock = new Mock<IConfiguration>();

            configureMock.Setup(x => x.GetSection("DisplayImageFolder").Value).Returns("/bannerImages/");

            var sut = new BannerViewModelMapper(configureMock.Object);

            var exampleImageName = new Guid().ToString();

            var exampleBannerViewModel = new BannerViewModel()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var result = sut.MapFrom(exampleBannerViewModel);

            Assert.IsInstanceOfType(result, typeof(BannerDto));
            Assert.AreEqual(result.Id, exampleBannerViewModel.Id);
            Assert.AreEqual(result.StartDate, exampleBannerViewModel.StartDate);
            Assert.AreEqual(result.EndDate, exampleBannerViewModel.EndDate);
            Assert.AreEqual(result.ImageName, exampleBannerViewModel.ImageName);
            Assert.AreEqual(result.Url, exampleBannerViewModel.Url);
        }

        [TestMethod]
        public void MapFromForEditing_Should_CorrectlyMap_EditBannerViewModel_To_BannerDto()
        {
            var configureMock = new Mock<IConfiguration>();

            configureMock.Setup(x => x.GetSection("DisplayImageFolder").Value).Returns("/bannerImages/");

            var sut = new BannerViewModelMapper(configureMock.Object);

            var exampleImageName = new Guid().ToString();

            var exampleBannerViewModel = new EditBannerViewModel()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var result = sut.MapFromForEditing(exampleBannerViewModel);

            Assert.IsInstanceOfType(result, typeof(BannerDto));
            Assert.AreEqual(result.Id, exampleBannerViewModel.Id);
            Assert.AreEqual(result.StartDate, exampleBannerViewModel.StartDate);
            Assert.AreEqual(result.EndDate, exampleBannerViewModel.EndDate);
            Assert.AreEqual(result.ImageName, exampleBannerViewModel.ImageName);
            Assert.AreEqual(result.Url, exampleBannerViewModel.Url);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfBannerViewModels_To_BannerDtos()
        {
            var configureMock = new Mock<IConfiguration>();

            configureMock.Setup(x => x.GetSection("DisplayImageFolder").Value).Returns("/bannerImages/");

            var sut = new BannerViewModelMapper(configureMock.Object);

            var exampleImageName = new Guid().ToString();

            var exampleBannerViewModelList = new List<BannerViewModel>()
            {
                new BannerViewModel()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                },
                new BannerViewModel()
                {
                    Id = 2,
                    StartDate = new DateTime(2018, 7, 15),
                    EndDate = new DateTime(2019, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example2.com"
                },
            };

            var result = sut.MapFrom(exampleBannerViewModelList);

            Assert.IsInstanceOfType(result, typeof(List<BannerDto>));
            Assert.AreEqual(result[0].Id, exampleBannerViewModelList[0].Id);
            Assert.AreEqual(result[0].StartDate, exampleBannerViewModelList[0].StartDate);
            Assert.AreEqual(result[0].EndDate, exampleBannerViewModelList[0].EndDate);
            Assert.AreEqual(result[0].ImageName, exampleBannerViewModelList[0].ImageName);
            Assert.AreEqual(result[0].Url, exampleBannerViewModelList[0].Url);

            Assert.AreEqual(result[1].Id, exampleBannerViewModelList[1].Id);
            Assert.AreEqual(result[1].StartDate, exampleBannerViewModelList[1].StartDate);
            Assert.AreEqual(result[1].EndDate, exampleBannerViewModelList[1].EndDate);
            Assert.AreEqual(result[1].ImageName, exampleBannerViewModelList[1].ImageName);
            Assert.AreEqual(result[1].Url, exampleBannerViewModelList[1].Url);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfDtos_To_BannerViewModels()
        {
            var configureMock = new Mock<IConfiguration>();

            configureMock.Setup(x => x.GetSection("DisplayImageFolder").Value).Returns("/bannerImages/");

            var sut = new BannerViewModelMapper(configureMock.Object);

            var exampleImageName = new Guid().ToString();

            var exampleBannerDtoList = new List<BannerDto>()
            {
                new BannerDto()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com"
                },
                new BannerDto()
                {
                    Id = 2,
                    StartDate = new DateTime(2018, 7, 15),
                    EndDate = new DateTime(2019, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example2.com"
                },
            };

            var result = sut.MapFrom(exampleBannerDtoList);

            Assert.IsInstanceOfType(result, typeof(List<BannerViewModel>));
            Assert.AreEqual(result[0].Id, exampleBannerDtoList[0].Id);
            Assert.AreEqual(result[0].StartDate, exampleBannerDtoList[0].StartDate);
            Assert.AreEqual(result[0].EndDate, exampleBannerDtoList[0].EndDate);
            Assert.AreEqual(result[0].ImageName, exampleBannerDtoList[0].ImageName);
            Assert.AreEqual(result[0].Url, exampleBannerDtoList[0].Url);
            Assert.AreEqual(result[0].ImagePath, "/bannerImages/" + exampleBannerDtoList[0].ImageName);


            Assert.AreEqual(result[1].Id, exampleBannerDtoList[1].Id);
            Assert.AreEqual(result[1].StartDate, exampleBannerDtoList[1].StartDate);
            Assert.AreEqual(result[1].EndDate, exampleBannerDtoList[1].EndDate);
            Assert.AreEqual(result[1].ImageName, exampleBannerDtoList[1].ImageName);
            Assert.AreEqual(result[1].Url, exampleBannerDtoList[1].Url);
            Assert.AreEqual(result[1].ImagePath, "/bannerImages/" + exampleBannerDtoList[1].ImageName);

        }

    }
}
