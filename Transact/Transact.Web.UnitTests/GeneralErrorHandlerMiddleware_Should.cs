﻿using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Transact.Services;
using Transact.Services.CustomExceptions;
using Transact.Web.Utils.Middlewares;

namespace Transact.Web.UnitTests
{
    [TestClass]
    public class GeneralErrorHandlerMiddleware_Should
    {
        [TestMethod]
        public async Task Invoke_Should_GoThrough_IfNoErrors()
        {
            var httpContextMock = new Mock<IHttpContextAccessor>();

            var exampleBody = "test response body";

            var context = new DefaultHttpContext();
            context.Response.Body = new MemoryStream();

            context.Response.Body.Seek(0, SeekOrigin.Begin);

            var sut = new GeneralErrorHandlerMiddleware(
                           async innerHttpContext => 
                           {
                               await innerHttpContext.Response.WriteAsync(exampleBody);
                           });

            await sut.Invoke(context);

            context.Response.Body.Seek(0, SeekOrigin.Begin);
            var reader = new StreamReader(context.Response.Body);
            var streamText = reader.ReadToEnd();

            Assert.AreEqual(streamText, exampleBody);
        }

        [TestMethod]
        public async Task Invoke_Should_ReturnCorrectExceptionMessageInBody_IfBusinessLogicExceptionIsThrown_WhileAjaxCall()
        {
            var httpContextMock = new Mock<IHttpContextAccessor>();

            var context = new DefaultHttpContext();
            context.Response.Body = new MemoryStream();
            context.Request.Headers.Add("X-Requested-With", "XMLHttpRequest");

            context.Response.Body.Seek(0, SeekOrigin.Begin);

            var sut = new GeneralErrorHandlerMiddleware(
                           async innerHttpContext =>
                           {
                               throw new BusinessLogicException(ExceptionMessages.ClientNull);
                           });

            await sut.Invoke(context);

            context.Response.Body.Seek(0, SeekOrigin.Begin);
            var reader = new StreamReader(context.Response.Body);
            var streamText = reader.ReadToEnd();

            Assert.AreEqual(streamText, ExceptionMessages.ClientNull);
        }

        [TestMethod]
        public async Task Invoke_Should_ReturnCorrectExceptionMessageInBody_IfAccountNotAvailableExceptionIsThrown_WhileAjaxCall()
        {
            var httpContextMock = new Mock<IHttpContextAccessor>();

            var context = new DefaultHttpContext();
            context.Response.Body = new MemoryStream();
            context.Request.Headers.Add("X-Requested-With", "XMLHttpRequest");

            context.Response.Body.Seek(0, SeekOrigin.Begin);

            var sut = new GeneralErrorHandlerMiddleware(
                           async innerHttpContext =>
                           {
                               throw new AccountNotAvailableException(ExceptionMessages.AccountNoLongerAvailable);
                           });

            await sut.Invoke(context);

            context.Response.Body.Seek(0, SeekOrigin.Begin);
            var reader = new StreamReader(context.Response.Body);

            var header = context.Response.Headers["customError"];

            var streamText = reader.ReadToEnd();

            Assert.AreEqual(header, "AccountNotAvailable");
            Assert.AreEqual(streamText, ExceptionMessages.AccountNoLongerAvailable);

        }

        [TestMethod]
        public async Task Invoke_Should_ReturnCorrectExceptionMessageInBody_IfExceptionIsThrown_WhileAjaxCall()
        {
            var httpContextMock = new Mock<IHttpContextAccessor>();

            var context = new DefaultHttpContext();
            context.Response.Body = new MemoryStream();
            context.Request.Headers.Add("X-Requested-With", "XMLHttpRequest");

            context.Response.Body.Seek(0, SeekOrigin.Begin);

            var sut = new GeneralErrorHandlerMiddleware(
                           async innerHttpContext =>
                           {
                               throw new AccountNotAvailableException(ExceptionMessages.GeneralOopsMessage);
                           });

            await sut.Invoke(context);

            context.Response.Body.Seek(0, SeekOrigin.Begin);
            var reader = new StreamReader(context.Response.Body);
            var streamText = reader.ReadToEnd();

            Assert.AreEqual(streamText, ExceptionMessages.GeneralOopsMessage);
        }

        [TestMethod]
        public async Task Invoke_Should_RedirectCorrectly_IfExceptionIsThrown()
        {
            var httpContextMock = new Mock<IHttpContextAccessor>();

            var context = new DefaultHttpContext();


            var sut = new GeneralErrorHandlerMiddleware(
                           async innerHttpContext =>
                           {
                               throw new Exception();
                           });

            await sut.Invoke(context);

            var result = context.Response.Headers["Location"];

            Assert.AreEqual(result, "error/generalerror");
        }
    }
}
