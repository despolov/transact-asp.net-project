﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Web.Areas.AppUser.Controllers;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.Mapper.Contracts;

namespace Transact.Web.UnitTests.Areas.AppUser
{
    [TestClass]
    public class AccountController_Should
    {
        [TestMethod]
        public async Task AccountDash_Should_ReturnCorrectViewModel_IfNoErrors()
        {
            var userServicesMock = new Mock<IUserServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var clientServicesMock = new Mock<IClientServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManagerMock = new Mock<IAuthorizationManager>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            contextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorizationManagerMock.Setup(x => x.GetLoggedUserId()).Returns(1);

            var exampleUser = new User()
            {
                Id = 1,
                Name = "ExampleUser1",
                UserName = "ExampleUserName1",
                Password = "ExamplePassword1",
                Role = new UserRole { RoleName = "User" },
                UsersAccounts = new List<UsersAccounts>(),
                UsersClients = new List<UsersClients>(),
            };

            var exampleListAccount = new List<AccountDto>
            {
                new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                }
            };

            var exampleListAccountViewModel = new List<AccountViewModel>
            {
                new AccountViewModel()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                }
            };

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                Name = "exampleUserDto",
                UserName = "exampleUserNameDto"
            };

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };
           
            accountServicesMock.Setup(x => x.GetAllUserAvailableAccountsAsync(It.IsAny<long>()))
                .ReturnsAsync(exampleListAccount);


            //mapperMock.Setup(x => x.MapFrom(exampleListAccount)).Returns(exampleListAccountViewModel);


            userServicesMock.Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(exampleUserDto);

            accountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleListAccountViewModel);

            var sut = new AccountController(                
                accountServicesMock.Object,
                clientServicesMock.Object,               
                accountMapperMock.Object,
                contextAccessorMock.Object,
                authorizationManagerMock.Object);

            var actionResult = await sut.AccountDash();

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));

            var viewResult = (ViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(AccountDashViewModel));
        }

        [TestMethod]
        public async Task FindSingleByAccName_Should_ReturnCorrectJson_IfNoErrors()
        {
            var userServicesMock = new Mock<IUserServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var clientServicesMock = new Mock<IClientServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManagerMock = new Mock<IAuthorizationManager>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            contextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorizationManagerMock.Setup(x => x.GetLoggedUserId()).Returns(1);

            var exampleUser = new User()
            {
                Id = 1,
                Name = "ExampleUser1",
                UserName = "ExampleUserName1",
                Password = "ExamplePassword1",
                Role = new UserRole { RoleName = "User" },
                UsersAccounts = new List<UsersAccounts>(),
                UsersClients = new List<UsersClients>(),
            };

            var exampleListAccount = new List<AccountDto>
            {
                new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientName = "ExampleClientName",
                    ClientId = 1,
                    NickName = "ExampleNickName"
                },

                new AccountDto()
                {
                    Id = 2,
                    AccountNumber = "1234555555",
                    Balance = 10000,
                    ClientName = "ExampleClientName",
                    ClientId = 1,
                    NickName = "ExampleNickName"
                }
            };

            var exampleListAccountViewModel = new List<AccountViewModel>
            {
                new AccountViewModel()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                }
            };

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                Name = "exampleUserDto",
                UserName = "exampleUserNameDto"
            };

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            accountServicesMock.Setup(x => x.FindAccountsContainingAsync("123", 0))
                .ReturnsAsync(exampleListAccount);


            userServicesMock.Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(exampleUserDto);

            accountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleListAccountViewModel);

            var sut = new AccountController(
                accountServicesMock.Object, 
                clientServicesMock.Object,
                accountMapperMock.Object,
                contextAccessorMock.Object,
                authorizationManagerMock.Object);

            var actionResult = await sut.FindSingleByAccName("123", 0);

            Assert.IsInstanceOfType(actionResult, typeof(JsonResult));

            JsonResult viewResult = actionResult as JsonResult;

            var accounts = viewResult.Value as List<AccountDto>;

            Assert.AreEqual(accounts?.Count, 2);

            Assert.AreEqual(accounts[0]?.Id, exampleListAccount[0].Id);
            Assert.AreEqual(accounts[0]?.NickName, exampleListAccount[0].NickName);
            Assert.AreEqual(accounts[0]?.ClientName, exampleListAccount[0].ClientName);
            Assert.AreEqual(accounts[0]?.Balance, exampleListAccount[0].Balance);
            Assert.AreEqual(accounts[0]?.AccountNumber, exampleListAccount[0].AccountNumber);
            Assert.AreEqual(accounts[0]?.ClientId, exampleListAccount[0].ClientId);

            Assert.AreEqual(accounts[1]?.Id, exampleListAccount[1].Id);
            Assert.AreEqual(accounts[1]?.NickName, exampleListAccount[1].NickName);
            Assert.AreEqual(accounts[1]?.ClientName, exampleListAccount[1].ClientName);
            Assert.AreEqual(accounts[1]?.Balance, exampleListAccount[1].Balance);
            Assert.AreEqual(accounts[1]?.AccountNumber, exampleListAccount[1].AccountNumber);
            Assert.AreEqual(accounts[1]?.ClientId, exampleListAccount[1].ClientId);
        }

        [TestMethod]
        public async Task FindSingleByAccName_Should_ReturnCorrectJsonWithIgnoredAccount_IfNoErrors()
        {
            var userServicesMock = new Mock<IUserServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var clientServicesMock = new Mock<IClientServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManagerMock = new Mock<IAuthorizationManager>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            contextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorizationManagerMock.Setup(x => x.GetLoggedUserId()).Returns(1);

            var exampleUser = new User()
            {
                Id = 1,
                Name = "ExampleUser1",
                UserName = "ExampleUserName1",
                Password = "ExamplePassword1",
                Role = new UserRole { RoleName = "User" },
                UsersAccounts = new List<UsersAccounts>(),
                UsersClients = new List<UsersClients>(),
            };

            var exampleListAccount = new List<AccountDto>
            {
                new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientName = "ExampleClientName",
                    ClientId = 1,
                    NickName = "ExampleNickName"
                }                
            };

            var exampleListAccountViewModel = new List<AccountViewModel>
            {
                new AccountViewModel()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                }
            };

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                Name = "exampleUserDto",
                UserName = "exampleUserNameDto"
            };

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            accountServicesMock.Setup(x => x.FindAccountsContainingAsync("123", 2))
                .ReturnsAsync(exampleListAccount);


            userServicesMock.Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(exampleUserDto);

            accountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleListAccountViewModel);

            var sut = new AccountController(
                accountServicesMock.Object,
                clientServicesMock.Object,
                accountMapperMock.Object,
                contextAccessorMock.Object,
                authorizationManagerMock.Object);

            var actionResult = await sut.FindSingleByAccName("123", 2);

            Assert.IsInstanceOfType(actionResult, typeof(JsonResult));

            JsonResult viewResult = actionResult as JsonResult;

            var accounts = viewResult.Value as List<AccountDto>;

            Assert.AreEqual(accounts?.Count, 1);
            Assert.AreEqual(accounts[0]?.Id, exampleListAccount[0].Id);
            Assert.AreEqual(accounts[0]?.NickName, exampleListAccount[0].NickName);
            Assert.AreEqual(accounts[0]?.ClientName, exampleListAccount[0].ClientName);
            Assert.AreEqual(accounts[0]?.Balance, exampleListAccount[0].Balance);
            Assert.AreEqual(accounts[0]?.AccountNumber, exampleListAccount[0].AccountNumber);
            Assert.AreEqual(accounts[0]?.ClientId, exampleListAccount[0].ClientId);
        }


        [TestMethod]
        public async Task FindSingleByAccName_Should_ReturnEmptyJson_IfInputIsNull()
        {
            var userServicesMock = new Mock<IUserServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var clientServicesMock = new Mock<IClientServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManagerMock = new Mock<IAuthorizationManager>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            contextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorizationManagerMock.Setup(x => x.GetLoggedUserId()).Returns(1);

            var exampleUser = new User()
            {
                Id = 1,
                Name = "ExampleUser1",
                UserName = "ExampleUserName1",
                Password = "ExamplePassword1",
                Role = new UserRole { RoleName = "User" },
                UsersAccounts = new List<UsersAccounts>(),
                UsersClients = new List<UsersClients>(),
            };

            var exampleListAccount = new List<AccountDto>
            {
                new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientName = "ExampleClientName",
                    ClientId = 1,
                    NickName = "ExampleNickName"
                }
            };

            var exampleListAccountViewModel = new List<AccountViewModel>
            {
                new AccountViewModel()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                }
            };

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                Name = "exampleUserDto",
                UserName = "exampleUserNameDto"
            };

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            accountServicesMock.Setup(x => x.FindAccountsContainingAsync("123", 2))
                .ReturnsAsync(exampleListAccount);


            userServicesMock.Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(exampleUserDto);

            accountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleListAccountViewModel);

            var sut = new AccountController(
                accountServicesMock.Object,
                clientServicesMock.Object,
                accountMapperMock.Object,
                contextAccessorMock.Object,
                authorizationManagerMock.Object);

            var actionResult = await sut.FindSingleByAccName(string.Empty, 2);

            Assert.IsInstanceOfType(actionResult, typeof(JsonResult));          

            Assert.AreEqual(actionResult.Value, string.Empty);
        }

        [TestMethod]
        public async Task EditAccount_Should_ReturnOk_IfNoErrors()
        {
            var userServicesMock = new Mock<IUserServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var clientServicesMock = new Mock<IClientServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManagerMock = new Mock<IAuthorizationManager>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            contextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorizationManagerMock.Setup(x => x.GetLoggedUserId()).Returns(1);

            var exampleUser = new User()
            {
                Id = 1,
                Name = "ExampleUser1",
                UserName = "ExampleUserName1",
                Password = "ExamplePassword1",
                Role = new UserRole { RoleName = "User" },
                UsersAccounts = new List<UsersAccounts>(),
                UsersClients = new List<UsersClients>(),
            };

            var exampleListAccount = new List<AccountDto>
            {
                new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                }
            };

            var exampleAccountDto = new AccountDto()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                ClientId = 1,
                NickName = "NewNick"
            };

            var exampleListAccountViewModel = new List<AccountViewModel>
            {
                new AccountViewModel()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                }
            };

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                Name = "exampleUserDto",
                UserName = "exampleUserNameDto"
            };

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            accountServicesMock.Setup(x => x.RenameAccountNicknameAsync(It.IsAny<long>(), It.IsAny<string>(), It.IsAny<long>()))
                .ReturnsAsync(exampleAccountDto);


            userServicesMock.Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(exampleUserDto);

            accountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleListAccountViewModel);

            var sut = new AccountController(
                accountServicesMock.Object,
                clientServicesMock.Object,
                accountMapperMock.Object,
                contextAccessorMock.Object,
                authorizationManagerMock.Object);

            var actionResult = await sut.EditAccountNickname(1, "NewNick");

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));

            var viewResult = (OkObjectResult)actionResult;

            Assert.AreEqual(viewResult.Value, "Account Nickname succesfully changed to: NewNick!");
        }

        [TestMethod]
        public async Task EditAccount_Should_ReturnBadRequest_IfAccountIdNull()
        {
            var userServicesMock = new Mock<IUserServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var clientServicesMock = new Mock<IClientServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManagerMock = new Mock<IAuthorizationManager>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            contextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorizationManagerMock.Setup(x => x.GetLoggedUserId()).Returns(1);

            var exampleUser = new User()
            {
                Id = 1,
                Name = "ExampleUser1",
                UserName = "ExampleUserName1",
                Password = "ExamplePassword1",
                Role = new UserRole { RoleName = "User" },
                UsersAccounts = new List<UsersAccounts>(),
                UsersClients = new List<UsersClients>(),
            };

            var exampleListAccount = new List<AccountDto>
            {
                new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                }
            };

            var exampleAccountDto = new AccountDto()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                ClientId = 1,
                NickName = "NewNick"
            };

            var exampleListAccountViewModel = new List<AccountViewModel>
            {
                new AccountViewModel()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                }
            };

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                Name = "exampleUserDto",
                UserName = "exampleUserNameDto"
            };

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            accountServicesMock.Setup(x => x.RenameAccountNicknameAsync(It.IsAny<long>(), It.IsAny<string>(), It.IsAny<long>()))
                .ReturnsAsync(exampleAccountDto);


            userServicesMock.Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(exampleUserDto);

            accountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleListAccountViewModel);

            var sut = new AccountController(
                accountServicesMock.Object,
                clientServicesMock.Object,
                accountMapperMock.Object,
                contextAccessorMock.Object,
                authorizationManagerMock.Object);

            var actionResult = await sut.EditAccountNickname(null, "NewNick");

            Assert.IsInstanceOfType(actionResult, typeof(BadRequestObjectResult));

            var viewResult = (BadRequestObjectResult)actionResult;

            Assert.AreEqual(viewResult.Value, "Invalid selected account!");
        }

        [TestMethod]
        public async Task EditAccount_Should_ReturnBadRequest_IfNewNicknameNull()
        {
            var userServicesMock = new Mock<IUserServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var clientServicesMock = new Mock<IClientServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManagerMock = new Mock<IAuthorizationManager>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            contextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorizationManagerMock.Setup(x => x.GetLoggedUserId()).Returns(1);

            var exampleUser = new User()
            {
                Id = 1,
                Name = "ExampleUser1",
                UserName = "ExampleUserName1",
                Password = "ExamplePassword1",
                Role = new UserRole { RoleName = "User" },
                UsersAccounts = new List<UsersAccounts>(),
                UsersClients = new List<UsersClients>(),
            };

            var exampleListAccount = new List<AccountDto>
            {
                new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "ExampleNickName"
                }
            };

            var exampleAccountDto = new AccountDto()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                ClientId = 1,
                NickName = "NewNick"
            };

            var exampleListAccountViewModel = new List<AccountViewModel>
            {
                new AccountViewModel()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                }
            };

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                Name = "exampleUserDto",
                UserName = "exampleUserNameDto"
            };

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            accountServicesMock.Setup(x => x.RenameAccountNicknameAsync(It.IsAny<long>(), It.IsAny<string>(), It.IsAny<long>()))
                .ReturnsAsync(exampleAccountDto);


            userServicesMock.Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(exampleUserDto);

            accountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleListAccountViewModel);

            var sut = new AccountController(
                accountServicesMock.Object,
                clientServicesMock.Object,
                accountMapperMock.Object,
                contextAccessorMock.Object,
                authorizationManagerMock.Object);

            var actionResult = await sut.EditAccountNickname(1, null);

            Assert.IsInstanceOfType(actionResult, typeof(BadRequestObjectResult));

            var viewResult = (BadRequestObjectResult)actionResult;

            Assert.AreEqual(viewResult.Value, "New Name cannot be empty!");
        }
    }
}
