﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Web.Areas.AppUser.Controllers;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.Controllers;

namespace Transact.Web.UnitTests.Areas.AppUser
{
    [TestClass]
    public class IdentityController_Should
    {
        [TestMethod]
        public async Task Login_Should_ReturnOk_IfNoErrors()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var cookieManagerMock = new Mock<ICookieManager>();

            var exampleUser = new User()
            {
                Id = 1,
                Name = "ExampleUser1",
                UserName = "ExampleUserName1",
                Password = "ExamplePassword1",
                Role = new UserRole { RoleName = "User" },
                UsersAccounts = new List<UsersAccounts>(),
                UsersClients = new List<UsersClients>(),
            };

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                UserName = "exampleUserDto",
                RoleName = "User"                
            };

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            userServicesMock.Setup(x => x.GetUserAsync(
                exampleUser.UserName,
                exampleUser.Password))
                .ReturnsAsync(exampleUserDto);

            tokenManagerMock.Setup(x => x.GenerateToken(exampleUser.UserName, exampleUser.Role.RoleName, exampleUser.Id.ToString()))
                .Returns(exampleToken);

            var exampleLoginViewModel = new LoginViewModel()
            {
                UserName = exampleUser.UserName,
                Password = exampleUser.Password
            };

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            httpContextAccessorMock.Setup(X => X.HttpContext
                                                .Response
                                                .Cookies
                                                .Append("SecurityToken", 
                                                exampleToken));

            var sut = new IdentityController(
                userServicesMock.Object,
                tokenManagerMock.Object,
                httpContextAccessorMock.Object,
                cookieManagerMock.Object);

            var actionResult = await sut.Login(exampleLoginViewModel);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }       

        [TestMethod]
        public async Task Login_Should_ReturnBadRequest_IfModelStateIsInvalid()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var cookieManagerMock = new Mock<ICookieManager>();


            var sut = new IdentityController(
                userServicesMock.Object,
                tokenManagerMock.Object,
                httpContextAccessorMock.Object,
                cookieManagerMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var result = await sut.Login(TestSamples.exampleLoginViewModel);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public async Task Logout_Should_ReturnOk_IfNoErrors()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var cookieManagerMock = new Mock<ICookieManager>();

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                UserName = "exampleUserDto"
            };

            userServicesMock.Setup(x => x.GetUserAsync(
                TestSamples.exampleUser.UserName,
                TestSamples.exampleUser.Password)).ReturnsAsync(exampleUserDto);

            tokenManagerMock.Setup(x => x.GenerateToken(TestSamples.exampleUser.UserName, TestSamples.exampleUser.Role.RoleName, TestSamples.exampleUser.Id.ToString())).Returns(TestSamples.exampleToken);

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new IdentityController(
                userServicesMock.Object,
                tokenManagerMock.Object,
                httpContextAccessorMock.Object,
                cookieManagerMock.Object);

            var actionResult = await sut.Logout();

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }      
    }
}

