﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Transact.FileManager.Contracts;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Areas.AppUser.Controllers;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.Mapper.Contracts;

namespace Transact.Web.UnitTests.Areas.AppUser
{
    [TestClass]
    public class DashboardController_Should
    {
        [TestMethod]
        public async Task LoginPage_ShouldReturnCorrectView_IfNoErrors()
        {
            var bannerMapperMock = new Mock<IBannerViewModelMapper>();

            var bannerServicesMock = new Mock<IBannerServices>();

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleImageName = new Guid().ToString();

            var exampleBannerDtoList = new List<BannerDto>
            {
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            var exampleBannerViewModelList = new List<BannerViewModel>
            {
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            bannerServicesMock.Setup(x => x.GetActiveBannersImagePathsAsync(DateTime.Now)).ReturnsAsync(exampleBannerDtoList);

            bannerMapperMock.Setup(x => x.MapFrom(It.IsAny<List<BannerDto>>())).Returns(exampleBannerViewModelList);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));


            var sut = new DashboardController(
                bannerServicesMock.Object,
                bannerMapperMock.Object);

            var actionResult = await sut.LoginPage();

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));

            var viewResult = (ViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(LoginViewModel));
        }

        [TestMethod]
        public async Task About_ShouldReturnView_IfNoErros()
        {
            var bannerMapperMock = new Mock<IBannerViewModelMapper>();

            var bannerServicesMock = new Mock<IBannerServices>();

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
       
            var sut = new DashboardController(
                bannerServicesMock.Object,
                bannerMapperMock.Object);

            var actionResult = await sut.About();

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));

        }
    }
}
