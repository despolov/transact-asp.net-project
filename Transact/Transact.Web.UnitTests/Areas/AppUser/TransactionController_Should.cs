﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Services.CustomExceptions;
using Transact.Web.Areas.AppUser.Controllers;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.Mapper.Contracts;
using Transact.Web.ViewModels;

namespace Transact.Web.UnitTests.Areas.AppUser
{
    [TestClass]
    public class TransactionController_Should
    {
        [TestMethod]
        public async Task AddTransaction_Should_ReturnCorrectView_IfNoErrors()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            contextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorizationManager.Setup(x => x.GetLoggedUserId()).Returns(1);

            var exampleUser = new User()
            {
                Id = 1,
                Name = "ExampleUser1",
                UserName = "ExampleUserName1",
                Password = "ExamplePassword1",
                Role = new UserRole { RoleName = "User" },
                UsersAccounts = new List<UsersAccounts>(),
                UsersClients = new List<UsersClients>(),
            };

            var exampleListAccount = new List<AccountDto>
            {
                new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    NickName = "ExampleNickName",
                    Balance = 10000                     //TODO: CHECK IF CLIENTID MAKES PROBLEMS
                }
            };

            var exampleListAccountViewModel = new List<AccountViewModel>
            {
                new AccountViewModel()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                }
            };

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                Name = "exampleUserDto",
                UserName = "exampleUserNameDto"
            };

            accountServicesMock.Setup(x => x.GetAllUserAvailableAccountsAsync(It.IsAny<long>()))
                .ReturnsAsync(exampleListAccount);


            mapperMock.Setup(x => x.MapFrom(exampleListAccount)).Returns(exampleListAccountViewModel);

            userServicesMock.Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(exampleUserDto);

            var sut = new TransactionController(
                userServicesMock.Object,
                accountServicesMock.Object,
                mapperMock.Object,
                transactionServicesMock.Object,
                authorizationManager.Object,
                contextAccessorMock.Object,
                transactionMapperMock.Object);

            var actionResult = await sut.AddTransaction(null);

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));

            var viewResult = (ViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(TransactionViewModel));
        }        

        [TestMethod]
        public async Task EditTransaction_Should_ReturnCorrectView_IfNoErrors()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            contextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorizationManager.Setup(x => x.GetLoggedUserId()).Returns(1);

            var exampleUser = new User()
            {
                Id = 1,
                Name = "ExampleUser1",
                UserName = "ExampleUserName1",
                Password = "ExamplePassword1",
                Role = new UserRole { RoleName = "User" },
                UsersAccounts = new List<UsersAccounts>(),
                UsersClients = new List<UsersClients>(),
            };

            var exampleListAccount = new List<AccountDto>
            {
                new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    NickName = "ExampleNickName",
                    Balance = 10000                   
                }
            };

            var exampleListAccountViewModel = new List<AccountViewModel>
            {
                new AccountViewModel()
                {
                    Id = 2,
                    AccountNumber = "1233367890",
                    Balance = 10000,
                    ClientId = 2,
                    NickName = "ExampleNickName2"
                }
            };

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                Name = "exampleUserDto",
                UserName = "exampleUserNameDto"
            };

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            var exampleTransactionViewModel = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                Description = "ExampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890"
            };

            accountServicesMock.Setup(x => x.GetAllUserAvailableAccountsAsync(It.IsAny<long>()))
                .ReturnsAsync(exampleListAccount);


            mapperMock.Setup(x => x.MapFrom(exampleListAccount)).Returns(exampleListAccountViewModel);

            userServicesMock.Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(exampleUserDto);

            transactionServicesMock.Setup(x => x.GetSingleTransactionForEditingAsync(It.IsAny<long>())).ReturnsAsync(exampleTransactionDto);

            transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionDto>())).Returns(exampleTransactionViewModel);

            var sut = new TransactionController(
                userServicesMock.Object,
                accountServicesMock.Object,
                mapperMock.Object,
                transactionServicesMock.Object,
                authorizationManager.Object,
                contextAccessorMock.Object,
                transactionMapperMock.Object);

            var actionResult = await sut.EditTransaction(1);

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));

            var viewResult = (ViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(TransactionViewModel));
        }

        [TestMethod]
        public async Task UpdateTransaction_Should_ReturnOk_IfNoErrors()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            var exampleTransaction = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                Description = "ExampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890"
            };

            transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionViewModel>())).Returns(exampleTransactionDto);

            transactionServicesMock.Setup(x => x.UpdateTransactionAsync(It.IsAny<TransactionDto>()))
                .ReturnsAsync(exampleTransactionDto);

            var sut = new TransactionController(
                userServicesMock.Object,
                accountServicesMock.Object,
                mapperMock.Object,
                transactionServicesMock.Object,
                authorizationManager.Object,
                contextAccessorMock.Object,
                transactionMapperMock.Object);

            var actionResult = await sut.UpdateTransaction(exampleTransaction);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }


        [TestMethod]
        public async Task UpdateTransaction_Should_ReturnBadRequest_IfBadModelState()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            var exampleTransaction = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                Description = "ExampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890"
            };

            transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionViewModel>())).Returns(exampleTransactionDto);

            transactionServicesMock.Setup(x => x.UpdateTransactionAsync(It.IsAny<TransactionDto>()))
                .ThrowsAsync(new Exception());

            var sut = new TransactionController(
                userServicesMock.Object,
                accountServicesMock.Object,
                mapperMock.Object,
                transactionServicesMock.Object,
                authorizationManager.Object,
                contextAccessorMock.Object,
                transactionMapperMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var actionResult = await sut.UpdateTransaction(exampleTransaction);

            Assert.IsInstanceOfType(actionResult, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public async Task SendUpdatedTransaction_Should_ReturnOk_IfNoErrors()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            var exampleTransaction = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                Description = "ExampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890"
            };

            transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionViewModel>())).Returns(exampleTransactionDto);

            transactionServicesMock.Setup(x => x.SendUpdatedTransactionAsync(It.IsAny<TransactionDto>()))
                .ReturnsAsync(exampleTransactionDto);

            var sut = new TransactionController(
                userServicesMock.Object,
                accountServicesMock.Object,
                mapperMock.Object,
                transactionServicesMock.Object,
                authorizationManager.Object,
                contextAccessorMock.Object,
                transactionMapperMock.Object);

            var actionResult = await sut.SendUpdatedTransaction(exampleTransaction);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }      

        [TestMethod]
        public async Task SendUpdatedTransaction_Should_ReturnBadRequest_IfBadModelState()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            var exampleTransaction = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                Description = "ExampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890"
            };

            transactionMapperMock.Setup(x => x.MapFrom(It.IsAny<TransactionViewModel>())).Returns(exampleTransactionDto);

            transactionServicesMock.Setup(x => x.SendUpdatedTransactionAsync(It.IsAny<TransactionDto>()))
                .ThrowsAsync(new Exception());

            var sut = new TransactionController(
                userServicesMock.Object,
                accountServicesMock.Object,
                mapperMock.Object,
                transactionServicesMock.Object,
                authorizationManager.Object,
                contextAccessorMock.Object,
                transactionMapperMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var actionResult = await sut.SendUpdatedTransaction(exampleTransaction);

            Assert.IsInstanceOfType(actionResult, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public async Task SaveTransaction_Should_ReturnOk_IfNoErrors()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();            
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            var exampleTransaction = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                Description = "ExampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890"
            };


            transactionServicesMock.Setup(x => x.SaveTransactionAsync(It.IsAny<TransactionDto>()))
                .ReturnsAsync(exampleTransactionDto);

            var sut = new TransactionController(
                userServicesMock.Object,
                accountServicesMock.Object,
                mapperMock.Object,
                transactionServicesMock.Object,
                authorizationManager.Object,
                contextAccessorMock.Object,
                transactionMapperMock.Object);

            var actionResult = await sut.SaveTransaction(exampleTransaction);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }        

        [TestMethod]
        public async Task SaveTransaction_Should_ReturnBadRequest_IfBadModelState()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleTransaction = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                Description = "ExampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890"
            };

            transactionServicesMock.Setup(x => x.SaveTransactionAsync(It.IsAny<TransactionDto>()))
                .Throws(new BusinessLogicException());

            var sut = new TransactionController(
                userServicesMock.Object,
                accountServicesMock.Object,
                mapperMock.Object,
                transactionServicesMock.Object,
                authorizationManager.Object,
                contextAccessorMock.Object,
                transactionMapperMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var actionResult = await sut.SaveTransaction(exampleTransaction);

            Assert.IsInstanceOfType(actionResult, typeof(BadRequestObjectResult));            
        }

        [TestMethod]
        public async Task SendTransaction_Should_ReturnOk_IfNoErrors()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleTransaction = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                Description = "ExampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890"
            };


            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            transactionServicesMock.Setup(x => x.SendTransactionAsync(1))
                .ReturnsAsync(exampleTransactionDto);

            var sut = new TransactionController(
                  userServicesMock.Object,
                  accountServicesMock.Object,
                  mapperMock.Object,
                  transactionServicesMock.Object,
                  authorizationManager.Object,
                  contextAccessorMock.Object,
                  transactionMapperMock.Object);

            var actionResult = await sut.SendTransaction(exampleTransaction.Id);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }     

        [TestMethod]
        public async Task SendTransaction_Should_View_IfBadModelState()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleTransaction = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                Description = "ExampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890"
            };

            transactionServicesMock.Setup(x => x.SendTransactionAsync(1))
                .Throws(new BusinessLogicException());

            var sut = new TransactionController(
                   userServicesMock.Object,
                   accountServicesMock.Object,
                   mapperMock.Object,
                   transactionServicesMock.Object,
                   authorizationManager.Object,
                   contextAccessorMock.Object,
                   transactionMapperMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var actionResult = await sut.SendTransaction(exampleTransaction.Id);

            Assert.IsInstanceOfType(actionResult, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public async Task SendJustCreatedTransaction_Should_ReturnOk_IfNoErrors()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleTransaction = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                Description = "ExampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890"
            };


            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            transactionServicesMock.Setup(x => x.AddTransactionAsync(It.IsAny<TransactionDto>()))
                .ReturnsAsync(exampleTransactionDto);

            var sut = new TransactionController(
                  userServicesMock.Object,
                  accountServicesMock.Object,
                  mapperMock.Object,
                  transactionServicesMock.Object,
                  authorizationManager.Object,
                  contextAccessorMock.Object,
                  transactionMapperMock.Object);

            var actionResult = await sut.SendJustCreatedTransaction(exampleTransaction);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }        

        [TestMethod]
        public async Task SendJustCreatedTransaction_Should_View_IfBadModelState()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleTransaction = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                Description = "ExampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890"
            };


            transactionServicesMock.Setup(x => x.AddTransactionAsync(It.IsAny<TransactionDto>()))
                .Throws(new BusinessLogicException());

            var sut = new TransactionController(
                 userServicesMock.Object,
                 accountServicesMock.Object,
                 mapperMock.Object,
                 transactionServicesMock.Object,
                 authorizationManager.Object,
                 contextAccessorMock.Object,
                 transactionMapperMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var actionResult = await sut.SendJustCreatedTransaction(exampleTransaction);

            Assert.IsInstanceOfType(actionResult, typeof(BadRequestObjectResult));
        }
       
      
        [TestMethod]
        public async Task ListTransactions_Should_ReturnCorrectView_IfNoErrors_WithoutPreSelectedAccount()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            var exampleTransaction = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                Description = "ExampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890"
            };


            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            var exampleAccountsList = new List<AccountDto>()
            {
                new AccountDto
                {
                    Id = 1
                },
                new AccountDto
                {
                    Id = 2
                },
            };

            var exampleAccountViewModelList = new List<AccountViewModel>()
            {
                new AccountViewModel
                {
                    Id = 1
                },
                new AccountViewModel
                {
                    Id = 2
                },
            };

            var exampleTransactionDtoList = new List<TransactionDto>()
            {
                new TransactionDto
                {
                    Id = 1
                },
                new TransactionDto
                {
                    Id = 2
                },
            };

            var exampleTransactionViewModelList = new List<TransactionViewModel>()
            {
                new TransactionViewModel
                {
                    Id = 1
                },
                new TransactionViewModel
                {
                    Id = 2
                },
            };

            contextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);

            authorizationManager.Setup(x => x.GetLoggedUserId()).Returns(6);

            accountServicesMock.Setup(x => x.GetAllUserAvailableAccountsAsync(It.IsAny<long>())).
                ReturnsAsync(exampleAccountsList);

            mapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleAccountViewModelList);


            transactionServicesMock.Setup(x => x.GetPageCountForTransactionsAsync(
                5, It.IsAny<long>(), It.IsAny<List<long>>()))
               .ReturnsAsync(1);


            transactionServicesMock.Setup(x => x.GetFiveTransactionsForAccountDescAsync
            (1, 1, 6)).ReturnsAsync(exampleTransactionDtoList);

            transactionMapperMock.Setup(x => x.MapFromWithAccount(It.IsAny<List<TransactionDto>>(), It.IsAny<long>()))
                .Returns(exampleTransactionViewModelList);

            var sut = new TransactionController(
                  userServicesMock.Object,
                  accountServicesMock.Object,
                  mapperMock.Object,
                  transactionServicesMock.Object,
                  authorizationManager.Object,
                  contextAccessorMock.Object,
                  transactionMapperMock.Object);

            var actionResult = await sut.ListTransactions(1);

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));

            var viewResult = (ViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(ListTransactionsViewModel));
        }

        [TestMethod]
        public async Task ListTransactions_Should_ReturnCorrectView_IfNoErrors_WithPreSelectedAccount()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleTransaction = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                Description = "ExampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890"
            };


            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";


            var exampleAccountsList = new List<AccountDto>()
            {
                new AccountDto
                {
                    Id = 1
                },
                new AccountDto
                {
                    Id = 2
                },
            };

            var exampleAccountViewModelList = new List<AccountViewModel>()
            {
                new AccountViewModel
                {
                    Id = 1
                },
                new AccountViewModel
                {
                    Id = 2
                },
            };

            var exampleTransactionDtoList = new List<TransactionDto>()
            {
                new TransactionDto
                {
                    Id = 1
                },
                new TransactionDto
                {
                    Id = 2
                },
            };

            var exampleTransactionViewModelList = new List<TransactionViewModel>()
            {
                new TransactionViewModel
                {
                    Id = 1
                },
                new TransactionViewModel
                {
                    Id = 2
                },
            };

            contextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);

            authorizationManager.Setup(x => x.GetLoggedUserId()).Returns(6);

            accountServicesMock.Setup(x => x.GetAllUserAvailableAccountsAsync(It.IsAny<long>())).
                ReturnsAsync(exampleAccountsList);

            mapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleAccountViewModelList);

            transactionServicesMock.Setup(x => x.GetFiveTransactionsForAccountDescAsync(
                 It.IsAny<int>(), It.IsAny<long>(), It.IsAny<long>()))
                .ReturnsAsync(exampleTransactionDtoList);

            transactionServicesMock.Setup(x => x.GetPageCountForTransactionsAsync(
                5, It.IsAny<long>(), null))
               .ReturnsAsync(1);

            transactionMapperMock.Setup(x => x.MapFromWithAccount(It.IsAny<List<TransactionDto>>(), It.IsAny<long>()))
                .Returns(exampleTransactionViewModelList);

            var sut = new TransactionController(
                  userServicesMock.Object,
                  accountServicesMock.Object,
                  mapperMock.Object,
                  transactionServicesMock.Object,
                  authorizationManager.Object,
                  contextAccessorMock.Object,
                  transactionMapperMock.Object);

            var actionResult = await sut.ListTransactions(1);

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));

            var viewResult = (ViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(ListTransactionsViewModel));
        }   

        [TestMethod]
        public async Task GetFiveTransactions_Should_CorrectPartialView_IfNoErrors_WithPreSelectedAccount()
        {
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IAccountViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var transactionServicesMock = new Mock<ITransactionServices>();
            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            var authorizationManager = new Mock<IAuthorizationManager>();
            var transactionMapperMock = new Mock<ITransactionVeiwModelMapper>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";


            var exampleTransaction = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                Description = "ExampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890"
            };


            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Description = "exampleDescription",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                ReceiverAccountNumber = "1234567890",
                TimeStamp = new DateTime(2017, 3, 3),
                Amount = 100
            };

            var exampleAccountsList = new List<AccountDto>()
            {
                new AccountDto
                {
                    Id = 1
                },
                new AccountDto
                {
                    Id = 2
                },
            };

            var exampleAccountViewModelList = new List<AccountViewModel>()
            {
                new AccountViewModel
                {
                    Id = 1
                },
                new AccountViewModel
                {
                    Id = 2
                },
            };

            var exampleTransactionDtoList = new List<TransactionDto>()
            {
                new TransactionDto
                {
                    Id = 1
                },
                new TransactionDto
                {
                    Id = 2
                },
            };

            var exampleTransactionViewModelList = new List<TransactionViewModel>()
            {
                new TransactionViewModel
                {
                    Id = 1
                },
                new TransactionViewModel
                {
                    Id = 2
                },
            };

            contextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);

            authorizationManager.Setup(x => x.GetLoggedUserId()).Returns(6);

            accountServicesMock.Setup(x => x.GetAllUserAvailableAccountsAsync(It.IsAny<long>())).
                ReturnsAsync(exampleAccountsList);

            mapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleAccountViewModelList);

            transactionServicesMock.Setup(x => x.GetFiveTransactionsForAccountDescAsync(
                 It.IsAny<int>(), It.IsAny<long>(), It.IsAny<long>()))
                .ReturnsAsync(exampleTransactionDtoList);

            transactionServicesMock.Setup(x => x.GetPageCountForTransactionsAsync(
                5, It.IsAny<long>(), null))
               .ReturnsAsync(1);

            transactionMapperMock.Setup(x => x.MapFromWithAccount(It.IsAny<List<TransactionDto>>(), It.IsAny<long>()))
                .Returns(exampleTransactionViewModelList);

            var sut = new TransactionController(
                  userServicesMock.Object,
                  accountServicesMock.Object,
                  mapperMock.Object,
                  transactionServicesMock.Object,
                  authorizationManager.Object,
                  contextAccessorMock.Object,
                  transactionMapperMock.Object);

            var actionResult = await sut.GetFiveTransactions(1, 2);

            Assert.IsInstanceOfType(actionResult, typeof(PartialViewResult));

            var viewResult = (PartialViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(FiveTransactionListViewModel));
        }     
    }
}
