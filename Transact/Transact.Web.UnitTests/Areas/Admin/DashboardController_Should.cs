﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Web.Areas.Admin.Controllers;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Mapper.Contracts;

namespace Transact.Web.UnitTests.Areas.Admin
{
    [TestClass]
    public class DashboardController_Should
    {
        [TestMethod]
        public async Task Index_Should_ReturnCorrectView_IfNoErrors()
        {
            var clientServicesMock = new Mock<IClientServices>();            
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            //var memoryCacheMock = new Mock<IMemoryCache>();

            var exampleClientDto = new List<ClientDto>()
            {
                new ClientDto()
                {
                    Id = 1,
                    Name = "exampleClientDto"
                },
                new ClientDto()
                {
                    Id = 2,
                    Name = "exampleClientDto"
                },
                new ClientDto()
                {
                    Id = 3,
                    Name = "exampleClientDto"
                },
            };

            var exampleUserDto = new List<UserDto>()
            {
                new UserDto()
                {
                    Id = 1,
                    Name = "exampleClientDto"
                },
                new UserDto()
                {
                    Id = 2,
                    Name = "exampleClientDto"
                },
                new UserDto()
                {
                    Id = 3,
                    Name = "exampleClientDto"
                },
            };

            clientServicesMock.Setup(x => x.GetSevenLatestClientsAsync()).ReturnsAsync(exampleClientDto);
            userServicesMock.Setup(x => x.GetTotalUsersCountAsync()).ReturnsAsync(18);
            userServicesMock.Setup(x => x.GetFiveUsersByIdAsync(It.IsAny<int>())).ReturnsAsync(exampleUserDto);
            var cache = new MemoryCache(new MemoryCacheOptions());

            var sut = new DashboardController(
                userServicesMock.Object,
                clientServicesMock.Object,
                usermapperMock.Object,
                clientmapperMock.Object,
                cache);

            var actionResult = await sut.Index();

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));

            var viewResult = (ViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(AdminDashViewModel));
        }

        [TestMethod]
        public async Task AboutPage_ShouldReturnCorrectView_IfNoErrors()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var cache = new MemoryCache(new MemoryCacheOptions());

            var sut = new DashboardController(
                userServicesMock.Object,
                clientServicesMock.Object,
                usermapperMock.Object,
                clientmapperMock.Object,
                cache);

            var actionResult = await sut.About();

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
        }
    }
}
