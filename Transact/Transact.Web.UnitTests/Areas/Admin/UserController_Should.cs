﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Services.CustomExceptions;
using Transact.Web.Areas.Admin.Controllers;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.Mapper.Contracts;

namespace Transact.Web.UnitTests.Areas.Admin
{
    [TestClass]
    public class UserController_Should
    {
        [TestMethod]
        public async Task AddUser_Should_ReturnCorrectView_IfNoErrors()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IUserViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                UserName = "exampleUserDto"
            };

            userServicesMock.Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(exampleUserDto);

            tokenManagerMock.Setup(x => x.GenerateToken(TestSamples.exampleUser.UserName, TestSamples.exampleUser.Role.RoleName, TestSamples.exampleUser.Id.ToString())).Returns(TestSamples.exampleToken);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));


            var sut = new UserController(
                userServicesMock.Object,
                mapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accMapperMock.Object);

            var actionResult = await sut.AddUser();

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));

            var viewResult = (ViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(AddUserViewModel));
        }

        [TestMethod]
        public async Task AddUser_Should_ReturnViewResult_IfModelStateIsInvalid()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IUserViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                UserName = "exampleUserDto"
            };

            userServicesMock.Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(exampleUserDto);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new UserController(
                userServicesMock.Object,
                mapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accMapperMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var result = await sut.AddUser();

            Assert.IsInstanceOfType(result, typeof(ViewResult));

            var viewResult = (ViewResult)result;

            Assert.IsInstanceOfType(viewResult.Model, typeof(AddUserViewModel));
        }

        [TestMethod]
        public async Task GetFiveUsersById_Should_ReturnCorrectPartialView_IfNoErrors()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IUserViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                UserName = "exampleUserDto"
            };

            userServicesMock.Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(exampleUserDto);

            tokenManagerMock.Setup(x => x.GenerateToken(TestSamples.exampleUser.UserName, TestSamples.exampleUser.Role.RoleName, TestSamples.exampleUser.Id.ToString())).Returns(TestSamples.exampleToken);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));


            var sut = new UserController(
                userServicesMock.Object,
                mapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accMapperMock.Object);

            var actionResult = await sut.GetFiveUsersById(1);

            Assert.IsInstanceOfType(actionResult, typeof(PartialViewResult));

            var viewResult = (PartialViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(UserListViewModel));
        }

        [TestMethod]
        public async Task GetFiveUsersById_Should_ReturnViewResult_IfModelStateIsInvalid()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IUserViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                UserName = "exampleUserDto",
            };

            userServicesMock.Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(exampleUserDto);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new UserController(
                userServicesMock.Object,
                mapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accMapperMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var result = await sut.GetFiveUsersById(1);

            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        [TestMethod]
        public async Task SaveUser_Should_ReturnCorrectOk_IfNoErrors()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IUserViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                UserName = "exampleUserDto"
            };

            userServicesMock.Setup(x => x.AddUserAsync(It.IsAny<UserDto>())).ReturnsAsync(exampleUserDto);

            tokenManagerMock.Setup(x => x.GenerateToken(TestSamples.exampleUser.UserName, TestSamples.exampleUser.Role.RoleName, TestSamples.exampleUser.Id.ToString())).Returns(TestSamples.exampleToken);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new UserController(
                userServicesMock.Object,
                mapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accMapperMock.Object);

            var actionResult = await sut.SaveUser(TestSamples.exampleManageusersViewModel);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }

        [TestMethod]
        public async Task SaveUser_Should_ReturnViewResult_IfModelStateIsInvalid()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IUserViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                UserName = "exampleUserDto"
            };

            userServicesMock.Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(exampleUserDto);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new UserController(
                userServicesMock.Object,
                mapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accMapperMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var result = await sut.SaveUser(TestSamples.exampleManageusersViewModel);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public async Task AssignUserToClient_Should_ReturnCorrectOk_IfNoErrors()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IUserViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleUserDto = new AccountDto()
            {
                Id = 1,
                AccountNumber = "1234567890"
            };

            accountServicesMock.Setup(x => x.AssignAccountToUserAsync(It.IsAny<string>(), It.IsAny<long>())).ReturnsAsync(exampleUserDto);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new UserController(
                userServicesMock.Object,
                mapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accMapperMock.Object);

            var actionResult = await sut.AssignAccountToUser(TestSamples.exampleAccount.AccountNumber, TestSamples.exampleUser.Id);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }

        [TestMethod]
        public async Task RemoveUserForClient_Should_ReturnCorrectOk_IfNoErrors()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IUserViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleUserDto = new AccountDto()
            {
                Id = 1,
                AccountNumber = "1234567890"
            };

            accountServicesMock.Setup(x => x.RemoveAccountForUser(It.IsAny<long>(), It.IsAny<long>())).ReturnsAsync(exampleUserDto);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new UserController(
                userServicesMock.Object,
                mapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accMapperMock.Object);

            var actionResult = await sut.RemoveUserForClient(TestSamples.exampleUser.Id, TestSamples.exampleUser.Id);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }

        [TestMethod]
        public async Task GetFiveAccountsForUserById_Should_ReturnCorrectPartialView_IfNoErrors()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IUserViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleAccountViewModelList = new List<AccountViewModel>()
            {
                new AccountViewModel
                {
                    Id = 1
                },
                new AccountViewModel
                {
                    Id = 2
                },
            };

            var exampleAccountDtoList = new List<AccountDto>()
            {
                new AccountDto
                {
                    Id = 1
                },
                new AccountDto
                {
                    Id = 2
                },
            };

            accountServicesMock.Setup(x => x.GetPageCountForAccountsWithUsersIdAsync(5, TestSamples.exampleUser.Id)).ReturnsAsync(1);

            accountServicesMock.Setup(x => x.GetFiveAccountsForUserByIdAsync(1, TestSamples.exampleUser.Id)).ReturnsAsync(exampleAccountDtoList);

            accMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleAccountViewModelList);

            var sut = new UserController(
                userServicesMock.Object,
                mapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accMapperMock.Object);

            var actionResult = await sut.GetFiveAccountsForUserById(1, TestSamples.exampleUser.Id);

            Assert.IsInstanceOfType(actionResult, typeof(PartialViewResult));

            var viewResult = (PartialViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(FiveAccountsViewModel));
        }

        [TestMethod]
        public async Task GetFiveAccountsForUserById_Should_ReturnViewResult_IfModelStateIsInvalid()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IUserViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleAccountViewModelList = new List<AccountViewModel>()
            {
                new AccountViewModel
                {
                    Id = 1
                },
                new AccountViewModel
                {
                    Id = 2
                },
            };

            var exampleAccountDtoList = new List<AccountDto>()
            {
                new AccountDto
                {
                    Id = 1
                },
                new AccountDto
                {
                    Id = 2
                },
            };

            accountServicesMock.Setup(x => x.GetPageCountForAccountsWithUsersIdAsync(5, TestSamples.exampleUser.Id)).ReturnsAsync(1);

            accountServicesMock.Setup(x => x.GetFiveAccountsForUserByIdAsync(1, TestSamples.exampleUser.Id)).ReturnsAsync(exampleAccountDtoList);

            accMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleAccountViewModelList);

            var sut = new UserController(
                userServicesMock.Object,
                mapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accMapperMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var actionResult = await sut.GetFiveAccountsForUserById(1, TestSamples.exampleUser.Id);

            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        [TestMethod]
        public async Task FindSingleByUserName_Should_ReturnCorrectJson_IfNoErrors()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IUserViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
            httpContextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);

            var exampleListUser = new List<UserDto>
            {
                new UserDto()
                {
                    Id = 1,
                    Name = "username",
                    UserName = "username",
                    RoleId = 1,
                    RoleName = "User"
                },

                new UserDto()
                {
                    Id = 2,
                    Name = "username2",
                    UserName = "username2",
                    RoleId = 1,
                    RoleName = "User"
                }
            };

            var exampleListUserViewModel = new List<UserViewModel>
            {
                new UserViewModel()
                {
                    Id = 1,
                    Name = "username",
                    UserName = "username",
                    RoleId = 1,
                    Role = "User"
                }
            };

            userServicesMock.Setup(x => x.FindUsersContainingAsync("user", TestSamples.exampleClient.Id)).ReturnsAsync(exampleListUser);

            mapperMock.Setup(x => x.MapFrom(It.IsAny<List<UserDto>>())).Returns(exampleListUserViewModel);

            var sut = new UserController(
                userServicesMock.Object,
                mapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accMapperMock.Object);

            var actionResult = await sut.FindSingleByUserName("user", TestSamples.exampleClient.Id);

            Assert.IsInstanceOfType(actionResult, typeof(JsonResult));

            JsonResult viewResult = actionResult as JsonResult;

            var users = viewResult.Value as List<UserViewModel>;

            Assert.AreEqual(users?.Count, 1);

            Assert.AreEqual(users[0]?.Id, exampleListUser[0].Id);
            Assert.AreEqual(users[0]?.Name, exampleListUser[0].Name);
            Assert.AreEqual(users[0]?.UserName, exampleListUser[0].UserName);
            Assert.AreEqual(users[0]?.RoleId, exampleListUser[0].RoleId);
            Assert.AreEqual(users[0]?.Role, exampleListUser[0].RoleName);
        }


        [TestMethod]
        public async Task FindSingleByUserName_Should_ReturnEmptyJson_IfInputIsNull()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IUserViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
            httpContextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);

            var exampleListUser = new List<UserDto>
            {
                new UserDto()
                {
                    Id = 1,
                    Name = "username",
                    UserName = "username",
                    RoleId = 1,
                    RoleName = "User"
                },

                new UserDto()
                {
                    Id = 2,
                    Name = "username2",
                    UserName = "username2",
                    RoleId = 1,
                    RoleName = "User"
                }
            };

            var exampleListUserViewModel = new List<UserViewModel>
            {
                new UserViewModel()
                {
                    Id = 1,
                    Name = "username",
                    UserName = "username",
                    RoleId = 1,
                    Role = "User"
                }
            };

            userServicesMock.Setup(x => x.FindUsersContainingAsync("user", TestSamples.exampleClient.Id)).ReturnsAsync(exampleListUser);

            mapperMock.Setup(x => x.MapFrom(It.IsAny<List<UserDto>>())).Returns(exampleListUserViewModel);

            var sut = new UserController(
                userServicesMock.Object,
                mapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accMapperMock.Object);

            var actionResult = await sut.FindSingleByUserName(string.Empty, TestSamples.exampleClient.Id);

            Assert.IsInstanceOfType(actionResult, typeof(JsonResult));

            Assert.AreEqual(actionResult.Value, string.Empty);
        }

        [TestMethod]
        public async Task FindSingleByAccountNumberForUser_Should_ReturnCorrectJson_IfNoErrors()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IUserViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
            httpContextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);

            var exampleListAccount = new List<AccountDto>
            {
                new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientName = "ExampleClientName1",
                    ClientId = 1,
                    NickName = "ExampleNickName1"
                },

                new AccountDto()
                {
                    Id = 2,
                    AccountNumber = "1234555555",
                    Balance = 10000,
                    ClientName = "ExampleClientName2",
                    ClientId = 2,
                    NickName = "ExampleNickName"
                }
            };

            var exampleListAccountViewModel = new List<AccountViewModel>
            {
                new AccountViewModel()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientName = "ExampleClientName1",
                    ClientId = 1,
                    NickName = "ExampleNickName1"
                }
            };

            accountServicesMock.Setup(x => x.FindAccountsForUserContainingAsync("1234", TestSamples.exampleUser.Id)).ReturnsAsync(exampleListAccount);

            accMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleListAccountViewModel);

            var sut = new UserController(
                userServicesMock.Object,
                mapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accMapperMock.Object);

            var actionResult = await sut.FindSingleByAccountNumberForUser("1234", TestSamples.exampleUser.Id);

            Assert.IsInstanceOfType(actionResult, typeof(JsonResult));

            JsonResult viewResult = actionResult as JsonResult;

            var account = viewResult.Value as List<AccountViewModel>;

            Assert.AreEqual(account?.Count, 1);

            Assert.AreEqual(account[0]?.Id, exampleListAccount[0].Id);
            Assert.AreEqual(account[0]?.NickName, exampleListAccount[0].NickName);
            Assert.AreEqual(account[0]?.ClientName, exampleListAccount[0].ClientName);
            Assert.AreEqual(account[0]?.Balance, exampleListAccount[0].Balance);
            Assert.AreEqual(account[0]?.AccountNumber, exampleListAccount[0].AccountNumber);
            Assert.AreEqual(account[0]?.ClientId, exampleListAccount[0].ClientId);
        }


        [TestMethod]
        public async Task FindSingleByAccountNumberForUser_Should_ReturnEmptyJson_IfInputIsNull()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var userServicesMock = new Mock<IUserServices>();
            var mapperMock = new Mock<IUserViewModelMapper>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
            httpContextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);

            var exampleListAccount = new List<AccountDto>
            {
                new AccountDto()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientName = "ExampleClientName1",
                    ClientId = 1,
                    NickName = "ExampleNickName1"
                },

                new AccountDto()
                {
                    Id = 2,
                    AccountNumber = "1234555555",
                    Balance = 10000,
                    ClientName = "ExampleClientName2",
                    ClientId = 2,
                    NickName = "ExampleNickName"
                }
            };

            var exampleListAccountViewModel = new List<AccountViewModel>
            {
                new AccountViewModel()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientName = "ExampleClientName1",
                    ClientId = 1,
                    NickName = "ExampleNickName1"
                }
            };

            accountServicesMock.Setup(x => x.FindAccountsForUserContainingAsync("1234", TestSamples.exampleUser.Id)).ReturnsAsync(exampleListAccount);

            accMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleListAccountViewModel);

            var sut = new UserController(
                userServicesMock.Object,
                mapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accMapperMock.Object);

            var actionResult = await sut.FindSingleByAccountNumberForUser(string.Empty, TestSamples.exampleUser.Id);

            Assert.IsInstanceOfType(actionResult, typeof(JsonResult));

            Assert.AreEqual(actionResult.Value, string.Empty);
        }

        [TestMethod]
        public async Task ManageUser_Should_ReturnCorrectView_IfNoErrors()
        {
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();

            var examplUserDto = new UserDto()
            {
                Id = 1,
                Name = "username",
                UserName = "username",
                RoleId = 1,
                RoleName = "User"
            };

            var exampleAccountsList = new List<AccountDto>()
            {
                new AccountDto
                {
                    Id = 1
                },
                new AccountDto
                {
                    Id = 2
                },
            };

            var exampleAccountViewModelList = new List<AccountViewModel>()
            {
                new AccountViewModel
                {
                    Id = 1
                },
                new AccountViewModel
                {
                    Id = 2
                },
            };

            var exampleUserViewModelList = new List<UserViewModel>()
            {
                new UserViewModel
                {
                    Id = 1,
                    Name = "username",
                    UserName = "username",
                    RoleId = 1,
                    Role = "User"
                },
                new UserViewModel
                {
                    Id = 2,
                    Name = "username2",
                    UserName = "username2",
                    RoleId = 1,
                    Role = "User"
                },
            };

            var exampleUserViewModel = new UserViewModel
            {
                Id = 1,
                Name = "username",
                UserName = "username",
                RoleId = 1,
                Role = "User"
            };

            var exampleUserDtoList = new List<UserDto>()
            {
                new UserDto
                {
                    Id = 1,
                    Name = "username",
                    UserName = "username",
                    RoleId = 1,
                    RoleName = "User"
                },
                new UserDto
                {
                    Id = 2,
                    Name = "username2",
                    UserName = "username2",
                    RoleId = 1,
                    RoleName = "User"
                },
            };

            var fiveAcc = new FiveAccountsViewModel()
            {
                CurrPage = 1,
                TotalPages = 1,
                ClientId = TestSamples.exampleClient.Id,
                FiveAccountsList = exampleAccountViewModelList
            };

            var examplemanageUserViewModel = new ManageUserAccountsViewModel()
            {
                UserId = TestSamples.exampleUser.Id,
                FiveAccountsList = fiveAcc
            };

            accountServicesMock.Setup(x => x.GetFiveAccountsForClientByIdAsync(It.IsAny<int>(), It.IsAny<long>())).ReturnsAsync(exampleAccountsList);

            accountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleAccountViewModelList);

            userServicesMock.Setup(x => x.GetUserByIdAsync(It.IsAny<long>())).ReturnsAsync(examplUserDto);

            usermapperMock.Setup(x => x.MapFrom(It.IsAny<UserDto>())).Returns(exampleUserViewModel);

            accountServicesMock.Setup(x => x.GetPageCountForAccountsAsync(5, TestSamples.exampleClient.Id)).ReturnsAsync(1);

            var sut = new UserController(
                userServicesMock.Object,
                usermapperMock.Object,
                httpContextAccessorMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object);

            var actionResult = await sut.ManageUser(TestSamples.exampleUser.Id);

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));

            var viewResult = (ViewResult)actionResult;  //TODO: moje bi ne taka 

            Assert.IsInstanceOfType(viewResult.Model, typeof(ManageUserViewModel));
        }
    }
}