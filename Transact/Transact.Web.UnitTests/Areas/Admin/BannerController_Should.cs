﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Transact.FileManager.Contracts;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Web.Areas.Admin.Controllers;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Mapper.Contracts;

namespace Transact.Web.UnitTests.Areas.Admin
{
    [TestClass]
    public class BannerController_Should
    {
        [TestMethod]
        public async Task AddBanner_Should_ReturnCorrectView_IfNoErrors()
        {  
            var bannerMapperMock = new Mock<IBannerViewModelMapper>();

            var bannerServicesMock = new Mock<IBannerServices>();

            var localStorageMangerMock = new Mock<ILocalStorageManager>();

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleImageName = new Guid().ToString();

            var exampleBannerDtoList = new List<BannerDto>
            {
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            var exampleBannerViewModelList = new List<BannerViewModel>
            {
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            bannerServicesMock.Setup(x => x.GetFiveBannersByExpirationDateAsync(It.IsAny<int>())).ReturnsAsync(exampleBannerDtoList);

            bannerMapperMock.Setup(x => x.MapFrom(It.IsAny<List<BannerDto>>())).Returns(exampleBannerViewModelList);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));


            var sut = new BannerController(
                bannerServicesMock.Object,
                bannerMapperMock.Object,
                localStorageMangerMock.Object);

            var actionResult = await sut.AddBanner();

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));

            var viewResult = (ViewResult)actionResult;

           Assert.IsInstanceOfType(viewResult.Model, typeof(BannerManagementViewModel));
        }

        [TestMethod]
        public async Task GetFiveBanners_Should_ReturnCorrectPartialView_IfNoErrors_AndPageNull()
        {
            var bannerMapperMock = new Mock<IBannerViewModelMapper>();

            var bannerServicesMock = new Mock<IBannerServices>();

            var localStorageMangerMock = new Mock<ILocalStorageManager>();

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleImageName = new Guid().ToString();

            var exampleBannerDtoList = new List<BannerDto>
            {
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            var exampleBannerViewModelList = new List<BannerViewModel>
            {
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            bannerServicesMock.Setup(x => x.GetFiveBannersByExpirationDateAsync(It.IsAny<int>())).ReturnsAsync(exampleBannerDtoList);

            bannerMapperMock.Setup(x => x.MapFrom(It.IsAny<List<BannerDto>>())).Returns(exampleBannerViewModelList);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));


            var sut = new BannerController(
                bannerServicesMock.Object,
                bannerMapperMock.Object,
                localStorageMangerMock.Object);

            var actionResult = await sut.GetFiveBanners(null);

            Assert.IsInstanceOfType(actionResult, typeof(PartialViewResult));

            var viewResult = (PartialViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(FiveBannersViewModel));
        }

        [TestMethod]
        public async Task GetFiveBanners_Should_ReturnCorrectPartialView_IfNoErrors_AndPageNotNull()
        {
            var bannerMapperMock = new Mock<IBannerViewModelMapper>();

            var bannerServicesMock = new Mock<IBannerServices>();

            var localStorageMangerMock = new Mock<ILocalStorageManager>();

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleImageName = new Guid().ToString();

            var exampleBannerDtoList = new List<BannerDto>
            {
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            var exampleBannerViewModelList = new List<BannerViewModel>
            {
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            bannerServicesMock.Setup(x => x.GetFiveBannersByExpirationDateAsync(It.IsAny<int>())).ReturnsAsync(exampleBannerDtoList);

            bannerMapperMock.Setup(x => x.MapFrom(It.IsAny<List<BannerDto>>())).Returns(exampleBannerViewModelList);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));


            var sut = new BannerController(
                bannerServicesMock.Object,
                bannerMapperMock.Object,
                localStorageMangerMock.Object);

            var actionResult = await sut.GetFiveBanners(1);

            Assert.IsInstanceOfType(actionResult, typeof(PartialViewResult));

            var viewResult = (PartialViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(FiveBannersViewModel));
        }

        [TestMethod]
        public async Task EditBanner_Should_ReturnViewModel_IfNoErrors()
        {
            var bannerMapperMock = new Mock<IBannerViewModelMapper>();

            var bannerServicesMock = new Mock<IBannerServices>();

            var localStorageMangerMock = new Mock<ILocalStorageManager>();

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleImageName = new Guid().ToString();

            var exampleBannerDtoList = new List<BannerDto>
            {
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            var exampleBannerViewModelList = new List<BannerViewModel>
            {
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            var exampleBannerDto = new BannerDto()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBannerViewModel = new EditBannerViewModel()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            bannerServicesMock.Setup(x => x.GetSingleBannerForEditingAsync(It.IsAny<long>())).ReturnsAsync(exampleBannerDto);

            bannerMapperMock.Setup(x => x.MapFromForEditing(It.IsAny<BannerDto>())).Returns(exampleBannerViewModel);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new BannerController(
                bannerServicesMock.Object,
                bannerMapperMock.Object,
                localStorageMangerMock.Object);

            var actionResult = await sut.EditBanner(1);

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));        
        }

        [TestMethod]
        public async Task DeleteBanner_Should_ReturnViewModel_IfNoErrors()
        {
            var bannerMapperMock = new Mock<IBannerViewModelMapper>();

            var bannerServicesMock = new Mock<IBannerServices>();

            var localStorageMangerMock = new Mock<ILocalStorageManager>();

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleImageName = new Guid().ToString();           

            var exampleBannerDto = new BannerDto()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBannerViewModel = new BannerViewModel()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            bannerServicesMock.Setup(x => x.DeleteBannerAsync(It.IsAny<long>())).ReturnsAsync(exampleBannerDto);

            bannerMapperMock.Setup(x => x.MapFrom(It.IsAny<BannerDto>())).Returns(exampleBannerViewModel);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new BannerController(
                bannerServicesMock.Object,
                bannerMapperMock.Object,
                localStorageMangerMock.Object);

            var actionResult = await sut.DeleteBanner(1);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));

            var viewResult = (OkObjectResult)actionResult;

            Assert.AreEqual(viewResult.Value, "Banner succesfuly deleted!");
        }

        [TestMethod]
        public async Task UpdateBanner_Should_ReturnOk_IfNoErrors()
        {
            var bannerMapperMock = new Mock<IBannerViewModelMapper>();

            var bannerServicesMock = new Mock<IBannerServices>();

            var localStorageMangerMock = new Mock<ILocalStorageManager>();

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleImageName = new Guid().ToString();

            var exampleBannerDto = new BannerDto()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBannerDtoList = new List<BannerDto>
            {
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            var exampleBannerViewModelList = new List<BannerViewModel>
            {
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            using (var stream = new MemoryStream())
            {
                var exampleFile = new FormFile(stream, 0, stream.Length, null, "")
                {
                    Headers = new HeaderDictionary(),
                    ContentType = "application/pdf"
                };

                var exampleBannerViewModel = new EditBannerViewModel()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com",
                    Image = exampleFile
                };

                localStorageMangerMock.Setup(x => x.SaveFileAsync(It.IsAny<IFormFile>())).ReturnsAsync(exampleImageName);

                bannerServicesMock.Setup(x => x.SaveBannerAsync(It.IsAny<BannerDto>())).ReturnsAsync(exampleBannerDto);

                bannerMapperMock.Setup(x => x.MapFrom(It.IsAny<BannerViewModel>())).Returns(exampleBannerDto);

                httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

                var sut = new BannerController(
                    bannerServicesMock.Object,
                    bannerMapperMock.Object,
                    localStorageMangerMock.Object);

                var actionResult = await sut.UpdateBanner(exampleBannerViewModel);

                Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));

                var viewResult = (OkObjectResult)actionResult;

                Assert.AreEqual(viewResult.Value, "Banner succesfuly edited!");
            };
        }

        [TestMethod]
        public async Task UpdateBanner_Should_ReturnBadResult_IfModelStateIsInvalid()
        {
            var bannerMapperMock = new Mock<IBannerViewModelMapper>();

            var bannerServicesMock = new Mock<IBannerServices>();

            var localStorageMangerMock = new Mock<ILocalStorageManager>();

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleImageName = new Guid().ToString();

            var exampleBannerDto = new BannerDto()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBannerDtoList = new List<BannerDto>
            {
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            var exampleBannerViewModelList = new List<BannerViewModel>
            {
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            using (var stream = new MemoryStream())
            {
                var exampleFile = new FormFile(stream, 0, stream.Length, null, "")
                {
                    Headers = new HeaderDictionary(),
                    ContentType = "application/pdf"
                };

                var exampleBannerViewModel = new EditBannerViewModel()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com",
                    Image = exampleFile
                };

                localStorageMangerMock.Setup(x => x.SaveFileAsync(It.IsAny<IFormFile>())).ReturnsAsync(exampleImageName);

                bannerServicesMock.Setup(x => x.SaveBannerAsync(It.IsAny<BannerDto>())).ReturnsAsync(exampleBannerDto);

                bannerMapperMock.Setup(x => x.MapFrom(It.IsAny<BannerViewModel>())).Returns(exampleBannerDto);

                httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

                var sut = new BannerController(
                    bannerServicesMock.Object,
                    bannerMapperMock.Object,
                    localStorageMangerMock.Object);

                sut.ModelState.AddModelError("test", "test");

                var actionResult = await sut.UpdateBanner(exampleBannerViewModel);

                Assert.IsInstanceOfType(actionResult, typeof(BadRequestObjectResult));
            };
        }

        [TestMethod]
        public async Task UpdateBanner_Should_CallLocalFileManagerOnce_IfIsNewImageIsTrue()
        {
            var bannerMapperMock = new Mock<IBannerViewModelMapper>();

            var bannerServicesMock = new Mock<IBannerServices>();

            var localStorageMangerMock = new Mock<ILocalStorageManager>();

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleImageName = new Guid().ToString();

            var exampleBannerDto = new BannerDto()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBannerDtoList = new List<BannerDto>
            {
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            var exampleBannerViewModelList = new List<BannerViewModel>
            {
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            using (var stream = new MemoryStream())
            {
                var exampleFile = new FormFile(stream, 0, stream.Length, null, "")
                {
                    Headers = new HeaderDictionary(),
                    ContentType = "application/pdf"
                };

                var exampleBannerViewModel = new EditBannerViewModel()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com",
                    Image = exampleFile,
                    IsNewImage = true
                };

                localStorageMangerMock.Setup(x => x.SaveFileAsync(It.IsAny<IFormFile>())).ReturnsAsync(exampleImageName);

                bannerServicesMock.Setup(x => x.SaveBannerAsync(It.IsAny<BannerDto>())).ReturnsAsync(exampleBannerDto);

                bannerMapperMock.Setup(x => x.MapFromForEditing(It.IsAny<EditBannerViewModel>())).Returns(exampleBannerDto);

                httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

                var sut = new BannerController(
                    bannerServicesMock.Object,
                    bannerMapperMock.Object,
                    localStorageMangerMock.Object);

                var actionResult = await sut.UpdateBanner(exampleBannerViewModel);

                localStorageMangerMock.Verify(x => x.SaveFileAsync(It.IsAny<IFormFile>()), Times.Once);
                localStorageMangerMock.Verify(x => x.DeleteFile(It.IsAny<string>()), Times.Once);
            };
        }

        [TestMethod]
        public async Task UpdateBanner_ShouldNot_CallLocalFileManager_IfIsNewImageIsFalse()
        {
            var bannerMapperMock = new Mock<IBannerViewModelMapper>();

            var bannerServicesMock = new Mock<IBannerServices>();

            var localStorageMangerMock = new Mock<ILocalStorageManager>();

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleImageName = new Guid().ToString();

            var exampleBannerDto = new BannerDto()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBannerDtoList = new List<BannerDto>
            {
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            var exampleBannerViewModelList = new List<BannerViewModel>
            {
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            using (var stream = new MemoryStream())
            {
                var exampleFile = new FormFile(stream, 0, stream.Length, null, "")
                {
                    Headers = new HeaderDictionary(),
                    ContentType = "application/pdf"
                };

                var exampleBannerViewModel = new EditBannerViewModel()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com",
                    Image = exampleFile,
                    IsNewImage = false
                };

                localStorageMangerMock.Setup(x => x.SaveFileAsync(It.IsAny<IFormFile>())).ReturnsAsync(exampleImageName);

                bannerServicesMock.Setup(x => x.SaveBannerAsync(It.IsAny<BannerDto>())).ReturnsAsync(exampleBannerDto);

                bannerMapperMock.Setup(x => x.MapFrom(It.IsAny<BannerViewModel>())).Returns(exampleBannerDto);

                httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

                var sut = new BannerController(
                    bannerServicesMock.Object,
                    bannerMapperMock.Object,
                    localStorageMangerMock.Object);

                var actionResult = await sut.UpdateBanner(exampleBannerViewModel);

                localStorageMangerMock.Verify(x => x.SaveFileAsync(It.IsAny<IFormFile>()), Times.Never);
                localStorageMangerMock.Verify(x => x.DeleteFile(It.IsAny<string>()), Times.Never);
            };
        }

        [TestMethod]
        public async Task SaveBanner_Should_ReturnOk_IfNoErrors()
        {
            var bannerMapperMock = new Mock<IBannerViewModelMapper>();

            var bannerServicesMock = new Mock<IBannerServices>();

            var localStorageMangerMock = new Mock<ILocalStorageManager>();

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleImageName = new Guid().ToString();

            var exampleBannerDto = new BannerDto()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBannerDtoList = new List<BannerDto>
            {
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            var exampleBannerViewModelList = new List<BannerViewModel>
            {
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            using (var stream = new MemoryStream())
            {
                var exampleFile = new FormFile(stream, 0, stream.Length, null, "")
                {
                    Headers = new HeaderDictionary(),
                    ContentType = "application/pdf"
                };                     

                var exampleBannerViewModel = new BannerViewModel()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com",
                    Image = exampleFile
                };

                localStorageMangerMock.Setup(x => x.SaveFileAsync(It.IsAny<IFormFile>())).ReturnsAsync(exampleImageName);

                bannerServicesMock.Setup(x => x.SaveBannerAsync(It.IsAny<BannerDto>())).ReturnsAsync(exampleBannerDto);

                bannerMapperMock.Setup(x => x.MapFrom(It.IsAny<BannerViewModel>())).Returns(exampleBannerDto);

                httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

                var sut = new BannerController(
                    bannerServicesMock.Object,
                    bannerMapperMock.Object,
                    localStorageMangerMock.Object);

                var actionResult = await sut.SaveBanner(exampleBannerViewModel);

                Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));

                var viewResult = (OkObjectResult)actionResult;

                Assert.AreEqual(viewResult.Value, "Banner succesfuly added!");
            };
        }

        [TestMethod]
        public async Task SaveBanner_Should_ReturnBadRequest_IfModelError()
        {
            var bannerMapperMock = new Mock<IBannerViewModelMapper>();

            var bannerServicesMock = new Mock<IBannerServices>();

            var localStorageMangerMock = new Mock<ILocalStorageManager>();

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleImageName = new Guid().ToString();

            var exampleBannerDto = new BannerDto()
            {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
            };

            var exampleBannerDtoList = new List<BannerDto>
            {
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerDto()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            var exampleBannerViewModelList = new List<BannerViewModel>
            {
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                },
                new BannerViewModel()
                {
                Id = 1,
                StartDate = new DateTime(2016, 7, 15),
                EndDate = new DateTime(2017, 7, 15),
                ImageName = exampleImageName,
                Url = "https://example.com"
                }
            };

            using (var stream = new MemoryStream())
            {
                var exampleFile = new FormFile(stream, 0, stream.Length, null, "")
                {
                    Headers = new HeaderDictionary(),
                    ContentType = "application/pdf"
                };

                var exampleBannerViewModel = new BannerViewModel()
                {
                    Id = 1,
                    StartDate = new DateTime(2016, 7, 15),
                    EndDate = new DateTime(2017, 7, 15),
                    ImageName = exampleImageName,
                    Url = "https://example.com",
                    Image = exampleFile
                };

                localStorageMangerMock.Setup(x => x.SaveFileAsync(It.IsAny<IFormFile>())).ReturnsAsync(exampleImageName);

                bannerServicesMock.Setup(x => x.SaveBannerAsync(It.IsAny<BannerDto>())).ReturnsAsync(exampleBannerDto);

                bannerMapperMock.Setup(x => x.MapFrom(It.IsAny<BannerViewModel>())).Returns(exampleBannerDto);

                httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

                var sut = new BannerController(
                    bannerServicesMock.Object,
                    bannerMapperMock.Object,
                    localStorageMangerMock.Object);

                sut.ModelState.AddModelError("test", "test");

                var actionResult = await sut.SaveBanner(exampleBannerViewModel);

                Assert.IsInstanceOfType(actionResult, typeof(BadRequestObjectResult));

                var viewResult = (BadRequestObjectResult)actionResult;

                Assert.AreEqual(viewResult.Value, "Please enter valid form!");
            };
        }

        [TestMethod]
        public void ManageBanners_ShouldReturnCorrectView_IfNoErrors()
        {
            var bannerMapperMock = new Mock<IBannerViewModelMapper>();
            var bannerServicesMock = new Mock<IBannerServices>();
            var localStorageMangerMock = new Mock<ILocalStorageManager>();

            var sut = new BannerController(
                   bannerServicesMock.Object,
                   bannerMapperMock.Object,
                   localStorageMangerMock.Object);

            var actionResult = sut.ManageBanners();

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
        }

    }
}
