﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Transact.Web.Areas.Admin.Controllers;

namespace Transact.Web.UnitTests.Areas.Admin
{
    [TestClass]
    public class HomeController_Should
    {
        [TestMethod]
        public async Task HomePage_ShouldReturnCorrectView_IfNoErrors()
        {
            var sut = new HomeController();

            var actionResult = await sut.AdminLogin();

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
        }
    }
}
