﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Web.Areas.Admin.Controllers;
using Transact.Web.Areas.Admin.ViewModels;

namespace Transact.Web.UnitTests.Areas.Admin
{
    [TestClass]
    public class IdentityController_Should
    {
        [TestMethod]
        public async Task Login_Should_ReturnCorretRedirectToAction_IfNoErrors()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var adminServicesMock = new Mock<IAdminServices>();
            var cookieManagerMock = new Mock<ICookieManager>();

            var exampleAdmin = new Transact.Models.Admin()
            {
                Id = 1,
                UserName = "ExampleUserName1",
                Password = "ExamplePassword1",
                Role = new UserRole { RoleName = "Admin" }
            };

            var exampleAdminDto = new AdminDto()
            {
                Id = 1,
                UserName = "exampleAdminDto",
                RoleName = "Admin"
            };

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            adminServicesMock.Setup(x => x.GetAdminAsync(
                exampleAdmin.UserName,
                exampleAdmin.Password))
                .ReturnsAsync(exampleAdminDto);

            tokenManagerMock.Setup(x => x.GenerateToken(exampleAdmin.UserName, exampleAdmin.Role.RoleName, exampleAdmin.Id.ToString()))
                .Returns(exampleToken);

            var exampleLoginViewModel = new AdminLoginViewModel()
            {
                UserName = exampleAdmin.UserName,
                Password = exampleAdmin.Password
            };

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            httpContextAccessorMock.Setup(X => X.HttpContext
                                                .Response
                                                .Cookies
                                                .Append("SecurityToken",
                                                exampleToken));

            var sut = new IdentityController(
                adminServicesMock.Object,
                tokenManagerMock.Object,
                httpContextAccessorMock.Object,
                cookieManagerMock.Object);

            var actionResult = await sut.Login(exampleLoginViewModel);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }

        [TestMethod]
        public async Task Login_Should_ReturnBadRequest_IfModelStateIsInvalid()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var adminServicesMock = new Mock<IAdminServices>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var cookieManagerMock = new Mock<ICookieManager>();


            var sut = new IdentityController(
                adminServicesMock.Object,
                tokenManagerMock.Object,
                httpContextAccessorMock.Object,
                cookieManagerMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var result = await sut.Login(TestSamples.exampleAdminLoginViewModel);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public async Task Logout_Should_Ok_IfNoErrors()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var adminServicesMock = new Mock<IAdminServices>();
            var cookieManagerMock = new Mock<ICookieManager>();

            var exampleAdminDto = new AdminDto()
            {
                Id = 1,
                UserName = "exampleAdminDto"
            };

            adminServicesMock.Setup(x => x.GetAdminAsync(
                TestSamples.exampleAdmin.UserName,
                TestSamples.exampleAdmin.Password)).ReturnsAsync(exampleAdminDto);

            tokenManagerMock.Setup(x => x.GenerateToken(TestSamples.exampleAdmin.UserName, TestSamples.exampleAdmin.Role.RoleName, TestSamples.exampleAdmin.Id.ToString())).Returns(TestSamples.exampleToken);

            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new IdentityController(
                adminServicesMock.Object,
                tokenManagerMock.Object,
                httpContextAccessorMock.Object,
                cookieManagerMock.Object);

            var actionResult = await sut.Logout();

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }
    }
}