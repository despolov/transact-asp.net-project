﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Web.Areas.Admin.Controllers;
using Transact.Web.Mapper.Contracts;

namespace Transact.Web.UnitTests.Areas.Admin
{
    [TestClass]
    public class AccountController_Should
    {
        [TestMethod]
        public async Task CreateAccount_Should_ReturnCorrectOk_IfNoErrors()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleAccountDto = new AccountDto()
            {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    ClientName = "ExampleClientName",
                    ClientId = 1,
                    NickName = "ExampleNickName"
            };

            accountServicesMock.Setup(x => x.AddAccountToClientAsync(It.IsAny<AccountDto>())).ReturnsAsync(exampleAccountDto);

            tokenManagerMock.Setup(x => x.GenerateToken(TestSamples.exampleAccount.AccountNumber, TestSamples.exampleAccount.NickName, TestSamples.exampleAccount.Id.ToString())).Returns(TestSamples.exampleToken);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new AccountController(
                accountServicesMock.Object,
                accMapperMock.Object,
                httpContextAccessorMock.Object);

            var actionResult = await sut.CreateAccount(TestSamples.exampleAccountViewModel);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }

        [TestMethod]
        public async Task CreateAccount_Should_ReturnViewResult_IfModelStateIsInvalid()
        {
            var tokenManagerMock = new Mock<ITokenManager>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accMapperMock = new Mock<IAccountViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var exampleAccountDto = new AccountDto()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                ClientName = "ExampleClientName",
                ClientId = 1,
                NickName = "ExampleNickName"
            };

            accountServicesMock.Setup(x => x.AddAccountToClientAsync(It.IsAny<AccountDto>())).ReturnsAsync(exampleAccountDto);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new AccountController(
                 accountServicesMock.Object,
                 accMapperMock.Object,
                 httpContextAccessorMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var result = await sut.CreateAccount(TestSamples.exampleAccountViewModel);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }
    }
}
