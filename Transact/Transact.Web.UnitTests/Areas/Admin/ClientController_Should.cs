﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Transact.Models.Dtos;
using Transact.Services.Contracts;
using Transact.Web.Areas.Admin.Controllers;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.Mapper.Contracts;

namespace Transact.Web.UnitTests.Areas.Admin
{
    [TestClass]
    public class ClientController_Should
    {
        [TestMethod]
        public async Task AddClient_Should_ReturnCorrectView_IfNoErrors()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleClientDto = new ClientDto()
            {
                Id = 1,
                Name = "exampleClientDto"
            };

            clientServicesMock.Setup(x => x.GetClientAsync(It.IsAny<string>())).ReturnsAsync(exampleClientDto);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            var actionResult = await sut.AddClient();

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));

            var viewResult = (ViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(AddClientsViewModel));
        }

        [TestMethod]
        public async Task AddClient_Should_ReturnViewResult_IfModelStateIsInvalid()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleClientDto = new ClientDto()
            {
                Id = 1,
                Name = "exampleClientDto"
            };

            clientServicesMock.Setup(x => x.GetClientAsync(It.IsAny<string>())).ReturnsAsync(exampleClientDto);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var result = await sut.AddClient();

            Assert.IsInstanceOfType(result, typeof(ViewResult));

            var viewResult = (ViewResult)result;

            Assert.IsInstanceOfType(viewResult.Model, typeof(AddClientsViewModel));
        }

        [TestMethod]
        public async Task GetFiveClientsById_Should_ReturnCorrectPartialView_IfNoErrors()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleClientDto = new ClientDto()
            {
                Id = 1,
                Name = "exampleClientDto"
            };

            clientServicesMock.Setup(x => x.GetClientAsync(It.IsAny<string>())).ReturnsAsync(exampleClientDto);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            var actionResult = await sut.GetFiveClientsById(1);

            Assert.IsInstanceOfType(actionResult, typeof(PartialViewResult));

            var viewResult = (PartialViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(ClientListViewModel));
        }

        [TestMethod]
        public async Task GetFiveClientsById_Should_ReturnViewResult_IfModelStateIsInvalid()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleClientDto = new ClientDto()
            {
                Id = 1,
                Name = "exampleClientDto"
            };

            clientServicesMock.Setup(x => x.GetClientAsync(It.IsAny<string>())).ReturnsAsync(exampleClientDto);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var result = await sut.GetFiveClientsById(1);

            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        [TestMethod]
        public async Task SaveClient_Should_ReturnCorrectOk_IfNoErrors()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleClientDto = new ClientDto()
            {
                Id = 1,
                Name = "exampleClientDto"
            };

            clientServicesMock.Setup(x => x.AddClientAsync(It.IsAny<ClientDto>())).ReturnsAsync(exampleClientDto);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            var actionResult = await sut.SaveClient(TestSamples.exampleManageClientsViewModel);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }

        [TestMethod]
        public async Task SaveClient_Should_ReturnViewResult_IfModelStateIsInvalid()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleClientDto = new ClientDto()
            {
                Id = 1,
                Name = "exampleClientDto"
            };

            clientServicesMock.Setup(x => x.GetClientAsync(It.IsAny<string>())).ReturnsAsync(exampleClientDto);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var result = await sut.SaveClient(TestSamples.exampleManageClientsViewModel);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public async Task AssignUserToClient_Should_ReturnCorrectOk_IfNoErrors()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                UserName = "exampleUserDto"
            };

            clientServicesMock.Setup(x => x.AssignUserToClientAsync(It.IsAny<string>(), It.IsAny<long>())).ReturnsAsync(exampleUserDto);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            var actionResult = await sut.AssignUserToClient(TestSamples.exampleUser.UserName, TestSamples.exampleClient.Id);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }

        [TestMethod]
        public async Task RemoveUserForClient_Should_ReturnCorrectOk_IfNoErrors()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleUserDto = new UserDto()
            {
                Id = 1,
                UserName = "exampleUserDto"
            };

            userServicesMock.Setup(x => x.RemoveUserForClient(It.IsAny<long>(), It.IsAny<long>())).ReturnsAsync(exampleUserDto);

            httpContextAccessorMock.Setup(X => X.HttpContext.Response.Cookies.Append("SecurityToken", TestSamples.exampleToken));

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            var actionResult = await sut.RemoveUserForClient(TestSamples.exampleClient.Id, TestSamples.exampleUser.Id);

            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
        }

        [TestMethod]
        public async Task GetFiveAccountsForClientById_Should_ReturnCorrectPartial_IfNoErrors()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleAccountViewModelList = new List<AccountViewModel>()
            {
                new AccountViewModel
                {
                    Id = 1
                },
                new AccountViewModel
                {
                    Id = 2
                },
            };

            var exampleAccountDtoList = new List<AccountDto>()
            {
                new AccountDto
                {
                    Id = 1
                },
                new AccountDto
                {
                    Id = 2
                },
            };

            accountServicesMock.Setup(x => x.GetPageCountForAccountsAsync(5, TestSamples.exampleClient.Id)).ReturnsAsync(1);

            accountServicesMock.Setup(x => x.GetFiveAccountsForClientByIdAsync(1, TestSamples.exampleClient.Id)).ReturnsAsync(exampleAccountDtoList);

            accountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleAccountViewModelList);

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            var actionResult = await sut.GetFiveAccountsForClientById(1, TestSamples.exampleClient.Id);

            Assert.IsInstanceOfType(actionResult, typeof(PartialViewResult));

            var viewResult = (PartialViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(FiveAccountsViewModel));
        }

        [TestMethod]
        public async Task GetFiveAccountsForClientById_Should_ReturnCorrectPartial_IfModelStateIsInvalid()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleAccountViewModelList = new List<AccountViewModel>()
            {
                new AccountViewModel
                {
                    Id = 1
                },
                new AccountViewModel
                {
                    Id = 2
                },
            };

            var exampleAccountDtoList = new List<AccountDto>()
            {
                new AccountDto
                {
                    Id = 1
                },
                new AccountDto
                {
                    Id = 2
                },
            };

            accountServicesMock.Setup(x => x.GetPageCountForAccountsAsync(5, TestSamples.exampleClient.Id)).ReturnsAsync(1);

            accountServicesMock.Setup(x => x.GetFiveAccountsForClientByIdAsync(1, TestSamples.exampleClient.Id)).ReturnsAsync(exampleAccountDtoList);

            accountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleAccountViewModelList);

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var actionResult = await sut.GetFiveAccountsForClientById(1, TestSamples.exampleClient.Id);

            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        [TestMethod]
        public async Task GetFiveUsersForClientById_Should_ReturnCorrectPartial_IfNoErrors()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleUserViewModelList = new List<UserViewModel>()
            {
                new UserViewModel
                {
                    Id = 1
                },
                new UserViewModel
                {
                    Id = 2
                },
            };

            var exampleUserDtoList = new List<UserDto>()
            {
                new UserDto
                {
                    Id = 1
                },
                new UserDto
                {
                    Id = 2
                },
            };

            userServicesMock.Setup(x => x.GetPageCountForUsersWithClientIdAsync(5, TestSamples.exampleClient.Id)).ReturnsAsync(1);

            userServicesMock.Setup(x => x.GetFiveUsersForClientByIdAsync(1, TestSamples.exampleClient.Id)).ReturnsAsync(exampleUserDtoList);

            usermapperMock.Setup(x => x.MapFrom(It.IsAny<List<UserDto>>())).Returns(exampleUserViewModelList);

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            var actionResult = await sut.GetFiveUsersForClientById(1, TestSamples.exampleClient.Id);

            Assert.IsInstanceOfType(actionResult, typeof(PartialViewResult));

            var viewResult = (PartialViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(FiveUsersViewModel));
        }

        [TestMethod]
        public async Task GetFiveUsersForClientById_Should_ReturnCorrectPartial_IfModelStateIsInvalid()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleUserViewModelList = new List<UserViewModel>()
            {
                new UserViewModel
                {
                    Id = 1
                },
                new UserViewModel
                {
                    Id = 2
                },
            };

            var exampleUserDtoList = new List<UserDto>()
            {
                new UserDto
                {
                    Id = 1
                },
                new UserDto
                {
                    Id = 2
                },
            };

            userServicesMock.Setup(x => x.GetPageCountForUsersWithClientIdAsync(5, TestSamples.exampleClient.Id)).ReturnsAsync(1);

            userServicesMock.Setup(x => x.GetFiveUsersForClientByIdAsync(1, TestSamples.exampleClient.Id)).ReturnsAsync(exampleUserDtoList);

            usermapperMock.Setup(x => x.MapFrom(It.IsAny<List<UserDto>>())).Returns(exampleUserViewModelList);

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            sut.ModelState.AddModelError("test", "test");

            var actionResult = await sut.GetFiveUsersForClientById(1, TestSamples.exampleClient.Id);

            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        [TestMethod]
        public async Task ManageClient_Should_ReturnCorrectView_IfNoErrors()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleClientDto = new ClientDto()
            {
                Id = 1,
                Name = "ExampleClient1"
            };

            var exampleAccountsList = new List<AccountDto>()
            {
                new AccountDto
                {
                    Id = 1
                },
                new AccountDto
                {
                    Id = 2
                },
            };

            var exampleAccountViewModelList = new List<AccountViewModel>()
            {
                new AccountViewModel
                {
                    Id = 1
                },
                new AccountViewModel
                {
                    Id = 2
                },
            };

            var exampleClientViewModelList = new List<Web.Areas.Admin.ViewModels.ClientViewModel>()
            {
                new Web.Areas.Admin.ViewModels.ClientViewModel
                {
                    Id = 1
                },
                new Web.Areas.Admin.ViewModels.ClientViewModel
                {
                    Id = 2
                },
            };

            var exampleClientViewModel = new Web.Areas.Admin.ViewModels.ClientViewModel
            {
                Id = 1
            };

            var exampleClientDtoList = new List<ClientDto>()
            {
                new ClientDto
                {
                    Id = 1
                },
                new ClientDto
                {
                    Id = 2
                },
            };

           

            var fiveAcc = new FiveAccountsViewModel()
            {
                CurrPage = 1,
                TotalPages = 1,
                ClientId = TestSamples.exampleClient.Id,
                FiveAccountsList = exampleAccountViewModelList
            };

            var examplemanageClientViewModel = new ManageClientAccountsViewModel()
            {
                ClientId = TestSamples.exampleClient.Id,
                FiveAccountsList = fiveAcc
            };

            accountServicesMock.Setup(x => x.GetFiveAccountsForClientByIdAsync(It.IsAny<int>(), It.IsAny<long>())).ReturnsAsync(exampleAccountsList);

            accountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleAccountViewModelList);

            clientServicesMock.Setup(x => x.GetClientByIdAsync(It.IsAny<long>())).ReturnsAsync(exampleClientDto);

            clientmapperMock.Setup(x => x.MapFrom(It.IsAny<ClientDto>())).Returns(exampleClientViewModel);

            accountServicesMock.Setup(x => x.GetPageCountForAccountsAsync(5, TestSamples.exampleClient.Id)).ReturnsAsync(1);

            configurationMock.Setup(x => x.GetSection("DefaultAccountAmount").Value).Returns("10000");

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            var actionResult = await sut.ManageClient(TestSamples.exampleClient.Id);

            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));

            var viewResult = (ViewResult)actionResult;  //TODO: moje bi ne taka 

            Assert.IsInstanceOfType(viewResult.Model, typeof(ManageClientViewModel));
        }

        [TestMethod]
        public async Task ManageUsersForClient_Should_ReturnCorrectView_IfNoErrors()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleUsersList = new List<UserDto>()
            {
                new UserDto
                {
                    Id = 1
                },
                new UserDto
                {
                    Id = 2
                },
            };

            var exampleUserViewModelList = new List<UserViewModel>()
            {
                new UserViewModel
                {
                    Id = 1
                },
                new UserViewModel
                {
                    Id = 2
                },
            };

            var fiveUsers = new FiveUsersViewModel()
            {
                CurrPage = 1,
                TotalPages = 1,
                FiveUsersList = exampleUserViewModelList,
                ClientId = TestSamples.exampleClient.Id
            };

            var viewModel = new ManageClientUsersViewModel()
            {
                ClientId = TestSamples.exampleClient.Id,

                FiveUsersList = fiveUsers
            };

            userServicesMock.Setup(x => x.GetFiveUsersForClientByIdAsync(1, TestSamples.exampleClient.Id)).ReturnsAsync(exampleUsersList);

            userServicesMock.Setup(x => x.GetPageCountForUsersWithClientIdAsync(5, TestSamples.exampleClient.Id)).ReturnsAsync(1);

            usermapperMock.Setup(x => x.MapFrom(It.IsAny<List<UserDto>>())).Returns(exampleUserViewModelList);

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            var actionResult = await sut.ManageUsersForClient(TestSamples.exampleClient.Id);

            Assert.IsInstanceOfType(actionResult, typeof(PartialViewResult));

            var viewResult = (PartialViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(ManageClientUsersViewModel));
        }

        [TestMethod]
        public async Task ManageAccountsForClient_Should_ReturnCorrectView_IfNoErrors()
        {
            var clientServicesMock = new Mock<IClientServices>();
            var accountServicesMock = new Mock<IAccountServices>();
            var accountMapperMock = new Mock<IAccountViewModelMapper>();
            var clientmapperMock = new Mock<IClientViewModelMapper>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var userServicesMock = new Mock<IUserServices>();
            var usermapperMock = new Mock<IUserViewModelMapper>();
            var configurationMock = new Mock<IConfiguration>();

            var exampleAccountsList = new List<AccountDto>()
            {
                new AccountDto
                {
                    Id = 1
                },
                new AccountDto
                {
                    Id = 2
                },
            };

            var exampleAccountViewModelList = new List<AccountViewModel>()
            {
                new AccountViewModel
                {
                    Id = 1
                },
                new AccountViewModel
                {
                    Id = 2
                },
            };

            var fiveAccounts = new FiveAccountsViewModel()
            {
                CurrPage = 1,
                TotalPages = 1,
                FiveAccountsList = exampleAccountViewModelList,
                ClientId = TestSamples.exampleClient.Id
            };

            var viewModel = new ManageClientAccountsViewModel()
            {
                ClientId = TestSamples.exampleClient.Id,

                FiveAccountsList = fiveAccounts
            };

            accountServicesMock.Setup(x => x.GetFiveAccountsForClientByIdAsync(1, TestSamples.exampleClient.Id)).ReturnsAsync(exampleAccountsList);

            accountServicesMock.Setup(x => x.GetPageCountForAccountsAsync(5, TestSamples.exampleClient.Id)).ReturnsAsync(1);

            accountMapperMock.Setup(x => x.MapFrom(It.IsAny<List<AccountDto>>())).Returns(exampleAccountViewModelList);

            configurationMock.Setup(x => x.GetSection("DefaultAccountAmount").Value).Returns("10000");

            var sut = new ClientController(
                clientServicesMock.Object,
                accountServicesMock.Object,
                accountMapperMock.Object,
                clientmapperMock.Object,
                httpContextAccessorMock.Object,
                userServicesMock.Object,
                usermapperMock.Object,
                configurationMock.Object);

            var actionResult = await sut.ManageAccountsForClient(TestSamples.exampleClient.Id);

            Assert.IsInstanceOfType(actionResult, typeof(PartialViewResult));

            var viewResult = (PartialViewResult)actionResult;

            Assert.IsInstanceOfType(viewResult.Model, typeof(ManageClientAccountsViewModel));
        }
    }
}