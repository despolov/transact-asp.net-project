﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Web.Areas.AppUser.ViewModels;
using Transact.Web.Mapper;

namespace Transact.Web.UnitTests
{
    [TestClass]
    public class TransactionViewModelMapper_Should
    {
        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_TransactionDto_To_TransactionViewModel()
        {
            var sut = new TransactionViewModelMapper();

            var exampleAccount = new Account()
            {
                Id = 1,
                AccountNumber = "1233367890",
                Balance = 10000,
                ClientId = 1,
                NickName = "ExampleNickName"
            };

            var exampleAccount2 = new Account()
            {
                Id = 2,
                AccountNumber = "2234567890",
                Balance = 12000,
                ClientId = 1,
                NickName = "ExampleNickName2"
            };

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Amount = 100,
                SenderAccountNumber = "2234567890",
                SenderAccountNickName = "ExampleNickName",
                SenderClientName = "ExampleSenderClient",
                ReceiverAccountNumber = "1233367890",
                ReceiverAccountNickName = "Example",
                ReceiverClientName = "ExampleReceiverClient",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                Description = "example Description",
                TimeStamp = new DateTime(2016, 7, 15),
                StatusName = "Saved",
            };

            var result = sut.MapFrom(exampleTransactionDto);

            Assert.IsInstanceOfType(result, typeof(TransactionViewModel));
            Assert.AreEqual(result.Id, exampleTransactionDto.Id);
            Assert.AreEqual(result.Amount, exampleTransactionDto.Amount);
            Assert.AreEqual(result.SenderAccountId, exampleTransactionDto.SenderAccountId);
            Assert.AreEqual(result.ReceiverAccountId, exampleTransactionDto.ReceiverAccountId);
            Assert.AreEqual(result.Description, exampleTransactionDto.Description);
            Assert.AreEqual(result.TimeStamp, exampleTransactionDto.TimeStamp);
            Assert.AreEqual(result.ReceiverAccountNumber, exampleAccount.AccountNumber);           
            Assert.AreEqual(result.SenderAccountNumber, exampleAccount2.AccountNumber);
            Assert.AreEqual(result.SenderAccountNickName, exampleAccount.NickName);
            Assert.AreEqual(result.ReceiverClientName, exampleTransactionDto.ReceiverClientName);
            Assert.AreEqual(result.ReceiverAccountNickName, exampleTransactionDto.ReceiverAccountNickName);
        }

        [TestMethod]
        public void MapFromWithAccount_Should_CorrectlyMapOutgoing_TransactionDto_To_TransactionViewModel_IfAccountIdNOTNull()
        {
            var sut = new TransactionViewModelMapper();

            var exampleAccount = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                ClientId = 1,
                NickName = "ExampleNickName"
            };

            var exampleAccount2 = new Account()
            {
                Id = 2,
                AccountNumber = "2234567890",
                Balance = 12000,
                ClientId = 1,
                NickName = "ExampleNickName2"
            };

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Amount = 100,
                SenderAccountNumber = "1234567890",
                SenderAccountNickName = "ExampleNickName",
                SenderClientName = "ExampleSenderClient",
                ReceiverAccountNumber = "2234567890",
                ReceiverClientName = "ExampleReceiverClient",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                Description = "example Description",
                TimeStamp = new DateTime(2016, 7, 15),
                StatusName = "Saved",
            };

            var result = sut.MapFromWithAccount(exampleTransactionDto, 1);

            Assert.IsInstanceOfType(result, typeof(TransactionViewModel));
            Assert.AreEqual(result.Id, exampleTransactionDto.Id);
            Assert.AreEqual(result.Amount, exampleTransactionDto.Amount);
            Assert.AreEqual(result.SenderAccountId, exampleTransactionDto.SenderAccountId);
            Assert.AreEqual(result.ReceiverAccountId, exampleTransactionDto.ReceiverAccountId);
            Assert.AreEqual(result.Description, exampleTransactionDto.Description);
            Assert.AreEqual(result.TimeStamp, exampleTransactionDto.TimeStamp);
            Assert.AreEqual(result.ReceiverAccountNumber, exampleAccount2.AccountNumber);
            Assert.AreEqual(result.SenderAccountNumber, exampleAccount.AccountNumber);
            Assert.AreEqual(result.SenderAccountNickName, exampleAccount.NickName);
            Assert.AreEqual(result.IOstatus, Utils.TransactionIOStatus.Outgoing);

        }

        [TestMethod]
        public void MapFromWithAccount_Should_CorrectlyMapIncoming_TransactionDto_To_TransactionViewModel_IfAccountIdNOTNull()
        {
            var sut = new TransactionViewModelMapper();

            var exampleAccount = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                ClientId = 1,
                NickName = "ExampleNickName"
            };

            var exampleAccount2 = new Account()
            {
                Id = 2,
                AccountNumber = "2234567890",
                Balance = 12000,
                ClientId = 1,
                NickName = "ExampleNickName2"
            };

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Amount = 100,
                SenderAccountNumber = "1234567890",
                SenderAccountNickName = "ExampleNickName",
                SenderClientName = "ExampleSenderClient",
                ReceiverAccountNumber = "2234567890",
                ReceiverClientName = "ExampleReceiverClient",
                SenderAccountId = 2,
                ReceiverAccountId = 1,
                Description = "example Description",
                TimeStamp = new DateTime(2016, 7, 15),
                StatusName = "Saved",
            };

            var result = sut.MapFromWithAccount(exampleTransactionDto, 1);


            Assert.IsInstanceOfType(result, typeof(TransactionViewModel));
            Assert.AreEqual(result.Id, exampleTransactionDto.Id);
            Assert.AreEqual(result.Amount, exampleTransactionDto.Amount);
            Assert.AreEqual(result.SenderAccountId, exampleTransactionDto.SenderAccountId);
            Assert.AreEqual(result.ReceiverAccountId, exampleTransactionDto.ReceiverAccountId);
            Assert.AreEqual(result.Description, exampleTransactionDto.Description);
            Assert.AreEqual(result.TimeStamp, exampleTransactionDto.TimeStamp);
            Assert.AreEqual(result.ReceiverAccountNumber, exampleAccount2.AccountNumber);
            Assert.AreEqual(result.SenderAccountNumber, exampleAccount.AccountNumber);
            Assert.AreEqual(result.SenderAccountNickName, exampleAccount.NickName);
            Assert.AreEqual(result.IOstatus, Utils.TransactionIOStatus.Incoming);
        }

        [TestMethod]
        public void MapFromWithAccount_Should_CorrectlyMap_TransactionDto_To_TransactionViewModel_IfAccountIdNull()
        {
            var sut = new TransactionViewModelMapper();

            var exampleAccount = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                ClientId = 1,
                NickName = "ExampleNickName"
            };

            var exampleAccount2 = new Account()
            {
                Id = 2,
                AccountNumber = "2234567890",
                Balance = 12000,
                ClientId = 1,
                NickName = "ExampleNickName2"
            };

            var exampleTransactionDto = new TransactionDto()
            {
                Id = 1,
                Amount = 100,
                SenderAccountNumber = "1234567890",
                SenderAccountNickName = "ExampleNickName",
                SenderClientName = "ExampleSenderClient",
                ReceiverAccountNumber = "2234567890",
                ReceiverClientName = "ExampleReceiverClient",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                Description = "example Description",
                TimeStamp = new DateTime(2016, 7, 15),
                StatusName = "Saved",
            };

            var result = sut.MapFromWithAccount(exampleTransactionDto, null);

            Assert.IsInstanceOfType(result, typeof(TransactionViewModel));
            Assert.AreEqual(result.Id, exampleTransactionDto.Id);
            Assert.AreEqual(result.Amount, exampleTransactionDto.Amount);
            Assert.AreEqual(result.SenderAccountId, exampleTransactionDto.SenderAccountId);
            Assert.AreEqual(result.ReceiverAccountId, exampleTransactionDto.ReceiverAccountId);
            Assert.AreEqual(result.Description, exampleTransactionDto.Description);
            Assert.AreEqual(result.TimeStamp, exampleTransactionDto.TimeStamp);
            Assert.AreEqual(result.ReceiverAccountNumber, exampleAccount2.AccountNumber);
            Assert.AreEqual(result.SenderAccountNumber, exampleAccount.AccountNumber);
            Assert.AreEqual(result.SenderAccountNickName, exampleAccount.NickName);
            Assert.AreEqual(result.IOstatus, Utils.TransactionIOStatus.IncomingAndOutgoing);

        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_TransactionViewModel_To_TransactionDto()
        {
            var sut = new TransactionViewModelMapper();

            var exampleTransactionViewModel = new TransactionViewModel()
            {
                Id = 1,
                Amount = 100,
                SenderAccountNumber = "1234567890",
                SenderAccountNickName = "ExampleNickName",
                SenderClientName = "ExampleSenderClient",
                ReceiverAccountNumber = "1233367890",
                ReceiverClientName = "ExampleReceiverClient",
                SenderAccountId = 1,
                ReceiverAccountId = 2,
                Description = "example Description",
                TimeStamp = new DateTime(2016, 7, 15),
                StatusName = "Saved"            
            };

            var result = sut.MapFrom(exampleTransactionViewModel);

            Assert.IsInstanceOfType(result, typeof(TransactionDto));
            Assert.AreEqual(result.Id, exampleTransactionViewModel.Id);
            Assert.AreEqual(result.Amount, exampleTransactionViewModel.Amount);
            Assert.AreEqual(result.SenderAccountId, exampleTransactionViewModel.SenderAccountId);
            Assert.AreEqual(result.ReceiverAccountId, exampleTransactionViewModel.ReceiverAccountId);
            Assert.AreEqual(result.ReceiverAccountNumber, exampleTransactionViewModel.ReceiverAccountNumber);
            Assert.AreEqual(result.Description, exampleTransactionViewModel.Description);
            Assert.AreEqual(result.TimeStamp, exampleTransactionViewModel.TimeStamp);           
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfTransactionDtos_To_ListOfTransactionViwModels()
        {
            var exampleTransactionDtoList = new List<TransactionDto>()
            {
                new TransactionDto()
                {                      
                    Id = 1,
                    Amount = 100,
                    SenderAccountNumber = "1234567890",
                    SenderAccountNickName = "ExampleNickName",
                    SenderClientName = "ExampleSenderClient",
                    ReceiverAccountNumber = "1233367890",
                    ReceiverClientName = "ExampleReceiverClient",
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusName = "Saved"
                },
                new TransactionDto()
                {
                    Id = 2,
                    Amount = 200,
                    SenderAccountNumber = "1234567890",
                    SenderAccountNickName = "ExampleNickName",
                    SenderClientName = "ExampleSenderClient",
                    ReceiverAccountNumber = "1233367890",
                    ReceiverClientName = "ExampleReceiverClient",
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 4, 15),
                    StatusName = "Saved"                    
                }
            };

            var sut = new TransactionViewModelMapper();

            var result = sut.MapFrom(exampleTransactionDtoList);

            Assert.IsInstanceOfType(result, typeof(List<TransactionViewModel>));
            Assert.AreEqual(result.Count, 2);

            Assert.AreEqual(result[0].Id, exampleTransactionDtoList[0].Id);
            Assert.AreEqual(result[0].Amount, exampleTransactionDtoList[0].Amount);
            Assert.AreEqual(result[0].SenderAccountId, exampleTransactionDtoList[0].SenderAccountId);
            Assert.AreEqual(result[0].ReceiverAccountId, exampleTransactionDtoList[0].ReceiverAccountId);
            Assert.AreEqual(result[0].Description, exampleTransactionDtoList[0].Description);
            Assert.AreEqual(result[0].TimeStamp, exampleTransactionDtoList[0].TimeStamp);
            Assert.AreEqual(result[0].SenderAccountNumber, exampleTransactionDtoList[0].SenderAccountNumber);
            Assert.AreEqual(result[0].SenderAccountNickName, exampleTransactionDtoList[0].SenderAccountNickName);
            Assert.AreEqual(result[0].ReceiverAccountNumber, exampleTransactionDtoList[0].ReceiverAccountNumber);
            Assert.AreEqual(result[0].StatusName, exampleTransactionDtoList[0].StatusName);
            Assert.AreEqual(result[1].Id, exampleTransactionDtoList[1].Id);
            Assert.AreEqual(result[1].Amount, exampleTransactionDtoList[1].Amount);
            Assert.AreEqual(result[1].SenderAccountId, exampleTransactionDtoList[1].SenderAccountId);
            Assert.AreEqual(result[1].ReceiverAccountId, exampleTransactionDtoList[1].ReceiverAccountId);
            Assert.AreEqual(result[1].Description, exampleTransactionDtoList[1].Description);
            Assert.AreEqual(result[1].TimeStamp, exampleTransactionDtoList[1].TimeStamp);
            Assert.AreEqual(result[1].SenderAccountNumber, exampleTransactionDtoList[1].SenderAccountNumber);
            Assert.AreEqual(result[1].SenderAccountNickName, exampleTransactionDtoList[1].SenderAccountNickName);
            Assert.AreEqual(result[1].ReceiverAccountNumber, exampleTransactionDtoList[1].ReceiverAccountNumber);
            Assert.AreEqual(result[1].StatusName, exampleTransactionDtoList[1].StatusName);

        }

        [TestMethod]
        public void MapFromWithAccount_Should_CorrectlyMap_CollectionOfTransactionDtos_To_ListOfTransactionViwModels_IfAccountIdNOTNull()
        {
            var exampleTransactionDtoList = new List<TransactionDto>()
            {
               new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountNumber = "1234567890",
                    SenderAccountNickName = "ExampleNickName",
                    SenderClientName = "ExampleSenderClient",
                    ReceiverAccountNumber = "2345678901",
                    ReceiverClientName = "ExampleReceiverClient",
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusName = "Saved"
                },
                new TransactionDto()
                {
                    Id = 2,
                    Amount = 200,
                    SenderAccountNumber = "2345678901",
                    SenderAccountNickName = "ExampleNickName",
                    SenderClientName = "ExampleSenderClient",
                    ReceiverAccountNumber = "1234567890",
                    ReceiverClientName = "ExampleReceiverClient",
                    SenderAccountId = 2,
                    ReceiverAccountId = 1,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 4, 15),
                    StatusName = "Saved"
                }
            };

            var sut = new TransactionViewModelMapper();

            var result = sut.MapFromWithAccount(exampleTransactionDtoList, 1);

            Assert.IsInstanceOfType(result, typeof(List<TransactionViewModel>));
            Assert.AreEqual(result.Count, 2);

            Assert.AreEqual(result[0].Id, exampleTransactionDtoList[0].Id);
            Assert.AreEqual(result[0].Amount, exampleTransactionDtoList[0].Amount);
            Assert.AreEqual(result[0].SenderAccountId, exampleTransactionDtoList[0].SenderAccountId);
            Assert.AreEqual(result[0].ReceiverAccountId, exampleTransactionDtoList[0].ReceiverAccountId);
            Assert.AreEqual(result[0].Description, exampleTransactionDtoList[0].Description);
            Assert.AreEqual(result[0].TimeStamp, exampleTransactionDtoList[0].TimeStamp);
            Assert.AreEqual(result[0].SenderAccountNumber, exampleTransactionDtoList[0].SenderAccountNumber);
            Assert.AreEqual(result[0].SenderAccountNickName, exampleTransactionDtoList[0].SenderAccountNickName);
            Assert.AreEqual(result[0].ReceiverAccountNumber, exampleTransactionDtoList[0].ReceiverAccountNumber);
            Assert.AreEqual(result[0].StatusName, exampleTransactionDtoList[0].StatusName);
            Assert.AreEqual(result[0].IOstatus, Utils.TransactionIOStatus.Outgoing);

            Assert.AreEqual(result[1].Id, exampleTransactionDtoList[1].Id);
            Assert.AreEqual(result[1].Amount, exampleTransactionDtoList[1].Amount);
            Assert.AreEqual(result[1].SenderAccountId, exampleTransactionDtoList[1].SenderAccountId);
            Assert.AreEqual(result[1].ReceiverAccountId, exampleTransactionDtoList[1].ReceiverAccountId);
            Assert.AreEqual(result[1].Description, exampleTransactionDtoList[1].Description);
            Assert.AreEqual(result[1].TimeStamp, exampleTransactionDtoList[1].TimeStamp);
            Assert.AreEqual(result[1].SenderAccountNumber, exampleTransactionDtoList[1].SenderAccountNumber);
            Assert.AreEqual(result[1].SenderAccountNickName, exampleTransactionDtoList[1].SenderAccountNickName);
            Assert.AreEqual(result[1].ReceiverAccountNumber, exampleTransactionDtoList[1].ReceiverAccountNumber);
            Assert.AreEqual(result[1].StatusName, exampleTransactionDtoList[1].StatusName);
            Assert.AreEqual(result[1].IOstatus, Utils.TransactionIOStatus.Incoming);            
        }

        [TestMethod]
        public void MapFromWithAccount_Should_CorrectlyMap_CollectionOfTransactionDtos_To_ListOfTransactionViwModels_IfAccountIdNull()
        {
            var exampleTransactionDtoList = new List<TransactionDto>()
            {
                new TransactionDto()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountNumber = "1234567890",
                    SenderAccountNickName = "ExampleNickName",
                    SenderClientName = "ExampleSenderClient",
                    ReceiverAccountNumber = "1233367890",
                    ReceiverClientName = "ExampleReceiverClient",
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusName = "Saved"
                },
                new TransactionDto()
                {
                    Id = 2,
                    Amount = 200,
                    SenderAccountNumber = "1234567890",
                    SenderAccountNickName = "ExampleNickName",
                    SenderClientName = "ExampleSenderClient",
                    ReceiverAccountNumber = "1233367890",
                    ReceiverClientName = "ExampleReceiverClient",
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 4, 15),
                    StatusName = "Saved"
                }
            };

            var sut = new TransactionViewModelMapper();

            var result = sut.MapFromWithAccount(exampleTransactionDtoList, null);

            Assert.IsInstanceOfType(result, typeof(List<TransactionViewModel>));
            Assert.AreEqual(result.Count, 2);

            Assert.AreEqual(result[0].Id, exampleTransactionDtoList[0].Id);
            Assert.AreEqual(result[0].Amount, exampleTransactionDtoList[0].Amount);
            Assert.AreEqual(result[0].SenderAccountId, exampleTransactionDtoList[0].SenderAccountId);
            Assert.AreEqual(result[0].ReceiverAccountId, exampleTransactionDtoList[0].ReceiverAccountId);
            Assert.AreEqual(result[0].Description, exampleTransactionDtoList[0].Description);
            Assert.AreEqual(result[0].TimeStamp, exampleTransactionDtoList[0].TimeStamp);
            Assert.AreEqual(result[0].SenderAccountNumber, exampleTransactionDtoList[0].SenderAccountNumber);
            Assert.AreEqual(result[0].SenderAccountNickName, exampleTransactionDtoList[0].SenderAccountNickName);
            Assert.AreEqual(result[0].ReceiverAccountNumber, exampleTransactionDtoList[0].ReceiverAccountNumber);
            Assert.AreEqual(result[0].StatusName, exampleTransactionDtoList[0].StatusName);
            Assert.AreEqual(result[0].IOstatus, Utils.TransactionIOStatus.IncomingAndOutgoing);

            Assert.AreEqual(result[1].Id, exampleTransactionDtoList[1].Id);
            Assert.AreEqual(result[1].Amount, exampleTransactionDtoList[1].Amount);
            Assert.AreEqual(result[1].SenderAccountId, exampleTransactionDtoList[1].SenderAccountId);
            Assert.AreEqual(result[1].ReceiverAccountId, exampleTransactionDtoList[1].ReceiverAccountId);
            Assert.AreEqual(result[1].Description, exampleTransactionDtoList[1].Description);
            Assert.AreEqual(result[1].TimeStamp, exampleTransactionDtoList[1].TimeStamp);
            Assert.AreEqual(result[1].SenderAccountNumber, exampleTransactionDtoList[1].SenderAccountNumber);
            Assert.AreEqual(result[1].SenderAccountNickName, exampleTransactionDtoList[1].SenderAccountNickName);
            Assert.AreEqual(result[1].ReceiverAccountNumber, exampleTransactionDtoList[1].ReceiverAccountNumber);
            Assert.AreEqual(result[1].StatusName, exampleTransactionDtoList[1].StatusName);
            Assert.AreEqual(result[0].IOstatus, Utils.TransactionIOStatus.IncomingAndOutgoing);
        }

        [TestMethod]
        public void MapFrom_Should_CorrectlyMap_CollectionOfTransactionViewModels_To_ListOfTransactionDtos()
        {
            var exampleTransactionViewModelList = new List<TransactionViewModel>()
            {
                new TransactionViewModel()
                {
                    Id = 1,
                    Amount = 100,
                    SenderAccountNumber = "1234567890",
                    SenderAccountNickName = "ExampleNickName",
                    SenderClientName = "ExampleSenderClient",
                    ReceiverAccountNumber = "1233367890",
                    ReceiverClientName = "ExampleReceiverClient",
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 7, 15),
                    StatusName = "Saved"
                },
                new TransactionViewModel()
                {
                    Id = 2,
                    Amount = 200,
                    SenderAccountNumber = "1234567890",
                    SenderAccountNickName = "ExampleNickName",
                    SenderClientName = "ExampleSenderClient",
                    ReceiverAccountNumber = "1233367890",
                    ReceiverClientName = "ExampleReceiverClient",
                    SenderAccountId = 1,
                    ReceiverAccountId = 2,
                    Description = "example Description",
                    TimeStamp = new DateTime(2016, 4, 15),
                    StatusName = "Saved"
                }
            };

            var sut = new TransactionViewModelMapper();

            var result = sut.MapFrom(exampleTransactionViewModelList);

            Assert.IsInstanceOfType(result, typeof(List<TransactionDto>));
            Assert.AreEqual(result.Count, 2);

            Assert.AreEqual(result[0].Id, exampleTransactionViewModelList[0].Id);
            Assert.AreEqual(result[0].Amount, exampleTransactionViewModelList[0].Amount);
            Assert.AreEqual(result[0].SenderAccountId, exampleTransactionViewModelList[0].SenderAccountId);
            Assert.AreEqual(result[0].ReceiverAccountId, exampleTransactionViewModelList[0].ReceiverAccountId);
            Assert.AreEqual(result[0].Description, exampleTransactionViewModelList[0].Description);
            Assert.AreEqual(result[0].TimeStamp, exampleTransactionViewModelList[0].TimeStamp);
            Assert.AreEqual(result[0].ReceiverAccountNumber, exampleTransactionViewModelList[0].ReceiverAccountNumber);

            Assert.AreEqual(result[1].Id, exampleTransactionViewModelList[1].Id);
            Assert.AreEqual(result[1].Amount, exampleTransactionViewModelList[1].Amount);
            Assert.AreEqual(result[1].SenderAccountId, exampleTransactionViewModelList[1].SenderAccountId);
            Assert.AreEqual(result[1].ReceiverAccountId, exampleTransactionViewModelList[1].ReceiverAccountId);
            Assert.AreEqual(result[1].Description, exampleTransactionViewModelList[1].Description);
            Assert.AreEqual(result[1].TimeStamp, exampleTransactionViewModelList[1].TimeStamp);
            Assert.AreEqual(result[1].ReceiverAccountNumber, exampleTransactionViewModelList[1].ReceiverAccountNumber);
        }
    }
}
