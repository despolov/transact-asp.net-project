﻿using System;
using System.Collections.Generic;
using System.Text;
using Transact.Models;
using Transact.Models.Dtos;
using Transact.Web.Areas.Admin.ViewModels;
using Transact.Web.Areas.AppUser.ViewModels;

namespace Transact.Web.UnitTests
{
    public class TestSamples
    {
        public static readonly string exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

        public static User exampleUser = new User()
        {
            Id = 1,
            Name = "ExampleUser1",
            UserName = "ExampleUserName1",
            Password = "ExamplePassword1",
            Role = new UserRole { RoleName = "User" },
            UsersAccounts = new List<UsersAccounts>(),
            UsersClients = new List<UsersClients>(),
        };

        public static Admin exampleAdmin = new Admin()
        {
            Id = 1,
            UserName = "ExampleUserName1",
            Password = "ExamplePassword1",
            Role = new UserRole { RoleName = "Admin" }
        };

        public static Client exampleClient = new Client()
        {
            Id = 1,
            Name = "ExampleClient1"
        };

        public static LoginViewModel exampleBrokenLoginViewModel = new LoginViewModel()
        {
            UserName = "1234567891011121314151617",
            Password = exampleUser.Password
        };

        public static LoginViewModel exampleLoginViewModel = new LoginViewModel()
        {
            UserName = exampleUser.UserName,
            Password = exampleUser.Password
        };

        public static AdminLoginViewModel exampleAdminLoginViewModel = new AdminLoginViewModel()
        {
            UserName = exampleUser.UserName,
            Password = exampleUser.Password
        };

        public static AddUserViewModel exampleManageusersViewModel = new AddUserViewModel()
        {
            UserForm = new UserViewModel()
            {
                Id = 1,
                UserName = exampleUser.UserName,
                Password = exampleUser.Password,
                Name = exampleUser.Name
            },
            UsersList = new UserListViewModel()
        };

        public static AddClientsViewModel exampleManageClientsViewModel = new AddClientsViewModel()
        {
            ClientForm = new Web.Areas.Admin.ViewModels.ClientViewModel()
            {
                Id = 1,
                Name = exampleClient.Name
            },
            ClientsList = new ClientListViewModel()
        };

        public static Account exampleAccount = new Account()
        {
            Id = 1,
            AccountNumber = "1234567890",
            Balance = 10000,
            ClientId = 1,
            NickName = "ExampleNickName"
        };

        public static AccountViewModel exampleAccountViewModel = new AccountViewModel()
        {
            Id = 2,
            AccountNumber = "1233367890",
            Balance = 10000,
            ClientId = 2,
            NickName = "ExampleNickName2"
        };

        //public static TransactionViewModel exampleTransactionViewModel = new TransactionViewModel()
        //{
        //    Id = 1,
        //    Amount = 100,
        //    Description = "ExampleDescription",
        //    SenderAccountId = 1,
        //    ReceiverAccountId = 2,
        //    ReceiverAccountNumber = "1234567890"
        //};

        public static Transaction exampleTransaction = new Transaction()
        {
            Id = 1,
            Amount = 100,
            SenderAccountId = 1,
            ReceiverAccountId = 2,
            Description = "example Description",
            TimeStamp = new DateTime(2016, 7, 15)
        };

        //public static TransactionDto exampleTransactionDto = new TransactionDto()
        //{
        //    Id = 1,
        //    Amount = 100
        //};


        //public static UserDto exampleUserDto = new UserDto()
        //{
        //    Id = 1,
        //    UserName = "exampleUserDto"
        //};

        //public static UserDto exampleFullUserDto = new UserDto()
        //{
        //    Id = 1,
        //    Name = "exampleUserDto",
        //    UserName = "exampleUserNameDto"
        //};

        public static List<UserDto> exampleUserDtoList = new List<UserDto>()
        {
            new UserDto
            {
                 Id = 1,
                 Name = "exampleUserDto",
                 UserName = "exampleUserNameDto"
            },
            new UserDto
            {
                 Id = 2,
                 Name = "exampleUserDto2",
                 UserName = "exampleUserNameDto2"
            }
        };
    }
}
