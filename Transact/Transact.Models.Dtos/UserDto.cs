﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transact.Models.Dtos
{
    public class UserDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        public string UserName { get; set; }

        public string RoleName { get; set; }

        public long RoleId { get; set; }
    }
}
