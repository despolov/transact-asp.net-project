﻿using System;

namespace Transact.Models.Dtos
{
    public class TransactionDto
    {
        public long Id { get; set; }

        public long SenderAccountId { get; set; }

        public long ReceiverAccountId { get; set; }

        public string Description { get; set; }

        public decimal Amount { get; set; }

        public DateTime TimeStamp { get; set; }

        public string StatusName { get; set; }

        public long StatusId { get; set; }

        public string SenderAccountNumber { get; set; }

        public string SenderClientName { get; set; }

        public string SenderAccountNickName { get; set; }

        public string ReceiverAccountNumber { get; set; }

        public string ReceiverAccountNickName { get; set; }

        public string ReceiverClientName { get; set; }
    }
}
