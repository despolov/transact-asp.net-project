﻿using Microsoft.AspNetCore.Http;
using System;

namespace Transact.Models.Dtos
{
    public class BannerDto
    {
        public long Id { get; set; }

        public string ImageName { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Url { get; set; }

        public IFormFile Image { get; set; }
    }
}
