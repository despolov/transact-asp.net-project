namespace Transact.Models.Dtos
{
    public class AccountDto
    {
        public long Id { get; set; }

        public string AccountNumber { get; set; }

        public string NickName { get; set; }

        public decimal Balance { get; set; }

        public long ClientId { get; set; }

        public string ClientName { get; set; }
    }
}
