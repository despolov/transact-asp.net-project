﻿namespace Transact.Models.Dtos
{
    public class ClientDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public int NumberOfAccounts { get; set; }
    }
}
