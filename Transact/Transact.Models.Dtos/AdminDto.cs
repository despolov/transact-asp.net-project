﻿namespace Transact.Models.Dtos
{
    public class AdminDto
    {
        public long Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string RoleName { get; set; }

        public long RoleId { get; set; }
    }
}
