﻿using System.ComponentModel.DataAnnotations;

namespace Transact.Models
{
    public class Admin
    {
        public long Id { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(16)]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        public long RoleId { get; set; }
        public UserRole Role { get; set; }
    }
}
