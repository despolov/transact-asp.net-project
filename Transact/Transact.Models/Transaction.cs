﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Transact.Models
{
    public class Transaction
    {
        public long Id { get; set; }

        public long SenderAccountId { get; set; }

        [ForeignKey("SenderAccountId")]
        [InverseProperty("SenderAccounts")]
        public Account SenderAccount { get; set; }

        public long ReceiverAccountId { get; set; }
        [ForeignKey("ReceiverAccountId")]
        [InverseProperty("ReceiverAccounts")]
        public Account ReceiverAccount { get; set; }

        public string Description { get; set; }

        [Range(0, Double.PositiveInfinity)]
        [Column(TypeName = "decimal(18,2)")]
        public decimal Amount { get; set; }


        public DateTime TimeStamp { get; set; }

        public long StatusId { get; set; }
        public Status Status { get; set; }
    }
}
