﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Transact.Models
{
    public class Banner
    {
        public long Id { get; set; }

        [Required]
        public string ImageName { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        public string Url { get; set; }
    }
}
