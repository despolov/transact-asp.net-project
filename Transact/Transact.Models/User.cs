﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Transact.Models
{
    public class User
    {
        public long Id { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(35)]
        public string Name { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(16)]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
        
        public long RoleId { get; set; }
        public UserRole Role { get; set; }

        public ICollection<UsersAccounts> UsersAccounts { get; set; }
        public ICollection<UsersClients> UsersClients { get; set; }

    }
}
