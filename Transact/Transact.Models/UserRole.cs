﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Transact.Models
{
    public class UserRole
    {
        public long Id { get; set; }

        [Required]
        public string RoleName { get; set; }

        public ICollection<User> Users { get; set; }

        public ICollection<Admin> Admins { get; set; }
    }
}
