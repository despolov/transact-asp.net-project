﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transact.Models
{
    public class UsersAccounts
    {
        public long UserId { get; set; }
        public User User { get; set; }

        public string UserAccountNickname { get; set; }

        public long AccountId { get; set; }
        public Account Account { get; set; }
    }
}
