﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Transact.Data.EntityConfiguration;
using Transact.Models;

namespace Transact.Data
{
    public class TransactContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Banner> Banners { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<UsersAccounts> UsersAccounts { get; set; }
        public DbSet<UsersClients> UsersClients { get; set; }

        private readonly string connectionString;

        public TransactContext(DbContextOptions options, IConfiguration configuration) : base(options)
        {
            this.connectionString = configuration
                                    .GetSection("ConnectionStrings:SmarterASPNetConnection")
                                    .Value;
        }

        public TransactContext(DbContextOptions options) : base(options)
        {
        }

        public TransactContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            if (!builder.IsConfigured)
            {
                builder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new UsersAccountsConfig());
            builder.ApplyConfiguration(new UsersClientsConfig());
            builder.ApplyConfiguration(new TransactionConfig());

            base.OnModelCreating(builder);
        }       
    }
}
