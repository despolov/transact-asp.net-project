﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transact.Models;

namespace Transact.Data.EntityConfiguration
{
    public class UsersClientsConfig : IEntityTypeConfiguration<UsersClients>
    {
        public void Configure(EntityTypeBuilder<UsersClients> builder)
        {
            builder.HasKey(u => new {
                u.UserId,
                u.ClientId
            });

            builder.HasOne(uc => uc.User)
               .WithMany(u => u.UsersClients)
               .HasForeignKey(uc => uc.UserId)
               .OnDelete(DeleteBehavior.Restrict)
               .IsRequired();

            builder.HasOne(uc => uc.Client)
                        .WithMany(c => c.UsersClients)
                        .HasForeignKey(uc => uc.ClientId)
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();
        }        

    }
}
