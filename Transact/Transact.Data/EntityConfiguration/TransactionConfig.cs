﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Transact.Models;

namespace Transact.Data.EntityConfiguration
{
    public class TransactionConfig : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.HasOne(tr => tr.SenderAccount)
                .WithMany(a => a.SenderAccounts)
                .HasForeignKey(tr => tr.SenderAccountId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasOne(tr => tr.ReceiverAccount)
                .WithMany(a => a.ReceiverAccounts)
                .HasForeignKey(tr => tr.ReceiverAccountId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();            
        }
    }
}
