﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transact.Models;

namespace Transact.Data.EntityConfiguration
{
    public class UsersAccountsConfig : IEntityTypeConfiguration<UsersAccounts>
    {
        public void Configure(EntityTypeBuilder<UsersAccounts> builder)
        {
            builder.HasKey(u => new { u.UserId, u.AccountId });

            builder.HasOne(ua => ua.User)
                .WithMany(u => u.UsersAccounts)
                .HasForeignKey(ua => ua.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasOne(ua => ua.Account)
                .WithMany(a => a.UsersAccounts)
                .HasForeignKey(ua => ua.AccountId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
