# [Transact ASP.NET Core Web App - First Place Award Winning Project](https://transactweb20190615094943.azurewebsites.net/)

[![Foo](https://i.ibb.co/s5y6CR9/1200x.png)](http://transact-bg.com)

## Made by: <br />

[![Foo](https://i.ibb.co/gwvy7dK/Fotoram-io-4.png)](http://softworkz.org/)

<br /> <b>
Team Members: <br />
 &nbsp;	&bull; Georgi Despolov - GitLab Profile: @despolov<br />
 &nbsp;	&bull; Dimitar Mihov - GitLab Profile: @dimitarmihov<br />

## Team leader:
<b>  &nbsp; &bull; Dimitar Mihov</b> <br />

## Project is Live at:
[Transact Web App](https://transactweb20190615094943.azurewebsites.net/) <br />

## Azure Board:
[AzureBoard: Transact Project](https://dev.azure.com/SoftWorkZbg/Transact-Asp.Net-App) <br />

## Technologies used: <br />
 <b>
 &bull; .NET Core 2.2<br />
 &bull; EntityFrameworkCore 2.2.3 <br />
 &bull; SQL Server Express 17.9 <br />
 &bull; VisualStudio 2017 Community<br />
 &bull; MS Test 1.3.2 <br />
 &bull; Moq 4.10.1 <br />
 &bull; JavaScript / jQuery <br />
 &bull; HTML/CSS <br />
 &bull; Razor engine <br />
 &bull; Git/GitLab
 </b> <br /> <br />
 
## Project Overview:

Proudly presenting the First Place Awarded project given by [Dais-Software](http://www.dais-set.com/software/) for the Telerik Academy Final Projects Assignment.<br />

Project is hosted in Azure and features a Three-Layer architecture:<br />

Presentation Layer: consists of ASP.NET Core 2.2 Web Project with fully responsive beautiful design.<br />
Business Layer: Services and business features processing class library.<br />
Data Layer: Holding the configuration for the data-base and additional mappings of table-relationships.<br />
Custom mappers and DtoObjects transferring data from one layer to another.<br />
App features Custom JSON-Web-Tokenization system for authentication and authorization.<br />
Thorough unit testing for the services and business features covering more than  %.<br />
Server side Pagination WITH java-script based dom elements manipulation for amazing customer experience without any page refreshes for various tables and forms.<br />                            
The Website allows the client to login and manage all of his resources.<br />
Most of the requests are Ajax based to enable beautiful handling of errors and model-state for various forms and situations.<br />
Project includes caching for some parts of its Collections to lower the database trips. <br />
Project was developed using end-to-end Feature branches (left un-deleted in the git-system for the purpose of showcasing the process)!

## Admins Can:

Enjoy the Admin Dashboard after login, providing basic recent activity with in-memory caching to reduce data-base trips for EVERY admin that logs.<br />
Get listing of all Users by last added and paginate on the server while changing the table with javascript without page-refreshing.<br />
Add Users and Clients with ajax form and enjoy model-uninteruptive response about already taken names or invalid form submissions.<br /><br />
[![](https://i.ibb.co/8P0tXNS/THIRD-SCREEN-800.jpg)](#)<br /><br />
List newly added users immediatelly with javascript inside dynamically updated table.<br />
Server-Side pagination on Users, Clients, Banners and Attatched Accounts tables with beautiful javascript dom manipulation allowing updates of records without page refreshes.<br />
Add clients for application to use as base for accounts being attatched and managed between users.<br />
Search for users to attatch (and attatch them) to clients via Javascript autocomplete using ajax remote source (server request) for the data, updating listing dynamically with javascript.<br />
Search for accounts to attatch (and attatch them) to clients via Javascript autocomplete using ajax remote source (server request) for the data.<br />
Remove Users from Clients Or Accounts from Users.<br />
Add, delete and remove banners, giving start and ending date for each one of them to be active on the user login page.<br />


## Users Can:

Enjoy advertising banners from the app management with promotational links on click.<br />
Find beautiful chart-pie of all their accounts' balances inside the account dash.<br /><br />
[![](https://i.ibb.co/hMX6Jcq/SCREEN-3-800.jpg)](#)<br /><br />
Have a listing of all their accounts with additional info for each of them.<br />
Inside Account Dash - click and view all of the Accounts Transactions.<br />
Inside Account Dash - click and initiate a new transaction with the account as pre-selected for Sender.<br />
Edit saved transactions or just directly send them.<br /><br />
[![](https://i.ibb.co/VT4xVQD/SCREEN-3-2.jpg)](#)<br /><br />
Inside Account Dash - click and edit their Personal Nickname for that account regardless of whom it's shared with.<br />
List all of their transactions inside responsive table by selecting some of the accounts they have at their disposal.<br />
Browse the listed transactions in beautiful responsive table with Server-Side pagination.<br />
Read the nicknames of only the accounts they own but not the ones they do not yet have transactions towards/from.<br />

